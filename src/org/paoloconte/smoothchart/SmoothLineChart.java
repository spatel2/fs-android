package org.paoloconte.smoothchart;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;

import com.freestyle.android.R;

public class SmoothLineChart extends View
{
	private static final int CHART_COLOR = 0xFF0099CC;
	private static final int CIRCLE_SIZE = 6;
	private static final int STROKE_SIZE = 2;      
	private static final float SMOOTHNESS = 0.5f; // the higher the smoother, but don't go over 0.5

	private final Paint mPaint;
	private final Path mPath;
	private final float mCircleSize;
	private final float mStrokeSize;
	private float mBorderHeight;
	private float mBorderWidth;
	private final float heightPatch;

	private PointF[] mValues;
	private float mMinY;
	private float mMaxY;

	ArrayList<String> mArrayListTime = new ArrayList<String>();
	ArrayList<String> mArrayListTimeCurrent = new ArrayList<String>();
	ArrayList<String> mArrayListHeight = new ArrayList<String>();

	public SmoothLineChart(Context context) 
	{
		this(context, null, 0);
	}

	public SmoothLineChart(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public SmoothLineChart(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
		float scale = context.getResources().getDisplayMetrics().density;

		mCircleSize = scale * CIRCLE_SIZE;
		mStrokeSize = scale * STROKE_SIZE;
		mBorderHeight = 10;
		heightPatch = 10;
		mBorderWidth = 0;

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setStrokeWidth(mStrokeSize);

		mPath = new Path();
	}
	public void setData(PointF[] values,ArrayList<String> time, ArrayList<String> height, ArrayList<String> timeCurrent)
	{
		mValues = values;              
		mArrayListTime  = new ArrayList<String>();
		mArrayListTime.addAll(time);
		mArrayListTimeCurrent  = new ArrayList<String>();
		mArrayListTimeCurrent.addAll(timeCurrent);
		mArrayListHeight  = new ArrayList<String>();
		mArrayListHeight.addAll(height);

		if (values != null && values.length > 0)
		{
			mMaxY = values[0].y;
			for (PointF point : values)
			{
				final float y = point.y;
				if (y > mMaxY)
					mMaxY = y;
			}
		}
		invalidate();
	}

	public void draw(Canvas canvas)
	{
		super.draw(canvas);

		if (mValues == null || mValues.length == 0)
			return;

		int size = mValues.length;

		final float height = getMeasuredHeight() - heightPatch;
		final float width = getMeasuredWidth() - 2*mBorderWidth;

		final float left = mValues[0].x;
		final float right = mValues[mValues.length-1].x;
		final float dX = (right - left) > 0 ? (right - left) : (2);    
		final float dY = (mMaxY-mMinY) > 0 ? (mMaxY-mMinY) : (2);

		mPath.reset();

		// calculate point coordinates
		List<PointF> points = new ArrayList<PointF>(size);             
		for (PointF point : mValues)
		{
			float x = mBorderWidth + (point.x-left)*width/dX;
			float y = mBorderHeight + height - (point.y-mMinY)*height/dY;
			points.add(new PointF(x,y));           
		}

		// calculate smooth path
		float lX = 0, lY = 0;
		mPath.moveTo(points.get(0).x, points.get(0).y);
		for (int i=1; i<size; i++) {   
			PointF p = points.get(i);       // current point

			// first control point
			PointF p0 = points.get(i-1);    // previous point
			float d0 = (float) Math.sqrt(Math.pow(p.x - p0.x, 2)+Math.pow(p.y-p0.y, 2));    // distance between p and p0
			float x1 = Math.min(p0.x + lX*d0, (p0.x + p.x)/2);      // min is used to avoid going too much right
			float y1 = p0.y + lY*d0;

			// second control point
			PointF p1 = points.get(i+1 < size ? i+1 : i);   // next point
			float d1 = (float) Math.sqrt(Math.pow(p1.x - p0.x, 2)+Math.pow(p1.y-p0.y, 2));  // distance between p1 and p0 (length of reference line)
			lX = (p1.x-p0.x)/d1*SMOOTHNESS;         // (lX,lY) is the slope of the reference line
			lY = (p1.y-p0.y)/d1*SMOOTHNESS;
			float x2 = Math.max(p.x - lX*d0, (p0.x + p.x)/2);       // max is used to avoid going too much left
			float y2 = p.y - lY*d0;

			// add line
			mPath.cubicTo(x1,y1,x2, y2, p.x, p.y);                                                 
		}

		// draw path
		mPaint.setColor(CHART_COLOR);
		mPaint.setStyle(Style.STROKE);
		canvas.drawPath(mPath, mPaint);

		// draw area
		if (size > 0)
		{
			mPaint.setStyle(Style.FILL);
			mPaint.setColor(Color.parseColor("#660099cc"));
			mPath.lineTo(points.get(size-1).x, height+mBorderHeight);      
			mPath.lineTo(points.get(0).x, height+mBorderHeight);   
			mPath.close();
			canvas.drawPath(mPath, mPaint);
		}

		// draw circles
		mPaint.setColor(CHART_COLOR);
		mPaint.setStyle(Style.FILL_AND_STROKE);
		Paint mPaint1 = new Paint();
		mPaint1.setColor(getResources().getColor(R.color.white));
		mPaint1.setStyle(Style.FILL);
		mPaint1.setTextSize(20);
		mPaint1.setTextAlign(Align.CENTER);

		Paint mPaint2 = new Paint();
		mPaint2.setColor(getResources().getColor(R.color.white));
		mPaint2.setStyle(Style.FILL);
		mPaint2.setTextSize(20);
		mPaint2.setTextAlign(Align.CENTER);

		Paint mPaintWhiteCircle = new Paint();
		mPaintWhiteCircle.setColor(getResources().getColor(R.color.white));
		mPaintWhiteCircle.setStrokeWidth(8);
		mPaintWhiteCircle.setStyle(Style.STROKE);

		int k = 0;

		for (int i = 0; i < points.size(); i++)
		{
			if(!mArrayListHeight.get(i).toString().equalsIgnoreCase(""))
			{
				k++;
				canvas.drawCircle(points.get(i).x, points.get(i).y, mCircleSize/2, mPaint);
			}

			if(mArrayListTimeCurrent.get(i).equals("current"))
			{
				canvas.drawCircle(points.get(i).x, points.get(i).y, mCircleSize/2, mPaint2);
				Paint whitePaint = new Paint();
				whitePaint.setStyle(Style.FILL);
				whitePaint.setColor(Color.parseColor("#000000"));
				canvas.drawCircle(points.get(i).x, points.get(i).y, (mCircleSize-mStrokeSize)/2, whitePaint);
			}
			else
			{
				if(k == 1)
				{
					canvas.drawText(mArrayListTime.get(i).toString(), points.get(i).x + 28, points.get(i).y + 53, mPaint1);
					canvas.drawText(mArrayListHeight.get(i).toString(), points.get(i).x  + 28, points.get(i).y + 28, mPaint2);
				}
				else if(k == 2)
				{
					canvas.drawText(mArrayListTime.get(i).toString(), points.get(i).x - 30, points.get(i).y + 53, mPaint1);
					canvas.drawText(mArrayListHeight.get(i).toString(), points.get(i).x  - 30, points.get(i).y + 28, mPaint2);
				}
				else if(k == 3)
				{
					canvas.drawText(mArrayListTime.get(i).toString(), points.get(i).x + 30, points.get(i).y + 53, mPaint1);
					canvas.drawText(mArrayListHeight.get(i).toString(), points.get(i).x  + 30, points.get(i).y + 28, mPaint2);
				}
				else if(k == 4)
				{
					canvas.drawText(mArrayListTime.get(i).toString(), points.get(i).x - 25, points.get(i).y + 53, mPaint1);
					canvas.drawText(mArrayListHeight.get(i).toString(), points.get(i).x  - 25, points.get(i).y + 28, mPaint2);
				}
			}
			if(mArrayListTimeCurrent.get(i).equals("current") && !mArrayListHeight.get(i).toString().equalsIgnoreCase(""))
			{
				canvas.drawText(mArrayListTime.get(i).toString(), points.get(i).x+10, points.get(i).y + 53, mPaint1);
				canvas.drawText(mArrayListHeight.get(i).toString(), points.get(i).x+10, points.get(i).y + 28, mPaint2);
			}
		}

		mPaint.setStyle(Style.FILL);
		mPaint.setColor(Color.parseColor("#000000"));
		for (int i = 0; i < points.size(); i++)
		{
			if(!mArrayListHeight.get(i).toString().equalsIgnoreCase(""))
			{
				canvas.drawCircle(points.get(i).x, points.get(i).y, (mCircleSize-mStrokeSize)/2, mPaint);
			}
		}
	}
}
