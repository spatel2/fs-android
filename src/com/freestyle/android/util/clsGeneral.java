package com.freestyle.android.util;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.freestyle.android.R;

import android.content.Context;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class clsGeneral {
	public static String SDCARD = Environment.getExternalStorageDirectory() + "/.CouponEasy/";
	public static String _SEARCHFOR, _ZIPCODE, _RADIUS = "40", _BUSINESSCATID,
			__BUSINESSCATNAME;
	// if location got once we should not get again
	public static boolean LOCATION_ALREADY_RECEIVED = false;
	public static int APP_RADIAS = 40;

	public enum PHPServices {
		  LOGIN, SIGN_UP, FORGOT_PASSWORD,UPDATE_ACCOUNT

		 }
	
		 private String getServiceName(PHPServices serviceName) {
		  if (serviceName == PHPServices.LOGIN) {
		   return "Login.php";
		  } else if (serviceName == PHPServices.SIGN_UP) {
		   return "SignUp.php";
		  } else if (serviceName == PHPServices.FORGOT_PASSWORD) {
		   return "ForgotPassword.php";
		  }else if (serviceName == PHPServices.UPDATE_ACCOUNT) {
			   return "UpdateAccount.php";
			  } else
		   return "";
		 }

	public static boolean checkInternetConnection(Context context) {
		ConnectivityManager _connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		boolean _isConnected = false;
		NetworkInfo _activeNetwork = _connManager.getActiveNetworkInfo();
		if (_activeNetwork != null) {
			_isConnected = _activeNetwork.isConnectedOrConnecting();
		}
		return _isConnected;
	}

	public void changeFonts(final Context context, final View v, boolean bold) {
		try {
			if (v instanceof ViewGroup) {
				ViewGroup vg = (ViewGroup) v;
				for (int i = 0; i < vg.getChildCount(); i++) {
					View child = vg.getChildAt(i);
					changeFonts(context, child, bold);
				}
			}
			if (bold) {
				if (v instanceof TextView) {
					((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf"));
				} else if (v instanceof Button) {
					((Button) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf"));
				} else if (v instanceof EditText) {
					((EditText) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf"));
				}
			} else {
				if (v instanceof TextView) {
					((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "latolight.ttf"), Typeface.BOLD);
				} else if (v instanceof Button) {
					((Button) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "latolight.ttf"), Typeface.BOLD);
				} else if (v instanceof EditText) {
					((EditText) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "latolight.ttf"), Typeface.BOLD);
				}
			}
		} catch (Exception e) {
			/* suppress errors */
		}
	}

	public static void ShowToast(Context context, String string) {
		Toast.makeText(context, "" + string, Toast.LENGTH_SHORT).show();
	}

	public String getJSONContentFromInternetService(Context context, PHPServices servicesName, List<NameValuePair> parameters, Boolean checkInternetConnectivity, Boolean encryptedDataTransfer) throws Exception {

		// CHeck internet connection
		if (checkInternetConnectivity == true && !checkInternetConnection(context)) {
			throw new Exception(context.getResources().getString(R.string.err_network_not_available));
		}

		// Call PHP services
		try {
			String _url = context.getResources().getString(R.string.server_service_path) + getServiceName(servicesName);
			HttpClient _httpClient = new DefaultHttpClient();

			HttpPost _httpPost = new HttpPost(_url);
			HttpResponse _httpResponse;
			HttpEntity _httpEntity;
			if (parameters != null) {
				_httpPost.setEntity(new UrlEncodedFormEntity(parameters));
			}
			System.out.println("======================INPUT PARAMETERS=========================");
			System.out.println("URL " + _url);
			System.out.println("PARA " + parameters.toString());
			_httpResponse = _httpClient.execute(_httpPost);
			_httpEntity = _httpResponse.getEntity();
			String _returnStr = EntityUtils.toString(_httpEntity); // JSON
			// Content

			System.out.println("======================OUTPUT PARAMETERS=========================");
			System.out.println("OUTPUT RESPONCE " + _returnStr);
			// Decrypt response
			if (encryptedDataTransfer) {
				_returnStr = URLDecoder.decode(_returnStr, "UTF-8");
			}
			JSONObject _jo = new JSONObject(_returnStr);
			// CHeck for errors
			if (_jo.get("success").equals("0")) {
//				JSONArray _jArr = new JSONArray(_returnStr);

//				if (_jArr.length() == 1) {
//					JSONObject _jObj = _jArr.getJSONObject(0);
					String _error = _jo.getString("message");
					throw new Exception(_error);
//				}
			}

			// remove null from return strings

			_returnStr = _returnStr.replace(":null", ":\"\"");
			_returnStr = _returnStr.replace("null", "");
			_returnStr = _returnStr.replace(":,", ":\"\",");

			return _returnStr;
		} catch (Exception e) {
			// throw new
			// Exception(context.getResources().getString(R.string.err_connecting_php_service));
			throw new Exception(e.getMessage());
		}
	}

	// Delete Folder
	// public static void deleteDir(File dir) {
	// File[] files = dir.listFiles();
	// for (File myFile : files) {
	// if (myFile.isDirectory()) {
	// deleteDir(myFile);
	// }
	// myFile.delete();
	// }
	// }

	public Address getLocationFromZipcode(String zipCode, Context context) {
		Address address = null;
		final Geocoder geocoder = new Geocoder(context);
		try {
			List<Address> addresses = geocoder.getFromLocationName(zipCode, 100);
			if (addresses != null && !addresses.isEmpty()) {
				for (int i = 0; i < addresses.size(); i++) {
					Address _address = addresses.get(i);
					String _country = _address.getCountryCode();
					if (_country.equals("US")) {
						address = addresses.get(i);
						break;
					}
				}

			} else {
			}
		} catch (IOException e) {
			// handle exception
		}
		return address;
	}
}
