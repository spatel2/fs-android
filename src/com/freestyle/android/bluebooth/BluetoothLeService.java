package com.freestyle.android.bluebooth;

import java.util.List;
import java.util.UUID;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.freestyle.android.activity.DemoApplication;

//Service for managing connection and data communication with a GATT server
//hosted on a given Bluetooth LE device.
public class BluetoothLeService extends Service
{
	private final static String TAG = "BLUETOOTH";
	private BluetoothManager	mBluetoothManager;
	private BluetoothAdapter	mBluetoothAdapter;
	private String				mBluetoothDeviceAddress;
	private BluetoothGatt		mBluetoothGatt;

	public final static String ACTION_GATT_DISCONNECTED			= "BL_ACTION_GATT_DISCONNECTED";
	public final static String ACTION_DATA_AVAILABLE			= "BL_ACTION_DATA_AVAILABLE";
	public final static String EXTRA_DATA						= "BL_EXTRA_DATA";
	public final static String BL_COMMAND                       = "com.freestyle.android.bluebooth.command";

	public static final UUID YEELIGHT_SERVICE		= UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
	public static final UUID PWM_SERV_UUID			= UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
	public static final UUID BLE_SEND_DATA_UUID		= UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb");
	public static final UUID BLE_UP_DATA_UUID		= UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb");
	public static final UUID PWM_CHAR_UUID			= UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb");
	public static final UUID CCC					= UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

	private String LastConnectedDeviceAddress = "";

	private DemoApplication app;
	// Implements callback methods for GATT events that the app cares about. For
	// example,
	// connection change and services discovered.
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback()
	{
		//Callback indicating when GATT client has connected/disconnected to/from a remote GATT server.
		//status Status of the connect or disconnect operation. BluetoothGatt.GATT_SUCCESS if the operation succeeds.
		//newState Returns the new connection state. Can be one of BluetoothProfile.STATE_DISCONNECTED or 
		//BluetoothProfile.STATE_CONNECTED
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState)
		{
			if ((newState == BluetoothProfile.STATE_CONNECTED) && (status == 0))
			{
				Log.i(TAG, "onConnectionStateChange State Connected");
				app.ChangeConnectionState(DemoApplication.STATE_CONNECTING,LastConnectedDeviceAddress);
				// Attempts to discover services after successful connection.
				//Discovers services offered by a remote device as well as their 
				//characteristics and descriptors. 
				//This is an asynchronous operation. Once service discovery is completed, 
				//the BluetoothGattCallback.onServicesDiscovered callback is triggered. 
				//If the discovery was successful, the remote services can be retrieved using the 
				//getServices function. 
				mBluetoothGatt.discoverServices();
			}
			else if (newState == BluetoothProfile.STATE_DISCONNECTED)
			{
				Log.i(TAG, "onConnectionStateChange State Disconnected");
				app.ChangeConnectionState(DemoApplication.STATE_DISCONNECTED,"");

				broadcastUpdate(ACTION_GATT_DISCONNECTED);
				mBluetoothGatt.disconnect();
				mBluetoothGatt.close();
				mBluetoothGatt = null;
				//mBluetoothAdapter.disable();
				//mBluetoothAdapter.enable();

				if (LastConnectedDeviceAddress.length() > 0)
				{
					Intent intent=new Intent("ReConnect");
					sendBroadcast(intent);
				}
			}
		}

		public void PrintBluetoothError(String ExtraInfo, int Status)
		{
			switch (Status)
			{
			case BluetoothGatt.GATT_READ_NOT_PERMITTED:				//GATT read operation is not permitted
				Log.i(TAG, ExtraInfo + " Error READ_NOT_PERMITTED");break;
			case BluetoothGatt.GATT_WRITE_NOT_PERMITTED:			//GATT write operation is not permitted
				Log.i(TAG, ExtraInfo + " Error WRITE_NOT_PERMITTED");break;
			case BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION:	//Insufficient authentication for a given operation
				Log.i(TAG, ExtraInfo + " Error INSUFFICIENT_AUTHENTICATION");break;
			case BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED:			//The given request is not supported
				Log.i(TAG, ExtraInfo + " Error REQUEST_NOT_SUPPORTED");break;
			case BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION:		//Insufficient encryption for a given operation
				Log.i(TAG, ExtraInfo + " Error INSUFFICIENT_ENCRYPTION");break;
			case BluetoothGatt.GATT_INVALID_OFFSET:					//A read or write operation was requested with an invalid offset
				Log.i(TAG, ExtraInfo + " Error INVALID_OFFSET");break;
			case BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH:		//A write operation exceeds the maximum length of the attribute
				Log.i(TAG, ExtraInfo + " Error INVALID_ATTRIBUTE_LENGTH");break;
			case BluetoothGatt.GATT_FAILURE:						//A GATT operation failed, errors other than the above
				Log.i(TAG, ExtraInfo + " Error FAILURE");break;
			default:
				Log.i(TAG, ExtraInfo + " Error UNKNOWN:" + Status);break;
			}
		}

		//Callback invoked when the list of remote services, characteristics and descriptors for the 
		//remote device have been updated, ie new services have been discovered.
		//Parameters:
		//gatt GATT client invoked BluetoothGatt.discoverServices
		//status BluetoothGatt.GATT_SUCCESS if the remote device has been explored successfully.
		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status)
		{
			if (status == BluetoothGatt.GATT_SUCCESS)
			{
				Log.i(TAG, "onServicesDiscovered");
				Testdiscovered();
			}
			else
				PrintBluetoothError("onServicesDiscovered", status);
		}

		private void Testdiscovered()
		{
			List<BluetoothGattService> gattServices = getSupportedGattServices();

			for (BluetoothGattService gattService : gattServices)
			{
				String uuid = gattService.getUuid().toString();
				Log.i("BLUETOOTH", "list of found devices:"+uuid);
				if(uuid.equals(BluetoothLeService.PWM_SERV_UUID.toString()))
				{
					List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
					for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics)
					{
						uuid = gattCharacteristic.getUuid().toString();
						Log.i("BLUETOOTH", "list of characteristics:"+uuid);
						if(uuid.equals(BluetoothLeService.BLE_UP_DATA_UUID.toString()))
						{
							BluetoothGattDescriptor mTxPowerccc = gattCharacteristic.getDescriptor(BluetoothLeService.CCC);
							Enable_Data(mTxPowerccc);
							Log.i("BLUETOOTH", "in setting");
							break;
						}
						else
						{
							Log.i("BLUETOOTH", "No BLE_UP_DATA_UUID");
						}
					}
				}
			} 
		}

		//Callback reporting the result of a characteristic read operation.
		//characteristic Characteristic that was read from the associated remote device.
		//status BluetoothGatt.GATT_SUCCESS if the read operation was completed successfully.
		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status)
		{
			if (status == BluetoothGatt.GATT_SUCCESS)
			{
				Log.i(TAG, "onCharacteristicRead" + characteristic);
				broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
			}
			else
				PrintBluetoothError("onCharacteristicRead", status);

		}

		//Callback triggered as a result of a remote characteristic notification.
		//characteristic Characteristic that has been updated as a result of a remote notification event.
		//A GATT characteristic is a basic data element used to construct a GATT service, 
		//BluetoothGattService. The characteristic contains a value as well as additional 
		//information and optional GATT descriptors, BluetoothGattDescriptor.
		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic)
		{
			Log.i(TAG, "onCharacteristicChanged" + characteristic);
			broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
		}

		//Callback reporting the result of a descriptor read operation.
		//descriptor Descriptor that was read from the associated remote device.
		//status BluetoothGatt.GATT_SUCCESS if the read operation was completed successfully
		//GATT Descriptors contain additional information and attributes of a GATT characteristic, 
		//BluetoothGattCharacteristic. They can be used to describe the characteristic's features 
		//or to control certain behaviours of the characteristic.
		@Override
		public void onDescriptorRead(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status)
		{
			if (status == BluetoothGatt.GATT_SUCCESS)
			{
				Log.i(TAG, "onDescriptorRead");
				BluetoothGattCharacteristic mTxPowerccc = descriptor.getCharacteristic();
				enableNotification(true, mTxPowerccc);
				app.ChangeConnectionState(DemoApplication.STATE_CONNECTED,LastConnectedDeviceAddress);
			}
			else
				PrintBluetoothError("onDescriptorRead", status);
		}

		//Callback indicating the result of a descriptor write operation.
		//descriptor Descriptor that was written to the associated remote device.
		//status The result of the write operation BluetoothGatt.GATT_SUCCESS if the operation succeeds.
		@Override
		public void onDescriptorWrite(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status)
		{
			super.onDescriptorWrite(gatt, descriptor, status);
		}

	};

	private void broadcastUpdate(final String action)
	{
		Log.i(TAG, "broadcastUpdate " + action);
		final Intent intent = new Intent(action);
		sendBroadcast(intent);
	}

	private void broadcastUpdate(final String action,
			final BluetoothGattCharacteristic characteristic)
	{
		Log.i(TAG, "broadcastUpdate " + action + "Characteristic " + characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0));
		if (BLE_UP_DATA_UUID.equals(characteristic.getUuid()))// Appendix A
		{
			try
			{
				Intent intent = new Intent();
				intent.putExtra(EXTRA_DATA, characteristic.getValue());
				intent.setAction(BL_COMMAND);
				intent.putExtra("CommandValue", characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0));
				sendBroadcast(intent);
			}
			catch (Exception e) {
			}
		}
	}

	private final BroadcastReceiver ReconnectReceiver = new BroadcastReceiver() 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			final String action = intent.getAction();
			Log.i("BLUETOOTH", "ReconnectReceiver  onReceive action:"+action);
			if (("ReConnect".equals(action)) && (DemoApplication.mConnectionState == DemoApplication.STATE_DISCONNECTED))
			{
				if (mBluetoothGatt != null)
				{
					Log.i("BLUETOOTH", "ReconnectReceiver reinitialise");
					try
					{
						mBluetoothGatt.disconnect();
						mBluetoothGatt.close();
						mBluetoothGatt = null;
					}
					catch(Exception ex)
					{
						mBluetoothGatt = null;
						System.out.println(ex);
					}
				}
				connect(LastConnectedDeviceAddress,true);
			} 
		}
	};

	public void onCreate()
	{
		IntentFilter filter = new IntentFilter("ReConnect");
		this.registerReceiver(ReconnectReceiver, filter);
	}

	public void onDestroy()
	{
		unregisterReceiver(ReconnectReceiver);
	}

	// Initializes a reference to the local Bluetooth adapter.
	// @return Return true if the initialization is successful.
	public boolean initialize(DemoApplication _app)
	{
		Log.i(TAG, "initialize");
		app = _app;

		// For API level 18 and above, get a reference to BluetoothAdapter through BluetoothManager.
		if (mBluetoothManager == null)
		{
			mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null)
			{
				Log.e(TAG, "Unable to initialize BluetoothManager.");
				return false;
			}
		}

		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null)
		{
			Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
			return false;
		}

		// Ensures Bluetooth is available on the device and it is enabled. If not,
		// displays a dialog requesting user permission to enable Bluetooth.
		//if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
		//    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		//startActivity(enableIntent);
		//}
		LastConnectedDeviceAddress = app.getSharedPreferences("alert", 0).getString("LastConnectedDeviceAddress", "");
		if (LastConnectedDeviceAddress.length() > 0)
		{
			Intent intent=new Intent("ReConnect");
			sendBroadcast(intent);
		}
		return true;
	}

	// Connects to the GATT server hosted on the Bluetooth LE device.
	// @param address The device address of the destination device.
	// @return Return true if the connection is initiated successfully. The connection result is reported 
	// asynchronously through the onConnectionStateChange callback.
	public boolean connect(final String address, boolean autoconnect)
	{
		if (address.length() <= 0) return false;

		LastConnectedDeviceAddress = address;

		app.getSharedPreferences("alert", 0).edit().putString("LastConnectedDeviceAddress", address).commit();
		Log.i(TAG, "connect");
		if (mBluetoothAdapter == null || address == null)
		{
			Log.w(TAG,"BluetoothAdapter not initialized or unspecified address.");
			return false;
		}

		// Previously connected device. Try to reconnect.
		if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null)
		{
			//Connect back to remote device. 
			//This method is used to re-connect to a remote device after the connection has been dropped. 
			//If the device is not in range, the re-connection will be triggered once the device is back in range.
			if (mBluetoothGatt.connect())
			{
				Log.d(TAG,"Trying to reconnect.");
				return true;
			}
			else
			{
				Log.d(TAG,"Trying to reconnect failed");
				return false;
			}
		}

		//Create a new connection
		final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		if (device == null)
		{
			Log.w(TAG, "Device not found. Unable to connect.");
			return false;
		}

		//Connect to GATT Server hosted by this device. Caller acts as GATT client. The callback is used 
		//to deliver results to Caller, such as connection status as well as any further GATT client 
		//operations. The method returns a BluetoothGatt instance. You can use BluetoothGatt to conduct 
		//GATT client operations.
		//callback GATT callback handler that will receive asynchronous callbacks.
		//autoConnect Whether to directly connect to the remote device (false) or to automatically connect as soon as 
		//the remote device becomes available (true).
		//Throws: IllegalArgumentException - if callback is null
		// We want to directly connect to the device, so we are setting the autoConnect parameter to false.
		Log.d(TAG, "Trying to create a new connection." + autoconnect);
		mBluetoothGatt = device.connectGatt(this, autoconnect, mGattCallback);
		mBluetoothDeviceAddress = address;

		if (autoconnect)
		{
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() 
			{
				@Override
				public void run() 
				{
					Intent intent=new Intent("ReConnect");
					sendBroadcast(intent);
				}
			}, 12000);
		}
		return true;
	}

	// Disconnects an existing connection or cancel a pending connection. The
	// disconnection result is reported asynchronously through the onConnectionStateChange callback.
	public void disconnect()
	{
		Log.i(TAG, "disconnect");
		//When we disconnect, we don't want to auto connect again
		LastConnectedDeviceAddress = "";
		app.getSharedPreferences("alert", 0).edit().putString("LastConnectedDeviceAddress", "").commit();

		if (mBluetoothAdapter == null || mBluetoothGatt == null)
		{
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.disconnect();
	}

	// After using a given BLE device, the app must call this method to ensure
	// resources are released properly.
	public void close()
	{
		Log.i(TAG, "close");
		if (mBluetoothGatt != null)
		{
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
	}

	// Request a read on a given BluetoothGattCharacteristic. The read result is reported asynchronously through the
	// onCharacteristicRead callback.
	// @param characteristic The characteristic to read from.
	public void readCharacteristic(BluetoothGattCharacteristic characteristic)
	{
		Log.i(TAG, "readCharacteristic " + characteristic);
		if (mBluetoothAdapter == null || mBluetoothGatt == null)
		{
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.readCharacteristic(characteristic);
	}

	// Enables or disables notification on a give characteristic.
	// @param characteristic Characteristic to act on.
	// @param enabled If true, enable notification. False otherwise.
	public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled)
	{
		Log.i(TAG, "setCharacteristicNotification " + characteristic + " enabled " + enabled);
		if (mBluetoothAdapter == null || mBluetoothGatt == null)
		{
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}

		mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
	}

	// Retrieves a list of supported GATT services on the connected device. This should be invoked only 
	// after BluetoothGatt#discoverServices() completes successfully.
	// @return A {@code List} of supported services.
	public List<BluetoothGattService> getSupportedGattServices()
	{
		Log.i(TAG, "getServices");
		if (mBluetoothGatt == null)
		{
			Log.w(TAG, "BluetoothAdapter not initialized");
			return null;
		}

		return mBluetoothGatt.getServices();
	}

	public void WriteAppendixB(int pwm_data_buf)
	{

		Log.i("TAG","Sending to watch:" + pwm_data_buf);
		if (mBluetoothGatt == null)
		{
			Log.e(TAG, "mBluetoothGatt service not found!");
			return;
		}

		BluetoothGattService alertService = mBluetoothGatt.getService(YEELIGHT_SERVICE);
		if (alertService == null)
		{
			Log.e(TAG, "Immediate Alert service not found!" + mBluetoothGatt);
			return;
		}

		BluetoothGattCharacteristic alertLevel = alertService.getCharacteristic(PWM_CHAR_UUID);
		if (alertLevel == null)
		{
			Log.e(TAG, "Immediate Alert Level charateristic not found!");
			return;
		}

		//alertLevel.getWriteType();
		alertLevel.setValue(pwm_data_buf, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
		alertLevel.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);

		//Writes a given characteristic and its values to the associated remote device. 
		//Once the write operation has been completed, the onCharacteristicWrite callback is invoked, 
		//reporting the result of the operation. 
		//characteristic Characteristic to write on the remote device
		//Returns: true, if the write operation was initiated successfully
		if (mBluetoothGatt.writeCharacteristic(alertLevel) != true);
			Log.e(TAG, "Error in writeCharacteristic");
	}

	//To send data from the phone to the watch
	public boolean Ble_Send_Data(byte[] ble_data)
	{

		Log.i("TAG","Sending to watch:" + ble_data.toString());
		if (null == mBluetoothGatt)
		{
			Log.e(TAG, "mBluetoothGatt service not found!");
			return false;
		}

		BluetoothGattService alertService = mBluetoothGatt.getService(YEELIGHT_SERVICE);
		if (alertService == null)
		{
			Log.e(TAG, "Service not found!");
			return false;
		}

		BluetoothGattCharacteristic alertLevel = alertService.getCharacteristic(BLE_SEND_DATA_UUID);
		if (alertLevel == null)
		{
			Log.e(TAG, "Immediate Alert Level charateristic not found!");
			return false;
		}

		//alertLevel.getWriteType();
		alertLevel.setValue(ble_data);
		alertLevel.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
		if (mBluetoothGatt.writeCharacteristic(alertLevel) != true)
		{
			Log.e(TAG, "Error in writeCharacteristic");
			return false;
		}

		return true;
	}

	public void Enable_Up_Data()
	{
		Log.e(TAG, "Enable_Up_Data");
		BluetoothGattService TxPowerService = mBluetoothGatt.getService(PWM_SERV_UUID);
		if (TxPowerService == null)
		{
			Log.e(TAG, "Tx power service not found!");
			return;
		}

		BluetoothGattCharacteristic TxPowerLevel = TxPowerService.getCharacteristic(BLE_UP_DATA_UUID);
		if (TxPowerLevel == null)
		{
			Log.e(TAG, "Tx power Level charateristic not found!");
			return;
		}

		BluetoothGattDescriptor mTxPowerccc = TxPowerLevel.getDescriptor(CCC);
		if (mTxPowerccc == null)
		{
			Log.e(TAG, "CCC for TX power level charateristic not found!");
			return;
		}

		if (mBluetoothGatt.readDescriptor(mTxPowerccc) == false)
		{
			Log.e(TAG, "readDescriptor() is failed");
			return;
		}
	}

	public void Enable_Data(BluetoothGattDescriptor mTxPowerccc)
	{
		Log.i(TAG, "Enable_Data");
		if (mTxPowerccc == null)
		{
			Log.e(TAG, "CCC for TX power level charateristic not found!");
			return;
		}

		if (mBluetoothGatt.readDescriptor(mTxPowerccc) == false)
		{
			Log.e(TAG, "readDescriptor() is failed");
			return;
		}
	}

	private boolean enableNotification(boolean enable, BluetoothGattCharacteristic characteristic)
	{
		Log.d(TAG, "enableNotification " + characteristic + " enable " + enable);
		if (mBluetoothGatt == null) 
		{
			Log.e(TAG, "mBluetoothGatt service not found!");
			return false;
		}	

		//Enable or disable notifications/indications for a given characteristic. 
		//Once notifications are enabled for a characteristic, a onCharacteristicChanged callback will 
		//be triggered if the remote device indicates that the given characteristic has changed. 
		if (!mBluetoothGatt.setCharacteristicNotification(characteristic,enable)) 
		{
			Log.e(TAG, "setCharacteristicNotification service not found!");
			return false;
		}	

		BluetoothGattDescriptor clientConfig = characteristic.getDescriptor(CCC);
		if (clientConfig == null)
		{
			Log.e(TAG, "CCC for TX power level charateristic not found!");
			return false;
		}

		if (enable)
			clientConfig.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		else
			clientConfig.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);

		return mBluetoothGatt.writeDescriptor(clientConfig);
	}

	//-----------------------------------------------------------------------------------------
	//---------------------------------- IBInder ----------------------------------------------
	//-----------------------------------------------------------------------------------------
	private final IBinder mBinder = new LocalBinder();

	public class LocalBinder extends Binder
	{
		public BluetoothLeService getService()
		{
			return BluetoothLeService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent)
	{
		// After using a given device, you should make sure that
		// BluetoothGatt.close() is called
		// such that resources are cleaned up properly. In this particular
		// example, close() is
		// invoked when the UI is disconnected from the Service.
		close();
		return super.onUnbind(intent);
	}
	
	@Override
	public void onTaskRemoved(Intent rootIntent) 
	{
		rootIntent.setClass(this, BluetoothLeService.class);
		stopService(rootIntent);
		super.onTaskRemoved(rootIntent);
	}
	
}
