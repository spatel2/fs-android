package com.freestyle.android.bluebooth;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONObject;
import com.freestyle.android.activity.DbTool;
import com.freestyle.android.activity.DemoApplication;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.Shared_preference;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

public class GPSLocationServices extends Service implements
GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener,
LocationListener, com.google.android.gms.location.LocationListener
{
	//NEw code start
	int seconds_refresh=15;
	private LocationManager mLocationManager=null;
	//new code End

	LocationManager LocationManagerObject;
	Context context;
	BluetoothLeService BluetoothService;
	public Location LastSavedLocation;
	boolean GPSEnabled;
	boolean NetworkEnabled;

	private GoogleApiClient mGoogleApiClient;
	private LocationRequest mLocationRequest;

	//	private float	TotalDistance;	//in meters

	boolean isFirstLocationSaved = true;

	@SuppressWarnings("unused")
	private float	tempDistance=0;	//in meters
	//	private float	CurrentSpeed;
	private int		HeartBeatsPerMinute;
	private long	StartTime = 0L;
	private double	Altitude;
	private Handler handler;

	@SuppressWarnings("unused")
	private DbTool TrackDataBase;
	private String 	LogFileName; 
	private int	direction;

	private Shared_preference shared;
	private boolean isMetricTemp = false;
	OutputStreamWriter LogFile;

	//Lat lon for elevation
	double elev_lat;
	double elev_lon;

	float currentSpeed_double = 0;
	long  current_locationtime = 0;

	//	private static boolean USEGOOGLE = false;
	//	private GoogleApiClient mGoogleApiClient;

	//Byte 0: Type (Always 0x06)
	//Byte 1: PackageSize (Always 0x14)
	//Byte 2:  Running Time (Hour, Decimal ) 
	//Byte 3:  Running Time (Minute, Decimal ) 
	//Byte 4:  Running Time (Second, Decimal ) 
	//Byte 5-8: Distance (Meter, Decimal ) 
	//Byte 9-12: Calorie Consumption (Cal, Decimal) 
	//Byte 13-14: Average Speed (0.01 km/hour, Decimal) 
	//Byte 15-16: Elevation (Meter, Decimal ) 
	//Byte 17-18: Direction (Degree, Decimal ) 
	//Byte 19: Heart Beats Per Minute 

	private static class SporstDataPackage 	{static byte[] Data = new byte[20];};


	private Timer SendTimer; 	 	
	private boolean isCurrentlyRecording; 

	private float KilometerPerHour;
	private String TempAvgSpeed;

	public GPSLocationServices() 
	{

	}

	@Override
	public void onCreate() 
	{

		DemoApplication.CurrentStartTime = SystemClock.elapsedRealtime();

		context = this;
		shared = new Shared_preference(context);
		BluetoothService = DemoApplication.mBluetoothLeService_temp;
		InitLocation();

		mGoogleApiClient = new GoogleApiClient.Builder(this)
		.addApi(LocationServices.API)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.build();

		if(shared.getPrefrencewithDefault(context, Constants.PROFILE.UNIT_IMPERIAL_METRIC,"false").toString().equals("true"))
		{
			isMetricTemp = true;
		}
		else
		{
			isMetricTemp = false;
		}

		super.onCreate();
	}

	@Override
	public IBinder onBind(Intent intent) 
	{

		return null;
	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) 
	{
		mGoogleApiClient.connect();
		super.onStart(intent, startId);
	}

	public GPSLocationServices( BluetoothLeService mBluetoothLeService)
	{
		if(check_Internet(context))
		{
			InitLocation();
		}
	}

	private void InitLocation()
	{
		try 
		{
			/* Use the LocationManager class to obtain GPS locations */
			if(mLocationManager==null)
			{
				mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
				if(mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
				{
					AddLocationManager();
				}
				else
				{		
					mLocationManager=null;

					Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
					context.startActivity(gpsOptionsIntent);
					return;
				}
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}

		handler = new Handler();
		isCurrentlyRecording = false; 	 	

		SendTimer = new Timer(); 	 	
		SendTimer.schedule(new TimerTask()  	 	
		{ 	 	
			@Override 	 	
			public void run()  	 	
			{ 	 	
				SendSportsDataToWatch(); 	 	
			} 	 
		}, 0, 4000); 

		//Temp static value
		StartUpdates(Integer.parseInt(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID)));
	}


	private void  AddLocationManager() 
	{
		if(mLocationManager!=null)
		{
			mLocationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, seconds_refresh*1000, 1, GPSLocationListener);
		}
	}

	public String StartUpdates(int _SportsType)
	{
		StartTime			= System.currentTimeMillis()/1000L;
		LogFileName			= String.valueOf(StartTime) + ".csv";
		HeartBeatsPerMinute =  110;

		//Heart Rate in prefrence
		shared.setPrefrence(context, Constants.PREFRENCE.HEART_RATE, HeartBeatsPerMinute+"");

		try 
		{
			FileOutputStream fOut = context.openFileOutput ( LogFileName , Context.MODE_PRIVATE ) ;
			LogFile = new OutputStreamWriter ( fOut ) ;
		} 
		catch ( IOException e) 
		{
			e.printStackTrace();
		}

		StartGPSUpdates();

		try
		{
			BluetoothService.WriteAppendixB(0x57); //Start Sport Data Sending
		}
		catch(Exception ex)
		{
			System.out.println("Service is not running");
		}
		return LogFileName;
	}

	public void PauseUpdates()
	{
		StopGPSUpdates();
	}

	public void ResumeUpdates()
	{
		StartGPSUpdates();
	}

	public void StopUpdates()
	{
		isCurrentlyRecording = false;
		BluetoothService.WriteAppendixB(0x58); //Stop Sport Data Sending
		TrackDataBase = new DbTool(context);

		try
		{
			DemoApplication.Track.TimeStamp = StartTime;
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}

		//Originally set from Roadbike class
		try
		{
			DemoApplication.Track.Type	=	0;
		}
		catch(Exception ex)
		{
			System.out.println(ex+"");
		}
		//		if(!shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals(""))
		//		{
		//			DemoApplication.Track.Type		= Integer.parseInt(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID));
		//		}

		DemoApplication.Track.Distance 	= 0;

		if(!shared.getPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE).equals(""))
		{
			DemoApplication.Track.Distance	= Float.parseFloat(shared.getPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE));
		}

		DemoApplication.Track.Duration	= "0";
		DemoApplication.Track.Calories	= (int) DemoApplication.CurrentCalories;
		DemoApplication.Track.Notes		= "";
		DemoApplication.Track.Direction	= "0.0";
		DemoApplication.Track.Altitude	= "0.0";



		try 
		{
			LogFile.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		StopGPSUpdates();
	}

	private void StartGPSUpdates()
	{
		isCurrentlyRecording = true; 
		handler.postDelayed(updateTimerThread, 1);
	}

	private void StopGPSUpdates()
	{
		handler.removeCallbacks(updateTimerThread);
	}

	private Runnable updateTimerThread = new Runnable()
	{
		@Override
		public void run() 
		{	
			long total_elepsedtime  = (SystemClock.elapsedRealtime() - DemoApplication.CurrentStartTime)/1000;
			DemoApplication.CurrentTotalTime = total_elepsedtime + DemoApplication.CurrentelapsedTime;

			handler.postDelayed(this, 1000);
			Log.i("Milliseconds", DemoApplication.CurrentTotalTime+"");
		}
	};

	private static final int TWO_MINUTES = 1000 * 60 * 2;

	// Determines whether one Location reading is better than the current Location fix
	// @param location  The new Location that you want to evaluate
	// @param currentBestLocation  The current Location fix, to which you want to compare the new one
	protected boolean isBetterLocation(Location location, Location currentBestLocation) 
	{
		if (currentBestLocation == null) 
		{
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(),
				currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
			return true;
		}
		return false;
	}


	byte DecToBCD(byte val)
	{
		return (byte) (((val/10)<<4) + (val%10)) ;
	}

	public boolean check_Internet(Context mContext)
	{
		ConnectivityManager mConnectivityManager;
		NetworkInfo mNetworkInfo;
		mConnectivityManager= (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

		if(mNetworkInfo != null && mNetworkInfo.isConnectedOrConnecting())
			return true;

		return false;
	}

	void UpdateLocation(Location NewLocation)
	{
		//	Log.e("---------", "*****************Location Arrived*****************");
		elev_lat = NewLocation.getLatitude();
		elev_lon = NewLocation.getLongitude();

		if(check_Internet(context))
		{
			new getElevation().execute();
		}
		if (NewLocation != null) 
		{
			//Getting City First
			if(check_Internet(context))
			{
				Geocoder geocoder = new Geocoder(context, Locale.getDefault());
				try
				{
					List<Address> addresses  = geocoder.getFromLocation(NewLocation.getLatitude(),NewLocation.getLongitude(), 1);
					String city = addresses.get(0).getLocality();
					String State = addresses.get(0).getAdminArea();
					Log.i("City", ""+city +" " + State);
					DemoApplication.CurrentCity = city+", "+State;
				}
				catch(Exception ex)
				{
					Log.e("Error to Get Location Name", ""+ex);
				}
			}
			//Note: This needs to be done before the assignment because otherwise
			//we will have two equal values
			if (LastSavedLocation != null)
			{
				if(/*LastSavedLocation.distanceTo(NewLocation)>=15 &&*/ isBetterLocation(NewLocation, LastSavedLocation))
				{
					//Toast.makeText(context, "Location Approved", Toast.LENGTH_SHORT).show();

					//Getting track of data
					DemoApplication.CurrentLatLong = DemoApplication.CurrentLatLong
							+	NewLocation.getLatitude()
							+	"#"
							+	NewLocation.getLongitude()
							+	",";

					DemoApplication.CurrentDistance += LastSavedLocation.distanceTo(NewLocation);

					currentSpeed_double = LastSavedLocation.distanceTo(NewLocation);
					current_locationtime = NewLocation.getTime() - LastSavedLocation.getTime();
					current_locationtime = (current_locationtime/1000);

					//Get Direction
					DemoApplication.CurrentDirection = getDirectionAngle(LastSavedLocation.getLatitude(), LastSavedLocation.getLongitude(), NewLocation.getLatitude(), NewLocation.getLongitude());

					Intent updateIntent = new Intent();
					updateIntent.setAction("LocationDataUpdate");
					context.sendBroadcast(updateIntent);
					context.sendBroadcast(new Intent("UpdateReceiver"));

					try 
					{
						LogFile.write((Long.toString(NewLocation.getTime()) + "," +
								Double.toString(NewLocation.getLatitude()) + "," +
								Double.toString(NewLocation.getLongitude()) + "," +
								Double.toString(NewLocation.getAltitude()) + "\n"));
						LogFile.flush();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}

					LastSavedLocation = NewLocation;

				}
			}
			else
			{
				LastSavedLocation = NewLocation;

				//Will save only first location
				if(isFirstLocationSaved)
				{
					DemoApplication.CurrentLatLong = DemoApplication.CurrentLatLong
							+	NewLocation.getLatitude()
							+	"#"
							+	NewLocation.getLongitude()
							+	",";
					isFirstLocationSaved = false;
				}
			}


			//Byte 0: Type (Always 0x06)
			SporstDataPackage.Data[0]= 0x06;

			//Byte 1: PackageSize (Always 0x14)
			SporstDataPackage.Data[1]= 0x14;
			//Byte 2:  Running Time (Hour, Decimal) 
			//Byte 3:  Running Time (Minute, Decimal ) 
			//Byte 4:  Running Time (Second, Decimal ) 

			byte hours	= (byte) (DemoApplication.CurrentTotalTime / 3600L);
			byte minutes = (byte) ((DemoApplication.CurrentTotalTime % 3600L) / 60L);
			byte seconds = (byte) (DemoApplication.CurrentTotalTime % 60L);

			SporstDataPackage.Data[2]= DecToBCD((byte) hours);
			SporstDataPackage.Data[3]= DecToBCD((byte) minutes);
			SporstDataPackage.Data[4]= DecToBCD((byte) seconds);

			//Byte 5-8: Distance (Meter, Decimal ) 
			int TempDistance = (int)(DemoApplication.CurrentDistance);

			SporstDataPackage.Data[8]= DecToBCD((byte) (TempDistance%100));  
			TempDistance = TempDistance / 100;
			SporstDataPackage.Data[7]= DecToBCD((byte) (TempDistance%100));  
			TempDistance = TempDistance / 100;
			SporstDataPackage.Data[6]= DecToBCD((byte) (TempDistance%100)); 
			TempDistance = TempDistance / 100;
			SporstDataPackage.Data[5]= DecToBCD((byte) (TempDistance%100));


			//Byte 13-14: Average Speed (0.01 km/hour, Decimal) 
			//TotalDistance (in m)
			//TotalTime (in sec)
			//Avoid Division through 0
			if (DemoApplication.CurrentTotalTime == 0) DemoApplication.CurrentTotalTime = 1;
			float MeterPerSeconds = currentSpeed_double/(float)current_locationtime;
			//  m/sec * 3.6 -> km/h
			float KilometerPerHour = (MeterPerSeconds * 3.6f); //Note: x100 because its 0.01 km/h not km/h

			String TempAvgSpeed = Float.toString(KilometerPerHour);

			if (TempAvgSpeed.contains("."))
			{
				String[] SplitStringAvgSpeed = TempAvgSpeed.split("\\.");

				if (SplitStringAvgSpeed[0].length() > 2)
					SplitStringAvgSpeed[0] = SplitStringAvgSpeed[0].substring(0, 2);

				if (SplitStringAvgSpeed[1].length() > 2)
					SplitStringAvgSpeed[1] = SplitStringAvgSpeed[1].substring(0, 2);

				SporstDataPackage.Data[13]	= DecToBCD((byte)Integer.parseInt(SplitStringAvgSpeed[0]));   
				SporstDataPackage.Data[14]	= DecToBCD((byte)Integer.parseInt(SplitStringAvgSpeed[1]));
			}
			else
			{
				if (TempAvgSpeed.length() > 2)
					TempAvgSpeed = TempAvgSpeed.substring(0, 2);

				try
				{
					SporstDataPackage.Data[13]	= DecToBCD((byte)Integer.parseInt(TempAvgSpeed));   
				}
				catch(Exception ex)
				{
					SporstDataPackage.Data[13]	= DecToBCD((byte)Integer.parseInt("0")); 
				}
				SporstDataPackage.Data[14]	= (byte)0x00;	
			}

			//Byte 15-16: Elevation (Meter, Decimal ) 
			Altitude = Double.parseDouble(DemoApplication.CurrentAltitude);
			int TempAltitude = (int) (Altitude);
			boolean NegativeAltitude = false;
			if (TempAltitude < 0) 
			{
				NegativeAltitude = true;
				TempAltitude = -TempAltitude;
			}

			SporstDataPackage.Data[16]	= DecToBCD((byte) (TempAltitude%100));   
			TempAltitude = TempAltitude / 100;
			SporstDataPackage.Data[15]	= DecToBCD((byte) (TempAltitude%100)); 

			if (NegativeAltitude)
				SporstDataPackage.Data[15] = (byte) (SporstDataPackage.Data[15] | 10000000);

			//Byte 17-18: Direction (Degree, Decimal ) 
			int TempDirection = direction;
			SporstDataPackage.Data[18]	= DecToBCD((byte) (TempDirection%100));    
			TempDirection = TempDirection / 100;
			SporstDataPackage.Data[17]	= DecToBCD((byte) (TempDirection%100));   

			//Byte 19: Heart Beats Per Minute 
			//SporstDataPackage.Data[19]	= (byte)HeartBeatsPerMinute;
		}
	}

	//Define a listener that responds to location updates
	LocationListener GPSLocationListener = new LocationListener()
	{
		@Override
		public void onLocationChanged(Location location)
		{
			UpdateLocation(location);
		}

		@Override
		public void onProviderDisabled(String provider)
		{
		}

		@Override
		public void onProviderEnabled(String provider)
		{
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
		}
	};


	@Override
	public void onConnected(Bundle connectionHint)
	{
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setInterval(1000); // Update location every second

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, this);
	}

	protected void createLocationRequest()
	{

	}

	protected void startLocationUpdates()
	{

	}

	protected void stopLocationUpdates()
	{

	}
	@Override
	public void onLocationChanged(Location location) 
	{
		UpdateLocation(location);
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0)
	{

	}
	@Override
	public void onConnectionSuspended(int arg0)
	{

	}

	public void SendSportsDataToWatch()
	{
		if (isCurrentlyRecording == false)
			return;

		new Thread(new Runnable() 
		{
			@SuppressLint("DefaultLocale") 
			public void run() 
			{
				try 
				{
					//For Calories
					//Data to calculate calories
					float user_age		=	0;
					float user_weight	=	0;

					int heart_rate = Integer.parseInt(shared.getPrefrence(context, Constants.PREFRENCE.HEART_RATE).toString());
					if(!shared.getPrefrence(context, Constants.PROFILE.USER_AGE).toString().equals("")
							&& !shared.getPrefrence(context, Constants.PROFILE.USER_HEIGHT).toString().equals("")
							&& !shared.getPrefrence(context, Constants.PROFILE.USER_WEIGHT).toString().equals(""))
					{
						user_age	=	Float.parseFloat(shared.getPrefrence(context, Constants.PROFILE.USER_AGE).toString());

						if(isMetricTemp)
						{
							user_weight	=	Float.parseFloat(shared.getPrefrence(context, Constants.PROFILE.USER_WEIGHT).toString());
						}
						else
						{
							//Feet to centimeter
							String mStringHeight	=	shared.getPrefrence(context, Constants.PROFILE.USER_HEIGHT).toString();
							mStringHeight = mStringHeight.replace("'", ".");

							//Pound to KG
							user_weight = Float.parseFloat(PoundToKg(Float.parseFloat(shared.getPrefrence(context, Constants.PROFILE.USER_WEIGHT).toString())).toString());
						}
					}

					Log.e("Calories Data", 
							" Heart: "		+	heart_rate 	
							+	"User age: "+ 	user_age	
							+	"weight: "	+	user_weight	
							+	"End."		);


					if(user_age!=0 &&  user_weight!=0)
					{
						double temp_vab = ((double)(DemoApplication.CurrentTotalTime)/60) / 4.184;

						System.out.println("---Main"+(0.4472 * 110 - 0.05741 * ((double)user_weight) + 0.074 * ((double)user_age) - 20.4022)+"");
						System.out.println("---Main"+(0.4472 * 110 - 0.05741 * ((double)user_weight) + 0.074 * ((double)user_age) - 20.4022)*temp_vab+"");

						if(shared.getPrefrence(context, Constants.PROFILE.USER_GENDER).toString().equals(""))
						{
							//Men
							DemoApplication.CurrentCalories = (0.4472 * 110 - 0.05741 * (((double)user_weight)*2.2046) + 0.074 * ((double)user_age) - 20.4022)*temp_vab;
						}
						else if(shared.getPrefrence(context, Constants.PROFILE.USER_GENDER).toString().equals("MALE"))
						{
							//Men
							DemoApplication.CurrentCalories = (0.4472 * 110 - 0.05741 * (((double)user_weight)*2.2046) + 0.074 * ((double)user_age) - 20.4022)*temp_vab;
						}
						else if(shared.getPrefrence(context, Constants.PROFILE.USER_GENDER).toString().equals("FEMALE"))
						{
							//Women
							DemoApplication.CurrentCalories = (0.4472 * 110 - 0.05741 * (((double)user_weight)*2.2046) + 0.074 * ((double)user_age) - 20.4022)*temp_vab;
						}

						//					     caloriesBurned = (((0.4472 * gActHeartRate) - (0.05741 * dWeight) + (0.074 * gActAge) - 20.4022) * ([timerlbl getTimeCounted] / 60)) / 4.184 ;

						Log.e("Calories", "Calories = "+DemoApplication.CurrentCalories);
					}
					else
					{
						Log.e("Error", "No Calories Data");
						//DemoApplication.CurrentCalories = 0;
					}

					if(shared.getPrefrencewithDefault(context, Constants.PROFILE.UNIT_IMPERIAL_METRIC,"false").toString().equals("true"))
					{
						isMetricTemp = true;
					}
					else
					{
						isMetricTemp = false;
					}

					//Byte 2:  Running Time (Hour, Decimal ) 
					//Byte 3:  Running Time (Minute, Decimal ) 
					//Byte 4:  Running Time (Second, Decimal ) 
					byte hours	= (byte) (DemoApplication.CurrentTotalTime / 3600L);
					byte minutes = (byte) ((DemoApplication.CurrentTotalTime % 3600L) / 60L);
					byte seconds = (byte) (DemoApplication.CurrentTotalTime % 60L);
					SporstDataPackage.Data[2]= DecToBCD((byte) hours);
					SporstDataPackage.Data[3]= DecToBCD((byte) minutes);
					SporstDataPackage.Data[4]= DecToBCD((byte) seconds);
					Log.e("Sport", "Time:"+hours+":"+minutes+":"+seconds);

					//Byte 13-14: Average Speed (0.01 km/hour, Decimal) 
					//TotalDistance (in m)
					//TotalTime (in sec)
					//Avoid Division through 0

					if (DemoApplication.CurrentTotalTime == 0) DemoApplication.CurrentTotalTime = 1;
					//For Actual average speed
					//	float MeterPerSeconds1 = DemoApplication.CurrentDistance/(float)DemoApplication.CurrentTotalTime;

					//For Current Speed
					float MeterPerSeconds = currentSpeed_double/(float)current_locationtime;

					tempDistance = DemoApplication.CurrentDistance;

					//	TotalDistance = TotalDistance+tempDistance;
					//  m/sec * 3.6 -> km/h
					KilometerPerHour = (MeterPerSeconds * 3.6f); 
					//Note: x100 because its 0.01 km/h not km/h
					//int TempKilometers = (int)(KilometerPerHour*100.f);//Math.round(KilometerPerHour*1000.f);

					if(isMetricTemp)
					{
						//Log.i("Unit Status:", "Matric");
					}
					else
					{
						//Log.i("Unit Status:", "Imperial");
						KilometerPerHour = (KilometerPerHour * 62)/100;
					}

					TempAvgSpeed = Double.toString(KilometerPerHour);
					if (TempAvgSpeed.contains("."))
					{
						String[] SplitStringAvgSpeed = TempAvgSpeed.split("\\.");

						if (SplitStringAvgSpeed[0].length() > 2)
							SplitStringAvgSpeed[0] = SplitStringAvgSpeed[0].substring(0, 2);

						if (SplitStringAvgSpeed[1].length() > 2)
							SplitStringAvgSpeed[1] = SplitStringAvgSpeed[1].substring(0, 2);

						double temp = (DemoApplication.CurrentDistance/1000);

						if(isMetricTemp)
						{
							//Log.i("Unit Status KM/H:", "Metric");
						}
						else
						{
							//Log.i("Unit Status M/H:", "Imperial");
							//Log.i("km ", temp+"");
							temp = (temp * 62)/100;
							Log.i("km to mile", temp+"");
						}

						if(temp<=0.001)
						{
							temp=0.000;
						}

						String temp_str = temp+"";
						String[] TotalDistance_arr = temp_str.split("\\.");

						if (TotalDistance_arr[1].length() > 2)
							TotalDistance_arr[1] = TotalDistance_arr[1].substring(0, 3);


						//Check for valid value
						//						try
						//						{
						//							Integer.parseInt(TotalDistance_arr[1]+"");
						//						}
						//						catch(Exception ex)
						//						{
						//							TotalDistance_arr[0]	=	"0";
						//							TotalDistance_arr[1]	=	"0";
						//						}

						Log.e("Sport", "2.Time:"+DemoApplication.CurrentTotalTime+" Dis:"+DemoApplication.CurrentDistance+" Avg:"+KilometerPerHour);
						Log.e("Sport", "3.AvgStr:"+TempAvgSpeed+" :"+SplitStringAvgSpeed[0]+" , "+SplitStringAvgSpeed[1]);
						Log.e("Altitude:", Altitude+" Bearing(Direction)"+direction);

						SporstDataPackage.Data[13]	= DecToBCD((byte)Integer.parseInt(SplitStringAvgSpeed[0]));   
						SporstDataPackage.Data[14]	= DecToBCD((byte)Integer.parseInt(SplitStringAvgSpeed[1]));

						try
						{
							shared.setPrefrence(context, Constants.PREFRENCE.ROAD_SPEED,  Integer.parseInt(SplitStringAvgSpeed[0])+"."+Integer.parseInt(SplitStringAvgSpeed[1]));
						}
						catch(Exception ex)
						{
							System.out.println(ex);
						}

						shared.setPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE, TotalDistance_arr[0]+"."+TotalDistance_arr[1]);
						Log.i("","*****************"+TotalDistance_arr[0]+"."+TotalDistance_arr[1]);

						int TempCalories = (int) DemoApplication.CurrentCalories;

						SporstDataPackage.Data[12]	= DecToBCD((byte) (DemoApplication.CurrentCalories%100)); 
						TempCalories = TempCalories / 100;

						//						SporstDataPackage.Data[11]	= DecToBCD((byte) (DemoApplication.CurrentCalories%100)); 
						//						TempCalories = TempCalories / 100;
						//						SporstDataPackage.Data[10]	= DecToBCD((byte) (DemoApplication.CurrentCalories%100)); 
						//						TempCalories = TempCalories / 100;
						//						SporstDataPackage.Data[9]	= DecToBCD((byte) (DemoApplication.CurrentCalories%100));

						Intent updateIntent = new Intent();
						updateIntent.setAction("LocationDataUpdate");
						context.sendBroadcast(updateIntent);
					}
					else
					{
						if (TempAvgSpeed.length() > 2)
							TempAvgSpeed = TempAvgSpeed.substring(0, 2);

						//						SporstDataPackage.Data[13]	= DecToBCD(((byte)(Integer.parseInt(TempAvgSpeed))));   
						//						SporstDataPackage.Data[14]	= (byte)0x00;	

						//						Log.e("Sport", "4.Time:"+DemoApplication.CurrentTotalTime+" Dis:"+DemoApplication.CurrentDistance+" Avg:"+KilometerPerHour);
					}  

					Intent SportDebugIntent = new Intent("SPORTDEBUG");
					Bundle bundle = new Bundle();
					String s1 = String.format
							("Pack0:%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d\n",
									SporstDataPackage.Data[0],SporstDataPackage.Data[1],SporstDataPackage.Data[2],SporstDataPackage.Data[3],SporstDataPackage.Data[4],
									SporstDataPackage.Data[5],SporstDataPackage.Data[6],SporstDataPackage.Data[7],SporstDataPackage.Data[8],SporstDataPackage.Data[9],
									SporstDataPackage.Data[10],SporstDataPackage.Data[11],SporstDataPackage.Data[12],SporstDataPackage.Data[13],SporstDataPackage.Data[14],
									SporstDataPackage.Data[15],SporstDataPackage.Data[16],SporstDataPackage.Data[17],SporstDataPackage.Data[18],SporstDataPackage.Data[19]);

					bundle.putString("SportsDataPackage", s1);
					SportDebugIntent.putExtras(bundle);
					context.sendBroadcast(SportDebugIntent);

					BluetoothService.WriteAppendixB(0x5B);
					android.os.SystemClock.sleep(3000);
					BluetoothService.Ble_Send_Data(SporstDataPackage.Data);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}).start();
	}

	@Override
	public void onDestroy() 
	{
		try
		{
			StopGPSUpdates();
			SendTimer.cancel();
			mLocationManager.removeUpdates(GPSLocationListener);
			mGoogleApiClient.disconnect();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		super.onDestroy();
	}

	private String convertToSingalDecimal(String val_str)
	{
		String[] SplitStringcal= val_str.split("\\.");
		if (SplitStringcal[0].length() > 2)
		{
			SplitStringcal[0] = SplitStringcal[0].substring(0, 2);
		}

		if (SplitStringcal[1].length() > 1)
		{
			SplitStringcal[1] = SplitStringcal[1].substring(0, 1);
		}
		return SplitStringcal[0]+"."+SplitStringcal[1];
	}

	public class getElevation extends AsyncTask<String, String, String> 
	{
		CommonClass mCommonClass = new CommonClass();
		@Override
		protected void onPostExecute(String result) 
		{   
			Log.e("Elevation", "Altitude"+result);
			try
			{
				System.out.println(result);
				JSONObject mJsonObject = new JSONObject(result);
				JSONArray mJsonArray = new JSONArray(mJsonObject.getString("results"));
				System.out.println("array: "+mJsonArray+"");

				for(int i=0;i<mJsonArray.length();i++)
				{
					JSONObject mainObj2 = mJsonArray.getJSONObject(i);
					DemoApplication.CurrentAltitude		=	 convertToSingalDecimal(mainObj2.get("elevation").toString());
				}
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
			super.onPostExecute(result);
		}

		@Override
		protected String doInBackground(String... params) 
		{
			return mCommonClass.GetConnection("https://maps.googleapis.com/maps/api/elevation/json?locations="+elev_lat+","+elev_lon);
		}
	}
	//Feet to centimeter Convert
	public String FtToCm(double ftvalue)
	{
		String mSUffix = "";
		String mString = Math.round((ftvalue* 30.48))+mSUffix;

		if(mString.contains("."))
		{
			mString =mString.substring(0, mString.indexOf("."));
		}

		return mString;
	}

	//Pound to KG
	public String PoundToKg(float poundValue)
	{
		String mSUffix = "";
		double POUNDS_PER_KILOGRAM = 2.2;
		String mString = Math.round((poundValue / POUNDS_PER_KILOGRAM))+ mSUffix;
		if(mString.contains("."))
		{
			mString =mString.substring(0, mString.indexOf("."));
		}
		return mString;
	}

	private String getDirectionAngle(double lat1,double lng1,double lat2,double lng2)
	{
		String direction_str = "N/A";
		double dLon = (lng2-lng1);
		double y = Math.sin(dLon) * Math.cos(lat2);
		double x = Math.cos(lat1)*Math.sin(lat2) - Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
		double brng = Math.toDegrees((Math.atan2(y, x)));
		brng = (360 - ((brng + 360) % 360));

		Log.e("Direction:", brng+" "+ brng/27+" "+convertToNoDecimal(brng));
		direction = (int) brng;
		switch (convertToNoDecimal(brng/27))
		{
		case "0": direction_str 	=	"N"; 	break;
		case "1": direction_str 	=	"NNE"; 	break;
		case "2": direction_str 	=	"NE"; 	break;
		case "3": direction_str 	=	"ENE"; 	break;
		case "4": direction_str 	=	"E"; 	break;
		case "5": direction_str 	=	"ESE"; 	break;
		case "6": direction_str 	=	"SE"; 	break;
		case "7": direction_str 	=	"SSE"; 	break;
		case "8": direction_str 	=	"S"; 	break;
		case "9": direction_str 	=	"SSW"; 	break;
		case "10": direction_str 	=	"SW"; 	break;
		case "11": direction_str 	=	"WSW";	break;
		case "12": direction_str 	=	"W"; 	break;
		case "13": direction_str 	=	"WNW";	break;
		case "14": direction_str 	=	"NW"; 	break;
		case "15": direction_str 	=	"NNW";	break;
		default: direction_str 		=	"N/A";	break;
		}
		return direction_str;
	}

	//Remove all the decimal points
	private String convertToNoDecimal(Double val)
	{
		String val_str = val+"";
		String[] SplitStringcal= val_str.split("\\.");

		return SplitStringcal[0];
	}


	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) 
	{
		if (provider1 == null) return provider2 == null;
		return provider1.equals(provider2);
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) 
	{
		rootIntent.setClass(context, GPSLocationServices.class);
		stopService(rootIntent);
		super.onTaskRemoved(rootIntent);
	}



	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{

	}

	@Override
	public void onProviderEnabled(String provider) 
	{

	}

	@Override
	public void onProviderDisabled(String provider) 
	{

	}


}