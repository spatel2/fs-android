package com.freestyle.android.config;


public class FavoriteData
{
	public String Favorite_Id = "";
	public String Spot_Id = "";
	public String SpotName = "";
	public String Sub_Region_Name = "";
	public String Sub_Region_Id = "";
	public String Region_Id = "";
	public String Country_Id = "";
	


	public FavoriteData() {
	}

	public String getFavorite_Id() {
		return Favorite_Id;
	}

	public void setFavorite_Id(String favorite_Id) {
		Favorite_Id = favorite_Id;
	}

	public String getSpotName() {
		return SpotName;
	}

	public void setSpotName(String spotName) {
		SpotName = spotName;
	}

	public String getSub_Region_Name() {
		return Sub_Region_Name;
	}

	public void setSub_Region_Name(String sub_Region_Name) {
		Sub_Region_Name = sub_Region_Name;
	}

	public String getSpot_Id() {
		return Spot_Id;
	}

	public void setSpot_Id(String spot_Id) {
		Spot_Id = spot_Id;
	}

	public String getSub_Region_Id() {
		return Sub_Region_Id;
	}

	public void setSub_Region_Id(String sub_Region_Id) {
		Sub_Region_Id = sub_Region_Id;
	}

	public String getRegion_Id() {
		return Region_Id;
	}

	public void setRegion_Id(String region_Id) {
		Region_Id = region_Id;
	}

	public String getCountry_Id() {
		return Country_Id;
	}

	public void setCountry_Id(String country_Id) {
		Country_Id = country_Id;
	}
	
	
}
