package com.freestyle.android.config;

import java.util.ArrayList;
import java.util.LinkedHashMap;


/**
 * class holds the ArrayList of LinkedHashMap, to store n number of data 
 * in row column structure 
 * @author Azhar Shaikh
 * @version $Revision: 1.0 $
 */
public class DataHolder 
{
	//Variable declaration
	/**
	 * Field mRow.
	 */
	private ArrayList<LinkedHashMap<String, String>> mRow;

	/**
	 * Field mColoumn.
	 */
	private LinkedHashMap<String, String> mColoumn;

	/**
	 * Constructor for DataHolder.
	 */
	public DataHolder() {
		super();
		mRow = new ArrayList<LinkedHashMap<String, String>>();
	}

	/**
	 * Method setmColoumn.
	 * @param col String
	 * @param value String
	 */
	public void setmColoumn(String col, String value) {
		this.mColoumn.put(col, value);
	}

	/**
	 * Method getmRow.
	 * @return ArrayList<LinkedHashMap<String,String>>
	 */
	public ArrayList<LinkedHashMap<String, String>> getmRow() {
		return mRow;
	}

	/**
	 * Method IsRowEmpty.
	 * @return boolean
	 */
	public boolean IsRowEmpty() {
		if (mRow.isEmpty())
			return true;
		return false;
	}

	/**
	 * Method CreateRow.
	 */
	public void CreateRow() {
		this.mColoumn = new LinkedHashMap<String, String>();
	}

	/**
	 * Method AddRow.
	 */
	public void AddRow() {
		this.mRow.add(this.mColoumn);
	}
	
	/**
	 * Method RemoveRow.
	 * @param position int
	 */
	public void RemoveRow(int position){
		this.mRow.remove(position);
	}

	/**
	 * Method mRowAvailable.
	 * @return boolean
	 */
	public boolean mRowAvailable() {
		if (mRow.size() > 0)
			return true;
	
		return false;
	}
}
