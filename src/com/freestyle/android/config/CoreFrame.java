package com.freestyle.android.config;

public interface CoreFrame 
{
	public void goBack(String target);
	public void goForward(String target);
	public void setTypefaceControls();
}
