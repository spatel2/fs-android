package com.freestyle.android.config;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Custom TextView with SegoeBold Font
 *
 */
public class ButtonClass extends Button {
	
	Typeface mTypeface;

	public ButtonClass(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public ButtonClass(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();

	}

	public ButtonClass(Context context) {
		super(context);
		init();

	}

	private void init() {
		if (!isInEditMode()) {

			mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/sinkin_400.otf");
			setTypeface(mTypeface);
		}
	}
}
