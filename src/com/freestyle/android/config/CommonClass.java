package com.freestyle.android.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

public class CommonClass 
{
	private ConnectivityManager connectivity=null;
	private NetworkInfo netinfo=null;
	
	/**This method use for check Network Connectivity
	 */
	
	public boolean CheckNetwork(Context mContext) 
	{
		    this.connectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
	        this.netinfo= connectivity.getActiveNetworkInfo();
	        if(netinfo!=null && netinfo.isConnected()==true)
	        {	      
	        	return true;
	        }
	        else
	        {
//	        	Toast.makeText(mContext,  getResources().getString(R.string.error_internet), Toast.LENGTH_LONG).show();
	        	return false;       	
	        }
		 
	}
	
	/**This method use for check Network Connectivity No Message
	 */
	public boolean CheckNetworkNoMessage(Context mContext) {
		    this.connectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
	        this.netinfo= connectivity.getActiveNetworkInfo();
	        if(netinfo!=null && netinfo.isConnected()==true)
	        {
	        	return true;
	        }
	        else
	        {
	        	return false;       	
	        }
		
	}
	
	/**This function use for check service running or not*/
	public static boolean IsServiceRunning(Context context,String package_class) {
	    ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (package_class.equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
    }
	
	
	/**This method use for PostConnection to Server
	 */
	public String PostConnection(String strUrl,ArrayList<NameValuePair> alstNameValuePair, String X_public, String X_hash, String device_id)
	{
		InputStream mInputStream = null;
		try {
			//This is the default apacheconnection.
			HttpClient mHttpClient = new DefaultHttpClient();
			
//			HttpConnectionParams.setConnectionTimeout(mHttpClient.getParams(), 60000); //Timeout Limit
			//Pathe of serverside 
			HttpPost mHttpPost = new HttpPost(strUrl);
			
			
			
			Log.i("X-PUBLIC: ", X_public);
			Log.i("X-HASH: ", X_hash);
			Log.i("Device ID", device_id);
			
			
			if(alstNameValuePair!=null)
			{ 
				//post the valur you want to pass.
				 mHttpPost.setEntity(new UrlEncodedFormEntity(alstNameValuePair));
			}
			
			//get the valu from the saerverside as response.
			HttpResponse mHttpResponse = mHttpClient.execute(mHttpPost);
			HttpEntity mHttpEntity = mHttpResponse.getEntity();
			mInputStream = mHttpEntity.getContent();
		
		  } 
		  catch (Exception e) {
			  e.printStackTrace();
		  }
		
		 String strLine = null;
		 String strResult = "";
		 
		//convert response in to the string.
		try {
				if(mInputStream!=null){
				  BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream,HTTP.UTF_8), 8);
				  StringBuilder mStringBuilder = new StringBuilder();
			  	  while((strLine = mBufferedReader.readLine()) != null) {
			  		mStringBuilder.append(strLine + "\n");
				  }
				  	  strResult = mStringBuilder.toString();
				  	  mInputStream.close();
				}
		   } 
		   catch (Exception e) {
			   	e.printStackTrace();
		    }
		
		 return strResult;
	}
	
	/**This method use for GetConnection to Server
	 */
	public String GetConnection(String strUrl ) 
	{
		InputStream mInputStream = null;
		try {
			//This is the default apacheconnection.
			HttpClient mHttpClient = new DefaultHttpClient();
//			HttpConnectionParams.setConnectionTimeout(mHttpClient.getParams(), 60000); //Timeout Limit
			//Pathe of serverside 
			HttpGet mHttpGet = new HttpGet(strUrl);
			
//			
//			Log.i("X-Hash: ", X_hash);
//			Log.i("X-Public: ", X_public);
//			Log.i("Device ID: ", device_id);
			
			//get the valu from the saerverside as response.
			HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
			HttpEntity mHttpEntity = mHttpResponse.getEntity();
			mInputStream = mHttpEntity.getContent();
		
			
			
			
		  } 
		  catch (Exception e) {
			e.printStackTrace();
		  }
		
		 String strLine = null;
		 String strResult = null;
		//convert response in to the string.
		try {
			  BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream,HTTP.UTF_8), 8);
			  StringBuilder mStringBuilder = new StringBuilder();
		  	  while((strLine = mBufferedReader.readLine()) != null) {
		  		mStringBuilder.append(strLine + "\n");
			  }
		  	  mInputStream.close();
		  	  strResult = mStringBuilder.toString();			
		   } 
		   catch (Exception e) {
			 e.printStackTrace();
		    }
		
		 return strResult;
	}
	
	/**This method use for GETHTTPConnection to Server
	 */
	public String GETHTTPConnection(String strUrl)
    {
		HttpURLConnection mHttpURLConnection=null;
        int intResponse = -1;
        try{
                URL mURL = new URL(strUrl); 
                URLConnection mURLConnection = mURL.openConnection();
                     
	            if (!(mURLConnection instanceof HttpURLConnection)) {           
	               	Log.e("HTTP","Error in HTTP connection Not Connect");
	                throw new IOException("Not an HTTP connection");
	            }
	            
                mHttpURLConnection = (HttpURLConnection) mURLConnection;
                mHttpURLConnection.setRequestMethod("GET");
                mHttpURLConnection.setUseCaches (false);
                mHttpURLConnection.setDoInput(true);
                mHttpURLConnection.setDoOutput(true);
                mHttpURLConnection.setInstanceFollowRedirects(true);
                mHttpURLConnection.connect(); 
                intResponse = mHttpURLConnection.getResponseCode();
                
                if (intResponse == HttpURLConnection.HTTP_OK) {
                	return InputStreamToString(mHttpURLConnection.getInputStream());                                       
                }
                else{
                	return null;
                }
        }            
        catch (Exception e)
        {
        	e.printStackTrace();  
        	return null;
        }
        finally{
        	if(mHttpURLConnection!=null){
        		mHttpURLConnection.disconnect();
        	}
        }
    }
	
	/**This method use for POSTHTTPConnection to Server
	 */
	public String POSTHTTPConnection(String strUrl,String[] Key,String[] Value)
    {
		HttpURLConnection mHttpURLConnection=null;
        int intResponse = -1;
        try{
                URL mURL = new URL(strUrl); 
                URLConnection mURLConnection = mURL.openConnection();
                     
	            if (!(mURLConnection instanceof HttpURLConnection)) {           
	               	Log.e("HTTP","Error in HTTP connection Not Connect");
	                throw new IOException("Not an HTTP connection");
	            }
	            
                mHttpURLConnection = (HttpURLConnection) mURLConnection;
                mHttpURLConnection.setRequestMethod("POST");
                mHttpURLConnection.setUseCaches (false);
                mHttpURLConnection.setDoInput(true);
                mHttpURLConnection.setDoOutput(true);
                mHttpURLConnection.setInstanceFollowRedirects(true);
                /**This comment code use if encode request UTF_8 format*/
                /*                String Query = String.format("version=%s", URLEncoder.encode("android", HTTP.UTF_8));
                                mHttpURLConnection.setRequestProperty("Accept-Charset", HTTP.UTF_8);
                                mHttpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" +  HTTP.UTF_8);
                                output.write(Query.getBytes(HTTP.UTF_8));*/
                
//                /**This code use for post request*/
                StringBuffer Query = new StringBuffer();
                if(Key!=null || Value!=null){
                	for(int i=0;i < Key.length;i++){
                		Query.append(String.format("%s=%s",Key[i],Value[i]).toString().trim());
                		if(i >= 0 && i < (Key.length -1) && Key.length > 1){
                			Query.append("&");
                		}
                	}
                }
               
                OutputStream mOutputStream = null;
                try {
                	 mOutputStream = mHttpURLConnection.getOutputStream();
                	 Log.i("Request = ", Query.toString());
                	 mOutputStream.write(Query.toString().getBytes());
                } finally {
                     if (mOutputStream != null) try { mOutputStream.close(); } catch (IOException logOrIgnore) {}
                }

                mHttpURLConnection.connect(); 
                intResponse = mHttpURLConnection.getResponseCode();
                
                if (intResponse == HttpURLConnection.HTTP_OK) {
                	return InputStreamToString(mHttpURLConnection.getInputStream());                                 
                }
                else{
                	return null;
                }
        }            
        catch (Exception e)
        {
        	e.printStackTrace();  
        	return null;
        }
        finally{
        	if(mHttpURLConnection!=null){
        		mHttpURLConnection.disconnect();
        	}
        }
    }
	
	/**This method use for convert InputStream To String
	 */
	private String InputStreamToString(InputStream mInputStream){
		 String strLine = null;
		//convert response in to the string.
		try {
			  if(mInputStream!=null){
				  BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream,HTTP.UTF_8), 8);
				  StringBuilder mStringBuilder = new StringBuilder();
			  	  while((strLine = mBufferedReader.readLine()) != null) {
			  		mStringBuilder.append(strLine);
				  }
			  	  mInputStream.close();
			  	  return mStringBuilder.toString();		
			  }
			  else{
				  return null;
			  }
		   } 
		   catch (Exception e) {
			   e.printStackTrace();
		       return null;
		  }
		   
//		try{
//				byte[] bytes = new byte[1024];
//				StringBuilder mStringBuilder = new StringBuilder();
//				int count = 0;
//				while ((count = mInputStream.read(bytes))  != -1) {
//					mStringBuilder.append(new String(bytes, 0, count));
//				}
//		  	    mInputStream.close();
//		  	    return mStringBuilder.toString();	
//		 }
//		 catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		 }
	}
	
	/**This method use for PostConnectionInputStream to Server
	 */
	public InputStream PostConnectionInputStream(String strUrl,ArrayList<NameValuePair> alstNameValuePair) {
		try {
				//This is the default apacheconnection.
				HttpClient mHttpClient = new DefaultHttpClient();
//				HttpConnectionParams.setConnectionTimeout(mHttpClient.getParams(), 60000); //Timeout Limit
				//Pathe of serverside 
				HttpPost mHttpPost = new HttpPost(strUrl);
				
				if(alstNameValuePair!=null)
				{ 
					//post the valur you want to pass.
					 mHttpPost.setEntity(new UrlEncodedFormEntity(alstNameValuePair));
				}
				//get the valu from the saerverside as response.
				HttpResponse mHttpResponse = mHttpClient.execute(mHttpPost);
				HttpEntity mHttpEntity = mHttpResponse.getEntity();
				return mHttpEntity.getContent();
		  } 
		  catch (Exception e) {
				e.printStackTrace();
				 return null;
		  }		
	}
	
	/**This method use for GetConnectionInputStream to Server
	 */
	public InputStream GetConnectionInputStream(String strUrl) 
	{
		try 
		{
			//This is the default apacheconnection.
			HttpClient mHttpClient = new DefaultHttpClient();
//				HttpConnectionParams.setConnectionTimeout(mHttpClient.getParams(), 60000); //Timeout Limit
			//Pathe of serverside 
			HttpGet mHttpGet = new HttpGet(strUrl);

			//get the valu from the saerverside as response.
			HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
			HttpEntity mHttpEntity = mHttpResponse.getEntity();
			return mHttpEntity.getContent();
		  } 
		  catch (Exception e) 
		  {
			e.printStackTrace();
			return null;
		  }		
	}
	
	/**This method use for InputStream_To_Bitmap 
	 */
	public Bitmap InputStream_To_Bitmap(InputStream inputstream)
	{
		return BitmapFactory.decodeStream(inputstream); 		
	}
	
	 /**This method use to Check Memory Card
	 */  
    public static Boolean MemoryCardCheck() 
    {
    	if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
	}
    
	 /**This method use to Write File String
	 */  
    public void WriteString(String FilePath,String Text)
    {
    	try
    	{
            File mFile=new File(FilePath);
            FileOutputStream mFileOutputStream = new FileOutputStream(mFile);
            OutputStreamWriter mOutputStreamWriter = new OutputStreamWriter(mFileOutputStream); 
            mOutputStreamWriter.write(Text);
            mOutputStreamWriter.flush();
            mOutputStreamWriter.close();
    	}
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
    }
}
