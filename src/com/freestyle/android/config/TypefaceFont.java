package com.freestyle.android.config;

import android.content.Context;
import android.graphics.Typeface;

public class TypefaceFont 
{
	Context context;
	Shared_preference shared;
	Typeface tf;
	public TypefaceFont(Context context) 
	{
		this.context = context;
		shared = new Shared_preference(context);
	}
	
	//set font given in asset
	public Typeface getTypeface()
	{
		//tf = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
		tf = Typeface.createFromAsset(context.getAssets(), "fonts/sinkin_400.otf");
		
		
		return tf;
	}
//	public SpannableString getSpan(CharSequence mTitle)
//	{
//		String language = shared.getPrefrence(context, Constants.PREFRENCE.LANGUAGE);
//		
//		SpannableString span = new SpannableString(mTitle);
//		if(!language.equals("english"))
//		{
//			span.setSpan(new TypefaceSpan(context, language+".ttf"), 0, span.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		}
//		return span;
//	}
}