package com.freestyle.android.config;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class GetAllData implements Parcelable {

	public List<GetGlobal> global;

	public GetAllData() {

	}

	public List<GetGlobal> getData() {
		return global;
	}

	public void setData(List<GetGlobal> data) {
		this.global = data;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(global);

	}

	public static Parcelable.Creator<GetAllData> CREATOR = new Creator<GetAllData>() {

		@Override
		public GetAllData[] newArray(int size) {
			return new GetAllData[size];
		}

		@Override
		public GetAllData createFromParcel(Parcel source) {
			return new GetAllData(source);
		}
	};

	public GetAllData(Parcel in) {
		global = new ArrayList<GetGlobal>();
		in.readTypedList(global, GetGlobal.CREATOR);
	}

	public static class GetGlobal implements Parcelable {

		public GetArea area;

		public GetGlobal() {

		}

		public GetArea getArea() {
			return area;
		}

		public void setArea(GetArea area) {
			this.area = area;
		}

		@Override
		public int describeContents() {

			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {

			dest.writeParcelable(area, flags);
		}

		public static Parcelable.Creator<GetGlobal> CREATOR = new Creator<GetGlobal>() {

			@Override
			public GetGlobal[] newArray(int size) {
				return new GetGlobal[size];
			}

			@Override
			public GetGlobal createFromParcel(Parcel source) {
				return new GetGlobal(source);
			}
		};

		public GetGlobal(Parcel in) {

			area = in.readParcelable(GetArea.class.getClassLoader());
		}

		public static class GetArea implements Parcelable {

			public String id = "";
			public List<GetAreaContent> content;
			public String name = "";

			public GetArea() {

			}

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public List<GetAreaContent> getContent() {
				return content;
			}

			public void setContent(List<GetAreaContent> content) {
				this.content = content;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			@Override
			public int describeContents() {
				return 0;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags) {

				dest.writeString(id);
				dest.writeTypedList(content);
				dest.writeString(name);
			}

			public static Parcelable.Creator<GetArea> CREATOR = new Creator<GetArea>() {

				@Override
				public GetArea[] newArray(int size) {
					return new GetArea[size];
				}

				@Override
				public GetArea createFromParcel(Parcel source) {
					return new GetArea(source);
				}
			};

			public GetArea(Parcel in) {

				id = in.readString();
				content = new ArrayList<GetAreaContent>();
				in.readTypedList(content,GetAreaContent.CREATOR);
				name = in.readString();
			}
		}

		public static class GetAreaContent implements Parcelable {

			public GetRegion region;

			public GetAreaContent() {

			}

			public GetRegion getRegion() {
				return region;
			}

			public void setRegion(GetRegion region) {
				this.region = region;
			}

			@Override
			public int describeContents() {
				return 0;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags) {

				dest.writeParcelable(region, flags);
			}

			public static Parcelable.Creator<GetAreaContent> CREATOR = new Creator<GetAreaContent>() {

				@Override
				public GetAreaContent[] newArray(int size) {
					return new GetAreaContent[size];
				}

				@Override
				public GetAreaContent createFromParcel(Parcel source) {
					return new GetAreaContent(source);
				}
			};

			public GetAreaContent(Parcel in) {

				region = in.readParcelable(GetRegion.class.getClassLoader());
			}

		}

		public static class GetRegion implements Parcelable {

			public String id = "";
			public List<GetRegionContent> content;
			public String name = "";

			public GetRegion() {

			}

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public List<GetRegionContent> getContent() {
				return content;
			}

			public void setContent(List<GetRegionContent> content) {
				this.content = content;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			@Override
			public int describeContents() {
				return 0;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags) {

				dest.writeString(id);
				dest.writeTypedList(content);
				dest.writeString(name);
			}

			public static Parcelable.Creator<GetRegion> CREATOR = new Creator<GetRegion>() {

				@Override
				public GetRegion[] newArray(int size) {
					return new GetRegion[size];
				}

				@Override
				public GetRegion createFromParcel(Parcel source) {
					return new GetRegion(source);
				}
			};

			public GetRegion(Parcel in) {

				id = in.readString();
				content = new ArrayList<GetRegionContent>();
				in.readTypedList(content,GetRegionContent.CREATOR);
				name = in.readString();
			}
		}

		public static class GetRegionContent implements Parcelable {

			public GetSubRegion subregion;

			public GetRegionContent() {

			}

			public GetSubRegion getSubregion() {
				return subregion;
			}

			public void setSubregion(GetSubRegion subregion) {
				this.subregion = subregion;
			}

			@Override
			public int describeContents() {
				return 0;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags) {

				dest.writeParcelable(subregion, flags);
			}

			public static Parcelable.Creator<GetRegionContent> CREATOR = new Creator<GetRegionContent>() {

				@Override
				public GetRegionContent[] newArray(int size) {
					return new GetRegionContent[size];
				}

				@Override
				public GetRegionContent createFromParcel(Parcel source) {
					return new GetRegionContent(source);
				}
			};

			public GetRegionContent(Parcel in) 
			{
				subregion = in.readParcelable(GetSubRegion.class.getClassLoader());
			}
		}


		public static class GetSubRegion implements Parcelable {

			public String id = "";
			public List<GetSpots> spots;
			public String name = "";

			public GetSubRegion() {

			}

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public List<GetSpots> getSpots() {
				return spots;
			}

			public void setSpots(List<GetSpots> spots) {
				this.spots = spots;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			@Override
			public int describeContents() {
				return 0;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags) {

				dest.writeString(id);
				dest.writeTypedList(spots);
				dest.writeString(name);
			}

			public static Parcelable.Creator<GetSubRegion> CREATOR = new Creator<GetSubRegion>() {

				@Override
				public GetSubRegion[] newArray(int size) {
					return new GetSubRegion[size];
				}

				@Override
				public GetSubRegion createFromParcel(Parcel source) {
					return new GetSubRegion(source);
				}
			};

			public GetSubRegion(Parcel in) {

				id = in.readString();
				spots = new ArrayList<GetSpots>();
				in.readTypedList(spots,GetSpots.CREATOR);
				name = in.readString();
			}
		}

		public static class GetSpots implements Parcelable {

			public String id = "";
			public String title = "";
			public String lat = "";
			public String cameratype = "";
			public String lon = "";

			public GetSpots() {

			}

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public String getTitle() {
				return title;
			}

			public void setTitle(String title) {
				this.title = title;
			}

			public String getLat() {
				return lat;
			}

			public void setLat(String lat) {
				this.lat = lat;
			}

			public String getCameratype() {
				return cameratype;
			}

			public void setCameratype(String cameratype) {
				this.cameratype = cameratype;
			}

			public String getLon() {
				return lon;
			}

			public void setLon(String lon) {
				this.lon = lon;
			}

			@Override
			public int describeContents() {
				return 0;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags) {

				dest.writeString(id);
				dest.writeString(title);
				dest.writeString(lat);
				dest.writeString(cameratype);
				dest.writeString(lon);
			}

			public static Parcelable.Creator<GetSpots> CREATOR = new Creator<GetSpots>() {

				@Override
				public GetSpots[] newArray(int size) {
					return new GetSpots[size];
				}

				@Override
				public GetSpots createFromParcel(Parcel source) {
					return new GetSpots(source);
				}
			};

			public GetSpots(Parcel in) {

				id = in.readString();
				title = in.readString();
				lat = in.readString();
				cameratype = in.readString();
				lon = in.readString();
			}

		}
	}
}
