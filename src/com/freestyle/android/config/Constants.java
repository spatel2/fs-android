package com.freestyle.android.config;


public class Constants 
{
	public static final class URL
	{
		public static final String BEACH_DATA_URL = "http://slapi01.surfline.com/v1/header?service=cams&user_key=abca50124d77d41504c4d53e29c8d359";
		public static final String FORECAST_URL = "http://slapi01.surfline.com/v1/forecasts/";
	}

	public static final class TableNames
	{
		public static final String FAVORITE = "Favorite";
		public static final String ACTIVITY_LOG = "activity_log";
		public static final String MAIN_ACTIVITIES = "main_activities";
		public static final String SUB_ACTIVITY = "sub_activity";
	}

	public static final class FAVORITE_FIELDS
	{
		public static final String SPOT_ID = "Spot_ID";
		public static final String SUB_REGION_ID = "Sub_Region_ID";
		public static final String REGION_ID = "Region_ID";
		public static final String COUNTRY_ID = "Country_ID";
	}

	public static final class ACTIVITY_LOG_FIELDS
	{
		public static final String ACTIVITY_NAME = "activity_name";
		public static final String ACTIVITY_TIME = "activity_time";
		public static final String ACTIVITY_DATE = "activity_date";
		public static final String ACTIVITY_LATITUDE = "activity_latitude";
		public static final String ACTIVITY_LONGTITUDE = "activity_longtitude";
	}

	public static final class MAIN_ACTIVITIES_FIELDS
	{
		public static final String INDEX_NO = "index_no";
		public static final String ACTIVITY_NAME = "activity_name";
		public static final String CHILD_ACTIVITY = "child_activity";
	}	

	public static final class SUB_ACTIVITY_FIELDS
	{
		public static final String INDEX_NO = "index_no";
		public static final String ACTIVITY_NAME = "activity_name";
		public static final String PARENT_ACTIVITY_ID = "parent_activity_id";
	}

	//For Activity Log
	public static final class PREFRENCE
	{
		public static final String ROAD_LATITUDE = "road_latitude";
		public static final String ROAD_LONGTITUDE = "road_longtitude";
		public static final String HOME_BEACH_ID = "home_beach_id";
		public static final String HOME_SUBREGION_ID = "home_subregion_id";
		public static final String HOME_REGION_ID = "home_region_id";
		public static final String HOME_AREA_ID = "home_area_id";

		public static final String API_day	= "API_DAY";
		public static final String API_hour	= "API_HOUR";
		
		public static final String ROAD_SPEED = "road_speed";
		public static final String ROAD_DISTANCE = "road_distance";
		public static final String IS_ACTIVITY_RUNNING = "is_activity_running";



		public static final String SPORTS_ID = "sports_id";
		public static final String HEART_RATE = "heart_rate";

		public static final String HOME_BEACH_LAT = "beack_lat";
		public static final String HOME_BEACH_LONG = "beack_long";

		public static final String ISACTIVITYPAUSED = "isActivityPaused";
		public static final String ISACTIVITYRUNNING = "isActivityRunning";

		//For Activity map
		public static final String MAP_START_LAT = "map_start_lat";
		public static final String MAP_START_LON = "map_start_lon";

		//For home Current location
		public static final String HOME_BEACH_LOCATION = "Home_beach_location";
		public static final String TEMP_LAT = "temp_lat";
		public static final String TEMP_LOT = "temp_lot";

		//Prefrence For Imperial and Matric
		public static final String METRIC_HEIGHT_DISTANCE = "metric_height_distance";
		public static final String METRIC_TEMP = "metric_temp";
		public static final String TIDE_ALERT_ENABLE = "Tide_Alert_Enable";
	}

	public static final class ERROR
	{
		public static final String HOME_BEACH_SELECTED = "Beach selected for home screen";
		public static final String REMOVE_BEACH = "Beach removed from favorite";
	}

	public static final class PROFILE
	{
		public static final String USER_IMAGE 		= "user_image";
		public static final String USER_GENDER 		= "user_gender";
		public static final String USER_AGE 		= "user_age";
		public static final String USER_HEIGHT		= "user_height";
		public static final String USER_WEIGHT 		= "user_weight";
		public static final String USER_MANUAL 		= "user_manual";
		public static final String SURF_AUTO_MANUAL = "surf_auto_manual";
		public static final String TIDE_ALERT 		= "tide_alert";
		public static final String UNIT_IMPERIAL_METRIC	 = "unit_imperial_metric";
	}	
	
	//	public static ArrayList<GetFacebookProfileResponse> mArrayListFacebookData = new ArrayList<GetFacebookProfileResponse>();
}