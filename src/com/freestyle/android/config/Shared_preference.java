package com.freestyle.android.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Shared_preference 
{
	SharedPreferences shared;
	Editor edit;
	
	public Shared_preference(Context context)
	{
		shared = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public boolean setPrefrence(Context context, String key, String value)
	{
		edit = shared.edit();
		edit.putString(key, value);
		edit.commit();
		return true;
	}
	
	public String getPrefrence(Context context, String key)
	{
		String str="";
		str = shared.getString(key, "").toString();
		return str;
	}
	
	public String getPrefrencewithDefault(Context context, String key, String def)
	{
		String str="";
		str = shared.getString(key, def).toString();
		return str;
	}
}
