package com.freestyle.android.config;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Class deal with Database
 * @author Azhar Shaikh
 *
 * @version $Revision: 1.0 $
 */
public class DatabaseConnectionAPI extends SQLiteOpenHelper 
{
	//Variable declaration
	/**
	 * Field myContext.
	 */
	
	String Query1 ="CREATE TABLE sub_activity (index_no INTEGER PRIMARY KEY, activity_name TEXT, parent_activity_id TEXT)";
	String Query2 ="INSERT INTO `sub_activity` VALUES(1,'Mountain Bike',1)";
	String Query3 ="INSERT INTO `sub_activity` VALUES(2,'Road Bike',1)";
	String Query4 ="INSERT INTO `sub_activity` VALUES(3,'Snowboard',4)";
	String Query5 ="INSERT INTO `sub_activity` VALUES(4,'Ski',4)";
	String Query6 ="INSERT INTO `sub_activity` VALUES(5,'Wakeboarding',7)";
	String Query7 ="INSERT INTO `sub_activity` VALUES(6,'Wake Surfing',7)";
	String Query8 ="CREATE TABLE 'main_activities' (index_no INTEGER PRIMARY KEY, activity_name TEXT, child_activity TEXT)";
	String Query9 ="INSERT INTO `main_activities` VALUES(1,'Bike',1)";
	String Query10 ="INSERT INTO `main_activities` VALUES(2,'Run','')";
	String Query11 ="INSERT INTO `main_activities` VALUES(3,'Skate','')";
	String Query12 ="INSERT INTO `main_activities` VALUES(4,'Snow',1)";
	String Query13 ="INSERT INTO `main_activities` VALUES(5,'Sup','')";
	String Query14 ="INSERT INTO `main_activities` VALUES(6,'Surf','')";
	String Query15 ="INSERT INTO `main_activities` VALUES(7,'Wake',1)";
	String Query16 ="CREATE TABLE area (spots_camera TEXT, spots_lon TEXT, spots_lat TEXT, spots_name TEXT, spots_id TEXT, subregion_name , subregion_id TEXT, region_name TEXT, region_id TEXT, area_id TEXT, area_name TEXT)";
	String Query17 ="CREATE TABLE activity_log (index_no INTEGER PRIMARY KEY, activity_name TEXT, activity_time TEXT, activity_date TEXT, activity_latitude TEXT, activity_longtitude TEXT)";
	String Query18 ="CREATE TABLE 'Favorite' ('Spot_ID' VARCHAR,'Sub_Region_ID' VARCHAR,'Region_ID' VARCHAR,'Country_ID' VARCHAR)";
	
	private final Context myContext;
	/**
	 * Field db.
	 */
	private static SQLiteDatabase db;
	/**
	 * Field DB_PATH.
	 * (value is ""/data/data/android.expert.invoice/databases/"")
	 */
	@SuppressLint("SdCardPath") 
	public final static String DB_PATH = "/data/data/com.freestyle.android/databases/";

	/**
	 * Field DB_NAME.
	 */
	public static String DB_NAME = "SurfFavorite.sqlite";

	public static final int INVALID = -1;



	

	/**
	 * Constructor Takes and keeps a reference of the passed context in order to
	 * access to the application assets and resources.
	 * 
	 * @param context
	 */
	public DatabaseConnectionAPI(Context context)
	{
		super(context, DB_NAME, null, 1);
		this.myContext = context;
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * * @throws IOException
	 */
	public void createDataBase() throws IOException {
//
//		boolean dbExist = checkDataBase();
//
//		if (dbExist) {
//			// do nothing
//		} else {
//			// By calling this method and empty database will be created into
//			// the default system path
//			// of your application so we are gonna be able to overwrite that
//			// database with our database.
//			this.getReadableDatabase();
//			try {
//				copyDataBase();
//			} catch (IOException e) {
//				throw new Error("Error copying database");
//			}
//		}
	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 

	 * @return true if it exists, false if it doesn't */
	private boolean checkDataBase() {

		SQLiteDatabase checkDB = null;
		try {
			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
		} catch (SQLiteException e) {
			//e.printStackTrace();
		}
		if (checkDB != null) {
			checkDB.close();
		}
		return checkDB != null ? true : false;
	}


	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * * @throws IOException
	 */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[2048];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	/**
	 * Method openDataBase.
	 * @throws SQLException
	 */
	public void openDataBase() throws SQLException {
		try {
			if (db != null) {
				if (db.isOpen()) {
					db.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Open the database
		String myPath = DB_PATH + DB_NAME;
		db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
	}

	/**
	 * Method close.
	 */
	@Override
	public synchronized void close() {
		if (db != null)
			db.close();
		super.close();
	}

	/**
	 * Method onCreate.
	 * @param db SQLiteDatabase
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(Query1);
		db.execSQL(Query2);
		db.execSQL(Query3);
		db.execSQL(Query4);
		db.execSQL(Query5);
		db.execSQL(Query6);
		db.execSQL(Query7);
		db.execSQL(Query8);
		db.execSQL(Query9);
		db.execSQL(Query10);
		db.execSQL(Query11);
		db.execSQL(Query12);
		db.execSQL(Query13);
		db.execSQL(Query14);
		db.execSQL(Query15);
		db.execSQL(Query16);
		db.execSQL(Query17);
		db.execSQL(Query18);
		
	}

	/**
	 * Method onUpgrade.
	 * @param db SQLiteDatabase
	 * @param oldVersion int
	 * @param newVersion int
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}


	/**
	 * Query the given table, returning a Cursor over the result set.
	 * 
	 * @param table
	 *            The table name to compile the query against.
	 * @param columns
	 *            A list of which columns to return. Passing null will return
	 *            all columns, which is discouraged to prevent reading data from
	 *            storage that isn't going to be used.
	 * @param selection
	 *            A filter declaring which rows to return, formatted as an SQL
	 *            WHERE clause (excluding the WHERE itself). Passing null will
	 *            return all rows for the given table.
	 * @param selectionArgs
	 *            You may include ?s in selection, which will be replaced by the
	 *            values from selectionArgs, in order that they appear in the
	 *            selection. The values will be bound as Strings.
	 * @param groupBy
	 *            A filter declaring how to group rows, formatted as an SQL
	 *            GROUP BY clause (excluding the GROUP BY itself). Passing null
	 *            will cause the rows to not be grouped.
	 * @param having
	 *            A filter declare which row groups to include in the cursor, if
	 *            row grouping is being used, formatted as an SQL HAVING clause
	 *            (excluding the HAVING itself). Passing null will cause all row
	 *            groups to be included, and is required when row grouping is
	 *            not being used.
	 * @param orderBy
	 *            How to order the rows, formatted as an SQL ORDER BY clause
	 *            (excluding the ORDER BY itself). Passing null will use the
	 *            default sort order, which may be unordered.

	 * @return A Cursor object, which is positioned before the first entry */
	public Cursor onQueryGetCursor(String table, String[] columns, String selection,
			String[] selectionArgs, String groupBy, String having, String orderBy) {
		Cursor query = null;
		try {
			openDataBase();
			query = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//		close();
		return query;
	}

	/**
	 * Use this function to set the value of a particular column
	 * @param TableName
	 * @param cols
	 * @param values
	 * @param whereClause
	 * @param whereArgs
	 */
	public void update(String TableName, String cols[], String values[], String whereClause, String whereArgs[]) {
		openDataBase();
		db.update(TableName, getContentValues(cols, values), whereClause, whereArgs);
		close();

	}

	/**
	 * Method updateRecord.
	 * @param tableName String
	 * @param cValue ContentValues
	 * @param WhereField String
	 * @param compareValue String[]
	 * @return int
	 */
	public int updateRecord(String tableName,ContentValues cValue, String WhereField, String[] compareValue)
	{
		//		TABLE_NAME, cv, Column + "= ?", new String[] {rowId}
		int mRowsAffected=0;
		openDataBase();
		try {
			mRowsAffected = db.update(tableName, cValue, WhereField, compareValue);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mRowsAffected;
	}

	/**
	 * Method read.
	 * @param mCursor Cursor
	 * @return DataHolder
	 */
	public DataHolder read(Cursor mCursor) {

		//		openDataBase();
		DataHolder _holder = null;

		if (mCursor != null && mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			_holder = new DataHolder();

			while (!mCursor.isAfterLast()) {
				int count = mCursor.getColumnCount();
				_holder.CreateRow();
				for (int i = 0; i < count; i++) {
					_holder.setmColoumn(mCursor.getColumnName(i), mCursor.getString(i));
				}
				_holder.AddRow();
				mCursor.moveToNext();
			}
		}
		return _holder;
	}
	/**
	 * Method read.
	 * @param sql String
	 * @return DataHolder
	 */
	public DataHolder read(String sql) {

		openDataBase();
		DataHolder _holder = null;

		Cursor c = db.rawQuery(sql, null);

		if (c != null) {
			c.moveToFirst();
			_holder = new DataHolder();

			while (!c.isAfterLast()) {

				int count = c.getColumnCount();

				_holder.CreateRow();

				for (int i = 0; i < count; i++) {
					_holder.setmColoumn(c.getColumnName(i), c.getString(i));
				}
				_holder.AddRow();
				c.moveToNext();
			}
		}
		c.close();
		close();
		return _holder;
	}


	/**
	 * Method exeSQL.
	 * @param sql String
	 */
	public void exeSQL(String sql) {
		openDataBase();
		try {
			db.execSQL(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}

		close();
	}

	/**
	 * Method readCursor.
	 * @param sql String
	 * @return Cursor
	 */
	public Cursor readCursor(String sql) {
		openDataBase();
		return db.rawQuery(sql, null);
	}

	/**
	 * Use this function to add the value.



	 * @param tableName String
	 * @param cValue ContentValues
	 * @return int
	 */
	public int insertRecords(String tableName,ContentValues cValue)
	{
		int id=INVALID;
		openDataBase();
		try {
			id = (int) db.insertOrThrow(tableName, null, cValue);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * Use this function to delete the value of a particular column
	 * @param TableName
	 * @param where
	 * @param whereArgs

	 * @return int
	 */
	public int deleteRecord(String TableName, String where, String whereArgs[]) {
		int id= INVALID;
		openDataBase();
		db.execSQL("PRAGMA foreign_keys = on");
		db.delete(TableName, where, whereArgs);
		try {
			id = db.delete(TableName, where, whereArgs);
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return id;
	}

	/**
	 * Method query.
	 * @param qry String
	 * @param whereArgs String[]
	 * @return Cursor
	 */
	public Cursor query(String qry,String whereArgs[]){
		Cursor query = null;
		try {
			openDataBase();
			query = db.rawQuery(qry,whereArgs);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//		close();
		return query;
	}

	/**
	 * Method query.
	 * @param sql String
	 * @return Cursor
	 */
	public Cursor query(String sql) {

		Cursor c = null;

		try {
			c = db.rawQuery(sql, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}


	/**
	 * Get the value from cursor and stored it in class object	
	 * @param TableName
	 * @param cols

	 * @param keyword

	 * @param selectionArgs String[]
	 * @return DataHolder
	 */
	public DataHolder readQuery(String TableName, String cols[], String selectionArgs[], String keyword) {

		openDataBase();
		DataHolder _holder = null;
		Cursor c = null;

		//		query = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);

		c = db.query(TableName, cols, keyword, selectionArgs, null, null, null);

		if (c != null) {
			c.moveToFirst();
			_holder = new DataHolder();
			while (!c.isAfterLast()) {
				int count = c.getColumnCount();
				_holder.CreateRow();
				for (int i = 0; i < count; i++) {
					_holder.setmColoumn(c.getColumnName(i), c.getString(i));
				}
				_holder.AddRow();
				c.moveToNext();
			}
		}
		c.close();
		close();
		return _holder;
	}

	/**
	 * Method readQuerys.
	 * @param TableName String
	 * @param cols String[]
	 * @param where String[]
	 * @param keyword String
	 * @return DataHolder
	 */
	public DataHolder readQuerys(String TableName, String cols[], String where[], String keyword) {

		openDataBase();
		DataHolder _holder = null;

		Cursor c = null;	

		c = db.query(TableName, cols, where[0], null, null, null, null);

		if (c != null) {
			c.moveToFirst();
			_holder = new DataHolder();
			while (!c.isAfterLast()) {
				int count = c.getColumnCount();
				_holder.CreateRow();
				for (int i = 0; i < count; i++) {
					_holder.setmColoumn(c.getColumnName(i), c.getString(i));
				}
				_holder.AddRow();
				c.moveToNext();
			}
		}
		c.close();
		close();
		return _holder;
	}

	/**
	 * get the ContentValue from String array.
	 * @param cols
	 * @param values

	 * @return ContentValues
	 */
	public ContentValues getContentValues(String cols[], String values[]) {
		ContentValues cv = new ContentValues();
		for (int i = 0; i < cols.length; i++) {
			cv.put(cols[i], values[i]);
		}
		return cv;
	}
	/**
	 * Insert the record in the database.
	 * @param tableName
	 * @param cValue
	 */
	public void insertRecord(String tableName,ContentValues cValue)
	{
		openDataBase();
		try {
			db.insertOrThrow(tableName, null, cValue);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteFavourateRecord(String TableName, String where)
	{
		openDataBase();
		try 
		{
			db.delete(TableName, where, null);
			close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

	}
	
}