package com.freestyle.android.config;

import java.io.File;
import java.net.URLDecoder;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;

public class PostParseGet {

	public Context mContext;
	public boolean isNetError = false;
	public boolean isOtherError = false;
	List<NameValuePair> nameValuePairs;

	ConnectivityManager mConnectivityManager;
	Shared_preference shared;
	NetworkInfo mNetworkInfo;


	Gson mGson;

	public PostParseGet(Context context) {
		mContext = context;
		mGson = new Gson();
		shared = new Shared_preference(mContext);
	}

	public Object PostHttpURLJson(String url, Object mObject)
	{
		if(!shared.getPrefrence(mContext, "location_json").toString().equals(""))
		{
			String data = "";
			Object mFillObject= null;

			data = shared.getPrefrence(mContext, "location_json");

			try
			{

				mFillObject = mObject.getClass().newInstance();
				mFillObject = mGson.fromJson(data, mFillObject.getClass());
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
			return mFillObject;
		}
		else
		{
			HttpGet httppost;
			HttpParams httpParameters;
			HttpClient httpclient = null;
			HttpResponse response = null;
			int timeoutConnection = 50000;
			isOtherError = false;
			String data = "";

			Object mFillObject= null;
			if(check_Internet())
			{
				try {                          

					url= URLDecoder.decode(url, "UTF-8");
					url = url.replaceAll(" ", "%20");

					mFillObject = mObject.getClass().newInstance();

					httppost = new HttpGet(url);
					httpParameters = new BasicHttpParams();
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutConnection);
					httpclient = new DefaultHttpClient(httpParameters);
					httppost.setHeader("Content-Type", "text/xml; charset=utf-8");



					// Execute HTTP Post Request
					response = httpclient.execute(httppost);
					data = EntityUtils.toString(response.getEntity());


					shared.setPrefrence(mContext, "location_json", data);

					mFillObject = mGson.fromJson(data, mFillObject.getClass());


				}
				catch (ConnectTimeoutException e) {
					e.printStackTrace();

				}
				catch (Exception e) {
					isOtherError = true;
					e.printStackTrace();
				}
			}
			return mFillObject;
		}
	}

	/**
	 * Function checks internet if avail, Post data to particular Url and filled
	 * json object relating to the response.
	 * 
	 * @param string
	 * @param nameValuePairs
	 * @param object
	 * @param context
	 * @return object
	 * @throws CustomException
	 * */
//	public Object postHttpURL(String url, List<NameValuePair> nameValuePairs, Object mObject)
//	{
//		HttpPost httppost;
//		HttpParams httpParameters;
//		int timeoutConnection = 50000;
//		HttpClient httpclient = null;
//		HttpResponse response = null;
//		String data = "";
//
//		isOtherError = false;
//
//		Object mFillObject= null;
//
//		if(check_Internet())
//		{
//			try 
//			{
//				mFillObject = mObject.getClass().newInstance();
//
//				url= URLDecoder.decode(url, "UTF-8");
//				url = url.replaceAll(" ", "%20");
//
//				httppost = new HttpPost(url);
//				httpParameters = new BasicHttpParams();
//				HttpConnectionParams.setSoTimeout(httpParameters, timeoutConnection);
//				httpclient = new DefaultHttpClient(httpParameters);
//
//				MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//
//				if (nameValuePairs != null)
//				{
//					for(int index=0; index < nameValuePairs.size(); index++)
//					{
//						String paramName=nameValuePairs.get(index).getName();
//						String paramValue=nameValuePairs.get(index).getValue();
//
//						if(paramName.equalsIgnoreCase("mem_profilepic"))
//						{
//							entity.addPart(paramName, new FileBody(new File (paramValue)));
//						} 
//						else
//						{
//							entity.addPart(nameValuePairs.get(index).getName(), new StringBody(nameValuePairs.get(index).getValue()));
//						}
//					}
//					httppost.setEntity(entity);
//				}
//
//				System.out.println("url..."+url.toString());
//
//				// Execute HTTP Post Request
//				response = httpclient.execute(httppost);
//				data = EntityUtils.toString(response.getEntity());
//
//				System.out.println("...Final Data :" + data);
//
//				mFillObject = mGson.fromJson(data, mFillObject.getClass());
//
//			} catch (Exception e) {
//
//				isOtherError = true;
//				e.printStackTrace();
//			}
//		}
//		return mFillObject;
//	}


	/**
	 * Function
	 * Takes and keeps a reference of the passed context in order to Check whether Internet is available or not.
	 * @param context
	 * @return The return will be true if the internet is available and false if not.
	 */
	public boolean check_Internet()
	{
		mConnectivityManager= (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

		if(mNetworkInfo != null && mNetworkInfo.isConnectedOrConnecting())
		{
			isNetError = false;
			return true;
		}
		else
		{
			isNetError = true;
			return false;
		}
	}

	public Object GetAllData(Object object) 
	{
		return PostHttpURLJson(String.format(Constants.URL.BEACH_DATA_URL), object);
	}
}
