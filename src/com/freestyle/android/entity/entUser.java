package com.freestyle.android.entity;

import android.content.Context;

public class entUser {
	private Context CONTEXT;
	private int USER_ID;
	private String FIRST_NAME;
	private String LAST_NAME;
	private String PASSWORD;
	private String EMAIL;
	private String GENDER;
	private String BIRTHDATE;
	private String FBID;
	private String PHONE_NUMBER;

	public entUser(Context context) {
		CONTEXT = context;
	}

	public String getGENDER() {
		return GENDER;
	}

	public void setGENDER(String gENDER) {
		GENDER = gENDER;
	}

	public String getBIRTHDATE() {
		return BIRTHDATE;
	}

	public void setBIRTHDATE(String bIRTHDATE) {
		BIRTHDATE = bIRTHDATE;
	}

	public String getFBID() {
		return FBID;
	}

	public void setFBID(String fBID) {
		FBID = fBID;
	}

	public int getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(int USER_ID) {
		this.USER_ID = USER_ID;
	}

	public String getFIRST_NAME() {
		return FIRST_NAME;
	}

	public void setFIRST_NAME(String FIRST_NAME) {
		this.FIRST_NAME = FIRST_NAME;
	}

	public String getLAST_NAME() {
		return LAST_NAME;
	}

	public void setLAST_NAME(String LAST_NAME) {
		this.LAST_NAME = LAST_NAME;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String PASSWORD) {
		this.PASSWORD = PASSWORD;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String EMAIL) {
		this.EMAIL = EMAIL;
	}

	public String getPHONE_NUMBER() {
		return PHONE_NUMBER;
	}

	public void setPHONE_NUMBER(String PHONE_NUMBER) {
		this.PHONE_NUMBER = PHONE_NUMBER;
	}

}
