package com.freestyle.android.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

import com.freestyle.android.activity.FindBeach_Activity;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.login.actRegistration;
import com.freestyle.android.util.clsGeneral;
import com.freestyle.test.clsPref;

public class clsUser {

	private Context CONTEXT;

	public clsUser(Context context) {
		CONTEXT = context;
	}

	public String login(String userName, String password) throws Exception {
		String _userId = "";
		try {
			List<NameValuePair> _parameters = new ArrayList<NameValuePair>();
			_parameters.add(new BasicNameValuePair("Email", userName));
			_parameters.add(new BasicNameValuePair("Password", password));

			String _jsonContent = new clsGeneral().getJSONContentFromInternetService(CONTEXT, clsGeneral.PHPServices.LOGIN, _parameters, true, false);
			JSONObject _job = new JSONObject(_jsonContent);
			// [{"UserId":"1","UserName":"admin"}]

			// if (_jArr.length() == 1) {
			JSONObject _jObj = _job.getJSONObject("response");

			// saving user details to sherd prefs
			// userId = jObj.getString("UserId");
			clsPref _prefs = new clsPref(CONTEXT);

			_prefs.clearAll();
			_prefs.putUserId(_jObj.getString("Id"));
			_prefs.putUserEmail(_jObj.getString("Email"));
			_prefs.putFBID(_jObj.getString("FacebookId"));
			_prefs.putUserPass(_jObj.getString("Password"));
			_prefs.putFirstName(_jObj.getString("FirstName"));
			_prefs.putLastName(_jObj.getString("LastName"));
			_prefs.putGender(_jObj.getString("Gender"));
			if (_jObj.getString("BirthDate").equalsIgnoreCase("0000-00-00")) {
				_prefs.putBirthDate("");

			} else {
				_prefs.putBirthDate(_jObj.getString("BirthDate"));
			}
			_prefs.putPhoneNumber(_jObj.getString("PhoneNo"));

			// }
		} catch (Exception ex) {
			throw ex;
		}
		return _userId;
	}

	public int signUp(String email, String pass, String firstName, String lastName, String facebookId, String gender, String birthDate, String phone, String latitude, String longitude) throws Exception {
		int _userId = 0;
		try {

			List<NameValuePair> _parameters = new ArrayList<NameValuePair>();
			_parameters.add(new BasicNameValuePair("Email", email));
			_parameters.add(new BasicNameValuePair("Password", pass));
			_parameters.add(new BasicNameValuePair("FacebookId", facebookId));
			_parameters.add(new BasicNameValuePair("FirstName", firstName));
			_parameters.add(new BasicNameValuePair("LastName", lastName));
			_parameters.add(new BasicNameValuePair("Gender", gender));
			_parameters.add(new BasicNameValuePair("BirthDate", birthDate));
			_parameters.add(new BasicNameValuePair("PhoneNo", phone));
			_parameters.add(new BasicNameValuePair("Lat", latitude));
			_parameters.add(new BasicNameValuePair("Long", longitude));

			String _jsonContent = new clsGeneral().getJSONContentFromInternetService(CONTEXT, clsGeneral.PHPServices.SIGN_UP, _parameters, true, false);
			JSONObject _job = new JSONObject(_jsonContent);
			// [{"UserId":"1","UserName":"admin"}]

			// if (_jArr.length() == 1) {
			JSONObject _jObj = _job.getJSONObject("response");

			clsPref _prefs = new clsPref(CONTEXT);

			_prefs.clearAll();
			_prefs.putUserId(_jObj.getString("Id"));
			_prefs.putUserEmail(_jObj.getString("Email"));
			_prefs.putFBID(_jObj.getString("FacebookId"));
			_prefs.putUserPass(_jObj.getString("Password"));
			_prefs.putFirstName(_jObj.getString("FirstName"));
			_prefs.putLastName(_jObj.getString("LastName"));
			_prefs.putGender(_jObj.getString("Gender"));
			if (_jObj.getString("BirthDate").equalsIgnoreCase("0000-00-00")) {
				_prefs.putBirthDate("");

			} else {
				_prefs.putBirthDate(_jObj.getString("BirthDate"));
			}
			_prefs.putPhoneNumber(_jObj.getString("PhoneNo"));

		} catch (Exception ex) {
			throw ex;
		}
		return _userId;
	}

	public String signUpdateAccount(String email, String pass, String firstName, String lastName, String facebookId, String gender, String birthDate, String phone, String latitude, String longitude) throws Exception {
		String msg = "";
		try {
			List<NameValuePair> _parameters = new ArrayList<NameValuePair>();
			_parameters.add(new BasicNameValuePair("Email", email));
			_parameters.add(new BasicNameValuePair("Password", pass));
			_parameters.add(new BasicNameValuePair("FacebookId", facebookId));
			_parameters.add(new BasicNameValuePair("FirstName", firstName));
			_parameters.add(new BasicNameValuePair("LastName", lastName));
			_parameters.add(new BasicNameValuePair("Gender", gender));
			_parameters.add(new BasicNameValuePair("BirthDate", birthDate));
			_parameters.add(new BasicNameValuePair("PhoneNo", phone));
			_parameters.add(new BasicNameValuePair("Lat", latitude));
			_parameters.add(new BasicNameValuePair("Long", longitude));
			_parameters.add(new BasicNameValuePair("UserId", new clsPref(CONTEXT).getUserId()));

			String _jsonContent = new clsGeneral().getJSONContentFromInternetService(CONTEXT, clsGeneral.PHPServices.UPDATE_ACCOUNT, _parameters, true, false);
			JSONObject _job = new JSONObject(_jsonContent);
			msg = _job.getString("message");
			// JSONObject _jObj = _job.getJSONObject("response");
			clsPref _prefs = new clsPref(CONTEXT);
			_prefs.clearAll();
			// _prefs.putUserId(_jObj.getString("Id"));
			_prefs.putUserEmail(email);
			_prefs.putUserPass(pass);
			_prefs.putFirstName(firstName);
			_prefs.putLastName(lastName);
			_prefs.putGender(gender);
			_prefs.putBirthDate(birthDate);
			_prefs.putPhoneNumber(phone);

		} catch (Exception ex) {
			throw ex;
		}
		return msg;
	}

	public String forgotPassword(String email) throws Exception {
		String _msg = "";
		try {
			List<NameValuePair> _parameters = new ArrayList<NameValuePair>();
			_parameters.add(new BasicNameValuePair("Email", email));
			String _jsonContent = new clsGeneral().getJSONContentFromInternetService(CONTEXT, clsGeneral.PHPServices.FORGOT_PASSWORD, _parameters, true, false);
			JSONObject _job = new JSONObject(_jsonContent);

			_msg = _job.getString("message");
		} catch (Exception ex) {
			throw ex;
		}
		return _msg;
	}

}