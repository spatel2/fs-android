package com.freestyle.android.login;

import com.freestyle.android.R;
import com.freestyle.android.R.id;
import com.freestyle.android.R.layout;
import com.freestyle.android.R.menu;
import com.freestyle.android.activity.BaseActivity;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.EditTextClass;
import com.freestyle.android.config.TextviewClass;
import com.freestyle.android.entity.clsUser;
import com.freestyle.android.entity.entUser;
import com.freestyle.android.util.clsGeneral;
import com.freestyle.test.clsPref;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

public class actLoginWithFacebook extends BaseActivity implements OnClickListener {
	EditTextClass EDT_EMAIL, EDT_PASSWORD;
	ButtonClass BTN_LOGIN;
	ProgressDialog PROGRESSDIALOG;
	String FNAME,LNAME,BIRTHDAY,GENDER,ID;
	ProgressDialog PROGRESSBAR;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_login_with_facebook);
		initPageControl();
		
		
	}

	private void initPageControl() {
		EDT_EMAIL = (EditTextClass) findViewById(R.id.edtEmail);
		EDT_PASSWORD = (EditTextClass) findViewById(R.id.edtPass);
		BTN_LOGIN = (ButtonClass) findViewById(R.id.btnLogin);
		BTN_LOGIN.setOnClickListener(this);
		
		Intent _intent = getIntent();
		FNAME = _intent.getStringExtra("FName");
		LNAME = _intent.getStringExtra("LName");
		BIRTHDAY = _intent.getStringExtra("Birthday");
		GENDER = _intent.getStringExtra("gender");
		ID = _intent.getStringExtra("ID");
}

	@Override
	public void onClick(View v) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		Intent _intent;
		switch (v.getId()) {
		case R.id.btnLogin:
			submitData();
			break;
		}

	}
	private void submitData() {
		boolean _isSubmit = true;
		String _userName, _password, _errorMsg = "";

		_userName = EDT_EMAIL.getText().toString();
		if (_isSubmit) {
			if (_userName.equalsIgnoreCase("")) {
				_isSubmit = false;
				_errorMsg = "Please enter username";
			}
		}
		String _lowerCaseEmail = _userName.toLowerCase();
		if (_isSubmit) {
			if (!_lowerCaseEmail.matches("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$")) {
				_isSubmit = false;
				_errorMsg = "Please enter Proper Email address";
			}
		}
		_password = EDT_PASSWORD.getText().toString();
		if (_isSubmit) {
			if (_password.equalsIgnoreCase("")) {
				_isSubmit = false;
				_errorMsg = "Please enter password";
			}
		}

		if (_isSubmit) {
			new asyncToRegisterUser(EDT_EMAIL.getText().toString().trim(), EDT_PASSWORD.getText().toString().trim(), FNAME, LNAME, ID, GENDER, BIRTHDAY, "").execute();
		} else {
			clsGeneral.ShowToast(getApplicationContext(), _errorMsg);
		}
	}

	
	
	
	private class asyncToRegisterUser extends AsyncTask<Void, Void, Void> {

		private Exception EX;
		String EMAIL, PASSWORD, FIRST_NAME, LAST_NAME,FACEBOOK_ID,GENDER,BIRTH_DATE,PHONE_NO;
		int USER_ID=0;
		
		public asyncToRegisterUser(String email, String password, String firstName, String LastName, String facebookId, String gender,String birthDate,String phone) {

			this.EMAIL = email;
			this.PASSWORD = password;
			this.BIRTH_DATE=birthDate;
			this.FACEBOOK_ID=facebookId;
			this.GENDER=gender;
			this.PHONE_NO=phone;
			this.FIRST_NAME=firstName;
			this.LAST_NAME=LastName;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			PROGRESSBAR = ProgressDialog.show(actLoginWithFacebook.this, "Loading", "Please wait");
		}

		@Override
		protected Void doInBackground(Void... voids) {
			try {
				 USER_ID =new clsUser(actLoginWithFacebook.this).signUp(EMAIL, PASSWORD,FIRST_NAME,LAST_NAME,FACEBOOK_ID,GENDER,BIRTH_DATE,PHONE_NO,new clsPref(actLoginWithFacebook.this).getLatitude(),new clsPref(actLoginWithFacebook.this).getLongitude());
			} catch (Exception ex) {
				EX = ex;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (PROGRESSBAR.isShowing()) {
				PROGRESSBAR.dismiss();
			}
			if (EX != null) {
				clsGeneral.ShowToast(actLoginWithFacebook.this, EX.getMessage().toString());
				return;
			}
			new clsGeneral().ShowToast(getApplicationContext(), "Signup Successfully");
			startActivity(new Intent(actLoginWithFacebook.this, actForgotPass.class));
			finish();
		}
	}
	
}
