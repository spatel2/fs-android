package com.freestyle.android.login;

import com.freestyle.android.R;
import com.freestyle.android.R.id;
import com.freestyle.android.R.layout;
import com.freestyle.android.R.menu;
import com.freestyle.android.activity.BaseActivity;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.EditTextClass;
import com.freestyle.android.config.TextviewClass;
import com.freestyle.android.entity.clsUser;
import com.freestyle.android.entity.entUser;
import com.freestyle.android.login.actLogin.asyncToAuthenticateUser;
import com.freestyle.android.util.clsGeneral;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

public class actForgotPass extends BaseActivity implements OnClickListener {
	ImageView IV_BACK;
	EditTextClass EDT_EMAIL;
	ButtonClass BTN_SEND;
	ProgressDialog PROGRESSDIALOG;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_forgot_pass);
		initPageControl();
	}

	private void initPageControl() {
		IV_BACK = (ImageView) findViewById(R.id.ivBack);
		IV_BACK.setOnClickListener(this);
		EDT_EMAIL = (EditTextClass) findViewById(R.id.edtEmail);
		BTN_SEND = (ButtonClass) findViewById(R.id.btnSend);
		BTN_SEND.setOnClickListener(this);
		PROGRESSDIALOG = new ProgressDialog(actForgotPass.this);

	}

	@Override
	public void onClick(View v) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		if (PROGRESSDIALOG.isShowing()) {
			return;
		}
		switch (v.getId()) {
		case R.id.btnSend:
			submitData();
			break;
		case R.id.ivBack:
			finish();
			break;
		}
	}

	private void submitData() {
		boolean _isSubmit = true;
		String _userName, _errorMsg = "";

		_userName = EDT_EMAIL.getText().toString();
		if (_isSubmit) {
			if (_userName.equalsIgnoreCase("")) {
				_isSubmit = false;
				_errorMsg = "Please enter username";
				// notifyUser(EDT_EMAIL);
			}
		}
		String _lowerCaseEmail = _userName.toLowerCase();
		if (_isSubmit) {
			if (!_lowerCaseEmail.matches("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$")) {
				_isSubmit = false;
				_errorMsg = "Please enter Proper Email address";
			}
		}

		if (_isSubmit) {
			new asyncToRetrivePassword(_userName).execute();
		} else {
			clsGeneral.ShowToast(getApplicationContext(), _errorMsg);
		}
	}

	public class asyncToRetrivePassword extends AsyncTask<Void, Void, Void> {
		String USR_EMAIL;
		Exception EX;
		String MSG;

		public asyncToRetrivePassword(String _userName) {
			this.USR_EMAIL = _userName;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			PROGRESSDIALOG = ProgressDialog.show(actForgotPass.this, "Loading", "Please wait");

		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				MSG = new clsUser(actForgotPass.this).forgotPassword(USR_EMAIL);
			} catch (Exception e) {
				EX = e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			PROGRESSDIALOG.dismiss();
			if (EX != null) {
				clsGeneral.ShowToast(actForgotPass.this, EX.getMessage().toString());
				return;
			}
			clsGeneral.ShowToast(actForgotPass.this, MSG);
			finish();

		}
	}

}
