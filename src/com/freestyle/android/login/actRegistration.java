package com.freestyle.android.login;

import java.util.Arrays;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.freestyle.android.R;
import com.freestyle.android.activity.BaseActivity;
import com.freestyle.android.activity.FindBeach_Activity;
import com.freestyle.android.activity.Home_Activity;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.entity.clsUser;
import com.freestyle.android.util.clsGeneral;
import com.freestyle.test.clsPref;

public class actRegistration extends BaseActivity implements OnClickListener {

	EditText ET_EMAIL, ET_FIRST_NAME, ET_LAST_NAME, ET_PASSWORD;
	Button BTN_SIGNUP;
	ProgressDialog PROGRESSBAR;
	Shared_preference mSharedPreferenceLatLong;
	Double currentLat = 0.0, currentLong = 0.0;
	LoginButton FB_LOGIN;
	ImageView IV_BACK;
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_registration);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		InitPageControl();

	}

	private void InitPageControl() {
		// TODO Auto-generated method stub
		PROGRESSBAR = new ProgressDialog(actRegistration.this);
		IV_BACK = (ImageView) findViewById(R.id.ivBack);
		IV_BACK.setOnClickListener(this);
		ET_EMAIL = (EditText) findViewById(R.id.edtEmail);
		ET_FIRST_NAME = (EditText) findViewById(R.id.edtFirstName);
		ET_LAST_NAME = (EditText) findViewById(R.id.edtLastName);
		ET_PASSWORD = (EditText) findViewById(R.id.edtPass);
		BTN_SIGNUP = (Button) findViewById(R.id.btnSignup);
		BTN_SIGNUP.setOnClickListener(this);
		FB_LOGIN = (LoginButton) findViewById(R.id.btnFB);
		FB_LOGIN.setBackgroundResource(R.color.btn_blue);
		Typeface mTypeface = Typeface.createFromAsset(getAssets(), "fonts/sinkin_400.otf");
		FB_LOGIN.setTypeface(mTypeface);
		FB_LOGIN.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
		// FB_LOGIN.setBackgroundResource(R.color.purpal);
		FB_LOGIN.setReadPermissions(Arrays.asList("email", "user_location", "user_birthday"));
		FB_LOGIN.setUserInfoChangedCallback(new UserInfoChangedCallback() {

			@Override
			public void onUserInfoFetched(GraphUser user) {
				// HelloFacebookSampleActivity.this.user = user;
				updateUI(user);
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

		switch (v.getId()) {

		case R.id.btnSignup:
			userRegistration();
			break;
		case R.id.ivBack:
			finish();
			break;
		}
	}

	private void userRegistration() {
		// TODO Auto-generated method stub

		String _firstName, _lastName, _email, _pass;
		_firstName = ET_FIRST_NAME.getText().toString().trim();
		_lastName = ET_LAST_NAME.getText().toString().trim();
		_email = ET_EMAIL.getText().toString().trim();
		_pass = ET_PASSWORD.getText().toString().trim();

		if (_email.equals("")) {

			new clsGeneral().ShowToast(actRegistration.this, "Please enter email!");
			return;
		}
		String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
		if (!(_email.matches(emailPattern))) {
			clsGeneral.ShowToast(actRegistration.this, "Invalid email id!");
			return;
		}

		if (_pass.equals("")) {
			clsGeneral.ShowToast(actRegistration.this, "Please enter password!");
			return;
		}
		if (_firstName.equals("")) {
			clsGeneral.ShowToast(actRegistration.this, "Please enter first name!");
			return;
		}
		if (_lastName.equals("")) {
			clsGeneral.ShowToast(actRegistration.this, "Please enter last name!");
			return;
		}
		new asyncToRegisterUser(_email, _pass, _firstName, _lastName, "", "", "", "").execute();

	}

	private class asyncToRegisterUser extends AsyncTask<Void, Void, Void> {

		private Exception EX;
		String EMAIL, PASSWORD, FIRST_NAME, LAST_NAME, FACEBOOK_ID, GENDER,
				BIRTH_DATE, PHONE_NO;
		int USER_ID = 0;

		public asyncToRegisterUser(String email, String password, String firstName, String LastName, String facebookId, String gender, String birthDate, String phone) {

			this.EMAIL = email;
			this.PASSWORD = password;
			this.BIRTH_DATE = birthDate;
			this.FACEBOOK_ID = facebookId;
			this.GENDER = gender;
			this.PHONE_NO = phone;
			this.FIRST_NAME = firstName;
			this.LAST_NAME = LastName;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			PROGRESSBAR = ProgressDialog.show(actRegistration.this, "Loading", "Please wait");

		}

		@Override
		protected Void doInBackground(Void... voids) {
			try {
				USER_ID = new clsUser(actRegistration.this).signUp(EMAIL, PASSWORD, FIRST_NAME, LAST_NAME, FACEBOOK_ID, GENDER, BIRTH_DATE, PHONE_NO, new clsPref(actRegistration.this).getLatitude(), new clsPref(actRegistration.this).getLongitude());
			} catch (Exception ex) {
				EX = ex;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (PROGRESSBAR.isShowing()) {
				PROGRESSBAR.dismiss();
			}
			if (EX != null) {
				clsGeneral.ShowToast(actRegistration.this, EX.getMessage().toString());
				return;
			}

			Intent _intent = new Intent(actRegistration.this, Home_Activity.class);
			_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// _intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(_intent);
			finish();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();

		// Call the 'deactivateApp' method to log an app event for use in
		// analytics and advertising
		// reporting. Do so in the onPause methods of the primary Activities
		// that an app may be launched into.
		// AppEventsLogger.deactivateApp(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
		callFacebookLogout(getApplicationContext());
	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		// if (pendingAction != PendingAction.NONE && (exception instanceof
		// FacebookOperationCanceledException || exception instanceof
		// FacebookAuthorizationException)) {
		// new
		// AlertDialog.Builder(HelloFacebookSampleActivity.this).setTitle(R.string.cancelled).setMessage(R.string.permission_not_granted).setPositiveButton(R.string.ok,
		// null).show();
		// pendingAction = PendingAction.NONE;
		// } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
		// handlePendingAction();
		//
		//
		// }

	}

	public static void callFacebookLogout(Context context) {
		System.out.println("LOGOUT CALL");
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}
	}

	private void updateUI(GraphUser user) {
		Session session = Session.getActiveSession();
		boolean enableButtons = (session != null && session.isOpened() && user != null);

		if (enableButtons) {
			System.out.println("FB LOGIN DONE");
			String _email = "";
			try {
				_email = user.asMap().get("email").toString();// user.getProperty("email").toString();
			} catch (Exception e) {
				e.printStackTrace();
				_email = "";
			}			String _profilePicPath = "https://graph.facebook.com/" + user.getId() + "/picture?type=large";
			String FName = user.getFirstName();
			String LName = user.getLastName();

			String ID = user.getId();
			String Birthday = user.getBirthday();
			String gender = user.asMap().get("gender").toString();

			// String city = user.getLocation().getName();
			System.out.println("_email  " + _email);
			System.out.println("_profilePicPath  " + _profilePicPath);
			System.out.println("FName  " + FName);
			System.out.println("ID  " + ID);
			System.out.println("Birthday  " + Birthday);
			System.out.println("gender  " + gender);
			callFacebookLogout(getApplicationContext());

			if (_email.equals("")) {
				Intent _intent = new Intent(actRegistration.this, actLoginWithFacebook.class);
				_intent.putExtra("FName", FName);
				_intent.putExtra("LName", LName);
				_intent.putExtra("Birthday", Birthday);
				_intent.putExtra("gender", gender.toUpperCase());
				_intent.putExtra("ID", ID);
				startActivity(_intent);

			} else {
				new asyncToRegisterUser(_email, "", FName, LName, ID, gender.toUpperCase(), Birthday, "").execute();
			}

		}

	}

}
