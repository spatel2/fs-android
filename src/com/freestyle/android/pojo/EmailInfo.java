package com.freestyle.android.pojo;

import java.io.Serializable;
import java.util.Date;

public class EmailInfo implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int folderType;
	private int unreadMailsCount;
	private int allMailsCount;
	private int newMailsCount;
	private boolean isSuccessful;
	private String email;
	private String password;
	private String imaps;
	private Date mail_date;

	
	
	public Date getMail_date() {
		return mail_date;
	}

	public void setMail_date(Date date) {
		this.mail_date = date;
	}

	public String getImaps()
	{
		return imaps;
	}

	public void setImaps(String imaps)
	{
		this.imaps = imaps;
	}

	public int getFolderType()
	{
		return folderType;
	}

	public void setFolderType(int folderType)
	{
		this.folderType = folderType;
	}

	public int getUnreadMailsCount()
	{
		return unreadMailsCount;
	}

	public void setUnreadMailsCount(int unreadMailsCount)
	{
		this.unreadMailsCount = unreadMailsCount;
	}

	public int getAllMailsCount()
	{
		return allMailsCount;
	}

	public void setAllMailsCount(int allMailsCount)
	{
		this.allMailsCount = allMailsCount;
	}

	public int getNewMailsCount()
	{
		return newMailsCount;
	}

	public void setNewMailsCount(int newMailsCount)
	{
		this.newMailsCount = newMailsCount;
	}

	public boolean isSuccessful()
	{
		return isSuccessful;
	}

	public void setSuccessful(boolean isSuccessful)
	{
		this.isSuccessful = isSuccessful;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
