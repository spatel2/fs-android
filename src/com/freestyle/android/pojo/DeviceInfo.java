/**
 * 
 */
package com.freestyle.android.pojo;

/**
 * 设备信息类
 * @author Jack
 */
public class DeviceInfo
{
	private String id;
	private String name;
	private String rssi;
	private String address;
	private String pic;
	private String time;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getRssi()
	{
		return rssi;
	}

	public void setRssi(String rssi)
	{
		this.rssi = rssi;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getPic()
	{
		return pic;
	}

	public void setPic(String pic)
	{
		this.pic = pic;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	@Override
	public String toString()
	{
		return "DeviceInfo [id=" + id + ", name=" + name + ", rssi=" + rssi
				+ ", address=" + address + ", pic=" + pic + ", time=" + time
				+ "]";
	}
}
