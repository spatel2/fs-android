package com.freestyle.android.pojo;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

/**
 * 工具类
 * 
 * @author LiaoJian
 * @version 1.0
 */
public class Tool
{
	private static String BROWSER_USERAGENT_DEFAULT = "IPCamViewer/3.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
	private static final int CONNECT_TIMEOUT = 6500;
	private static final int SOCKET_TIMEOUT = 6500;
	private static String IP_URL = "http://user.ipcam.hk/api/getdevinfo.asp?id=";
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
	/**
	 * 获取制定url图片
	 * 
	 * @param bitmapUrl
	 * @return Bitmap
	 */
	public static Bitmap downloadBitmap(URL bitmapUrl)
	{
		HttpURLConnection conn;
		InputStream is = null;
		BufferedInputStream bis = null;
		Bitmap bm = null;
		try
		{
			conn = (HttpURLConnection) bitmapUrl.openConnection();
			conn.connect();
			is = conn.getInputStream();
			bis = new BufferedInputStream(is);
			bm = BitmapFactory.decodeStream(bis);
		}
		catch (IOException e)
		{
			Log.i("BitmapUtil", e.getMessage());
		}
		finally
		{
			try
			{
				if (bis != null)
				{
					bis.close();
				}
				if (is != null)
				{
					is.close();
				}
			}
			catch (IOException e)
			{
				Log.i("BitmapUtil", e.getMessage());
			}
		}
		return bm;
	}

	/**
	 * 重绘图片
	 * 
	 * @param srcBitmap
	 * @param destWidth
	 * @param destHeight
	 * @return BitmapDrawable
	 */
	public static BitmapDrawable drawBitmap(Bitmap srcBitmap, int destWidth,
			int destHeight)
	{
		int width = srcBitmap.getWidth();
		int height = srcBitmap.getHeight();

		float scaleWidth = ((float) destWidth) / width;
		float scaleHeight = ((float) destHeight) / height;

		float minScale = Math.min(scaleWidth, scaleHeight);
		Matrix matrix = new Matrix();
		matrix.postScale(minScale, minScale);
		Bitmap newBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, width, height,
				matrix, true);

		return new BitmapDrawable(newBitmap);
	}

	/**
	 * 创建图片
	 * 
	 * @param context
	 * @param bitAdress
	 * @return Bitmap
	 */
	public static Bitmap CreatImage(Context context, int bitAdress)
	{
		Bitmap bitmaptemp = null;
		bitmaptemp = BitmapFactory.decodeResource(context.getResources(),
				bitAdress);
		return bitmaptemp;
	}

	/**
	 * 除去相关字符
	 * 
	 * @param name
	 * @return name
	 */
	public static String cut(String name)
	{
		if (null != name && name.contains("\n"))
		{
			name = name.replaceAll("\n", "");
			name = name.trim();
		}
		if (null != name && name.contains("\r"))
		{
			name = name.replaceAll("\r", "");
			name = name.trim();
		}
		return name;
	}

	public static String getTime(String fomat)
	{
		SimpleDateFormat sDateFormat = new SimpleDateFormat(fomat);
		String date = sDateFormat.format(new java.util.Date());
		return date;
	}

	/**
	 * 功能：检测当前URL是否可连接或是否有效, 描述：最多连接网络 3 次, 如果 3 次都不成功，视为该地址不可用
	 * 
	 * @param urlStr
	 *            指定URL网络地址
	 * @return URL
	 */
	public static synchronized boolean isConnect(String urlStr,
			String userName, String password)
	{
		int counts = 0;
		boolean temp = false;
		if (urlStr == null || urlStr.length() <= 0)
		{
			return false;
		}
		while (counts < 2)
		{
			long start = 0;
			try
			{
				URL url = new URL(urlStr);
				HttpURLConnection httpConn = (HttpURLConnection) url
						.openConnection();
				byte[] encodedPassword = (userName + ":" + password).getBytes();
				httpConn.setRequestProperty("Authorization",
						"Basic " + Base64.encodeBytes(encodedPassword));
				httpConn.setAllowUserInteraction(true);
				httpConn.setConnectTimeout(CONNECT_TIMEOUT);
				httpConn.setReadTimeout(SOCKET_TIMEOUT);
				httpConn.setInstanceFollowRedirects(true);
				httpConn.setRequestMethod("GET");
				httpConn.setDoInput(true);
				httpConn.setDoOutput(true);
				httpConn.setRequestProperty("Connection", "Keep-Alive");
				httpConn.setRequestProperty("User-Agent",
						BROWSER_USERAGENT_DEFAULT);
				httpConn.setRequestProperty("Content-Type", "image/jpeg");
				int state = httpConn.getResponseCode();
				if (state == 200)
				{
					temp = true;
				}
				// Log.i("Authorization", "Basic :" +
				// Base64.encodeBytes(encodedPassword)+" state:"+state);
				break;

			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				counts++;
				Log.i("isConnect",
						"请求断开的URL一次需要：" + (System.currentTimeMillis() - start)
								+ "毫秒");
				Log.i("isConnect", "连接第 " + counts + " 次，" + urlStr + "--不可用");
				continue;

			}
		}
		return temp;
	}

	/**
	 * 功能：获取服务端IP地址和端口
	 * 
	 * @param urlStr
	 *            指定URL网络地址
	 * @return URL
	 */
	public static synchronized String getIP(String id)
	{
		int counts = 0;
		String temp = "";
		InputStream in = null;
		while (counts < 2)
		{
			try
			{
				String urls = IP_URL + id;
				URL url = new URL(urls);
				Log.i("ip url", "IP_URL :" + urls + " back:" + temp);
				HttpURLConnection httpConn = (HttpURLConnection) url
						.openConnection();
				httpConn.setAllowUserInteraction(true);
				httpConn.setConnectTimeout(CONNECT_TIMEOUT);
				httpConn.setReadTimeout(SOCKET_TIMEOUT);
				httpConn.setInstanceFollowRedirects(true);
				httpConn.setRequestMethod("GET");
				httpConn.setDoInput(true);
				httpConn.setDoOutput(true);
				httpConn.setRequestProperty("Connection", "Keep-Alive");
				httpConn.setRequestProperty("User-Agent",
						BROWSER_USERAGENT_DEFAULT);
				httpConn.setRequestProperty("Content-Type", "text/html");
				int state = httpConn.getResponseCode();
				if (state == 200)
				{
					in = httpConn.getInputStream();
					temp = inputStream2String(in);
					in.close();
				}

				break;

			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				try
				{
					in.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				continue;
			}
		}
		return temp;
	}

	public static String inputStream2String(InputStream in) throws IOException
	{
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[1096];
		for (int n; (n = in.read(b)) != -1;)
		{
			out.append(new String(b, 0, n));
		}
		return out.toString();
	}

	/**
	 * 二进制转十进制
	 * 
	 * @param cars
	 * @return
	 */
	public static int convertAlgorism(char[] cars)
	{
		int result = 0;
		int num = 0;
		for (int i = cars.length - 1; 0 <= i; i--)
		{
			int temp = 2;
			if (num == 0)
			{
				temp = 1;
			}
			else if (num == 1)
			{
				temp = 2;
			}
			else
			{
				for (int j = 1; j < num; j++)
				{
					temp = temp * 2;
				}
			}
			int sum = Integer.parseInt(String.valueOf(cars[i]));
			result = result + (sum * temp);
			num++;
		}

		return result;
	}

	/**
	 * 获取apk版本号
	 * 
	 * @param context
	 *            上下文
	 * @return
	 * @throws NameNotFoundException
	 */
	public static String getVersionName(Context context)
	{
		// 获取PackageManager 实例
		PackageManager packageManager = context.getPackageManager();
		// 获得context所属类的包名，0表示获取版本信息
		PackageInfo packageInfo = null;
		try
		{
			packageInfo = packageManager.getPackageInfo(
					context.getPackageName(), 0);
		}
		catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}
		return packageInfo.versionName;
	}

	/**
	 * 找寻数组中匹配的内容的下标值
	 * 
	 * @param text
	 * @param content
	 * @return
	 */
	public static int getSelected(String text, String[] content)
	{
		int index = 0;
		for (int i = 0; i < content.length; i++)
		{
			if (text.equals(content[i]))
			{
				index = i;
				break;
			}
		}
		// Log.i("dddd", "index:"+index);
		return index;
	}

	public static boolean isMobileNO(String mobiles)
	{
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	public static boolean isEmail(String email)
	{
		String str = "^([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)*@([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)+[\\.][A-Za-z]{2,3}([\\.][A-Za-z]{2})?$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);
		return m.matches();
	}
	
	public static boolean isIP(String ip)
	{
		String ipAndPort="((1?\\d{1,2}|2[1-5][1-5])\\.){3}(1?\\d{1,2}|2[1-5][1-5]):([1-5]?\\d{1,4}|6[1-5][1-5][1-3][1-5])"; 
		Pattern pattern = Pattern.compile(ipAndPort);

		Matcher matcher = pattern.matcher(ip);
		
		return matcher.matches();
	}
	
    public static int[] string2ASCII(String s) {// 字符串转换为ASCII码  
        if (s == null || "".equals(s)) {  
            return null;  
        }  
  
        char[] chars = s.toCharArray();  
        int[] asciiArray = new int[chars.length];  
  
        for (int i = 0; i < chars.length; i++) {  
            asciiArray[i] = char2ASCII(chars[i]);  
        }  
        return asciiArray;  
    } 
    
    public static int char2ASCII(char c) {  
        return (int) c;  
    }  

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		System.out.println(isIP("22.11.33.11:1234"));
	}

}
