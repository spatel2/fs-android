package com.freestyle.android.pojo;

import java.io.Serializable;
/**
 * 用户信息类
 * @author Jack
 *
 */
public class User implements Serializable
{
	private static final long serialVersionUID = 4914778171133360173L;
	private String id;
	private String imageUrl;
	private String userName;
	private String sex;
	private String age;
	private String height;
	private String weight;

	public User()
	{
		super();
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public String getAge()
	{
		return age;
	}

	public void setAge(String age)
	{
		this.age = age;
	}

	public String getHeight()
	{
		return height;
	}

	public void setHeight(String height)
	{
		this.height = height;
	}

	public String getWeight()
	{
		return weight;
	}

	public void setWeight(String weight)
	{
		this.weight = weight;
	}

	@Override
	public String toString()
	{
		return "User [id=" + id + ", imageUrl=" + imageUrl + ", userName="
				+ userName + ", sex=" + sex + ", age=" + age + ", height="
				+ height + ", weight=" + weight + "]";
	}
		
}