package com.freestyle.android.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.freestyle.android.R;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.GetAllData;
import com.freestyle.android.config.GetAllData.GetGlobal.GetRegion;
import com.freestyle.android.config.TextviewClass;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class Region_Activity extends BaseActivity implements OnClickListener,CoreFrame
{
	Context context;
	Bundle mBundle;
	String country_id = "";
	CommonClass mCommonClass = new CommonClass();
	PullToRefreshListView mListViewRegion;
	ImageView region_back_img;
	Intent mIntent;

	GetAllData mGetAllData;

	ArrayList<GetRegion> mArrayListRegion;
	RegionListAdpter mRegionListAdpter;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.region_activity);
		
		initDrawer(Region_Activity.this);
		
		context = getApplicationContext();
		mListViewRegion = (PullToRefreshListView)findViewById(R.id.region_listview);
		region_back_img = (ImageView)findViewById(R.id.region_back_img);
		
		region_back_img.setOnClickListener(this);
		
		country_id = getIntent().getStringExtra("area_id");
		mGetAllData = DemoApplication.mGetAllData;
		getAllData();
	}

	@Override
	public void onBackPressed() 
	{
		goBack("back");
	}

	@Override
	public void goBack(String target)
	{
		mIntent = new Intent(context, FindBeach_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target) 
	{
	}

	@Override
	public void setTypefaceControls() 
	{
	}

	@Override
	public void onClick(View v) 
	{
		if(v==region_back_img)
		{
			goBack("");
		}
	}


	public class RegionListAdpter extends BaseAdapter{

		ViewHolder mViewHolder;

		@Override
		public int getCount() {
			return mArrayListRegion.size();
		}

		@Override 
		public Object getItem(int position) {
			return mArrayListRegion.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null)
			{
				mViewHolder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.infl_location_lay, null);
				
				mViewHolder.mTextViewTiltle = (TextviewClass) convertView.findViewById(R.id.country_txt_single);
				mViewHolder.mLinearLayout = (LinearLayout) convertView.findViewById(R.id.data_layout);
				
				convertView.setTag(mViewHolder);
			}
			else 
			{
				mViewHolder = (ViewHolder) convertView.getTag();
			}

			mViewHolder.mTextViewTiltle.setText(mArrayListRegion.get(position).getName());

			mViewHolder.mLinearLayout.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{				
					Intent intent = new Intent(context, Region2_Activity.class);
					intent.putExtra("country_id", country_id);
					intent.putExtra("region_id", mArrayListRegion.get(position).getId().toString());										
					startActivity(intent);
					finish();

				}
			});

			return convertView;
		}
	}

	class ViewHolder
	{
		LinearLayout mLinearLayout;
		TextviewClass mTextViewTiltle;
	}

	public void getAllData()
	{
		if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
		{
			mArrayListRegion = new ArrayList<GetRegion>();
			for (int i = 0; i < mGetAllData.getData().size(); i++) 
			{
				if(mGetAllData.getData().get(i).getArea().getId().equalsIgnoreCase(country_id))
				{
					for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) {
						GetRegion mAreaData= new GetRegion();
						mAreaData.setId(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId());
						mAreaData.setName(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getName());
						mArrayListRegion.add(mAreaData);
					}
				}
			}
			sortRegion(mArrayListRegion);
			mRegionListAdpter = new RegionListAdpter();
			mListViewRegion.setAdapter(mRegionListAdpter);
		}
	}
	
	public void sortRegion(List<GetRegion> itemList) {
		if(itemList!=null)
		{
			Collections.sort(itemList, new Comparator<GetRegion>() {
				@Override
				public int compare(GetRegion o1, GetRegion o2) {
					return o1.name.compareTo(o2.name);
				}           
			});
		}
	}
}
