package com.freestyle.android.activity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.paoloconte.smoothchart.SmoothLineChart;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.freestyle.android.R;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.GetAllData;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.config.TextviewClass;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@SuppressLint({ "ShowToast", "SimpleDateFormat" })
public class Nearest_beaches_fragment extends BaseActivity implements OnClickListener, CoreFrame {
	String region_id;
	ProgressDialog mProgressDialog;
	String beach_id = "", region2_id = "", beach_lat = "", beach_long = "";
	String TAG = "Nearest Beach";
	String result_surf = "", beach_name = "", subtitle_txt = "";

	TextviewClass nearest_beach_name_txt, sub_title_txt, surf_height_txt,
			surf_sec, low_weather_temp, surf_weather_type,/* high_weather_temp, */
			low_water_temp,/* high_water_temp, */low_tide_txt,
			sunrise_time_txt, sunset_time_txt, wind_speed_txt;
	TextviewClass wind_direction_txt, surf_direction_txt, textViewtemp1,
			tomorrow_text;
	ImageView nearest_beach_close_icon, sethome_beach_pin_icon;
	ImageView mImageViewWeatherIcon, tide_imageview;

	RelativeLayout today_layout, forcast_layout;
	float watertemp_min = 0;
	float watertemp_max = 0;
	float weather_min = 0;
	float weather_max = 0;
	int wind_min = 0;
	int wind_max = 0;
	String tide_high = "";
	String tide_low = "";
	String sunrise = "", sunset = "";

	ArrayList<String> tide_graph_array = new ArrayList<String>();
	double data[];
	double mLastMinTide;
	private LineChart mChart;
	int mCurrentTimeIndex;
	private String WeatherType = "";

	public String getWeatherType() {
		return WeatherType;
	}

	String sunrise_time = "", sunset_time = "";
	Intent mIntent;
	String country_id;
	String sub_region_id;
	GetAllData mGetAllData;

	// Today variable
	boolean today = true;
	String surfjson = "";

	// Watch Code
	Surfline mSurfline;
	ImageView nearest_beach_map_pin;

	private int SwellTempDirection;
	private String SwellDirection;

	public String getSwellDirection() {
		return SwellDirection;
	};

	private int SwellTempPeriod;
	private String SwellPeriod = "";

	public String getSwellPeriod() {
		return SwellPeriod;
	}

	public ArrayList<String> mArrayListTime = new ArrayList<String>();
	public ArrayList<String> mArrayListTimeCurrent = new ArrayList<String>();
	public ArrayList<String> mArrayListHeight = new ArrayList<String>();
	public ArrayList<String> mArrayListTideType = new ArrayList<String>();

	// For prefrence of imperial and matric
	boolean isMetricTemp = false;

	String TideTimeZone = "";
	String RootTimeZone = "";
	ArrayList<Calendar> mArrayCalender = new ArrayList<Calendar>();
	ArrayList<Calendar> mArrayListCalendarArray = new ArrayList<Calendar>();

	String mStringIndexDate = "";

	int mIntSunpointCounter = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.nearest_beaches_fragment);

		context = Nearest_beaches_fragment.this;
		shared = new Shared_preference(context);

		initDrawer(Nearest_beaches_fragment.this);

		mGetAllData = DemoApplication.mGetAllData;

		Intent intent = getIntent();
		if (intent.getExtras() != null) {
			sub_region_id = getIntent().getStringExtra("subregion_id");
			region_id = getIntent().getStringExtra("region_id");
			country_id = getIntent().getStringExtra("country_id");
			beach_id = intent.getExtras().getString("beach_id");
		}

		nearest_beach_name_txt = (TextviewClass) findViewById(R.id.nearest_beach_name_txt);
		sub_title_txt = (TextviewClass) findViewById(R.id.sub_title_txt);
		surf_height_txt = (TextviewClass) findViewById(R.id.surf_height_txt);
		surf_sec = (TextviewClass) findViewById(R.id.surf_sec);
		low_weather_temp = (TextviewClass) findViewById(R.id.low_weather_temp);
		// high_weather_temp = (TextView)findViewById(R.id.high_weather_temp);
		surf_weather_type = (TextviewClass) findViewById(R.id.surf_weather_type);
		low_water_temp = (TextviewClass) findViewById(R.id.low_water_temp);
		// high_water_temp = (TextView)findViewById(R.id.high_water_temp);
		low_tide_txt = (TextviewClass) findViewById(R.id.low_tide_txt);
		sunrise_time_txt = (TextviewClass) findViewById(R.id.sunrise_time_txt);
		sunset_time_txt = (TextviewClass) findViewById(R.id.sunset_time_txt);
		wind_speed_txt = (TextviewClass) findViewById(R.id.wind_speed_txt);
		wind_direction_txt = (TextviewClass) findViewById(R.id.wind_direction_txt);
		surf_direction_txt = (TextviewClass) findViewById(R.id.surf_direction_txt);
		textViewtemp1 = (TextviewClass) findViewById(R.id.temp_txt1_surf);
		tomorrow_text = (TextviewClass) findViewById(R.id.tomorrow_text);
		today_layout = (RelativeLayout) findViewById(R.id.today_layout);
		forcast_layout = (RelativeLayout) findViewById(R.id.forcast_layout);

		mImageViewWeatherIcon = (ImageView) findViewById(R.id.surf_imageview_weather_icon);
		nearest_beach_close_icon = (ImageView) findViewById(R.id.nearest_beach_close_icon);
		sethome_beach_pin_icon = (ImageView) findViewById(R.id.sethome_beach_pin_icon);
		nearest_beach_map_pin = (ImageView) findViewById(R.id.nearest_beach_map_pin);
		tide_imageview = (ImageView) findViewById(R.id.tide_imageview);

		// For Type Conversion if Data in imperial or metric
		shared = new Shared_preference(context);

		if (shared.getPrefrencewithDefault(context, "unit_imperial_metric", "false").toString().equals("true")) {
			isMetricTemp = true;
		} else {
			isMetricTemp = false;
		}

		getAllData();
		region2_id = shared.getPrefrence(context, "region_id");

		nearest_beach_close_icon.setOnClickListener(this);
		today_layout.setOnClickListener(this);
		forcast_layout.setOnClickListener(this);
		nearest_beach_name_txt.setOnClickListener(this);
		nearest_beach_map_pin.setOnClickListener(this);
		sethome_beach_pin_icon.setOnClickListener(this);

		if (mCommonClass.CheckNetwork(context)) {
			NearestBeachDetails_Async mNearestBeachDetails_Async = new NearestBeachDetails_Async();
			mNearestBeachDetails_Async.execute();
		} else {
			Toast.makeText(context, getResources().getString(R.string.error_internet), 5).show();
		}
	}

	public void getAllData() {
		if (mGetAllData != null && mGetAllData.getData() != null && mGetAllData.getData().size() > 0) {
			for (int i = 0; i < mGetAllData.getData().size(); i++) {
				if (mGetAllData.getData().get(i).getArea().getId().equalsIgnoreCase(country_id)) {
					for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) {

						if (mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId().equalsIgnoreCase(region_id)) {
							for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) {

								if (mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId().equalsIgnoreCase(sub_region_id)) {
									for (int m = 0; m < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().size(); m++) {
										if (mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId().equalsIgnoreCase(beach_id)) {
											nearest_beach_name_txt.setText(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getTitle());
											sub_title_txt.setText(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getName());
											beach_name = nearest_beach_name_txt.getText().toString();
											subtitle_txt = sub_title_txt.getText().toString();

											// Get Coordinates
											beach_lat = mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLat();
											beach_long = mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLon();
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void showAlertWithTitle(Activity mActivity, String title, String message, String buttonMessage) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNegativeButton(buttonMessage, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.show();
	}

	@SuppressLint("DefaultLocale")
	public class NearestBeachDetails_Async extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			Log.i("Beach ID: ", beach_id + "");
			mProgressDialog = ProgressDialog.show(context, "Loading", "Please wait");
			mProgressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected synchronized String doInBackground(Void... params) {

			String strURL = "";
			if (isMetricTemp) {
				strURL = com.freestyle.android.config.Constants.URL.FORECAST_URL + beach_id + "?package=partner&units=m&user_key=abca50124d77d41504c4d53e29c8d359";
			} else {
				strURL = com.freestyle.android.config.Constants.URL.FORECAST_URL + beach_id + "?package=partner&user_key=abca50124d77d41504c4d53e29c8d359";
			}

			System.out.println("url =" + strURL);
			return mCommonClass.GetConnection(strURL);
		}

		@SuppressLint("SimpleDateFormat")
		@SuppressWarnings("unused")
		protected void onPostExecute(String result) {
			result_surf = result;

			try {
				try {
					JSONObject JsonObject = new JSONObject(result);
					Log.i(TAG, JsonObject.toString());

					// Location
					// ----------------------------------------------------------------------------------
					String Location = JsonObject.getString("name");
					int LocationLength = Location.length();

					// Sunrise Sunset
					// ----------------------------------------------------------------------------------
					RootTimeZone = JsonObject.getString("timezone");
					TideTimeZone = JsonObject.getJSONObject("Tide").getString("timezone");
					JSONArray TempSunPointArray = (JSONArray) JsonObject.getJSONObject("Tide").get("SunPoints");

					boolean gotSunrise = false;
					boolean gotSunset = false;
					long SunriseEpoch1 = 0;
					long SunsetEpoch1 = 0;

					if (today) {
						for (int i = 0; i < TempSunPointArray.length(); i++) {
							JSONObject TempObject = (JSONObject) TempSunPointArray.get(i);
							if (TempObject.getString("type").equals("Sunrise")) {
								gotSunrise = true;
								SunriseEpoch1 = Long.parseLong(TempObject.getString("time"));
							} else {
								gotSunset = true;
								SunsetEpoch1 = Long.parseLong(TempObject.getString("time"));
							}

							if (gotSunrise && gotSunset)
								break;
						}
					} else {
						for (int i = 0; i < TempSunPointArray.length(); i++) {
							if (i >= 2) {
								JSONObject TempObject = (JSONObject) TempSunPointArray.get(i);

								if (TempObject.getString("type").equals("Sunrise")) {
									gotSunrise = true;
									SunriseEpoch1 = Long.parseLong(TempObject.getString("time"));

								} else {
									gotSunset = true;
									SunsetEpoch1 = Long.parseLong(TempObject.getString("time"));

								}
								if (gotSunrise && gotSunset)
									break;
							}
						}
					}

					if (gotSunrise) {
						Date mSunRiseDate = new Date(SunriseEpoch1 * 1000L);

						String mStringSunRise = UTCTime(mSunRiseDate);

						Calendar mCalendarSunRise = Calendar.getInstance();
						DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
						mCalendarSunRise.setTime(converter.parse(mStringSunRise));
						SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");

						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								mCalendarSunRise.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								mCalendarSunRise.add(Calendar.MINUTE, 30);
							} else {
								mCalendarSunRise.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
							}
						}
						sunrise = String.format(dateFormat.format(mCalendarSunRise.getTime()));
					} else {
						sunrise = "00:00";
					}

					if (gotSunset) {
						Date mSunSetDate = new Date(SunsetEpoch1 * 1000L);

						String mStringSunSet = UTCTime(mSunSetDate);

						Calendar mCalendarSunset = Calendar.getInstance();
						DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
						mCalendarSunset.setTime(converter.parse(mStringSunSet));
						SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
						if (TideTimeZone.contains(".")) {
							mCalendarSunset.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
							mCalendarSunset.add(Calendar.MINUTE, 30);
						} else {
							mCalendarSunset.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
						}

						sunset = String.format(dateFormat.format(mCalendarSunset.getTime()));
					} else {
						sunset = "00:00";
					}

					sunrise_time_txt.setText(sunrise + "");
					sunset_time_txt.setText(sunset + "");

					// Air Temperature
					// ----------------------------------------------------------------------------------
					JSONArray TempAirMinArray = (JSONArray) JsonObject.getJSONObject("Weather").get("temp_min");
					if (today) {
						weather_min = (int) Math.round(Double.parseDouble(TempAirMinArray.getString(0)));
					} else {
						weather_min = (int) Math.round(Double.parseDouble(TempAirMinArray.getString(1)));
					}

					JSONArray TempAirMaxArray = (JSONArray) JsonObject.getJSONObject("Weather").get("temp_max");
					if (today) {
						weather_max = (int) Math.round(Double.parseDouble(TempAirMaxArray.getString(0)));
					} else {
						weather_max = (int) Math.round(Double.parseDouble(TempAirMaxArray.getString(1)));
					}

					DecimalFormat mDecimalFormatTwoDigit;
					mDecimalFormatTwoDigit = new DecimalFormat("0", new DecimalFormatSymbols(Locale.ENGLISH));

					if (isMetricTemp) {
						low_weather_temp.setText(mDecimalFormatTwoDigit.format(weather_min) + " - " + mDecimalFormatTwoDigit.format(weather_max) + (char) 0x00B0 + "c");
					} else {
						low_weather_temp.setText(mDecimalFormatTwoDigit.format(weather_min) + " - " + mDecimalFormatTwoDigit.format(weather_max) + (char) 0x00B0 + "f");
					}

					JSONArray TempWeatherArray = (JSONArray) JsonObject.getJSONObject("Weather").get("weather_type");

					if (today) {
						WeatherType = TempWeatherArray.getString(0);
					} else {
						WeatherType = TempWeatherArray.getString(1);
					}

					if (WeatherType.equals("clear")) {
						WeatherType = "clear";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					} else if (WeatherType.equals("mostly sunny")) {
						WeatherType = "mostly\nsunny";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					} else if (WeatherType.equals("sunny")) {
						WeatherType = "sunny";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					} else if (WeatherType.equals("partly sunny")) {
						WeatherType = "partly\nsunny";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.partly_cloudy_day);
					} else if (WeatherType.equals("cloudy")) {
						WeatherType = "cloudy";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("mostly cloudy")) {
						WeatherType = "mostly\ncloudy";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.overcast);
					} else if (WeatherType.equals("overcast")) {
						WeatherType = "overcast";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.overcast);
					} else if (WeatherType.equals("partly cloudy")) {
						WeatherType = "partly\ncloudy";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.partly_cloudy_day);
					} else if (WeatherType.equals("rain")) {
						WeatherType = "rain";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("heavy rain")) {
						WeatherType = "heavy\nrain";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("scattered showers")) {
						WeatherType = "scattered\nshowers";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("sleet")) {
						WeatherType = "sleet";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.snow);
					} else if (WeatherType.equals("snow")) {
						WeatherType = "snow";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.snow);
					} else if (WeatherType.equals("flurries")) {
						WeatherType = "flurries";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.snow);
					} else if (WeatherType.equals("fog")) {
						WeatherType = "fog";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.fog);
					} else if (WeatherType.equals("hazy")) {
						WeatherType = "hazy";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.fog);
					} else if (WeatherType.equals("scattered showers possible t-storms")) {
						WeatherType = "scattered showers\npossible t-storms";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("heavy rain possible t-storms")) {
						WeatherType = "heavy rain\npossible t-storms";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("scattered showers t-storms")) {
						WeatherType = "scattered showers\nt-storms";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("possible t-storms")) {
						WeatherType = "possible\nt-storms";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("thunderstorms")) {
						WeatherType = "thunderstorms";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else {
						WeatherType = "clear";
						surf_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					}

					// Water Temperature
					// ----------------------------------------------------------------------------------
					watertemp_min = (int) Math.round(Double.parseDouble(JsonObject.getJSONObject("WaterTemp").getString("watertemp_min")));
					watertemp_max = (int) Math.round(Double.parseDouble(JsonObject.getJSONObject("WaterTemp").getString("watertemp_max")));

					if (isMetricTemp) {
						low_water_temp.setText(mDecimalFormatTwoDigit.format(watertemp_min) + (char) 0x00B0 + "c");
					} else {
						low_water_temp.setText(mDecimalFormatTwoDigit.format(watertemp_min) + (char) 0x00B0 + "f");
					}

					// Tide Info
					// ----------------------------------------------------------------------------------
					// chart Data
					JSONArray TempTidePointArray = (JSONArray) JsonObject.getJSONObject("Tide").get("dataPoints");
					TideTimeZone = JsonObject.getJSONObject("Tide").getString("timezone");

					tide_graph_array = new ArrayList<String>();
					tide_graph_array.clear();
					mArrayListTideType = new ArrayList<String>();
					mArrayListTideType.clear();
					mArrayListCalendarArray = new ArrayList<Calendar>();
					mArrayListCalendarArray.clear();

					mIntSunpointCounter = 0;
					for (int i = 0; i < TempTidePointArray.length(); i++) {
						JSONObject TempObject = (JSONObject) TempTidePointArray.get(i);

						Calendar c1 = Calendar.getInstance();

						DateFormat df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
						c1.setTime(df.parse(TempObject.getString("utctime") + ""));
						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								c1.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								c1.add(Calendar.MINUTE, 30);
							} else {
								c1.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
							}
						}
						Calendar c2 = Calendar.getInstance();

						boolean isSameDay = isSameDay(c1, c2);

						Calendar c3 = Calendar.getInstance();
						c3.add(Calendar.DAY_OF_YEAR, 1);

						String mString = TempObject.getString("type");

						if (today) {
							if (isSameDay) {
								if (mString.equalsIgnoreCase("Sunrise") || mString.equalsIgnoreCase("Sunset")) {
									mIntSunpointCounter++;
								} else {
									double value1 = TempObject.getDouble("height");
									value1 = Double.parseDouble(new DecimalFormat("#.#").format(value1));

									tide_graph_array.add(String.valueOf(value1));

									String type = TempObject.getString("type");
									mArrayListTideType.add(type);

									Calendar dateObj = Calendar.getInstance();
									DateFormat sdfAM = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
									dateObj.setTime(sdfAM.parse(TempObject.getString("utctime") + ""));

									if (!TideTimeZone.equalsIgnoreCase("")) {
										if (TideTimeZone.contains(".")) {
											dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
											dateObj.add(Calendar.MINUTE, 30);
										} else {
											dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
										}
									}

									// Calendar update
									mArrayCalender.add(dateObj);
									mArrayListCalendarArray.add(dateObj);

									SimpleDateFormat df1 = new SimpleDateFormat("hh:mm a");
									String currentTime = df1.format(dateObj.getTime());

									mArrayListTime.add(currentTime);

									double value = TempObject.getDouble("height");
									value = Double.parseDouble(new DecimalFormat("#.#").format(value));
									if (isMetricTemp) {
										mArrayListHeight.add(String.valueOf(value) + " m");
									} else {
										mArrayListHeight.add(String.valueOf(value) + " ft");
									}
								}
							}
						} else {
							if (isSameDay(c1, c3)) {
								if (mString.equalsIgnoreCase("Sunrise") || mString.equalsIgnoreCase("Sunset")) {
									mIntSunpointCounter++;
								} else {
									double value1 = TempObject.getDouble("height");
									value1 = Double.parseDouble(new DecimalFormat("#.#").format(value1));

									tide_graph_array.add(String.valueOf(value1));

									String type = TempObject.getString("type");
									mArrayListTideType.add(type);

									Calendar dateObj = Calendar.getInstance();
									DateFormat sdfAM = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
									dateObj.setTime(sdfAM.parse(TempObject.getString("utctime") + ""));
									if (!TideTimeZone.equalsIgnoreCase("")) {
										if (TideTimeZone.contains(".")) {
											dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
											dateObj.add(Calendar.MINUTE, 30);
										} else {
											dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
										}
									}

									// Calender update
									mArrayCalender.add(dateObj);
									mArrayListCalendarArray.add(dateObj);

									SimpleDateFormat df1 = new SimpleDateFormat("hh:mm a");
									String currentTime = df1.format(dateObj.getTime());

									mArrayListTime.add(currentTime);

									double value = TempObject.getDouble("height");
									value = Double.parseDouble(new DecimalFormat("#.#").format(value));

									if (isMetricTemp) {
										mArrayListHeight.add(String.valueOf(value) + " m");
									} else {
										mArrayListHeight.add(String.valueOf(value) + " ft");
									}
								}
							}
						}
					}

					float Tidemax = 0, Tidemin = 0;
					Calendar mCalendarCurrentGMTTime = Calendar.getInstance();
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
					mCalendarCurrentGMTTime.setTime(df.parse(CurrentGMTTime()));

					if (today) {
						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								mCalendarCurrentGMTTime.add(Calendar.MINUTE, 30);
							} else {
								mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
							}
						}
					} else {
						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, 24);
								mCalendarCurrentGMTTime.add(Calendar.MINUTE, 30);
							} else {
								mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
								mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, 24);
							}
						}
					}
					String TidePointTime = "";
					for (int i = 0; i < mArrayListCalendarArray.size(); i++) {
						if (mCalendarCurrentGMTTime.before(mArrayListCalendarArray.get(i)) || mCalendarCurrentGMTTime.equals(mArrayListCalendarArray.get(i))) {
							Tidemin = (float) Double.parseDouble(tide_graph_array.get(i));
							// Calendar mcal = Calendar.getInstance();
							// SimpleDateFormat sdf1 = new
							// SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
							// String currentPointTime =
							// sdf1.format(mArrayListCalendarArray.get(i).getTime());
							SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
							TidePointTime = sdf1.format(mArrayListCalendarArray.get(i).getTime());

							// textViewtemp2.setText("INDEX-"+(i+mIntSunpointCounter)+"/TIDE- "+currentPointTime);
							if (mArrayListTideType.get(i).equalsIgnoreCase("high")) {
								// up arrowmcal
								tide_imageview.setVisibility(View.VISIBLE);
								tide_imageview.setImageResource(R.drawable.arrow_up);
							} else if (mArrayListTideType.get(i).equalsIgnoreCase("low")) {
								// down arrow
								tide_imageview.setVisibility(View.VISIBLE);
								tide_imageview.setImageResource(R.drawable.arrow_down);
							} else {
								// nothing draw
								tide_imageview.setVisibility(View.GONE);
							}
							break;
						} else {
							Tidemin = 0;
						}
					}
					Tidemin = Float.parseFloat(convertToSingalDecimal(String.valueOf(Tidemin)));

					if (isMetricTemp) {
						low_tide_txt.setText(Tidemin + " m");
						// textViewtemp1.setText("CURRENT TIDE : "+Tidemin +
						// " m " + TidePointTime.toUpperCase());
					} else {
						low_tide_txt.setText(Tidemin + " ft");
						// textViewtemp1.setText("CURRENT TIDE : "+Tidemin +
						// " ft " + TidePointTime.toUpperCase());
					}

					// if(today)
					// {
					// textViewtemp1.setVisibility(View.VISIBLE);
					// }
					// else
					// {
					// textViewtemp1.setVisibility(View.GONE);
					// }

					float max1 = 0, min1 = 1000, max2 = 0, min2 = 1000;
					int index1 = 0, index2 = 0, index3 = 0, index4 = 0;
					if (today) {
						for (int i = 2; i < tide_graph_array.size(); i++) {
							Calendar mCalendarTemp = mArrayCalender.get(i);
							SimpleDateFormat sdfhour = new SimpleDateFormat("HH");
							int Hour = Integer.parseInt(sdfhour.format(mCalendarTemp.getTime()));
							if (Hour < 12) {
								float a = (float) Float.parseFloat(tide_graph_array.get(i));
								if (a > max1) {
									max1 = a;
									index1 = i;
								}
								float b = (float) Float.parseFloat(tide_graph_array.get(i));
								if (b <= min1) {
									min1 = b;
									index3 = i;
								}
							}
						}

						for (int i = 0; i < tide_graph_array.size() - 2; i++) {
							Calendar mCalendarTemp = mArrayCalender.get(i);
							SimpleDateFormat sdfhour = new SimpleDateFormat("HH");
							int Hour = Integer.parseInt(sdfhour.format(mCalendarTemp.getTime()));
							if (Hour >= 12) {
								float a = (float) Float.parseFloat(tide_graph_array.get(i));
								if (a >= max2) {
									max2 = a;
									index2 = i;
								}
								float b = (float) Float.parseFloat(tide_graph_array.get(i));
								if (b < min2) {
									min2 = b;
									index4 = i;
								}
							}
						}

						// for (int i = 0; i < mArrayListTime.size(); i++) {
						// if (i != index1 && i != index2 && i != index3 && i !=
						// index4) {
						// mArrayListTime.set(i, "");
						// }
						// }
						//
						// for (int i = 0; i < mArrayListHeight.size(); i++) {
						// if (i != index1 && i != index2 && i != index3 && i !=
						// index4) {
						// mArrayListHeight.set(i, "");
						// }
						// }
					} else {
						for (int i = 2; i < tide_graph_array.size(); i++) {
							Calendar mCalendarTemp = mArrayCalender.get(i);
							SimpleDateFormat sdfhour = new SimpleDateFormat("HH");
							int Hour = Integer.parseInt(sdfhour.format(mCalendarTemp.getTime()));
							if (Hour < 12) {
								float a = (float) Float.parseFloat(tide_graph_array.get(i));
								if (a > max1) {
									max1 = a;
									index1 = i;
								}
								float b = (float) Float.parseFloat(tide_graph_array.get(i));
								if (b <= min1) {
									min1 = b;
									index3 = i;
								}
							}
						}

						for (int i = 0; i < tide_graph_array.size() - 2; i++) {
							Calendar mCalendarTemp = mArrayCalender.get(i);
							SimpleDateFormat sdfhour = new SimpleDateFormat("HH");
							int Hour = Integer.parseInt(sdfhour.format(mCalendarTemp.getTime()));
							if (Hour >= 12) {
								float a = (float) Float.parseFloat(tide_graph_array.get(i));
								if (a >= max2) {
									max2 = a;
									index2 = i;
								}
								float b = (float) Float.parseFloat(tide_graph_array.get(i));
								if (b < min2) {
									min2 = b;
									index4 = i;
								}
							}
						}

						// for (int i = 0; i < mArrayListTime.size(); i++) {
						// if (i != index1 && i != index2 && i != index3 && i !=
						// index4) {
						// mArrayListTime.set(i, "");
						// }
						// }
						//
						// for (int i = 0; i < mArrayListHeight.size(); i++) {
						// if (i != index1 && i != index2 && i != index3 && i !=
						// index4) {
						// mArrayListHeight.set(i, "");
						// }
						// }

					}

					// Surf Info
					// ----------------------------------------------------------------------------------
					float SurfTempMin = 0, SurfTempMax = 0;
					String SurfSize;
					JSONArray TempSwellSizeArray;
					JSONArray SurfPeriodSchedule;

					String mStringSurfLocalDate = JsonObject.getJSONObject("Wind").getString("startDate_pretty_LOCAL");
					Calendar CalendarSurfStartDateGMT = Calendar.getInstance();
					SimpleDateFormat sdf1 = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
					CalendarSurfStartDateGMT.setTime(sdf1.parse(mStringSurfLocalDate));

					SimpleDateFormat df1 = new SimpleDateFormat("HHmm");
					String surfGMTtime = df1.format(CalendarSurfStartDateGMT.getTime());

					int mIntsurfGMTTime = Integer.parseInt(surfGMTtime);

					int SurfArrayValue = 0;

					if (mIntsurfGMTTime < 0700) {
						SurfArrayValue = 0;

					} else if (mIntsurfGMTTime >= 0700 && mIntsurfGMTTime < 1400) {
						SurfArrayValue = 1;
					} else if (mIntsurfGMTTime >= 1400 && mIntsurfGMTTime < 1900) {
						SurfArrayValue = 2;
					} else if (mIntsurfGMTTime >= 1900) {
						SurfArrayValue = 3;
					}

					// swell height
					TempSwellSizeArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_height1");
					String mStringsurfHeight = "";
					if (TempSwellSizeArray.length() > 0) {
						JSONArray TempSwellSizeObject;
						if (today) {
							TempSwellSizeObject = (JSONArray) TempSwellSizeArray.get(0);
						} else {
							TempSwellSizeObject = (JSONArray) TempSwellSizeArray.get(1);
						}

						float surfHeight = (float) TempSwellSizeObject.getDouble(SurfArrayValue);
						surfHeight = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(surfHeight)));
						if (isMetricTemp) {
							mStringsurfHeight = (surfHeight) + " m";
						} else {
							mStringsurfHeight = (surfHeight) + " ft";
						}
					}

					// Surf Min
					JSONArray TempSurfMin = (JSONArray) JsonObject.getJSONObject("Surf").get("surf_min");

					if (TempSurfMin.length() > 0) {

						JSONArray TempSurfMinObject;
						if (today) {
							TempSurfMinObject = (JSONArray) TempSurfMin.get(0);
						} else {
							TempSurfMinObject = (JSONArray) TempSurfMin.get(1);
						}

						SurfTempMin = (float) TempSurfMinObject.getDouble(SurfArrayValue);

					}

					// Surf Max
					JSONArray TempSurfMax = (JSONArray) JsonObject.getJSONObject("Surf").get("surf_max");

					if (TempSurfMax.length() > 0) {

						JSONArray TempSurfMaxObject;
						if (today) {
							TempSurfMaxObject = (JSONArray) TempSurfMax.get(0);
						} else {
							TempSurfMaxObject = (JSONArray) TempSurfMax.get(1);
						}
						SurfTempMax = (float) TempSurfMaxObject.getDouble(SurfArrayValue);
					}

					if (isMetricTemp) {
						SurfTempMin = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMin)));
						SurfTempMax = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMax)));
						SurfSize = (SurfTempMin) + "-" + (SurfTempMax) + " m";
					} else {
						SurfTempMin = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMin)));
						SurfTempMax = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMax)));
						SurfSize = (SurfTempMin) + "-" + (SurfTempMax) + " ft";
					}

					surf_height_txt.setText(SurfSize);

					// Swell Direction
					// ----------------------------------------------------------------------------------
					JSONArray TempSwellDirectionArray;
					TempSwellDirectionArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_direction1");

					if (TempSwellDirectionArray.length() > 0) {
						JSONArray TempSwellDirectionObject;
						if (today) {
							TempSwellDirectionObject = (JSONArray) TempSwellDirectionArray.get(0);
						} else {
							TempSwellDirectionObject = (JSONArray) TempSwellDirectionArray.get(1);
						}
						SwellTempDirection = (int) TempSwellDirectionObject.getDouble(SurfArrayValue);

						if (SwellTempDirection <= 10)
							SwellDirection = "N";
						else if (SwellTempDirection > 10 && SwellTempDirection <= 30)
							SwellDirection = "NNE";
						else if (SwellTempDirection > 30 && SwellTempDirection <= 50)
							SwellDirection = "NE";
						else if (SwellTempDirection > 50 && SwellTempDirection <= 80)
							SwellDirection = "ENE";
						else if (SwellTempDirection > 80 && SwellTempDirection <= 100)
							SwellDirection = "E";
						else if (SwellTempDirection > 100 && SwellTempDirection <= 130)
							SwellDirection = "ESE";
						else if (SwellTempDirection > 130 && SwellTempDirection <= 150)
							SwellDirection = "SE";
						else if (SwellTempDirection > 150 && SwellTempDirection <= 170)
							SwellDirection = "SSE";
						else if (SwellTempDirection > 170 && SwellTempDirection <= 190)
							SwellDirection = "S";
						else if (SwellTempDirection > 190 && SwellTempDirection <= 210)
							SwellDirection = "SSW";
						else if (SwellTempDirection > 210 && SwellTempDirection <= 230)
							SwellDirection = "SW";
						else if (SwellTempDirection > 230 && SwellTempDirection <= 260)
							SwellDirection = "WSW";
						else if (SwellTempDirection > 260 && SwellTempDirection <= 280)
							SwellDirection = "W";
						else if (SwellTempDirection > 280 && SwellTempDirection <= 310)
							SwellDirection = "WNW";
						else if (SwellTempDirection > 310 && SwellTempDirection <= 330)
							SwellDirection = "NW";
						else if (SwellTempDirection > 330 && SwellTempDirection <= 350)
							SwellDirection = "NNW";
						else if (SwellTempDirection > 350)
							SwellDirection = "N";
					} else {
						SwellTempDirection = 0;
						SwellDirection = "N/A";
					}

					// Swell Period
					// ----------------------------------------------------------------------------------

					JSONArray TempSwellPeriodArray;
					TempSwellPeriodArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_period1");
					if (TempSwellPeriodArray.length() > 0) {
						JSONArray TempSwellPeriodObject;
						if (today) {
							TempSwellPeriodObject = (JSONArray) TempSwellPeriodArray.get(0);
						} else {
							TempSwellPeriodObject = (JSONArray) TempSwellPeriodArray.get(1);
						}
						SwellTempPeriod = (int) (TempSwellPeriodObject.getDouble(SurfArrayValue));
					} else {
						SwellTempPeriod = 0;
					}

					SwellPeriod = String.format("%d" + "s", SwellTempPeriod);

					surf_sec.setText(mStringsurfHeight + " @ " + SwellPeriod + " " + SwellDirection);
					surf_direction_txt.setText(SwellDirection);

				} catch (Exception e) {
					e.printStackTrace();
				}

				JSONObject mJsonObject = new JSONObject(result);

				// Wind Speed

				int max = 0, min = 1000;

				String mStringLocalDate = mJsonObject.getJSONObject("Wind").getString("startDate_pretty_LOCAL");

				Calendar CalendarWindStartDateGMT = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
				CalendarWindStartDateGMT.setTime(sdf.parse(mStringLocalDate));

				SimpleDateFormat df1 = new SimpleDateFormat("HHmm");
				String WindGMTtime = df1.format(CalendarWindStartDateGMT.getTime());

				int mIntWindGMTTime = Integer.parseInt(WindGMTtime);

				int WindArrayValue = 0;

				if (mIntWindGMTTime < 0200) {
					WindArrayValue = 0;
				} else if (mIntWindGMTTime >= 0200 && mIntWindGMTTime < 0500) {
					WindArrayValue = 1;
				} else if (mIntWindGMTTime >= 0500 && mIntWindGMTTime < Integer.parseInt("0800")) {
					WindArrayValue = 2;
				} else if (mIntWindGMTTime >= Integer.parseInt("0800") && mIntWindGMTTime < 1200) {
					WindArrayValue = 3;
				} else if (mIntWindGMTTime >= 1200 && mIntWindGMTTime < 1500) {
					WindArrayValue = 4;
				} else if (mIntWindGMTTime >= 1500 && mIntWindGMTTime < 1800) {
					WindArrayValue = 5;
				} else if (mIntWindGMTTime >= 1800 && mIntWindGMTTime < 2100) {
					WindArrayValue = 6;
				} else if (mIntWindGMTTime >= 2100) {
					WindArrayValue = 7;
				}

				String wind_speed_str = mJsonObject.getString("Wind");
				JSONObject wind_data_obj = new JSONObject(wind_speed_str);
				JSONArray wind_arr = wind_data_obj.getJSONArray("wind_speed");

				JSONArray wind_speed;
				if (today) {
					wind_speed = new JSONArray(wind_arr.getString(0));
				} else {
					wind_speed = new JSONArray(wind_arr.getString(1));
				}

				min = (int) Math.round(Double.parseDouble(wind_speed.getString(WindArrayValue)));
				max = (int) Math.round(Double.parseDouble(wind_speed.getString(WindArrayValue)) * 1.3);

				wind_min = min;
				wind_max = max;

				if (isMetricTemp) {
					wind_speed_txt.setText(wind_min + "-" + wind_max + " km/h");
				} else {
					wind_speed_txt.setText(wind_min + "-" + wind_max + " kts");
				}

				// Wind Direction
				// ----------------------------------------------------------------------------------

				JSONArray TempWindDirectionArray = (JSONArray) mJsonObject.getJSONObject("Wind").get("wind_direction");
				String WindDirection = "";
				int WindTempDirection = 0;
				if (TempWindDirectionArray.length() > 0) {
					JSONArray TempWindDirectionObject;
					if (today) {
						TempWindDirectionObject = (JSONArray) TempWindDirectionArray.get(0);// Note:
						// Also
						// an
						// Array
					} else {
						TempWindDirectionObject = (JSONArray) TempWindDirectionArray.get(1);// Note:
						// Also
						// an
						// Array
					}

					WindTempDirection = (int) TempWindDirectionObject.getDouble(WindArrayValue);

					if (WindTempDirection <= 10)
						WindDirection = "N";
					else if (WindTempDirection > 10 && WindTempDirection <= 30)
						WindDirection = "NNE";
					else if (WindTempDirection > 30 && WindTempDirection <= 50)
						WindDirection = "NE";
					else if (WindTempDirection > 50 && WindTempDirection <= 80)
						WindDirection = "ENE";
					else if (WindTempDirection > 80 && WindTempDirection <= 100)
						WindDirection = "E";
					else if (WindTempDirection > 100 && WindTempDirection <= 130)
						WindDirection = "ESE";
					else if (WindTempDirection > 130 && WindTempDirection <= 150)
						WindDirection = "SE";
					else if (WindTempDirection > 150 && WindTempDirection <= 170)
						WindDirection = "SSE";
					else if (WindTempDirection > 170 && WindTempDirection <= 190)
						WindDirection = "S";
					else if (WindTempDirection > 190 && WindTempDirection <= 210)
						WindDirection = "SSW";
					else if (WindTempDirection > 210 && WindTempDirection <= 230)
						WindDirection = "SW";
					else if (WindTempDirection > 230 && WindTempDirection <= 260)
						WindDirection = "WSW";
					else if (WindTempDirection > 260 && WindTempDirection <= 280)
						WindDirection = "W";
					else if (WindTempDirection > 280 && WindTempDirection <= 310)
						WindDirection = "WNW";
					else if (WindTempDirection > 310 && WindTempDirection <= 330)
						WindDirection = "NW";
					else if (WindTempDirection > 330 && WindTempDirection <= 350)
						WindDirection = "NNW";
					else if (WindTempDirection > 350)
						WindDirection = "N";
				} else {
					WindTempDirection = 0;
					WindDirection = "N/A";
				}
				wind_direction_txt.setText(WindDirection);

				// Code for Graph
				try {
					// Add current date data
					Date mDate = new Date();
					String utc_time_str = UTCTime(mDate);
					Calendar mCalendar = Calendar.getInstance();

					DateFormat df = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
					mCalendar.setTime(df.parse(utc_time_str));

					if (today) {
						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								mCalendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								mCalendar.add(Calendar.MINUTE, 30);
							} else {
								mCalendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
							}
						}
					} else {
						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								mCalendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								mCalendar.add(Calendar.HOUR_OF_DAY, 24);
								mCalendar.add(Calendar.MINUTE, 30);
							} else {
								mCalendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
								mCalendar.add(Calendar.HOUR_OF_DAY, 24);
							}
						}
					}

					mArrayListTimeCurrent = new ArrayList<String>();
					mArrayListTimeCurrent.addAll(mArrayListTime);

					System.out.println("-- mcalendar =" + mCalendar.getTime());

					for (int i = 0; i < mArrayListTime.size() - 1; i++) {
						// System.out.println("-- Calendar array object =" +
						// mArrayCalender.get(i).getTime() + " ,  " +
						// mArrayCalender.get(i + 1).getTime());
						if (mCalendar.after(mArrayCalender.get(i)) && mCalendar.before(mArrayCalender.get(i + 1))) {
							Log.e("get current point1", mArrayCalender.get(i).getTime().toString());
							if (today) {
								mArrayListTimeCurrent.set(i, "current");
								// mCurrentTimeIndex = i;
								Log.e("get current point2", mArrayCalender.get(i).getTime().toString());
							}
							break;
						}
						if (today) {
							if (i == mArrayListTime.size() - 2) {
								mArrayListTimeCurrent.set(i, "current");
							}
						}
					}

					if (today) {
						Calendar mcal = Calendar.getInstance();
						SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
						tomorrow_text.setText("UPDATED AT " + sdf1.format(mCalendar.getTime()));
						tomorrow_text.setVisibility(View.VISIBLE);
					} else {
						tomorrow_text.setText("TOMORROW");
						tomorrow_text.setVisibility(View.VISIBLE);
					}

					// Getting smallest point
					// double temp_graph_arr = 0.0;
					// for (int i = 0; i < tide_graph_array.size(); i++) {
					// if
					// (Double.parseDouble(tide_graph_array.get(i).toString().trim())
					// < temp_graph_arr) {
					// temp_graph_arr =
					// Double.parseDouble(tide_graph_array.get(i).toString().trim());
					// }
					// }
					//
					// if (temp_graph_arr < 0) {
					// temp_graph_arr = temp_graph_arr * (-3);
					// temp_graph_arr = temp_graph_arr + temp_graph_arr +
					// temp_graph_arr + temp_graph_arr;
					// } else if (temp_graph_arr == 0) {
					// temp_graph_arr = temp_graph_arr + 3;
					// } else if (temp_graph_arr <= 0.6) {
					// temp_graph_arr = temp_graph_arr + 3;
					// }

					// Add smallest point to everyone
					// for (int i = 0; i < tide_graph_array.size(); i++) {
					// tide_graph_array.set(i,
					// (Double.parseDouble(tide_graph_array.get(i).toString().trim())
					// + temp_graph_arr) + "");
					// }

					SmoothLineChart chart = (SmoothLineChart) findViewById(R.id.smoothChart);

					// ArrayList<String> mArrayListTemp1 = new
					// ArrayList<String>();
					// ArrayList<String> mArrayListTemp2 = new
					// ArrayList<String>();
					// ArrayList<String> mArrayListTemp3 = new
					// ArrayList<String>();
					// ArrayList<String> mArrayListTemp4 = new
					// ArrayList<String>();
					//
					// ArrayList<String> mArrayListTemp11 = new
					// ArrayList<String>();
					// ArrayList<String> mArrayListTemp22 = new
					// ArrayList<String>();
					// ArrayList<String> mArrayListTemp33 = new
					// ArrayList<String>();
					// ArrayList<String> mArrayListTemp44 = new
					// ArrayList<String>();
					//
					// mArrayListTemp1.addAll(tide_graph_array);
					// mArrayListTemp2.addAll(mArrayListTime);
					// mArrayListTemp3.addAll(mArrayListHeight);
					// mArrayListTemp4.addAll(mArrayListTimeCurrent);
					//
					// ArrayList<String> mtemp =new ArrayList<String>();
					// for (int i = 0; i < mArrayListTemp4.size(); i++)
					// {
					// if(!mArrayListTemp4.get(i).equalsIgnoreCase(""))
					// {
					// mtemp.add(""+i);
					// }
					// }
					//
					// for (int i = 0; i < mArrayListTemp1.size(); i++)
					// {
					// if(i == Integer.parseInt(mtemp.get(0)) || i ==
					// Integer.parseInt(mtemp.get(1)) || i ==
					// Integer.parseInt(mtemp.get(2)) || i ==
					// Integer.parseInt(mtemp.get(3)) || i ==
					// Integer.parseInt(mtemp.get(4)))
					// {
					// mArrayListTemp11.add(mArrayListTemp1.get(i));
					// }
					// }
					// for (int i = 0; i < mArrayListTemp2.size(); i++)
					// {
					// if(i == Integer.parseInt(mtemp.get(0)) || i ==
					// Integer.parseInt(mtemp.get(1)) || i ==
					// Integer.parseInt(mtemp.get(2)) || i ==
					// Integer.parseInt(mtemp.get(3)) || i ==
					// Integer.parseInt(mtemp.get(4)))
					// {
					// mArrayListTemp22.add(mArrayListTemp2.get(i));
					// }
					// }
					// for (int i = 0; i < mArrayListTemp3.size(); i++)
					// {
					// if(i == Integer.parseInt(mtemp.get(0)) || i ==
					// Integer.parseInt(mtemp.get(1)) || i ==
					// Integer.parseInt(mtemp.get(2)) || i ==
					// Integer.parseInt(mtemp.get(3)) || i ==
					// Integer.parseInt(mtemp.get(4)))
					// {
					// mArrayListTemp33.add(mArrayListTemp3.get(i));
					// }
					// }
					// for (int i = 0; i < mArrayListTemp4.size(); i++)
					// {
					// if(i == Integer.parseInt(mtemp.get(0)) || i ==
					// Integer.parseInt(mtemp.get(1)) || i ==
					// Integer.parseInt(mtemp.get(2)) || i ==
					// Integer.parseInt(mtemp.get(3)) || i ==
					// Integer.parseInt(mtemp.get(4)))
					// {
					// mArrayListTemp44.add(mArrayListTemp4.get(i));
					// }
					// }
					//
					// tide_graph_array.clear();
					// tide_graph_array.addAll(mArrayListTemp11);
					// mArrayListTime.clear();
					// mArrayListTime.addAll(mArrayListTemp22);
					// mArrayListHeight.clear();
					// mArrayListHeight.addAll(mArrayListTemp33);
					// mArrayListTimeCurrent.clear();
					// mArrayListTimeCurrent.addAll(mArrayListTemp44);
					//
					// tide_graph_array.add(0, mArrayListTemp1.get(0));
					// tide_graph_array.add(mArrayListTemp1.get(mArrayListTemp1.size()-1));
					// mArrayListTime.add(0, mArrayListTemp2.get(0));
					// mArrayListTime.add(mArrayListTemp2.get(mArrayListTemp2.size()-1));
					// mArrayListHeight.add(0, mArrayListTemp3.get(0));
					// mArrayListHeight.add(mArrayListTemp3.get(mArrayListTemp3.size()-1));
					// mArrayListTimeCurrent.add(0, mArrayListTemp4.get(0));
					// mArrayListTimeCurrent.add(mArrayListTemp4.get(mArrayListTemp4.size()-1));
					// data = new double[tide_graph_array.size()];
					PointF[] mPointFs = new PointF[tide_graph_array.size()];
					for (int i = 0; i < tide_graph_array.size(); i++) {
						mPointFs[i] = new PointF(i, Float.parseFloat(tide_graph_array.get(i).toString()));
						// data[i] =
						// Double.parseDouble(tide_graph_array.get(i).toString());

					}
					chart.setData(mPointFs, mArrayListTime, mArrayListHeight, mArrayListTimeCurrent);

					System.out.println("points " + tide_graph_array);
					System.out.println("Time " + mArrayListTime);

					System.out.println("Height " + mArrayListHeight);
					System.out.println("Time current " + mArrayListTimeCurrent);

					tide_graph_array.clear();
					mArrayCalender.clear();
					mArrayListCalendarArray.clear();
					tide_graph_array = new ArrayList<String>();

					// New parsing code
					ArrayList<Double> _heightArray = new ArrayList<Double>();
					ArrayList<String> _timeArray = new ArrayList<String>();
					ArrayList<Calendar> _calanderArray = new ArrayList<Calendar>();
					JSONObject _jobj = new JSONObject(new JSONObject(result).getString("Tide"));

					JSONArray _jArrray = _jobj.getJSONArray("dataPoints");
					String _timeZone = _jobj.getString("timezone");
					int _cnt = 0;
					int _currentIndex = -1;
					int _nextDayFirstIndex = 0;
					boolean _currentAdded = false;
					for (int i = 0; i < _jArrray.length(); i++) {
						JSONObject TempObject = (JSONObject) _jArrray.get(i);

						SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
						dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));

						// Local time zone
						SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

						Calendar _cal = Calendar.getInstance();
						Calendar _calCurrent = Calendar.getInstance();

						_calCurrent.setTime(dateFormatLocal.parse(dateFormatGmt.format(new Date())));
						if (!today) {
							_calCurrent.add(Calendar.DAY_OF_YEAR, 1);
						}
						// System.out.println("after " + _calCurrent.getTime());
						DateFormat _df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
						_cal.setTime(_df.parse(TempObject.getString("utctime") + ""));

						if (!_timeZone.equalsIgnoreCase("")) {
							if (_timeZone.contains(".")) {
								String[] _time = new String[2];
								_time[0] = _timeZone.substring(0, _timeZone.indexOf("."));
								_time[1] = _timeZone.substring(_timeZone.indexOf(".") + 1);
								_cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_time[0]));
								_calCurrent.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_time[0]));
								if (_time[1].length() == 1) {
									_time[1] = _time[1] + "0";
								}
								int _minuts = (Integer.parseInt(_time[1]) * 60) / 100;
								_cal.add(Calendar.MINUTE, _minuts);
								_calCurrent.add(Calendar.MINUTE, _minuts);
							} else {
								_cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_timeZone));
								_calCurrent.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_timeZone));

							}

							if ((TempObject.getString("type").equalsIgnoreCase("NORMAL") || TempObject.getString("type").equals("Low") || TempObject.getString("type").equals("High")) && (getDate(_cal).equalsIgnoreCase(getDate(_calCurrent)))) {
								double _value = TempObject.getDouble("height");
								// _value = Double.parseDouble(new
								// DecimalFormat("#.#").format(_value));
								_nextDayFirstIndex = i;
								_heightArray.add(_value);
								SimpleDateFormat _df1 = new SimpleDateFormat("hh:mm a");
								String _currentTime = _df1.format(_cal.getTime());
								_timeArray.add(_currentTime);
								_calanderArray.add(_cal);
								System.out.println("curr date " + _calCurrent.getTime() + "  Tide date " + _cal.getTime());
								if (_calCurrent.before(_cal) && !_currentAdded) {
									_currentIndex = _cnt;
									_currentAdded = true;
									if (isMetricTemp) {
										low_tide_txt.setText(round(_value, 1) + " m");
									} else {
										low_tide_txt.setText(round(_value, 1) + " ft");
									}
									if (TempObject.getString("type").equalsIgnoreCase("high")) {
										// up arrowmcal
										tide_imageview.setVisibility(View.VISIBLE);
										tide_imageview.setImageResource(R.drawable.arrow_up);
									} else if (TempObject.getString("type").equalsIgnoreCase("low")) {
										// down arrow
										tide_imageview.setVisibility(View.VISIBLE);
										tide_imageview.setImageResource(R.drawable.arrow_down);
									} else {
										// nothing draw
										tide_imageview.setVisibility(View.GONE);
									}
								}
								_cnt++;
							}
						}
					}

					System.out.println("cur index "+_currentIndex);
					if (_currentAdded == false && _currentIndex == -1 && today) {
						_currentIndex = _heightArray.size() - 1;
					}
					if(!_currentAdded){
						JSONObject TempObject = (JSONObject) _jArrray.get(_nextDayFirstIndex+1);
						double _value = TempObject.getDouble("height");
						if (isMetricTemp) {
							surf_height_txt.setText(round(_value, 1) + " m");
						} else {
							surf_height_txt.setText(round(_value, 1) + " ft");
						}
						if (TempObject.getString("type").equalsIgnoreCase("high")) {
							// up arrowmcal
							tide_imageview.setVisibility(View.VISIBLE);
							tide_imageview.setImageResource(R.drawable.arrow_up);
						} else if (TempObject.getString("type").equalsIgnoreCase("low")) {
							// down arrow
							tide_imageview.setVisibility(View.VISIBLE);
							tide_imageview.setImageResource(R.drawable.arrow_down);
						} else {
							// nothing draw
							tide_imageview.setVisibility(View.GONE);
						}
					}
					mCurrentTimeIndex = _currentIndex - 1;
					mArrayListHeight = new ArrayList<String>();
					mArrayListTime = new ArrayList<String>();
					data = new double[_timeArray.size()];
					for (int _i = 0; _i < _timeArray.size(); _i++) {
						data[_i] = Double.parseDouble(_heightArray.get(_i).toString());
						mArrayListTime.add(_timeArray.get(_i));
						Double _value = Double.parseDouble(_heightArray.get(_i).toString());
						_value = round(_value, 1);// Double.parseDouble(new
													// DecimalFormat("#.#").format(_value));
						if (isMetricTemp) {
							mArrayListHeight.add(String.valueOf(_value) + " m");
						} else {
							mArrayListHeight.add(String.valueOf(_value) + " ft");
						}
					}
					System.out.println("Curr Ind " + mCurrentTimeIndex + "   New Cur IndX  " + _currentIndex);
					System.out.println("points >>> " + _heightArray);
					System.out.println("Time >>> " + _timeArray);
					setTideChart();
				} catch (Exception ex) {
					System.out.println(ex);
				}

				mProgressDialog.dismiss();

				// SendDatatoWatch mSendDatatoWatch = new SendDatatoWatch();
				// mSendDatatoWatch.execute();
			} catch (Exception e) {
				System.out.println(e + "");
				mProgressDialog.dismiss();
			}
		}
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public void setTideChart() {
		mChart = (LineChart) findViewById(R.id.chart1);
		// if enabled, the chart will always start at zero on the y-axis

		// no description text
		mChart.setDescription("");

		// enable value highlighting
		mChart.setHighlightEnabled(true);

		// enable touch gestures
		mChart.setTouchEnabled(false);

		// enable scaling and dragging
		mChart.setDragEnabled(false);
		mChart.setScaleEnabled(true);
		mChart.setHighlightEnabled(false);
		// if disabled, scaling can be done on x- and y-axis separately
		mChart.setPinchZoom(false);

		mChart.setDrawGridBackground(false);

		// tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

		XAxis x = mChart.getXAxis();
		// x.setTypeface(tf);
		x.setEnabled(false);

		YAxis y = mChart.getAxisLeft();
		// y.setTypeface(tf);
		y.setLabelCount(5);
		y.setEnabled(false);

		mChart.getAxisRight().setEnabled(false);

		// add data
		setData();

		mChart.getLegend().setEnabled(false);

		// mChart.animateXY(2000, 2000);

		// dont forget to refresh the drawing
		mChart.invalidate();
	}

	private void setData() {

		ArrayList<Double> _FirstHalf = new ArrayList<Double>();
		ArrayList<Double> _SecondHalf = new ArrayList<Double>();
		double _fMin = 50, _fMax = 0, _sMin = 50, _sMax = 0;
		int _firstMinIndex = 0, _firstMaxIndex = 0, _secondMaxIndex = 0, _secondMinIndex = 0;
		int _hafIndex = data.length / 2;
		ArrayList<String> xVals = new ArrayList<String>();
		double _data[] = new double[data.length];
		for (int i = 0; i < data.length; i++) {
			if (data[i] < mLastMinTide) {
				mLastMinTide = data[i];
			}
			xVals.add((1990 + i) + "");
			if (i >= 0 && i < data.length) {

				if (i < _hafIndex) {
					_FirstHalf.add(data[i]);
					if (i > 1) {
						if (i == 2) {
							_firstMinIndex = 2;
							_fMin = data[i];
							_firstMaxIndex = 2;
							_fMax = data[i];
						}
						if (data[i] < _fMin) {
							_fMin = data[i];
							_firstMinIndex = i;
						}
						if (data[i] > _fMax) {
							_fMax = data[i];
							_firstMaxIndex = i;
						}
					}
				} else if (i >= _hafIndex) {
					_SecondHalf.add(data[i]);
					if (i < data.length - 2) {
						if (i == _hafIndex + 1) {
							_secondMinIndex = i;
							_sMin = data[i];
							_secondMaxIndex = i;
							_sMax = data[i];
						}

						if (data[i] < _sMin) {
							_sMin = data[i];
							_secondMinIndex = i;
						}
						if (data[i] > _sMax) {
							_sMax = data[i];
							_secondMaxIndex = i;

						}
					}
				}
			}
		}

		// / add lastMinTide
		for (int _i = 0; _i < data.length; _i++) {
			double absVal = Math.abs(mLastMinTide);
			_data[_i] = data[_i] + absVal + 1.5;
		}

		// //////////fine tune index start

		if (data.length > 0) {
			// fine tune idexes
			if (_firstMinIndex < _firstMaxIndex && _secondMaxIndex < _secondMinIndex) {
				// calculate first max again, which should be less then firstmin
				// index
				_firstMaxIndex = 0;
				float _firstMaxHeight = 0;
				for (int i = 1; i < _firstMinIndex; i++) {
					float _height = Float.parseFloat(_FirstHalf.get(i).toString());
					if (i == 1) {
						_firstMaxIndex = 1;
						_firstMaxHeight = _height;
					}
					if (_height > _firstMaxHeight) {
						_firstMaxHeight = _height;
						_firstMaxIndex = i;
					}
				}
			}

			if (_firstMaxIndex < _firstMinIndex && _secondMinIndex < _secondMaxIndex && _secondMinIndex - _firstMinIndex < 5) {
				// calculate second min again, which should be greater then
				// secondmax index
				_secondMinIndex = -1;
				float _secondMinHeight = 0;
				for (int i = _secondMaxIndex - _FirstHalf.size() + 1; i < _SecondHalf.size() - 2; i++) {
					float _height = Float.parseFloat(_SecondHalf.get(i).toString());
					if (i == _secondMaxIndex - _FirstHalf.size() + 1) {
						_secondMinIndex = _secondMaxIndex - _FirstHalf.size() + 1;
						_secondMinHeight = _height;
					}

					if (_height < _secondMinHeight) {
						_secondMinHeight = _height;
						_secondMinIndex = i;
					}
				}
				if (_secondMinIndex != -1) {
					_secondMinIndex = _secondMinIndex + _FirstHalf.size();
				}
			}
		}

		// //////////fine tune index End

		ArrayList<Entry> vals1 = new ArrayList<Entry>();

		ArrayList<String> _time = new ArrayList<String>();
		ArrayList<String> _val = new ArrayList<String>();
		int cnt = 0;
		// System.out.println("SEC min Indx " + _secondMinIndex);
		for (int i = 0; i < _data.length; i++) {
			// float mult = (range + 1);
			// float val = (float) (Math.random() * mult) + 20;// + (float)
			// ((mult *
			// 0.1) / 10);
			if (i % 2 == 0 || i == 0 || i == _data.length - 1 || i == _firstMaxIndex || i == _firstMinIndex || i == _secondMaxIndex || i == _secondMinIndex || i == mCurrentTimeIndex) {
				System.out.println("I VALUW >>> " + i);
				_time.add(mArrayListTime.get(i));
				_val.add(mArrayListHeight.get(i));
				vals1.add(new Entry(Float.parseFloat(String.valueOf(_data[i])), i));
				if (i == _firstMaxIndex) {
					_firstMaxIndex = cnt;
				}
				if (i == _firstMinIndex) {
					_firstMinIndex = cnt;
				}
				if (i == _secondMaxIndex) {
					_secondMaxIndex = cnt;
				}
				if (i == _secondMinIndex) {
					_secondMinIndex = cnt;
				}
				if (i == mCurrentTimeIndex) {
					mCurrentTimeIndex = cnt;
				}
				cnt++;
			}
		}

		// create a dataset and give it a type
		LineDataSet set1 = new LineDataSet(vals1, "DataSet 1");
		set1.setDrawCubic(true);
		set1.setCubicIntensity(0.2f);
		set1.setDrawFilled(true);
		set1.setDrawCircles(true);
		set1.setLineWidth(1f);
		// set1.setDrawCircleHole(true);

		set1.setCircleSize(2.5f);
		set1.setCircleColor(Color.parseColor("#2699CD"));
		set1.setHighLightColor(Color.parseColor("#0168CD"));
		set1.setCircleColorHole(Color.BLACK);
		set1.setFillAlpha(70);
		// set1.setHighLightColor(Color.rgb(244, 117, 117));
		// set1.setColor(Color.rgb(104, 241, 175));
		set1.setFillColor(Color.parseColor("#990168CD"));

		// create a data object with the datasets
		LineData data = new LineData(xVals, set1);
		// data.setValueTypeface(tf);
		data.setValueTextSize(9f);
		data.setDrawValues(false);
		// mChart.setLogEnabled(true);
		// set data
		mChart.setData(data, _firstMinIndex, _firstMaxIndex, _secondMinIndex, _secondMaxIndex, mCurrentTimeIndex, _time, Math.abs(mLastMinTide), _val, this.data.length, Nearest_beaches_fragment.this);
	}

	private String convertToSingalDecimal(String val_str) {
		String[] SplitStringcal = val_str.split("\\.");
		int i = 0;
		if (SplitStringcal[1].length() == 2) {
			SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
			i = Integer.parseInt(SplitStringcal[1]);
			if (i % 10 >= 5) {
				i = i + (10 - (i % 10));
			} else {
				i = Integer.parseInt(SplitStringcal[1].substring(0, 1));
			}
		} else {
			i = Integer.parseInt(SplitStringcal[1]);
		}
		return SplitStringcal[0] + "." + i;
	}

	private String convertToSingalDecimalForSurf(String val_str) {
		String[] SplitStringcal = val_str.split("\\.");
		int i = 0;
		if (SplitStringcal[1].length() >= 2) {
			SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
			i = Integer.parseInt(SplitStringcal[1]);
			if (i % 10 >= 5) {
				i = i + (10 - (i % 10));
			} else {
				i = Integer.parseInt(SplitStringcal[1].substring(0, 1));
			}
		} else {
			i = Integer.parseInt(SplitStringcal[1]);
		}
		return SplitStringcal[0] + "." + i;
	}

	public boolean isSameDay(Calendar cal1, Calendar cal2) {
		if (cal1 == null || cal2 == null)
			return false;
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
	}

	// public class SendDatatoWatch extends AsyncTask<Void, Void, String>
	// {
	// boolean isOfline=false;
	// @Override
	// protected void onPreExecute()
	// {
	// if(mCommonClass.CheckNetwork(context))
	// {
	// isOfline = false;
	// }
	// else
	// {
	// isOfline = true;
	// }
	// mSurfline = new Surfline(mBluetoothLeService);
	// super.onPreExecute();
	// }
	//
	// @Override
	// protected synchronized String doInBackground(Void... params)
	// {
	// mSurfline.UpdateData(isMetricTemp, Integer.parseInt(beach_id),
	// false,result_surf,context);
	// return "";
	// }
	// @Override
	// protected void onPostExecute(String result)
	// {
	// super.onPostExecute(result);
	// mProgressDialog.dismiss();
	// }
	// }

	@Override
	public void onClick(View v) {
		if (v == nearest_beach_close_icon) {
			Intent mIntent = new Intent(context, Favourate_beach_Activity.class);
			startActivity(mIntent);
			finish();
		} else if (v == today_layout) {
			tomorrow_text.setVisibility(View.INVISIBLE);
			today_layout.setBackgroundResource(R.drawable.today_bg);
			forcast_layout.setBackgroundResource(R.drawable.forecast_bg);
			today = true;

			tide_graph_array.clear();
			mArrayListTime.clear();
			mArrayListTimeCurrent.clear();
			mArrayListHeight.clear();
			tide_graph_array = new ArrayList<String>();
			mArrayListTime = new ArrayList<String>();
			mArrayListTimeCurrent = new ArrayList<String>();
			mArrayListHeight = new ArrayList<String>();

			if (mCommonClass.CheckNetwork(context)) {
				NearestBeachDetails_Async mNearestBeachDetails_Async = new NearestBeachDetails_Async();
				mNearestBeachDetails_Async.execute();
			} else {
				Toast.makeText(context, getResources().getString(R.string.error_internet), 5).show();
			}
		} else if (v == forcast_layout) {
			tomorrow_text.setVisibility(View.INVISIBLE);
			forcast_layout.setBackgroundResource(R.drawable.today_bg);
			today_layout.setBackgroundResource(R.drawable.forecast_bg);
			today = false;

			tide_graph_array.clear();
			mArrayListTime.clear();
			mArrayListTimeCurrent.clear();
			mArrayListHeight.clear();
			tide_graph_array = new ArrayList<String>();
			mArrayListTime = new ArrayList<String>();
			mArrayListTimeCurrent = new ArrayList<String>();
			mArrayListHeight = new ArrayList<String>();

			if (mCommonClass.CheckNetwork(context)) {
				NearestBeachDetails_Async mNearestBeachDetails_Async = new NearestBeachDetails_Async();
				mNearestBeachDetails_Async.execute();
			} else {
				Toast.makeText(context, getResources().getString(R.string.error_internet), 5).show();
			}
		} else if (v == nearest_beach_name_txt) {
			Intent intent = new Intent(context, Map_Activity.class);
			startActivity(intent);
		} else if (v == nearest_beach_map_pin) {
			mIntent = new Intent(context, Map_Activity.class);
			mIntent.putExtra("beach_lat", beach_lat + "");
			mIntent.putExtra("beach_long", beach_long + "");
			mIntent.putExtra("beach_name", beach_name + "");
			mIntent.putExtra("subtitle_txt", subtitle_txt + "");
			startActivity(mIntent);
		} else if (v == sethome_beach_pin_icon) {
			shared.setPrefrence(context, "home_beach_title", nearest_beach_name_txt.getText().toString());
			shared.setPrefrence(context, "home_beach_ssw", "SSW " + surf_height_txt.getText().toString());
			shared.setPrefrence(context, "home_beach_water_temp_low", low_water_temp.getText().toString());
			shared.setPrefrence(context, "home_beach_wind", "SSW " + wind_speed_txt.getText().toString());
			shared.setPrefrence(context, "home_beach_tide", low_tide_txt.getText().toString());
			// Toast.makeText(context, "Data set on home screen successfully.",
			// Toast.LENGTH_SHORT).show();
		}
	}

	public String sendTimeFormat(Double milliseconds) {
		boolean am = true;
		int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
		int hours = (int) ((milliseconds / (1000 * 60 * 60 * 60)) % 24);
		System.out.println(hours + " " + minutes);
		if (hours >= 12) {
			am = false;
			hours = hours - 12;
		}
		if (am) {
			return hours + ":" + minutes + " " + "AM";
		} else {
			return hours + ":" + minutes + " " + "PM";
		}
	}

	public void onBackPressed() {
		goBack("back");
	}

	@Override
	public void goBack(String target) {
		mIntent = new Intent(context, Favourate_beach_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target) {
	}

	@Override
	public void setTypefaceControls() {
	};

	public String UTCTime(Date mdate) {
		// creating DateFormat for converting time from local timezone to GMT
		DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return converter.format(mdate);
	}

	public String getUTCTime(Date mdate) {
		// creating DateFormat for converting time from local timezone to GMT
		DateFormat converter = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return converter.format(mdate);
	}

	public String CurrentGMTTime() {
		Date mDate = new Date();
		// creating DateFormat for converting time from local timezone to GMT
		DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return converter.format(mDate);
	}

	public String getDate(Calendar cal) {
		String _date = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
		_date = dateFormat.format(cal.getTime());
		// System.out.println(_date);
		return _date;
	}
}