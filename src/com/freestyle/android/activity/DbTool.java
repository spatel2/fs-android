package com.freestyle.android.activity;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.freestyle.android.pojo.DeviceInfo;
import com.freestyle.android.pojo.Sport;
import com.freestyle.android.pojo.Tool;
import com.freestyle.android.pojo.User;

public class DbTool extends SQLiteOpenHelper
{
	private final static String DATABASE_NAME = "watch_db";
	private final static int DATABASE_VERSION = 1;
	public final static String DEVICE_TABLENAME = "device_tname";
	public final static String DEVICE_ID = "_id";
	public final static String DEVICE_NAME = "devicename";
	public final static String DEVICE_RSSI_NAME = "rssiname";
	public final static String DEVICE_ADDRESS_NAME = "address";
	public final static String DEVICE_PIC_NAME = "pic";
	public final static String DEVICE_TIME = "time";

	public final static String USER_TABLENAME = "user_tname";
	public final static String USER_ID = "_id";
	public final static String USER_NAME = "userName";
	public final static String USER_PIC = "pic";
	public final static String USER_GENDER = "sex";
	public final static String USER_AGE = "age";
	public final static String USER_HEIGHT = "height";
	public final static String USER_WEIGHT = "weight";

	public final static String SPORT_TABLENAME = "sport_tname";
	public final static String SPORT_ID = "_id";
	public final static String SPORT_TYPE = "type";
	public final static String SPORT_AVESPEED = "avespeed";
	public final static String SPORT_MAXSPEED = "maxspeed";
	public final static String SPORT_MINSPEED = "minspeed";
	public final static String SPORT_DISTANCE = "distance";
	public final static String SPORT_HEARTSATE = "heartsate";
	public final static String SPORT_CALORIES = "calories";
	public final static String SPORT_DIRECTION = "direction";
	public final static String SPORT_ALTITIDE = "altitide";
	public final static String SPORT_POSITION = "position";
	public final static String SPORT_DATEID = "dateid";
	public final static String SPORT_CITY = "city";
	public final static String SPORT_WEATHER = "weather";
	public final static String SPORT_SPEED = "speed";
	public final static String SPORT_TIME = "time";
	public final static String SPORT_MAXHEART = "maxheart";

	public DbTool(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		try
		{
			SQLiteDatabase db = this.getWritableDatabase();
//			String sqlquery = "SHOW TABLES FROM " + DATABASE_NAME + " LIKE " + TRACK_TABLENAME; 
			 
			Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+TRACK_TABLENAME+"'", null);
			//Cursor cursor = db.rawQuery(sqlquery, null);
			//cursor.close();
			
			     
			if (cursor.getCount() == 0)
			{
				CreateTrackTable(db);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	// Called when the database is created for the first time. This is where the 
	// creation of tables and the initial population of the tables should happen.
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		String sql = "Create table " + DEVICE_TABLENAME + "(" 
				+ DEVICE_ID + " integer primary key autoincrement," 
				+ DEVICE_NAME + " text," 
				+ DEVICE_RSSI_NAME + " text," 
				+ DEVICE_ADDRESS_NAME + " text," 
				+ DEVICE_PIC_NAME + " text," 
				+ DEVICE_TIME + " text)";
		db.execSQL(sql);
		
		sql = "Create table " + USER_TABLENAME + "(" 
				+ USER_ID + " integer primary key autoincrement," 
				+ USER_NAME + " text,"
				+ USER_GENDER + " text," 
				+ USER_PIC + " text," 
				+ USER_AGE + " text," 
				+ USER_WEIGHT + " text," 
				+ USER_HEIGHT + " text)";
		db.execSQL(sql);
		
		sql = "Create table " + SPORT_TABLENAME  + "(" 
				+ SPORT_ID + " integer primary key autoincrement," 
				+ SPORT_TYPE  + " text,"
				+ SPORT_AVESPEED  + " text," 
				+ SPORT_MAXSPEED  + " text," 
				+ SPORT_MINSPEED + " text," 
				+ SPORT_HEARTSATE  + " text," 
				+ SPORT_CALORIES + " text,"
				+ SPORT_DISTANCE  + " text," 
				+ SPORT_DIRECTION  + " text," 
				+ SPORT_ALTITIDE + " text," 
				+ SPORT_POSITION  + " text," 
				+ SPORT_DATEID + " text,"
				+ SPORT_MAXHEART  + " text,"
				+ SPORT_CITY  + " text," 
				+ SPORT_WEATHER  + " text," 
				+ SPORT_SPEED + " text," 
				+ SPORT_TIME + " text)";
		db.execSQL(sql);
		
		CreateTrackTable(db);
	}

	//Called when the database needs to be upgraded. The implementation should use 
	//this method to drop tables, add tables, or do anything else it needs to upgrade 
	//to the new schema version. 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		String sql = "DROP TABLE IF EXISTS " + DEVICE_TABLENAME;
		db.execSQL(sql);
		sql = "DROP TABLE IF EXISTS " + USER_TABLENAME;
		db.execSQL(sql);
		sql = "DROP TABLE IF EXISTS " + SPORT_TABLENAME;
		db.execSQL(sql);
		
		DeleteTrackTable(db);

		onCreate(db);
	}


	//----------------------------------------------------------------------
	//---------------------------- Device ----------------------------------
	//----------------------------------------------------------------------

	public long insertDevice(DeviceInfo device)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(DEVICE_NAME, device.getName());
		cv.put(DEVICE_RSSI_NAME, device.getRssi());
		cv.put(DEVICE_ADDRESS_NAME, device.getAddress());
		cv.put(DEVICE_PIC_NAME, device.getPic());
		cv.put(DEVICE_TIME, Tool.getTime("yyyy-MM-dd HH:mm:ss"));
		long row = db.insert(DEVICE_TABLENAME, null, cv);
		return row;
	}

	public void updateDevice(DeviceInfo device)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String where = DEVICE_ID + "=?";
		String[] whereValue = { device.getId() };
		ContentValues cv = new ContentValues();
		cv.put(DEVICE_NAME, device.getName());
		cv.put(DEVICE_RSSI_NAME, device.getRssi());
		cv.put(DEVICE_ADDRESS_NAME, device.getAddress());
		cv.put(DEVICE_PIC_NAME, device.getPic());
		cv.put(DEVICE_TIME, Tool.getTime("yyyy-MM-dd HH:mm:ss"));
		db.update(DEVICE_TABLENAME, cv, where, whereValue);
	}

	public Cursor getDevice(String address)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select * from " + DEVICE_TABLENAME + " where " + DEVICE_ADDRESS_NAME + "='" + address + "'";
		Cursor rec = db.rawQuery(sql, null);
		return rec;
	}

	public void deleteDevice(String address)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String where = DEVICE_ADDRESS_NAME + "=?";
		String[] whereValue = { address };
		db.delete(DEVICE_TABLENAME, where, whereValue);
	}

	public boolean checkDeviceTable()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("select count(*) from sqlite_master where type='table' and name='"
						+ DEVICE_TABLENAME + "'", null);
		if (cursor.getCount() > 0)
		{
			return true;
		}
		return false;
	}

	//----------------------------------------------------------------------
	//------------------------------ User ----------------------------------
	//----------------------------------------------------------------------
	public long insertUser(User user)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(USER_AGE, user.getAge());
		cv.put(USER_HEIGHT, user.getHeight());
		cv.put(USER_PIC, user.getImageUrl());
		cv.put(USER_GENDER, user.getSex());
		cv.put(USER_WEIGHT, user.getWeight());
		cv.put(USER_NAME, user.getUserName());
		long row = db.insert(USER_TABLENAME, null, cv);
		Log.i("db", "insertUser:"+row);
		return row;
	}

	public void updateUser(User user)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String where = USER_ID + "=?";
		String[] whereValue = { user.getId() };
		ContentValues cv = new ContentValues();
		cv.put(USER_AGE, user.getAge());
		cv.put(USER_HEIGHT, user.getHeight());
		cv.put(USER_PIC, user.getImageUrl());
		cv.put(USER_GENDER, user.getSex());
		cv.put(USER_WEIGHT, user.getWeight());
		cv.put(USER_NAME, user.getUserName());
		db.update(USER_TABLENAME, cv, where, whereValue);
		Log.i("db", "updateUser:"+user.getId());
	}

	public boolean checkUser()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from "+USER_TABLENAME, null);
		if (cursor.getCount() > 0)
		{
			Log.i("db", "checkUser:"+cursor.getCount());
			return true;
		}
		Log.i("db", "checkUser:"+cursor.getCount());
		return false;
	}

	public Cursor getUser()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from "+USER_TABLENAME, null);
		Log.i("db", "getUser:"+cursor.getCount());
		return cursor;
	}
	
	//----------------------------------------------------------------------
	//----------------------------- Sport ----------------------------------
	//----------------------------------------------------------------------
	
	public long insertSport(Sport sport)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(SPORT_TYPE , sport.getType());
		cv.put(SPORT_AVESPEED , sport.getAveSpeed());
		cv.put(SPORT_MAXSPEED , sport.getMaxSpeed());
		cv.put(SPORT_MINSPEED , sport.getMinSpeed());
		cv.put(SPORT_DISTANCE  , sport.getDistance());
		cv.put(SPORT_HEARTSATE  , sport.getHeartSate());
		cv.put(SPORT_CALORIES  , sport.getCalories());
		cv.put(SPORT_DIRECTION  , sport.getDirection());
		cv.put(SPORT_ALTITIDE  , sport.getAltitide());
		cv.put(SPORT_POSITION  , sport.getPosition());
		cv.put(SPORT_DATEID  , sport.getDateId());
		cv.put(SPORT_CITY  , sport.getCity());
		cv.put(SPORT_WEATHER  , sport.getWeather());
		cv.put(SPORT_SPEED  , sport.getSpeed());
		cv.put(SPORT_TIME   , sport.getTime());
		cv.put(SPORT_MAXHEART   , sport.getMaxHeartSate());
		long row = db.insert(SPORT_TABLENAME , null, cv);
		return row;
	}

//	public void updateSport(Sport sport)
//	{
//		SQLiteDatabase db = this.getWritableDatabase();
//		String where = SPORT_ID + "=?";
//		String[] whereValue = { sport.getId() };
//		ContentValues cv = new ContentValues();
//		cv.put(SPORT_TYPE , sport.getType());
//		cv.put(SPORT_AVESPEED , sport.getAveSpeed());
//		cv.put(SPORT_MAXSPEED , sport.getMaxSpeed());
//		cv.put(SPORT_MINSPEED , sport.getMinSpeed());
//		cv.put(SPORT_DISTANCE  , sport.getDistance());
//		cv.put(SPORT_HEARTSATE  , sport.getHeartSate());
//		cv.put(SPORT_CALORIES  , sport.getCalories());
//		cv.put(SPORT_DIRECTION  , sport.getDirection());
//		cv.put(SPORT_ALTITIDE  , sport.getAltitide());
//		cv.put(SPORT_POSITION  , sport.getPosition());
//		cv.put(SPORT_DATEID  , sport.getDateId());
//		cv.put(SPORT_CITY  , sport.getCity());
//		cv.put(SPORT_WEATHER  , sport.getWeather());
//		cv.put(SPORT_SPEED  , sport.getSpeed());
//		cv.put(SPORT_TIME   , sport.getTime());
//		cv.put(SPORT_MAXHEART   , sport.getMaxHeartSate());
//		//long row = db.insert(SPORT_TABLENAME , null, cv);
//		db.update(SPORT_TABLENAME, cv, where, whereValue);
//	}

	public Cursor getSportList()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery( "select * from "+SPORT_TABLENAME+" order by "+DEVICE_ID+" desc", null);
		//Cursor cursor = db.query(SPORT_TABLENAME, null, null, null, null, null, SPORT_DATEID);
		//Log.i("db", "getUser:"+cursor.getCount());
		return cursor;
	}

	public Cursor getSportByID(String id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select * from " + SPORT_TABLENAME + " where "
				+ SPORT_ID + "='" + id + "'";
		Cursor rec = db.rawQuery(sql, null);
		return rec;
	}

	public void deleteSport(String id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String where = SPORT_ID + "=?";
		String[] whereValue =
			{id};
		db.delete(SPORT_TABLENAME, where, whereValue);
	}

	//----------------------------------------------------------------------
	//----------------------------- Track ----------------------------------
	//----------------------------------------------------------------------

	public final static String TRACK_TABLENAME	= "TrackTable";

	public final static String TRACK_ID			= "ID";
	public final static String TRACK_TIMESTAMP	= "Time";
	public final static String TRACK_SPORT_TYPE	= "Type";
	public final static String TRACK_DISTANCE	= "Distance";
	public final static String TRACK_DURATION	= "Duration";
	public final static String TRACK_CALORIES	= "Calories";
	public final static String TRACK_NOTES		= "Notes";
	public final static String TRACK_DIRECTION	= "direction";
	public final static String TRACK_ALTITUDE	= "altitude";
	public final static String TRACK_DATE		= "track_date";
	public final static String TRACK_TIME		= "track_time";
	public final static String TRACK_UNIT		= "track_unit";
	public final static String TRACK_LATLON		= "track_latlon";
	
	
	

	public static class TrackInfo
	{
		public int		ID=0;
		public long		TimeStamp=0;
		public int		Type=0;
		public float	Distance=00;
		public String	Duration="";
		public int		Calories=00;
		public String	Notes="";
		public String	Direction="0.0";
		public String	Altitude="0.0";
		public String	track_date="";
		public String	track_time="";
		public String 	track_unit="i";
		public String	track_latlon= "";
	}

	private void CreateTrackTable(SQLiteDatabase db)
	{
		String sql = "Create table " + TRACK_TABLENAME  + "(" 
				+ TRACK_ID			+ " integer primary key autoincrement," 
				+ TRACK_TIMESTAMP	+ " integer,"
				+ TRACK_SPORT_TYPE  + " integer,"
				+ TRACK_DISTANCE	+ " real," 
				+ TRACK_DURATION	+ " text," 
				+ TRACK_CALORIES	+ " integer,"
				+ TRACK_NOTES		+ " text,"
				+ TRACK_DIRECTION	+ " text,"
				+ TRACK_ALTITUDE	+ " text,"
				+ TRACK_DATE		+ " text,"
				+ TRACK_TIME		+ " text,"
				+ TRACK_UNIT		+ " text,"
				+ TRACK_LATLON		+ " text)";
		
		db.execSQL(sql);
	}

	private void DeleteTrackTable(SQLiteDatabase db)
	{
		String sql = "DROP TABLE IF EXISTS " + TRACK_TABLENAME;
		db.execSQL(sql);
	}

//	private ContentValues getContentValues(TrackInfo track)
//	{
//		ContentValues cv = new ContentValues();
//
//		cv.put(TRACK_TIMESTAMP,		track.TimeStamp);
//		cv.put(TRACK_SPORT_TYPE,	track.Type);
//		cv.put(TRACK_DISTANCE,		track.Distance);
//		cv.put(TRACK_DURATION,		track.Duration);
//		cv.put(TRACK_CALORIES,		track.Calories);
//		cv.put(TRACK_NOTES,			track.Notes);
//		cv.put(TRACK_DIRECTION,		track.Direction);
//		cv.put(TRACK_ALTITUDE,		track.Altitude);
//		
//
//		return cv;
//	}
	//Returns: the row ID of the newly inserted row, or -1 if an error occurred
	public long AddTrack(ContentValues NewTrack)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		long RetValue = db.insert(TRACK_TABLENAME, null, NewTrack);
		db.close();

		return RetValue;
	}

	private TrackInfo FillTrackInfo(Cursor cursor)
	{
		TrackInfo ReturnObject = new TrackInfo();
		ReturnObject.ID			= cursor.getInt(0);
		ReturnObject.TimeStamp	= cursor.getLong(1);
		ReturnObject.Type		= cursor.getInt(2);
		ReturnObject.Distance	= cursor.getFloat(3);
		ReturnObject.Duration	= cursor.getString(4);
		ReturnObject.Calories	= cursor.getInt(5);
		ReturnObject.Notes 		= cursor.getString(6);
		ReturnObject.Direction	= cursor.getString(7);
		ReturnObject.Altitude	= cursor.getString(8);
		ReturnObject.track_date	= cursor.getString(9);
		ReturnObject.track_time	= cursor.getString(10);
		ReturnObject.track_unit	= cursor.getString(11);
		ReturnObject.track_latlon	= cursor.getString(12);
		
		
		return ReturnObject;
	}

	//returns A Cursor object, which is positioned before the first entry. Note 
	//that Cursors are not synchronized, see the documentation for more details.
	public TrackInfo GetTrack(String trackid)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.query(
				TRACK_TABLENAME,				//The table name to compile the query against.
				null,							//A list of which columns to return. Passing null will return all columns, which is discouraged to prevent reading data from storage that isn't going to be used.
				TRACK_ID + "=?",				//A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself). Passing null will return all rows for the given table.
				new String[] { trackid },		// You may include ?s in selection, which will be replaced by the values from selectionArgs, in order that they appear in the selection. The values will be bound as Strings. 
				null, 							//groupBy A filter declaring how to group rows, formatted as an SQL GROUP BY clause (excluding the GROUP BY itself). Passing null will cause the rows to not be grouped.
				null,							//having A filter declare which row groups to include in the cursor, if row grouping is being used, formatted as an SQL HAVING clause (excluding the HAVING itself). Passing null will cause all row groups to be included, and is required when row grouping is not being used. 
				null, 							//orderBy How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort order, which may be unordered.
				null);							//limit Limits the number of rows returned by the query, formatted as LIMIT clause. Passing null denotes no LIMIT clause.
		if (cursor != null)
			cursor.moveToFirst();

		return FillTrackInfo(cursor);
	}

	public List<TrackInfo> GetAllTracks() 
	{
		List<TrackInfo> TrackList = new ArrayList<TrackInfo>();

		// Select All Query
		String selectQuery = "SELECT * FROM " + TRACK_TABLENAME;

		SQLiteDatabase db = this.getWritableDatabase();
		try
		{
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) 
			{
				do 
				{
					TrackList.add(FillTrackInfo(cursor));
				} while (cursor.moveToNext());
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		// return contact list
		return TrackList;
	}

	public int GetTrackCount() 
	{
		String countQuery = "SELECT  * FROM " + TRACK_TABLENAME;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}


	//Returns: the number of rows affected if a whereClause is passed in, 0 otherwise. 
	// To remove all rows and get a count pass "1" as the whereClause.
	public int deleteTrack(int IDToDelete)
	{
		SQLiteDatabase db = this.getReadableDatabase();

		String whereClause = TRACK_ID + "=?";
		String[] whereArgs = {String.valueOf(IDToDelete)};

		return db.delete(
				TRACK_TABLENAME,	//the table to delete from 
				whereClause, 		//the optional WHERE clause to apply when deleting. Passing null will delete all rows.
				whereArgs);			//You may include ?s in the where clause, which will be replaced by the values from whereArgs. The values will be bound as Strings.
	}
}
