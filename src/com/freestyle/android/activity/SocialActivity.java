package com.freestyle.android.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.freestyle.android.R;
import com.freestyle.android.R.drawable;
import com.freestyle.android.config.CoreFrame;

public class SocialActivity extends BaseActivity implements CoreFrame
{
	ImageView ivmenu, twiter, facebook, instagram, pinterest,youtube;
	ViewGroup llcontainer;
	String sStrTwitterURL 	= "http://208.131.135.12/~elegantm/geneva_api/fs/twitter/feed.php";
	String sStrFaceBookURL 	= "http://208.131.135.12/~elegantm/geneva_api/fs/facebook/feed.php";
	String sStrInstagramURL = "http://208.131.135.12/~elegantm/geneva_api/fs/instagram/feed.php";
	String sStrPinterestURL = "http://208.131.135.12/~elegantm/geneva_api/fs/pinterest/feed_ios.php";
	String sStrYoutubeURL 	= "http://208.131.135.12/~elegantm/geneva_api/fs/youtube/feed.php";

	Intent mIntent;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_social);

		llcontainer = (LinearLayout) findViewById(R.id.llsocialcontainer);

		twiter 		= 	(ImageView) 	findViewById(R.id.twitter);
		facebook 	= 	(ImageView) 	findViewById(R.id.facebook);
		instagram 	= 	(ImageView) 	findViewById(R.id.instagram);
		pinterest 	= 	(ImageView) 	findViewById(R.id.pinterest);
		youtube 	= 	(ImageView)		findViewById(R.id.youtube);

		if(check_Internet(SocialActivity.this))
		{
			WebView WebView = new WebView(getApplicationContext());
			llcontainer.removeAllViews();
			initWebViewSettings(WebView, sStrTwitterURL);						
			llcontainer.addView(WebView);
		}
		else
		{
			Toast.makeText(SocialActivity.this,  getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
		}
		youtube.setBackgroundResource(R.drawable.youtube_inactive);
		twiter.setBackgroundResource(drawable.n_top_icon_twitter_hover);
		facebook.setBackgroundResource(drawable.n_top_icon_facebook);
		instagram.setBackgroundResource(drawable.n_top_icon_instagram);
		pinterest.setBackgroundResource(drawable.n_top_icon_pinterest);
		youtube.setBackgroundResource(R.drawable.youtube_inactive);

		initDrawer(SocialActivity.this);

		twiter.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				if(check_Internet(SocialActivity.this))
				{

					WebView oWebViewTwitter = new WebView(getApplicationContext());
					llcontainer.removeAllViews();
					initWebViewSettings(oWebViewTwitter, sStrTwitterURL);
					llcontainer.addView(oWebViewTwitter);
				}
				else
				{
					Toast.makeText(SocialActivity.this,  getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
				}
				youtube.setBackgroundResource(R.drawable.youtube_inactive);
				twiter.setBackgroundResource(drawable.n_top_icon_twitter_hover);
				facebook.setBackgroundResource(drawable.n_top_icon_facebook);
				instagram.setBackgroundResource(drawable.n_top_icon_instagram);
				pinterest.setBackgroundResource(drawable.n_top_icon_pinterest);
			}
		});

		facebook.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				if(check_Internet(SocialActivity.this))
				{
					WebView oWebViewFacebook = new WebView(getApplicationContext());
					llcontainer.removeAllViews();
					initWebViewSettings(oWebViewFacebook, sStrFaceBookURL);
					llcontainer.addView(oWebViewFacebook);
				}
				else
				{
					Toast.makeText(SocialActivity.this,  getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
				}
				youtube.setBackgroundResource(R.drawable.youtube_inactive);
				facebook.setBackgroundResource(drawable.n_top_icon_facebook_hover);
				twiter.setBackgroundResource(drawable.n_top_icon_twitter);
				instagram.setBackgroundResource(drawable.n_top_icon_instagram);
				pinterest.setBackgroundResource(drawable.n_top_icon_pinterest);
			}
		});

		pinterest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(check_Internet(SocialActivity.this))
				{
					WebView oWebViewPinterest = new WebView(getApplicationContext());
					llcontainer.removeAllViews();
					initWebViewSettings(oWebViewPinterest, sStrPinterestURL);
					llcontainer.addView(oWebViewPinterest);
				}
				else
				{
					Toast.makeText(SocialActivity.this,  getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
				}
				youtube.setBackgroundResource(R.drawable.youtube_inactive);
				pinterest.setBackgroundResource(drawable.n_top_icon_pinterest_hover);
				twiter.setBackgroundResource(drawable.n_top_icon_twitter);
				facebook.setBackgroundResource(drawable.n_top_icon_facebook);
				instagram.setBackgroundResource(drawable.n_top_icon_instagram);
			}
		});

		instagram.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{
				if(check_Internet(SocialActivity.this))
				{
					WebView oWebViewInstagram = new WebView(getApplicationContext());
					llcontainer.removeAllViews();
					initWebViewSettings(oWebViewInstagram, sStrInstagramURL);
					llcontainer.addView(oWebViewInstagram);
				}
				else
				{
					Toast.makeText(SocialActivity.this,  getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
				}
				youtube.setBackgroundResource(R.drawable.youtube_inactive);
				instagram.setBackgroundResource(drawable.n_top_icon_instagram_hover);
				twiter.setBackgroundResource(drawable.n_top_icon_twitter);
				facebook.setBackgroundResource(drawable.n_top_icon_facebook);
				pinterest.setBackgroundResource(drawable.n_top_icon_pinterest);
			}
		});

		youtube.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(check_Internet(SocialActivity.this))
				{
					WebView oWebViewInstagram = new WebView(getApplicationContext());
					llcontainer.removeAllViews();
					initWebViewSettings(oWebViewInstagram, sStrYoutubeURL);
					llcontainer.addView(oWebViewInstagram);
				}
				else
				{
					Toast.makeText(SocialActivity.this,  getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
				}
				youtube.setBackgroundResource(R.drawable.youtube);
				instagram.setBackgroundResource(drawable.n_top_icon_instagram);
				twiter.setBackgroundResource(drawable.n_top_icon_twitter);
				facebook.setBackgroundResource(drawable.n_top_icon_facebook);
				pinterest.setBackgroundResource(drawable.n_top_icon_pinterest);
			}
		});
	}

	@Override
	public void goBack(String target) 
	{
		finish();
	}

	@Override
	public void goForward(String target)
	{
	}

	@Override
	public void setTypefaceControls() 
	{
	}

	/**
	 * Social links under page logic goes here
	 */
	@SuppressLint("SetJavaScriptEnabled") 
	private void initWebViewSettings(WebView webView,String url)
	{
		if(webView != null)
		{
			webView.getSettings().setJavaScriptEnabled(true);
			webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);		
			webView.getSettings().setSupportZoom(false);
			webView.getSettings().setAppCacheEnabled(true);
			webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			webView.setVerticalScrollBarEnabled(false);
			webView.setHorizontalScrollBarEnabled(false);
			if(url.equalsIgnoreCase(sStrPinterestURL))
			{
				webView.getSettings().setSupportMultipleWindows(false);
			}
			else
			{
				webView.getSettings().setSupportMultipleWindows(true);
			}
			webView.getSettings().setSupportZoom(false);
			webView.setVerticalScrollBarEnabled(false);
			webView.setHorizontalScrollBarEnabled(false);

			webView.loadUrl(url);
			webView.setWebViewClient(new MyWebViewClient());
			webView.setWebChromeClient(new myWebChromeClient());
		}
	}
	private class MyWebViewClient extends WebViewClient {

		boolean isPageLoaded = false;
		String mOldUrl = "";

		@Override
		public void onPageFinished(WebView view, String url) {
			isPageLoaded = true;
			mOldUrl = url;
			super.onPageFinished(view, url);
		}

		@Override
		public void doUpdateVisitedHistory(WebView view, String url,boolean isReload) {
			// solution for twitter this code must exist
			// if your dialog is called twice due to
			// doUpdateVisitedHistory && shouldOverrideUrlLoading
			// just put a condition for facebook and twitter here
			// execute only if it matches with facebook and twitter here

			if (shouldBlock(url) && isPageLoaded) 
			{
				openIntent(url);
				view.loadUrl(mOldUrl);
				super.doUpdateVisitedHistory(view, mOldUrl, true);
				return;
			}
			super.doUpdateVisitedHistory(view, url, isReload);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) 
		{
			isPageLoaded = false;
			super.onPageStarted(view, url, favicon);

		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) 
		{
			// Solution for instagram this code must exist
			// if your dialog is called twice due to
			// doUpdateVisitedHistory && shouldOverrideUrlLoading
			// just put a condition for instagram & pintrest here
			// execute only if it matches with instagram & pintrest
			if (shouldBlock(url)) 
			{
				view.stopLoading();
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
				return true;
			} else {
				return false;
			}
		}

		public boolean shouldBlock(String url) 
		{
			if (sStrTwitterURL.equalsIgnoreCase(url) || sStrFaceBookURL.equalsIgnoreCase(url) || sStrInstagramURL.equalsIgnoreCase(url) || sStrPinterestURL.equalsIgnoreCase(url)) 
			{
				return false;
			}
			return true;
		}
	}

	private class myWebChromeClient extends WebChromeClient 
	{
		@Override
		public boolean onCreateWindow(WebView view, boolean isDialog,boolean isUserGesture, Message resultMsg) 
		{
			WebView.HitTestResult result = view.getHitTestResult();
			if (result == null) 
			{
				return false;
			}
			int type = result.getType();
			String mLink = result.getExtra();

			switch (type) {

			case WebView.HitTestResult.UNKNOWN_TYPE:
				return false;
			case WebView.HitTestResult.EDIT_TEXT_TYPE:				
				return false;
			case WebView.HitTestResult.EMAIL_TYPE:				
				return false;
			case WebView.HitTestResult.GEO_TYPE:				
				return false;
			case WebView.HitTestResult.IMAGE_TYPE:				
				return false;
			case WebView.HitTestResult.PHONE_TYPE:				
				return false;
			case WebView.HitTestResult.SRC_ANCHOR_TYPE:		
				openIntent(mLink);
				return false;
			case WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE:				
				return false;

			default:
				break;
			}
			return super.onCreateWindow(view, isDialog, isUserGesture,resultMsg);
		}
	}

	public void openIntent(String url) {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(intent);
	}
}
