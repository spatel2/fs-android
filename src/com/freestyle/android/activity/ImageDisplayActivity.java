package com.freestyle.android.activity;

import com.freestyle.android.R;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageDisplayActivity extends Activity 
{
	static byte[] imageToShow=null;

	ImageView mImageview;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.imageview);

		mImageview = (ImageView)findViewById(R.id.captured_image);

		if (imageToShow == null) 
		{
			finish();
		}
		else 
		{

			try
			{
				new Handler().postDelayed(new Runnable()
				{ 
					public void run()
					{
						finish();
					}
				}, 1 * 1000);
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "Your Phone Camera do not support this Feature", Toast.LENGTH_SHORT).show();
			}

			BitmapFactory.Options mOptions=new BitmapFactory.Options();

			mOptions.inPurgeable=true;
			mOptions.inInputShareable=true;
			mOptions.inMutable=false;
			mOptions.inSampleSize=2;

			mImageview.setImageBitmap(BitmapFactory.decodeByteArray(imageToShow,0,imageToShow.length,mOptions));
			imageToShow=null;
		}
	}
}
