package com.freestyle.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.freestyle.android.R;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.TextviewClass;

public class Splash_Connect_your_watch_Activity extends BaseActivity
{
	TextviewClass intro_txt_view,intro_txt_view2,intro_txt_view3;
	ButtonClass intro_exit_button, intro_next_button;
	Intent mIntent;
	Context context;
	SharedPreferences mSharedPreferenceSplash;
	Editor mEditorSplash;
	boolean mBooleanSplashFlag = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_connect_watch);

		context = Splash_Connect_your_watch_Activity.this;

		intro_txt_view = (TextviewClass)findViewById(R.id.intro_txt_view);
		intro_txt_view2 = (TextviewClass)findViewById(R.id.intro_txt_view2);
		intro_txt_view3 = (TextviewClass)findViewById(R.id.intro_txt_view3);


		intro_exit_button = (ButtonClass)findViewById(R.id.intro_exit_button); 
		intro_next_button = (ButtonClass)findViewById(R.id.intro_next_button);


		intro_txt_view.setText(Html.fromHtml("Press <b>INF/MODE</b> to place your watch in the default"));
		intro_txt_view2.setText(Html.fromHtml("<b>TIME MODE</b>. Now press and hold the <font ><i>CONNECT</i></font>"));
		intro_txt_view3.setText(Html.fromHtml("button for 2 seconds and tap NEXT below."));

		intro_exit_button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				//Changing Prefrence
				final Dialog mDialog = new Dialog(Splash_Connect_your_watch_Activity.this);
				mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				mDialog.setCancelable(false);
				mDialog.setContentView(R.layout.splash_dialog);
				mDialog.show();
				ButtonClass ok = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_okbutton);
				ButtonClass cancel = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_cancelbutton);

				ok.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value),true);
						mEditorSplash.commit();
						Intent mIntent = new Intent(Splash_Connect_your_watch_Activity.this,Home_Activity.class);
						startActivity(mIntent);
						finish();
					}
				});

				cancel.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value),false);
						mEditorSplash.commit();
						Intent mIntent = new Intent(Splash_Connect_your_watch_Activity.this,Home_Activity.class);
						startActivity(mIntent);
						finish();
					}
				});
			}
		});

		intro_next_button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				mIntent = new Intent(context, SplashActivity.class);
				startActivity(mIntent);
				finish();
			}
		});
	}

	protected void onResume() {
		super.onResume();
		mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
		mBooleanSplashFlag = mSharedPreferenceSplash.getBoolean(getResources().getString(R.string.splash_flag_value), true);

		if(!mBooleanSplashFlag)
		{
			Intent mIntent = new Intent(Splash_Connect_your_watch_Activity.this,Home_Activity.class);
			startActivity(mIntent);
			finish();
		} 
	};
}
