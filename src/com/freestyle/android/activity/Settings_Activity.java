package com.freestyle.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RelativeLayout;
import com.freestyle.android.R;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.login.actSignUp;
import com.freestyle.test.clsPref;

public class Settings_Activity extends BaseActivity implements OnClickListener, CoreFrame {
	RelativeLayout setting_connect_lay, setting_help_lay, setting_profile_lay,
			setting_watch_lay, setting_logout_lay;
	Context context;
	Intent mIntent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_activity);

		context = Settings_Activity.this;
		initDrawer(Settings_Activity.this);

		setting_connect_lay = (RelativeLayout) findViewById(R.id.setting_connect_lay);
		setting_help_lay = (RelativeLayout) findViewById(R.id.setting_help_lay);
		setting_profile_lay = (RelativeLayout) findViewById(R.id.setting_profile_lay);
		setting_watch_lay = (RelativeLayout) findViewById(R.id.setting_watch_lay);
		setting_logout_lay = (RelativeLayout) findViewById(R.id.setting_logout_lay);

		setting_connect_lay.setOnClickListener(this);
		setting_help_lay.setOnClickListener(this);
		setting_profile_lay.setOnClickListener(this);
		setting_watch_lay.setOnClickListener(this);
		setting_logout_lay.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == setting_connect_lay) {
			mIntent = new Intent(context, Search_Activity.class);
			startActivity(mIntent);
			finish();
		} else if (v == setting_help_lay) {
			mIntent = new Intent(context, Help_Activity.class);
			startActivity(mIntent);
		} else if (v == setting_profile_lay) {
			mIntent = new Intent(context, Profile_Activity.class);
			startActivity(mIntent);
			finish();
		} else if (v == setting_watch_lay) {
			goForward("watch_command");
		} else if (v == setting_logout_lay) {
			new clsPref(Settings_Activity.this).clearAll();
			mIntent = new Intent(context, actSignUp.class);
			mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(mIntent);
			finish();
		}
	}

	@Override
	public void onBackPressed() {
		goBack("back");
	}

	@Override
	public void goBack(String target) {
		mIntent = new Intent(context, Home_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target) {
		if (target.equals("watch_command")) {
			mIntent = new Intent(context, WatchCommand_Activity.class);
			startActivity(mIntent);
			finish();
		}
	}

	@Override
	public void setTypefaceControls() {
	}

	@Override
	public void gotoSetting() {
	}
}
