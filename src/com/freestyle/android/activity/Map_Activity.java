package com.freestyle.android.activity;

import java.util.Timer;
import java.util.TimerTask;
import com.freestyle.android.R;
import com.freestyle.android.config.CommonClass;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Map_Activity extends BaseActivity 
{
	//For map
	private GoogleMap googleMap;
	CommonClass mCommonClass;
	Context context;
	Intent mIntent = null;
	Double beach_lat=0.0, beach_long=0.0;
	String beach_name="",subtitle_txt="";
	TextView map_beach_name_txt,map_beach_subtitle_txt;
	ImageView map_close_icon;
	boolean islatlongAvailable = true;
	private Timer SendTimer; 
	LocationClient mLocationClient;
	LatLng mLatLng;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);

		context = Map_Activity.this;
		initDrawer(Map_Activity.this);

		map_beach_name_txt = (TextView)findViewById(R.id.map_beach_name_txt);
		map_beach_subtitle_txt = (TextView)findViewById(R.id.map_beach_subtitle_txt);
		map_close_icon = (ImageView)findViewById(R.id.map_close_icon);

		
		mIntent = getIntent();
		if(mIntent.getExtras()!=null)
		{
			try
			{
				beach_name = mIntent.getExtras().getString("beach_name");
				subtitle_txt = mIntent.getExtras().getString("subtitle_txt");
				
				map_beach_name_txt.setText(beach_name);
				map_beach_subtitle_txt.setText(subtitle_txt);
				
				beach_lat = Double.parseDouble(mIntent.getExtras().getString("beach_lat"));
				beach_long = Double.parseDouble(mIntent.getExtras().getString("beach_long"));
			}
			catch(Exception ex)
			{
				islatlongAvailable = false;
				System.out.println(ex);
			}
		}
		
		try
		{
			initilizeMap();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		map_close_icon.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
		
		SendTimer = new Timer(); 
		SendTimer.schedule(new TimerTask()  	 	
		{ 	 	
			@Override 	 	
			public void run()  	 	
			{ 	 	
				 	
			} 	 	
		}, 0, 10000);
	}

	@SuppressLint("NewApi")
	private void initilizeMap() 
	{
		if (googleMap == null) 
		{
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_nlp)).getMap();

			if (googleMap == null) 
			{
				Toast.makeText(getApplicationContext(),"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
			}

			googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

			googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback()
			{
				@Override
				public void onMapLoaded() 
				{
					if(islatlongAvailable)
					{
						LatLng mapCenter = new LatLng(beach_lat, beach_long);
						googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, 16));
					}
					else
					{
//						Toast.makeText(context, "No coordinates available", Toast.LENGTH_SHORT).show();
					}
					googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
				}
			});
		}
	}
}
