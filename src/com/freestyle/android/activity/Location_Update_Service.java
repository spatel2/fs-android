package com.freestyle.android.activity;

import com.freestyle.android.config.Constants;
import com.freestyle.android.config.Shared_preference;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

@SuppressLint("FloatMath") 
public class Location_Update_Service extends Service 
{
	Shared_preference shared;
	Context context;
	int seconds_refresh=20;
	boolean isPending=false;
	double lat_prev = 0;
	double long_prev = 0;
	double distance = 0;
	
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		
		context = this;
		shared = new Shared_preference(context);
		getLocation();
	}
	
	private LocationManager mLocationManager=null;
	private LocationListener mLocationListener =null;
	
	private void getLocation()
	{
		/* Use the LocationManager class to obtain GPS locations */
	 	if(mLocationManager==null)
	 	{
			mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
			if(mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
			{
				AddLocationManager();
	    	}
	    	else
	    	{		
			    Toast.makeText( getApplicationContext(), "Location Provider not available, please turn on GPS",Toast.LENGTH_LONG).show();
			    mLocationManager=null;
			    mLocationListener=null;
	    	}
	   }
	}
	
	private void  AddLocationManager() 
	{
		if(mLocationManager!=null)
		{
			mLocationListener = new MyLocationListener();
			mLocationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, seconds_refresh*1000, 1, mLocationListener);
		}
	}

	private void RemoveLocationManager() 
	{
		if(mLocationListener!=null && mLocationManager!=null)
		{
			mLocationManager.removeUpdates(mLocationListener);
			mLocationListener=null;
			mLocationManager=null;
		}
	}
	
    /** Class My Location Listener */
	private class MyLocationListener implements LocationListener
	{
		@SuppressLint("NewApi")
		@Override
		public void onLocationChanged(Location loc)
		{
			Log.i("Latitude: ", loc.getLatitude()+"");
			Log.i("Longtitude: ", loc.getLongitude()+"");
			
			shared.setPrefrence(context, Constants.PREFRENCE.ROAD_LATITUDE, loc.getLatitude()+"");
			shared.setPrefrence(context, Constants.PREFRENCE.ROAD_LONGTITUDE, loc.getLongitude()+"");
			
			RemoveLocationManager();
		}
		
		@Override	
		public void onProviderDisabled(String provider)
		{
			
		}
		@Override
		public void onProviderEnabled(String provider)
		{
			
		}
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
	
		}
   }
	
	@Override
	public IBinder onBind(Intent intent) 
	{
		return null;
	}

	@Override
	public void onDestroy()
	{
		RemoveLocationManager();
		super.onDestroy();
	}
	
	
	@Override
	public void onTaskRemoved(Intent rootIntent) 
	{
		rootIntent.setClass(context, Location_Update_Service.class);
		stopService(rootIntent);
		super.onTaskRemoved(rootIntent);
	}
}
