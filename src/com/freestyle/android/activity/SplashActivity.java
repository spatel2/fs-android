package com.freestyle.android.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.freestyle.android.R;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.TextviewClass;

public class SplashActivity extends BaseActivity
{
	TextviewClass mImageViewWatch;
	ButtonClass mButtonExit;
	ButtonClass mButtonNext;
	ListView mListView;

	SharedPreferences mSharedPreferenceSplash;
	Editor mEditorSplash;

	boolean mBooleanSplashFlag = true;

	ProgressDialog mProcessDialog;
	private Handler mHandlers;

	private static final long SCAN_PERIOD = 3000;
	private DeviceAdapter deviceAdapter;

	static List<String> deviceList = new ArrayList<String>();

	private String c1;
	private String c2;
	private String c3;

	private String countries[] = null;
	TextviewClass splash_active_txt_bld;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.splash_main_watch);

		mImageViewWatch = (TextviewClass)findViewById(R.id.btnSearch);
		mButtonExit = (ButtonClass)findViewById(R.id.button_exit_setup);
		mButtonNext = (ButtonClass)findViewById(R.id.button_alert_setup);

		splash_active_txt_bld = (TextviewClass)findViewById(R.id.splash_active_txt_bld);

		splash_active_txt_bld.setText(Html.fromHtml("Tap <b>ACTIVATE</b> below to initiate"));

		mListView = (ListView)findViewById(R.id.main_devicesListview);

		mHandlers = new Handler();

		countries = new String[]
				{ "Camera", "Play/Pause Music", "Find Phone", "Next Song", "Off"};

		showDefaultWin("C1");
		showDefaultWin("C2");
		showDefaultWin("C3");

		mImageViewWatch.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				scanLeDevice(true);	
			}
		});

		mButtonExit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {

				final Dialog mDialog = new Dialog(SplashActivity.this);
				mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				mDialog.setCancelable(false);
				mDialog.setContentView(R.layout.splash_dialog);
				mDialog.show();
				ButtonClass ok = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_okbutton);
				ButtonClass cancel = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_cancelbutton);

				ok.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value),true);
						mEditorSplash.commit();
						Intent mIntent = new Intent(SplashActivity.this,Home_Activity.class);
						startActivity(mIntent);
						finish();
					}
				});

				cancel.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value),false);
						mEditorSplash.commit();
						Intent mIntent = new Intent(SplashActivity.this,Home_Activity.class);
						startActivity(mIntent);
						finish();
					}
				});
			}
		});

		mButtonNext.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				Intent mIntent = new Intent(SplashActivity.this,SplashControlActivity.class);
				startActivity(mIntent);
				finish();
			}
		});

		mListView = (ListView) findViewById(R.id.main_devicesListview);
		deviceAdapter = new DeviceAdapter();
		mListView.setAdapter(deviceAdapter);
		mListView.setOnItemClickListener(mDeviceClickListener);

		if (deviceList.isEmpty())
		{
			if ((DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTED) ||
					(DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTING))
			{
				deviceList.add(DemoApplication.mBluetoothDevice);
			}
			else
				scanLeDevice(true);
		}
	}

	class ViewHolder
	{
		TextviewClass mTextViewName;
		TextviewClass mTextViewConnected;
		ImageView mImageViewConnectedImage;
		TextviewClass mButtonConnectDisconnect;
	}

	public class DeviceAdapter extends BaseAdapter
	{
		ViewHolder mViewHolder;

		public DeviceAdapter()
		{}

		@Override
		public int getCount() 
		{
			return deviceList.size();
		}

		public void clear() 
		{
			deviceList.clear();
		}

		@Override 
		public Object getItem(int position) 
		{
			return deviceList.get(position);
		}

		@Override
		public void notifyDataSetChanged() 
		{
			super.notifyDataSetChanged();
			ChangeAdapterSize();
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}

		public void ChangeAdapterSize()
		{
			float scale = getResources().getDisplayMetrics().density;
			ListView mListView = (ListView)   findViewById(R.id.main_devicesListview);
			android.widget.RelativeLayout.LayoutParams list2 = (android.widget.RelativeLayout.LayoutParams) mListView.getLayoutParams();
			list2.height = (int) Math.ceil((float)deviceList.size() * 106.f * scale);
			mListView.setLayoutParams(list2);
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent)
		{
			if (getCount() == 0)
			{
				return null;
			}

			if(convertView == null)
			{
				mViewHolder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.device_element, null);

				mViewHolder.mTextViewName				= (TextviewClass) convertView.findViewById(R.id.name);
				mViewHolder.mTextViewConnected			= (TextviewClass) convertView.findViewById(R.id.pairedtxt);
				mViewHolder.mImageViewConnectedImage	= (ImageView) convertView.findViewById(R.id.paired);

				mViewHolder.mButtonConnectDisconnect	= (TextviewClass)convertView.findViewById(R.id.btnConnectDisconnect);
				convertView.setTag(mViewHolder);
			} 
			else
			{
				mViewHolder = (ViewHolder) convertView.getTag();
			}

			ChangeAdapterSize();

			String DeviceAddress = deviceList.get(position);

			mViewHolder.mTextViewName.setText("Shark Tooth");

			switch (DemoApplication.mConnectionState)
			{
			case DemoApplication.STATE_CONNECTED:
				if (DeviceAddress.equals(DemoApplication.mBluetoothDevice))
				{
					mViewHolder.mButtonConnectDisconnect.setText("DISCONNECT");
					mViewHolder.mTextViewConnected.setVisibility(View.VISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.VISIBLE);
				}
				else
				{
					mViewHolder.mButtonConnectDisconnect.setText("CONNECT");
					mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				}
				break;
			case DemoApplication.STATE_CONNECTING:
				if (DeviceAddress.equals(DemoApplication.mBluetoothDevice))
				{
					mViewHolder.mButtonConnectDisconnect.setText("CONNECTING");
					mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				}
				else
				{
					mViewHolder.mButtonConnectDisconnect.setText("CONNECT");
					mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				}
				break;
			case DemoApplication.STATE_DISCONNECTED:
				mViewHolder.mButtonConnectDisconnect.setText("CONNECT");
				mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
				mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				break;
			}
			return convertView;
		}
	}

	private OnItemClickListener mDeviceClickListener = new OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id)
		{
			String device = deviceList.get(position);

			if (DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTED)
			{
				String DeviceAddress = device;
				if (DeviceAddress.equals(DemoApplication.mBluetoothDevice))
				{
					mBluetoothLeService.disconnect();
					DemoApplication.mBluetoothDevice = null;

					deviceAdapter.clear();
					deviceAdapter.notifyDataSetChanged();
				}
			}
			else
			{
				//Note: This cancels the progressdialog, that's why this needs to be before showing the new one
				scanLeDevice(false);

				mProcessDialog = ProgressDialog.show(SplashActivity.this, "Please wait...", "Connecting...",true,true);

				DemoApplication.mBluetoothDevice = device;

				//Clear all other devices
				int NumDevices = deviceAdapter.getCount(); //Note, need to do this here, because in the loop this value changes due to deletion
				for (int i = NumDevices-1;i>=0;i--)
				{
					if (!deviceList.get(i).equals(DemoApplication.mBluetoothDevice))
						deviceList.remove(i);
				}

				deviceAdapter.notifyDataSetChanged();

				mBluetoothLeService.connect(DemoApplication.mBluetoothDevice,true);
			}
		}
	};

	@SuppressWarnings("deprecation")
	private synchronized void scanLeDevice(final boolean enable) 
	{
		if (enable) 
		{
			mProcessDialog = ProgressDialog.show(SplashActivity.this, "Please wait...", "Looking for watches...",true,true);
			// Stops scanning after a pre-defined scan period.
			mHandlers.postDelayed(new Runnable() 
			{
				@Override
				public void run() 
				{
					try 
					{
						if(mProcessDialog != null)
							mProcessDialog.dismiss();
					}
					catch (Exception e) 
					{
						e.printStackTrace();
					}

					mBluetoothAdapter.stopLeScan(mLeScanCallback);
				}
			}, SCAN_PERIOD);

			mBluetoothAdapter.startLeScan(mLeScanCallback);
		} 
		else 
		{
			if (mProcessDialog != null)// Note: this is needed, otherwise we will get a nullpointer OnDestroy, in cases where we entered the page, but never created the Progress Dialog
				mProcessDialog.dismiss();
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}  
	public void connected()
	{
		// Stops scanning after a pre-defined scan period.
		mHandlers.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				if(mProcessDialog != null && mProcessDialog.isShowing())
				{
					mProcessDialog.dismiss();
				}
				deviceAdapter.notifyDataSetChanged();
			}
		}, SCAN_PERIOD);

		if (deviceList.isEmpty())
		{
			if ((DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTED) ||
					(DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTING))
			{
				deviceList.add(DemoApplication.mBluetoothDevice);
			}
		}

		deviceAdapter.notifyDataSetChanged();
	}

	public void connecting()
	{
		if (deviceList.isEmpty())
		{
			if ((DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTED) || 
					(DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTING))
			{
				deviceList.add(DemoApplication.mBluetoothDevice);
			}
		}
		deviceAdapter.notifyDataSetChanged();
	}

	public void disconnect()
	{
		if (mProcessDialog != null)
			mProcessDialog.dismiss();

		try
		{
			if(deviceAdapter != null && deviceAdapter.getCount() > 0)
			{
				deviceList.clear();
				deviceAdapter.notifyDataSetChanged();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback()
	{
		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) 
		{
			runOnUiThread(new Runnable() 
			{
				@Override
				public void run() 
				{
					if(device != null)
					{
						String DeviceName = device.getName();

						try
						{
							if (DeviceName.equals("Geneva BLE Watch"))
							{
								String DeviceAddress = device.getAddress();
								boolean DevicePresent = false;

								for (int i=0; i<deviceList.size(); i++) 
								{
									if (deviceList.get(i).equals(DeviceAddress))
										DevicePresent = true;
								}

								if(!DevicePresent) 
								{
									deviceList.add(DeviceAddress);
								}

								deviceAdapter.notifyDataSetChanged();
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}	
				}
			});
		}
	};		

	protected void onResume() {
		super.onResume();
		mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
		mBooleanSplashFlag = mSharedPreferenceSplash.getBoolean(getResources().getString(R.string.splash_flag_value), true);

		if(!mBooleanSplashFlag)
		{
			Intent mIntent = new Intent(SplashActivity.this,Home_Activity.class);
			startActivity(mIntent);
			finish();
		} 
	};

	private void showDefaultWin(final String flag)
	{
		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0]);
		c2 = sharedata.getString("C2", countries[1]);
		c3 = sharedata.getString("C3", countries[2]);
		if (flag.equals("C1"))
		{
			SharedPreferences.Editor sharedata1 = getSharedPreferences("control", 0).edit();
			sharedata1.putString("C1", c1);
			sharedata1.commit();
		}
		else if (flag.equals("C2"))
		{
			SharedPreferences.Editor sharedata2 = getSharedPreferences("control", 0).edit();
			sharedata2.putString("C2", c2);
			sharedata2.commit();

		}
		else if (flag.equals("C3"))
		{
			SharedPreferences.Editor sharedata3 = getSharedPreferences("control", 0).edit();
			sharedata3.putString("C3", c3);
			sharedata3.commit();
		}

		if (flag.equals("C1"))
		{
			c1 = countries[0];
		}
		else if (flag.equals("C2"))
		{
			c2 = countries[1];
		}
		else if (flag.equals("C3"))
		{
			c3 = countries[2];
		}

		if (flag.equals("C1"))
		{
			setPic(c1);
		}
		else if (flag.equals("C2"))
		{
			setPic(c2);
		}
		else if (flag.equals("C3"))
		{
			setPic(c3);
		}		
	}

	private int setPic(String title)
	{
		for (int i = 0; i < countries.length; i++)
		{
			if (title.equals(countries[i]))
			{
				return i;
			}
		}
		return 0;
	}
}
