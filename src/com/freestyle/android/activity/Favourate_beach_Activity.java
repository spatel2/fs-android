package com.freestyle.android.activity;

import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dragdrop.listview.DragSortListView;
import com.dragdrop.listview.DragSortListView.DropListener;
import com.dragdrop.listview.DragSortListView.RemoveListener;
import com.freestyle.android.R;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.DataHolder;
import com.freestyle.android.config.DatabaseConnectionAPI;
import com.freestyle.android.config.FavoriteData;
import com.freestyle.android.config.GetAllData;
import com.freestyle.android.config.PostParseGet;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.config.TextviewClass;

@SuppressLint("InflateParams")
public class Favourate_beach_Activity extends BaseActivity implements CoreFrame, OnClickListener
{
	RelativeLayout favourate_add_beach_img;
	DragSortListView mListViewFavoriteBeach;
	Context mContext;
	Intent mIntent;

	GetAllData mGetAllData;
	FavoriteListAdpter mFavoriteListAdpter;
	private DatabaseConnectionAPI mDatabaseConnectionAPI;
	private DataHolder mDataHolder;
	private Cursor mCursor;
	ArrayList<FavoriteData> mArrayListFavoriteData;
	ArrayList<FavoriteData> mArrayListSpots = new ArrayList<FavoriteData>();
	Shared_preference mSharedPreferenceFavorite;
	boolean Fav_Flag = false;
	ProgressDialog mProgressDialogFindBeach;
	PostParseGet mPostParseGet;
	String country_id="",region_id="",subregion_id="",beach_id="";
	String result_surf="";
	Surfline mSurfline;
	boolean isMetricTemp	=	false;

	Shared_preference mPreference;
	String isFromLocation = "false";
	TextviewClass mTextviewBackToLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favourate_beach_activity);

		mContext = Favourate_beach_Activity.this;
		mSharedPreferenceFavorite = new Shared_preference(Favourate_beach_Activity.this);

		mPreference = new Shared_preference(mContext);
		isFromLocation = mPreference.getPrefrencewithDefault(mContext, "back_to_location", "false");

		mDataHolder = new DataHolder();
		mDatabaseConnectionAPI=new DatabaseConnectionAPI(Favourate_beach_Activity.this);
		try
		{
			mDatabaseConnectionAPI.createDataBase();
			mDatabaseConnectionAPI.getWritableDatabase();
			mDatabaseConnectionAPI.openDataBase();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		initDrawer(Favourate_beach_Activity.this);
		mListViewFavoriteBeach = (DragSortListView)findViewById(R.id.favourate_beach_list);

		mListViewFavoriteBeach.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) 
			{
				isFromLocation = "false";
				mPreference.setPrefrence(mContext, "back_to_location", "false");
				Intent intent = new Intent(Favourate_beach_Activity.this, Nearest_beaches_fragment.class);
				intent.putExtra("country_id", mArrayListFavoriteData.get(position).getCountry_Id());
				intent.putExtra("region_id", mArrayListFavoriteData.get(position).getRegion_Id());	
				intent.putExtra("subregion_id", mArrayListFavoriteData.get(position).getSub_Region_Id());
				intent.putExtra("beach_id", mArrayListFavoriteData.get(position).getSpot_Id());
				startActivity(intent);
				finish();
			}
		});

		View mView = (View)getLayoutInflater().inflate(R.layout.fav_list_footer, null);
		mListViewFavoriteBeach.addFooterView(mView);

		if(shared.getPrefrencewithDefault(context, "unit_imperial_metric","false").toString().equals("true"))
		{
			isMetricTemp = true;
		}
		else
		{
			isMetricTemp = false;
		}

		mFavoriteListAdpter = new FavoriteListAdpter();
		mListViewFavoriteBeach.setAdapter(mFavoriteListAdpter);

		mTextviewBackToLocation = (TextviewClass)mView.findViewById(R.id.back_to_location);
		favourate_add_beach_img = (RelativeLayout)mView.findViewById(R.id.favourate_add_beach_img);

		favourate_add_beach_img.setOnClickListener(this);
		mTextviewBackToLocation.setOnClickListener(this);

		if(isFromLocation.equalsIgnoreCase("true"))
		{
			mTextviewBackToLocation.setVisibility(View.VISIBLE);
		}
		else
		{
			mTextviewBackToLocation.setVisibility(View.GONE);
		}

		mGetAllData = DemoApplication.mGetAllData;

		mPostParseGet=new PostParseGet(Favourate_beach_Activity.this);

		Fav_Flag = Boolean.parseBoolean(mSharedPreferenceFavorite.getPrefrence(Favourate_beach_Activity.this, "favorite_data"));

		if(Fav_Flag)
		{
			mGetAllData = new GetAllData();

			if (mGetAllData.getData() == null) 
			{
				new GetAllDataProcess().execute();
			} 
		}
	}

	@Override
	public void onBackPressed() 
	{
		goBack("back");
	}

	@Override
	public void goBack(String target) 
	{
		isFromLocation = "false";
		mPreference.setPrefrence(mContext, "back_to_location", "false");
		mIntent = new Intent(mContext, Home_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target)
	{
		if(target.equals("find_beach"))
		{
			isFromLocation = "false";
			mPreference.setPrefrence(mContext, "back_to_location", "false");
			mIntent = new Intent(mContext, FindBeach_Activity.class);
			startActivity(mIntent);
			finish();
		}
	}

	@Override
	public void setTypefaceControls() 
	{
	}

	public void getSpotData()
	{
		if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
		{
			mArrayListSpots = new ArrayList<FavoriteData>();

			for (int f = 0; f < mArrayListFavoriteData.size(); f++) 
			{
				for (int i = 0; i < mGetAllData.getData().size(); i++) 
				{
					if(mGetAllData.getData().get(i).getArea().getId().equalsIgnoreCase(mArrayListFavoriteData.get(f).getCountry_Id()))
					{
						for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) {

							if(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId().equalsIgnoreCase(mArrayListFavoriteData.get(f).getRegion_Id()))
							{
								for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) {

									if(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId().equalsIgnoreCase(mArrayListFavoriteData.get(f).getSub_Region_Id()))
									{
										for (int m = 0; m < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().size(); m++) 
										{
											if(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId().equalsIgnoreCase(mArrayListFavoriteData.get(f).getSpot_Id()))
											{
												FavoriteData mFavoriteData = new FavoriteData();
												mFavoriteData.setCountry_Id(mGetAllData.getData().get(i).getArea().getId());
												mFavoriteData.setRegion_Id(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId());
												mFavoriteData.setSub_Region_Id(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId());
												mFavoriteData.setSpot_Id(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId());
												mFavoriteData.setSpotName(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getTitle());
												mFavoriteData.setSub_Region_Name(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getName());
												mArrayListSpots.add(mFavoriteData);
											}
										}
										mFavoriteListAdpter = new FavoriteListAdpter();
										mListViewFavoriteBeach.setAdapter(mFavoriteListAdpter);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void getData()
	{
		mCursor = mDatabaseConnectionAPI.onQueryGetCursor(Constants.TableNames.FAVORITE, null,null,null, null, null, null);

		if (mCursor != null) 
		{
			if (mCursor.getCount() >0) 
			{
				mDataHolder = mDatabaseConnectionAPI.read(mCursor);
				mArrayListFavoriteData = new ArrayList<FavoriteData>();
				mListViewFavoriteBeach.setVisibility(View.VISIBLE);
				for (int i = 0; i < mDataHolder.getmRow().size(); i++)
				{
					FavoriteData mData = new FavoriteData();
					mData.setSpot_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.SPOT_ID));
					mData.setSub_Region_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.SUB_REGION_ID));
					mData.setRegion_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.REGION_ID));
					mData.setCountry_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.COUNTRY_ID));
					mArrayListFavoriteData.add(mData);
				}
				getSpotData();
			}
			else
			{
				mFavoriteListAdpter = new FavoriteListAdpter();
				mListViewFavoriteBeach.setAdapter(mFavoriteListAdpter);
			}
		}
		else
		{
			mFavoriteListAdpter = new FavoriteListAdpter();
			mListViewFavoriteBeach.setAdapter(mFavoriteListAdpter);
		}
	}

	public class FavoriteListAdpter extends BaseAdapter implements DropListener,RemoveListener
	{
		ViewHolder mViewHolder;

		@Override
		public int getCount() 
		{
			return mArrayListSpots.size();
		}

		@Override 
		public Object getItem(int position) 
		{
			return mArrayListSpots.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return position;
		}

		@Override
		public boolean isEmpty() {
			return false;
		}

		@SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null)
			{
				mViewHolder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.infl_favourate_beach_single, null);
				mViewHolder.mTextViewTiltle = (TextviewClass) convertView.findViewById(R.id.fav_beach_name_single);
				mViewHolder.mTextviewSubTitle = (TextviewClass) convertView.findViewById(R.id.fav_beach_surf_single);
				mViewHolder.infl_fav_home_img_single = (ImageView)convertView.findViewById(R.id.infl_fav_home_img_single);
				mViewHolder.infl_fav_remove_img_single = (ImageView)convertView.findViewById(R.id.infl_fav_remove_img_single);
				convertView.setTag(mViewHolder);
			} 
			else
			{
				mViewHolder = (ViewHolder) convertView.getTag();
			}


			if(position == 0)
			{
				mViewHolder.mTextViewTiltle.setTextColor(getResources().getColor(R.color.btn_blue));
				mViewHolder.mTextviewSubTitle.setTextColor(getResources().getColor(R.color.btn_blue));
				mViewHolder.infl_fav_home_img_single.setImageResource(R.drawable.home_icon_fs);

				mSharedPreferenceFavorite.setPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID, mArrayListSpots.get(position).getSpot_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID,mArrayListSpots.get(position).getCountry_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID,mArrayListSpots.get(position).getRegion_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID,mArrayListSpots.get(position).getSub_Region_Id());

				getData();

			}

			if(mSharedPreferenceFavorite != null && mSharedPreferenceFavorite.getPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID) != null)
			{
				String mstrbeachid = mSharedPreferenceFavorite.getPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID);

				if(mDataHolder.getmRow().get(position).get(Constants.FAVORITE_FIELDS.SPOT_ID).equalsIgnoreCase(mstrbeachid))
				{
					mViewHolder.infl_fav_home_img_single.setImageResource(R.drawable.home_icon_fs);
				}
				else
				{
					mViewHolder.infl_fav_home_img_single.setImageResource(R.drawable.home_icon_fs_hover);
				}
			}

			mViewHolder.mTextViewTiltle.setText(mArrayListSpots.get(position).getSpotName());
			mViewHolder.mTextviewSubTitle.setText(mArrayListSpots.get(position).getSub_Region_Name());

			mViewHolder.infl_fav_home_img_single.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					mSharedPreferenceFavorite.setPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID, mArrayListSpots.get(position).getSpot_Id());
					mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID,mArrayListSpots.get(position).getCountry_Id());
					mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID,mArrayListSpots.get(position).getRegion_Id());
					mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID,mArrayListSpots.get(position).getSub_Region_Id());

					//					Toast.makeText(context, Constants.ERROR.HOME_BEACH_SELECTED, Toast.LENGTH_SHORT).show();

					mFavoriteListAdpter = new FavoriteListAdpter();
					mListViewFavoriteBeach.setAdapter(mFavoriteListAdpter);

					FavoriteData mData= mArrayListSpots.get(position);
					mFavoriteListAdpter.notifyDataSetChanged();
					mArrayListSpots.remove(mArrayListSpots.get(position));
					mArrayListSpots.add(0,mData);
					mFavoriteListAdpter.notifyDataSetChanged();

					mDatabaseConnectionAPI.deleteRecord(Constants.TableNames.FAVORITE, null, null);

					for (int i = 0; i < mArrayListSpots.size(); i++) 
					{
						ContentValues mContentValues=new ContentValues();
						mContentValues.put(Constants.FAVORITE_FIELDS.SPOT_ID,mArrayListSpots.get(i).getSpot_Id());
						mContentValues.put(Constants.FAVORITE_FIELDS.SUB_REGION_ID,mArrayListSpots.get(i).getSub_Region_Id());
						mContentValues.put(Constants.FAVORITE_FIELDS.REGION_ID,mArrayListSpots.get(i).getRegion_Id());
						mContentValues.put(Constants.FAVORITE_FIELDS.COUNTRY_ID,mArrayListSpots.get(i).getCountry_Id());
						mDatabaseConnectionAPI.insertRecord(Constants.TableNames.FAVORITE, mContentValues);
					}
					getData();

					if(mCommonClass.CheckNetwork(context))
					{
						NearestBeachDetails_Async mNearestBeachDetails_Async = new NearestBeachDetails_Async();
						mNearestBeachDetails_Async.execute();
					}
					else
					{
						Toast.makeText(context, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
					}
				}
			});

			mViewHolder.infl_fav_remove_img_single.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					if(mArrayListSpots.size() == 1)
					{
						shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_ID, "");
					}
					mArrayListSpots.remove(position);

					mDatabaseConnectionAPI.deleteFavourateRecord(Constants.TableNames.FAVORITE, Constants.FAVORITE_FIELDS.SPOT_ID+"="+mDataHolder.getmRow().get(position).get(Constants.FAVORITE_FIELDS.SPOT_ID));

					getData();

					if(position != 0)
					{
						mSharedPreferenceFavorite.setPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID, mArrayListSpots.get(0).getSpot_Id());
						mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID,mArrayListSpots.get(0).getCountry_Id());
						mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID,mArrayListSpots.get(0).getRegion_Id());
						mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID,mArrayListSpots.get(0).getSub_Region_Id());
						//						Toast.makeText(context, Constants.ERROR.HOME_BEACH_SELECTED, Toast.LENGTH_SHORT).show();
						mFavoriteListAdpter = new FavoriteListAdpter();
						mListViewFavoriteBeach.setAdapter(mFavoriteListAdpter);

						getData();

						if(mCommonClass.CheckNetwork(context))
						{
							NearestBeachDetails_Async mNearestBeachDetails_Async = new NearestBeachDetails_Async();
							mNearestBeachDetails_Async.execute();
						}
						else
						{
							Toast.makeText(context,  getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
						}
					}
					//					Toast.makeText(context, Constants.ERROR.REMOVE_BEACH, Toast.LENGTH_SHORT).show();
				}
			});

			if(mArrayListSpots.size() == 1)
			{
				mSharedPreferenceFavorite.setPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID, mArrayListSpots.get(0).getSpot_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID,mArrayListSpots.get(0).getCountry_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID,mArrayListSpots.get(0).getRegion_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID,mArrayListSpots.get(0).getSub_Region_Id());

				if(mCommonClass.CheckNetwork(context))
				{
					NearestBeachDetails_Async mNearestBeachDetails_Async = new NearestBeachDetails_Async();
					mNearestBeachDetails_Async.execute();
				}
				else
				{
					Toast.makeText(context, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
				}
			}

			return convertView;
		}

		@Override
		public void drop(int from, int to) 
		{
			if(to == 0)
			{
				FavoriteData mData= mArrayListSpots.get(from);
				mFavoriteListAdpter.notifyDataSetChanged();
				mArrayListSpots.remove(mArrayListSpots.get(from));
				mArrayListSpots.add(to,mData);
				mFavoriteListAdpter.notifyDataSetChanged();

				mDatabaseConnectionAPI.deleteRecord(Constants.TableNames.FAVORITE, null, null);

				for (int i = 0; i < mArrayListSpots.size(); i++) 
				{
					ContentValues mContentValues=new ContentValues();
					mContentValues.put(Constants.FAVORITE_FIELDS.SPOT_ID,mArrayListSpots.get(i).getSpot_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.SUB_REGION_ID,mArrayListSpots.get(i).getSub_Region_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.REGION_ID,mArrayListSpots.get(i).getRegion_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.COUNTRY_ID,mArrayListSpots.get(i).getCountry_Id());
					mDatabaseConnectionAPI.insertRecord(Constants.TableNames.FAVORITE, mContentValues);
				}
				getData();

				mSharedPreferenceFavorite.setPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID, mArrayListSpots.get(to).getSpot_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID,mArrayListSpots.get(to).getCountry_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID,mArrayListSpots.get(to).getRegion_Id());
				mSharedPreferenceFavorite.setPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID,mArrayListSpots.get(to).getSub_Region_Id());

				//				Toast.makeText(context, Constants.ERROR.HOME_BEACH_SELECTED, Toast.LENGTH_SHORT).show();

				mFavoriteListAdpter = new FavoriteListAdpter();
				mListViewFavoriteBeach.setAdapter(mFavoriteListAdpter);

				FavoriteData mData1= mArrayListSpots.get(to);
				mFavoriteListAdpter.notifyDataSetChanged();
				mArrayListSpots.remove(mArrayListSpots.get(to));
				mArrayListSpots.add(0,mData1);
				mFavoriteListAdpter.notifyDataSetChanged();

				mDatabaseConnectionAPI.deleteRecord(Constants.TableNames.FAVORITE, null, null);

				for (int i = 0; i < mArrayListSpots.size(); i++) 
				{
					ContentValues mContentValues=new ContentValues();
					mContentValues.put(Constants.FAVORITE_FIELDS.SPOT_ID,mArrayListSpots.get(i).getSpot_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.SUB_REGION_ID,mArrayListSpots.get(i).getSub_Region_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.REGION_ID,mArrayListSpots.get(i).getRegion_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.COUNTRY_ID,mArrayListSpots.get(i).getCountry_Id());
					mDatabaseConnectionAPI.insertRecord(Constants.TableNames.FAVORITE, mContentValues);
				}
				getData();

				if(mCommonClass.CheckNetwork(context))
				{
					NearestBeachDetails_Async mNearestBeachDetails_Async = new NearestBeachDetails_Async();
					mNearestBeachDetails_Async.execute();
				}
				else
				{
					Toast.makeText(context,  getResources().getString(R.string.error_internet),  Toast.LENGTH_SHORT).show();
				}
			}
			else
			{
				FavoriteData mData= mArrayListSpots.get(from);
				mFavoriteListAdpter.notifyDataSetChanged();
				mArrayListSpots.remove(mArrayListSpots.get(from));
				mArrayListSpots.add(to,mData);
				mFavoriteListAdpter.notifyDataSetChanged();

				mDatabaseConnectionAPI.deleteRecord(Constants.TableNames.FAVORITE, null, null);

				for (int i = 0; i < mArrayListSpots.size(); i++) 
				{
					ContentValues mContentValues=new ContentValues();
					mContentValues.put(Constants.FAVORITE_FIELDS.SPOT_ID,mArrayListSpots.get(i).getSpot_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.SUB_REGION_ID,mArrayListSpots.get(i).getSub_Region_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.REGION_ID,mArrayListSpots.get(i).getRegion_Id());
					mContentValues.put(Constants.FAVORITE_FIELDS.COUNTRY_ID,mArrayListSpots.get(i).getCountry_Id());
					mDatabaseConnectionAPI.insertRecord(Constants.TableNames.FAVORITE, mContentValues);
				}
				getData();
			}
		}

		@Override
		public void remove(int which) 
		{
			mArrayListSpots.remove(mArrayListSpots.get(which));
		}
	}

	class ViewHolder
	{
		TextviewClass mTextViewTiltle;
		TextviewClass mTextviewSubTitle;
		ImageView infl_fav_home_img_single,infl_fav_remove_img_single;
	}

	@Override
	public void onClick(View v)
	{
		if(v==favourate_add_beach_img)
		{
			goForward("find_beach");
		}
		else if(v==mTextviewBackToLocation)
		{
			Intent intent = new Intent(context, Region3_Activity.class);

			String mCountryid = mPreference.getPrefrence(mContext, "COUNTRY_ID");
			String mRegionid = mPreference.getPrefrence(mContext, "REGION_ID");
			String mSubRegionid = mPreference.getPrefrence(mContext, "SUB_REGION_ID");

			intent.putExtra("country_id", mCountryid);
			intent.putExtra("region_id", mRegionid);	
			intent.putExtra("subregion_id", mSubRegionid);
			startActivity(intent);
			finish();
		}
	}

	@Override
	public void gotoSurf() 
	{
	}

	public class GetAllDataProcess extends AsyncTask<Void, Void, Void> 
	{
		@Override
		protected void onPreExecute() 
		{
			mProgressDialogFindBeach = ProgressDialog.show(Favourate_beach_Activity.this,"", "Loading...", false);
			mProgressDialogFindBeach.setCancelable(false);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			mGetAllData = (GetAllData) mPostParseGet.GetAllData(mGetAllData);
			return null;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Void result)
		{			

			if (mProgressDialogFindBeach != null)
			{
				mProgressDialogFindBeach.dismiss();
			}
			if (mPostParseGet.isNetError)
			{
				showAlert(Favourate_beach_Activity.this, getResources().getString(R.string.error_internet), getString(R.string.ok));
			}
			else if (mPostParseGet.isOtherError)
			{
				showAlert(Favourate_beach_Activity.this, getResources().getString(R.string.error_response), getString(R.string.ok));
			}
			else 
			{
				if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
				{
					DemoApplication.mGetAllData = mGetAllData;
					getData();
				}
			}
			super.onPostExecute(result);
		}
	}

	public void showAlert(Activity mActivity, String message, String buttonMessage)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(message);
		builder.setNegativeButton( buttonMessage,new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.cancel();
			}
		});
		builder.show();
	}

	@SuppressLint("DefaultLocale")
	public class NearestBeachDetails_Async extends AsyncTask<Void, Void, String> 
	{
		ProgressDialog mProgressDialogBeach;
		@Override
		protected void onPreExecute() 
		{
			Log.i("Beach ID: ", beach_id+"");
			mProgressDialogBeach = ProgressDialog.show(context, "Loading", "Please wait");
			mProgressDialogBeach.show();
			super.onPreExecute();
		}
		@Override
		protected synchronized String doInBackground(Void... params) 
		{
			beach_id = mSharedPreferenceFavorite.getPrefrence(context, Constants.PREFRENCE.HOME_BEACH_ID);
			String strURL = "";
			if(isMetricTemp)
			{
				strURL = com.freestyle.android.config.Constants.URL.FORECAST_URL+beach_id+"?package=partner&units=m&user_key=abca50124d77d41504c4d53e29c8d359";
			}
			else
			{
				strURL = com.freestyle.android.config.Constants.URL.FORECAST_URL+beach_id+"?package=partner&user_key=abca50124d77d41504c4d53e29c8d359";
			}
			return mCommonClass.GetConnection(strURL);
		}
		protected void onPostExecute(String result) 
		{
			mProgressDialogBeach.dismiss();
			Log.i("Result", result);
			result_surf = result;
			try
			{
				SendDatatoWatch mSendDatatoWatch = new SendDatatoWatch();
				mSendDatatoWatch.execute();
			}
			catch (Exception e)
			{
				System.out.println(e+"");
			}
		}
	}

	public class SendDatatoWatch extends AsyncTask<Void, Void, String> 
	{
		boolean isOfline=false;
		@Override
		protected void onPreExecute()
		{
			if(mCommonClass.CheckNetwork(context))
			{
				isOfline = false;
			}
			else
			{
				isOfline = true;
			}
			mSurfline = new Surfline(mBluetoothLeService);
			super.onPreExecute();
		}

		@Override
		protected synchronized String doInBackground(Void... params) 
		{
			mSurfline.UpdateData(isMetricTemp, Integer.parseInt(beach_id), false,result_surf,context);
			return "";
		}
		@Override																																																																							
		protected void onPostExecute(String result) 
		{
			super.onPostExecute(result);
		}
	}
}