package com.freestyle.android.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.freestyle.android.R;
import com.freestyle.android.config.Constants;

public class ActivityLog_Activity extends BaseActivity
{
	Context context;
	Intent mIntent;
	ListView activity_log_listview;
	private List<DbTool.TrackInfo> TrackList;
	private ListViewAdapter adapter;
	private DbTool db;

	ArrayList<String> activity_date_arry = new ArrayList<String>();
	ArrayList<String> activity_time_arry = new ArrayList<String>();
	ArrayList<String> activity_name_arry = new ArrayList<String>();
	ArrayList<String> activity_lat_arry = new ArrayList<String>();
	ArrayList<String> activity_long_arry = new ArrayList<String>();

	//For preference of imperial and metric
	boolean isMetricTemp			=	false;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitylog_activity);

		context = ActivityLog_Activity.this;
		db = new DbTool(this);
		TrackList = db.GetAllTracks();
		initDrawer(ActivityLog_Activity.this);

		if(shared.getPrefrencewithDefault(context, Constants.PROFILE.UNIT_IMPERIAL_METRIC,"false").toString().equals("true"))
		{
			isMetricTemp = true;
		}


		adapter = new ListViewAdapter(this, TrackList);

		activity_log_listview = (ListView)findViewById(R.id.activity_log_listview);
		activity_log_listview.setAdapter(adapter);
	}

	class ListViewAdapter extends BaseAdapter
	{
		private LayoutInflater mInflater;
		private List<DbTool.TrackInfo> list;

		public ListViewAdapter(Context context, List<DbTool.TrackInfo> list)
		{
			this.mInflater = LayoutInflater.from(context);
			this.list = list;
		}
		@Override
		public int getCount()
		{
			return list.size();
		}
		public void removeItem(int position)
		{
			list.remove(position);
		}
		@Override
		public Object getItem(int arg0)
		{
			return list.get(arg0);
		}
		@Override
		public long getItemId(int position)
		{
			return 0;
		}
		@SuppressLint("InflateParams") 
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView	= mInflater.inflate(R.layout.infl_activity_log, null);
			}
			TextView log_distance_single 			= (TextView)	convertView.findViewById(R.id.log_distance_single);
			TextView log_date_single 				= (TextView)	convertView.findViewById(R.id.log_date_single);
			TextView log_name_single				= (TextView)	convertView.findViewById(R.id.log_name_single);
			RelativeLayout activity_log_rel_single 	= (RelativeLayout)convertView.findViewById(R.id.activity_log_rel_single);

			final DbTool.TrackInfo track = list.get(position);
			log_date_single.setText(TrackList.get(position).track_date);
			log_name_single.setText(getSportNameById(track.Type));


			if(isMetricTemp)
			{
				if(TrackList.get(position).track_unit.equals("m"))
				{
					log_distance_single.setText(convertToOneDecimal(TrackList.get(position).Distance)+" KM");
				}
				else
				{
					log_distance_single.setText((convertImperialToMetric(TrackList.get(position).Distance+""))+" KM");
				}
			}
			else
			{
				if(TrackList.get(position).track_unit.equals("m"))
				{
					log_distance_single.setText((convertMetricToImperial(TrackList.get(position).Distance+""))+" MI");
				}
				else
				{
					log_distance_single.setText(convertToOneDecimal(TrackList.get(position).Distance)+" MI");
				}
			}
			
			activity_log_rel_single.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					mIntent = new Intent(context, Activity_log_display.class);
					mIntent.putExtra("sportid", track.ID);
					mIntent.putExtra("sportname", getSportNameById(track.Type));
					startActivity(mIntent);
					finish();
				}
			});
			return convertView;
		}
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		if(null!= db)
		{
			db.close();
		}
	}
	@Override
	public void gotoActivityLog() 
	{
	}
	@Override
	public void onBackPressed() 
	{
		mIntent = new Intent(context, Home_Activity.class);
		startActivity(mIntent);
		finish();
	}

	private String getSportNameById(int sport_id)
	{
		if(sport_id==1)
		{
			return "SURF";
		}
		else if(sport_id==2)
		{
			return "SUP";
		}
		else if(sport_id==3)
		{
			return "SNOWBOARD";
		}
		else if(sport_id==4)
		{
			return "SKIING";
		}
		else if(sport_id==5)
		{
			return "MOUNTAIN BIKE";
		}
		else if(sport_id==6)
		{
			return "ROAD BIKE";
		}
		else if(sport_id==7)
		{
			return "SKATE";
		}
		else if(sport_id==8)
		{
			return "RUN";
		}
		else if(sport_id==9)
		{
			return "WAKEBOARDING";
		}
		else if(sport_id==10)
		{
			return "WAKE SURFING";
		}
		else
		{
			return "Activity";
		}
	}

	private String convertToOneDecimal(float value)
	{
		if(value==0.0)
		{
			return "0.00";
		}
		else
		{
			String val_str = value+"";

			String[] SplitStringcal= val_str.split("\\.");

			if (SplitStringcal[1].length() > 1)
			{
				SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
			}
			return SplitStringcal[0]+"."+SplitStringcal[1];
		}
	}

	private String convertMetricToImperial(String mString)
	{
		if(!mString.equals(""))
		{
			try
			{
				String val_str = ((Float.parseFloat(mString)*60)/100)+"";

				String[] SplitStringcal= val_str.split("\\.");

				if (SplitStringcal[1].length() > 1)
				{
					SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
				}
				return SplitStringcal[0]+"."+SplitStringcal[1];
			}
			catch(Exception ex)
			{
				System.out.println(ex);
				return "0";
			}
		}
		else
		{
			return "0";
		}

	}

	private String convertImperialToMetric(String mString)
	{
		if(!mString.equals(""))
		{
			try
			{
				String val_str = ((Float.parseFloat(mString)*100)/60)+"";

				String[] SplitStringcal= val_str.split("\\.");

				if (SplitStringcal[1].length() > 1)
				{
					SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
				}
				return SplitStringcal[0]+"."+SplitStringcal[1];
			}
			catch(Exception ex)
			{
				System.out.println(ex);
				return "0";
			}
		}
		else
		{
			return "0";
		}

	}

}