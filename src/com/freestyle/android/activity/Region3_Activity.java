package com.freestyle.android.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.freestyle.android.R;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.DataHolder;
import com.freestyle.android.config.DatabaseConnectionAPI;
import com.freestyle.android.config.FavoriteData;
import com.freestyle.android.config.GetAllData;
import com.freestyle.android.config.GetAllData.GetGlobal.GetSpots;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.config.TextviewClass;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class Region3_Activity extends BaseActivity implements OnClickListener,CoreFrame
{
	Context context;
	Bundle mBundle;
	Shared_preference shared;
	String region_id;
	String country_id;
	String sub_region_id;
	CommonClass mCommonClass = new CommonClass();
	PullToRefreshListView mListviewSpots;
	ImageView region3_back_img;

	Intent mIntent;
	ImageView drawer_icon;

	GetAllData mGetAllData;
	ArrayList<GetSpots> mArrayListSpots;
	FavoriteListAdpter mSpotsListAdpter;

	private DatabaseConnectionAPI mDatabaseConnectionAPI;
	private Cursor mCursor;
	ArrayList<FavoriteData> mArrayListFavoriteData;
	private DataHolder mDataHolder;
	boolean flag = true;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.region3_activity);

		context = getApplicationContext();
		shared = new Shared_preference(context);

		region3_back_img = (ImageView)findViewById(R.id.region3_back_img);
		region3_back_img.setOnClickListener(this);

		mDataHolder = new DataHolder();
		mDatabaseConnectionAPI=new DatabaseConnectionAPI(Region3_Activity.this);

		try 
		{
			mDatabaseConnectionAPI.createDataBase();
			mDatabaseConnectionAPI.getWritableDatabase();
			mDatabaseConnectionAPI.openDataBase();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		initDrawer(Region3_Activity.this);
		mListviewSpots = (PullToRefreshListView)findViewById(R.id.region3_listview);

		sub_region_id = getIntent().getStringExtra("subregion_id");
		region_id = getIntent().getStringExtra("region_id");
		country_id = getIntent().getStringExtra("country_id");

		mGetAllData = DemoApplication.mGetAllData;

		getAllData();
	}

	@Override
	public void onBackPressed() 
	{
		goBack("back");
	}

	@Override
	public void goBack(String target) 
	{
		mIntent = new Intent(context, Region2_Activity.class);
		mIntent.putExtra("country_id", country_id);
		mIntent.putExtra("region_id", region_id);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target) 
	{
	}

	@Override
	public void setTypefaceControls() 
	{
	}

	@Override
	public void onClick(View v) 
	{
		if(v==region3_back_img)
		{
			goBack("");
		}
	}

	public class FavoriteListAdpter extends BaseAdapter
	{
		ViewHolder mViewHolder;
		
		@Override
		public int getCount() 
		{
			return mArrayListSpots.size();
		}

		@Override 
		public Object getItem(int position) 
		{
			return mArrayListSpots.get(position);
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}

		@SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null)
			{
				mViewHolder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.infl_location_lay, null);

				mViewHolder.mTextViewTiltle = (TextviewClass) convertView.findViewById(R.id.country_txt_single);
				mViewHolder.mLinearLayout = (LinearLayout) convertView.findViewById(R.id.data_layout);

				convertView.setTag(mViewHolder);
			}
			else 
			{
				mViewHolder = (ViewHolder) convertView.getTag();
			}

			mViewHolder.mTextViewTiltle.setText(mArrayListSpots.get(position).getTitle());

			mViewHolder.mLinearLayout.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{				
					getData();

					if(mArrayListFavoriteData != null && mArrayListFavoriteData.size() > 0)
					{
						for (int i = 0; i < mArrayListFavoriteData.size(); i++) 
						{
							if(mArrayListFavoriteData.get(i).getSpot_Id().equalsIgnoreCase(mArrayListSpots.get(position).getId()))
							{
								flag = false;
							}
						}
					}
					if(!flag)
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(Region3_Activity.this);
						builder.setMessage("Already added as favorite");
						builder.setNegativeButton( "OK",new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								dialog.cancel();
								flag = true;
							}
						});
						builder.show();
					}
					else
					{
						ContentValues mContentValues=new ContentValues();
						mContentValues.put(Constants.FAVORITE_FIELDS.SPOT_ID,mArrayListSpots.get(position).getId());
						mContentValues.put(Constants.FAVORITE_FIELDS.SUB_REGION_ID,sub_region_id);
						mContentValues.put(Constants.FAVORITE_FIELDS.REGION_ID,region_id);
						mContentValues.put(Constants.FAVORITE_FIELDS.COUNTRY_ID,country_id);
						mDatabaseConnectionAPI.insertRecord(Constants.TableNames.FAVORITE, mContentValues);
						
						Shared_preference mPreference = new Shared_preference(Region3_Activity.this);
						mPreference.setPrefrence(Region3_Activity.this, "back_to_location", "true");
						mPreference.setPrefrence(Region3_Activity.this, "SUB_REGION_ID", sub_region_id);
						mPreference.setPrefrence(Region3_Activity.this, "REGION_ID", region_id);
						mPreference.setPrefrence(Region3_Activity.this, "COUNTRY_ID", country_id);
						
						Intent intent = new Intent(context, Favourate_beach_Activity.class);
						startActivity(intent);
						finish();
					}
				}
			});
			return convertView;
		}
	}
	
	class ViewHolder
	{
		LinearLayout mLinearLayout;
		TextviewClass mTextViewTiltle;
	}

	public void getData()
	{

		mCursor = mDatabaseConnectionAPI.onQueryGetCursor(Constants.TableNames.FAVORITE, null,null,null, null, null, null);

		if (mCursor != null) 
		{
			if (mCursor.getCount() >0) 
			{
				mDataHolder = mDatabaseConnectionAPI.read(mCursor);
				mArrayListFavoriteData = new ArrayList<FavoriteData>();

				for (int i = 0; i < mDataHolder.getmRow().size(); i++) 
				{
					FavoriteData mData = new FavoriteData();
					mData.setSpot_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.SPOT_ID));
					mData.setSub_Region_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.SUB_REGION_ID));
					mData.setRegion_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.REGION_ID));
					mData.setCountry_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.COUNTRY_ID));
					mArrayListFavoriteData.add(mData);
				}
			}
		}
	}

	public void getAllData()
	{
		if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
		{
			mArrayListSpots = new ArrayList<GetSpots>();
			for (int i = 0; i < mGetAllData.getData().size(); i++) 
			{
				if(mGetAllData.getData().get(i).getArea().getId().equalsIgnoreCase(country_id))
				{
					for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) 
					{

						if(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId().equalsIgnoreCase(region_id))
						{
							for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) 
							{
								if(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId().equalsIgnoreCase(sub_region_id))
								{
									for (int m = 0; m < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().size(); m++) 
									{
										GetSpots mAreaData= new GetSpots();
										mAreaData.setId(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId());
										mAreaData.setTitle(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getTitle());
										mArrayListSpots.add(mAreaData);
									}
								}
							}
						}
					}
				}
			}
			sortSpots(mArrayListSpots);
			mSpotsListAdpter = new FavoriteListAdpter();
			mListviewSpots.setAdapter(mSpotsListAdpter);
		}
	}
	
	public void sortSpots(List<GetSpots> itemList) 
	{
		if(itemList!=null)
		{
			Collections.sort(itemList, new Comparator<GetSpots>() 
			{
				@Override
				public int compare(GetSpots o1, GetSpots o2) 
				{
					return o1.title.compareTo(o2.title);
				}           
			});
		}
	}
}