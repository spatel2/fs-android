package com.freestyle.android.activity;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.freestyle.android.R;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.test.clsPref;

public class Choose_activity extends BaseActivity implements CoreFrame, OnClickListener
{
	RelativeLayout choose_ski_lay,choose_sup_lay,choose_skate_lay,choose_run_lay;
	TextView beach_name1_txt;
	Context context;
	ProgressDialog mProcessDialog;
	CommonClass mCommonClass = new CommonClass();
	String activity_name_str="";
	Intent mIntent;

	RelativeLayout snow_rel,wake_lay,bike_lay;
	RelativeLayout h_snowboard_rel,h_skiing_rel,h_mountainbike_rel,h_roadbike_rel,h_wakebording_rel,h_wakesurfing_rel;
	
	AlertDialog alert;
	ListView pop_subactivity_list;
	
	private int activity_id = 0;
	ArrayList<String> activity_list = new ArrayList<String>();

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_activity);

		context = Choose_activity.this;
		initDrawer(Choose_activity.this);
		choose_ski_lay = (RelativeLayout)findViewById(R.id.choose_ski_lay);
		choose_sup_lay = (RelativeLayout)findViewById(R.id.choose_sup_lay);

		choose_skate_lay = (RelativeLayout)findViewById(R.id.choose_skete_lay);
		choose_run_lay = (RelativeLayout)findViewById(R.id.choose_run_lay);
		
		snow_rel = (RelativeLayout)findViewById(R.id.snow_rel);
		wake_lay = (RelativeLayout)findViewById(R.id.wake_lay);
		bike_lay = (RelativeLayout)findViewById(R.id.bike_lay); 
		
		h_snowboard_rel = (RelativeLayout)findViewById(R.id.h_snowboard_rel);
		h_skiing_rel = (RelativeLayout)findViewById(R.id.h_skiing_rel);
		h_mountainbike_rel = (RelativeLayout)findViewById(R.id.h_mountainbike_rel);
		h_roadbike_rel = (RelativeLayout)findViewById(R.id.h_roadbike_rel);

		h_wakebording_rel = (RelativeLayout)findViewById(R.id.h_wakebording_rel);
		h_wakesurfing_rel = (RelativeLayout)findViewById(R.id.h_wakesurfing_rel);
		
		snow_rel.setOnClickListener(this);
		wake_lay.setOnClickListener(this);
		bike_lay.setOnClickListener(this);
		
		choose_ski_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 1;
				show_error_dialog();
			}
		});

		choose_sup_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 2;
				show_error_dialog();
			}
		});
		
		h_snowboard_rel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 3;
				show_error_dialog();
			}
		});
		
		h_skiing_rel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 4;
				show_error_dialog();
			}
		});
		
		h_mountainbike_rel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 5;
				show_error_dialog();
			}
		});
		
		h_roadbike_rel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 6;
				show_error_dialog();
			}
		});
		
		choose_skate_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 7;
				show_error_dialog();
			}
		});
		
		choose_run_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				activity_id = 8;
				show_error_dialog();
			}
		});
		
		h_wakebording_rel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				activity_id = 9;
				show_error_dialog();
			}
		});
		
		h_wakesurfing_rel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				activity_id = 10;
				show_error_dialog();
			}
		});
	}

	@Override
	public void onBackPressed() 
	{
		goBack("back");
	}

	@Override
	public void goBack(String target)
	{
		mIntent = new Intent(context, Home_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target) 
	{
		
	}

	@Override
	public void setTypefaceControls() 
	{

	}

	@Override
	public void gotoStartActivity() 
	{
		
	}

	@Override
	public void onClick(View v)
	{
		if(v==snow_rel)
		{
			if(h_snowboard_rel.getVisibility()==View.VISIBLE)
			{
				h_snowboard_rel.setVisibility(View.GONE);
				h_skiing_rel.setVisibility(View.GONE);
			}
			else
			{
				h_snowboard_rel.setVisibility(View.VISIBLE);
				h_skiing_rel.setVisibility(View.VISIBLE);
			}
		}
		else if(v==bike_lay)
		{
			if(h_mountainbike_rel.getVisibility()==View.VISIBLE)
			{
				h_mountainbike_rel.setVisibility(View.GONE);
				h_roadbike_rel.setVisibility(View.GONE);
			}
			else
			{
				h_mountainbike_rel.setVisibility(View.VISIBLE);
				h_roadbike_rel.setVisibility(View.VISIBLE);
			}
		}
		else if(v==wake_lay)
		{
			if(h_wakebording_rel.getVisibility()==View.VISIBLE)
			{
				h_wakebording_rel.setVisibility(View.GONE);
				h_wakesurfing_rel.setVisibility(View.GONE);
			}
			else
			{
				h_wakebording_rel.setVisibility(View.VISIBLE);
				h_wakesurfing_rel.setVisibility(View.VISIBLE);
			}
		}
	}
	
	@SuppressLint("InflateParams") 
	protected void show_error_dialog()
	{
		if(activity_id!=0)
		{
			//*S* Start
			clsPref _pref = new clsPref(Choose_activity.this);
			String _activityName=getSportNameById(activity_id);
			if(_pref.getIsAnActivityRunning()==false ||_pref.getRunningActivityName().equals(_activityName))
			{
				if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
				{
					if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals(activity_id+""))
					{
						mIntent = new Intent(context, RoadBike_Activity.class);	
						mIntent.putExtra("sports_id", activity_id+"");
						startActivity(mIntent);
						finish();
					}
					else
					{
						activityRunningDialog();
					}
				}
				else
				{
					mIntent = new Intent(context, RoadBike_Activity.class);	
					//mIntent = new Intent(context, RoadBike_Activity.class);
					mIntent.putExtra("sports_id", activity_id+"");
					startActivity(mIntent);
					finish();
				}
			}
			else
			{
				Toast.makeText(Choose_activity.this, ""+_pref.getRunningActivityName()+" activity is running first stop it.", Toast.LENGTH_LONG).show();
			}
			//*S* End
		}
	}
	
	protected void onStart()
	{
		super.onStart();
		
		//Check if any activity is Running
		if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
		{
			if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("1"))
			{
				choose_ski_lay.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("2"))
			{
				choose_sup_lay.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("3"))
			{
				h_snowboard_rel.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("4"))
			{
				h_skiing_rel.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("5"))
			{
				h_mountainbike_rel.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("6"))
			{
				h_roadbike_rel.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("7"))
			{
				choose_skate_lay.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("8"))
			{
				choose_run_lay.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("9"))
			{
				h_wakebording_rel.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals("10"))
			{
				h_wakesurfing_rel.setBackgroundColor(getResources().getColor(R.color.btn_blue));
			}
			else
			{
				Log.i("Activit log", "No current activity running");
			}
		}
	};
	
	private String getSportNameById(int sport_id) 
	{
		if (sport_id == 1) 
		{
			return "SURF";
		}
		else if (sport_id == 2) 
		{
			return "SUP";
		}
		else if (sport_id == 3) 
		{
			return "SNOWBOARD";
		}
		else if (sport_id == 4) 
		{
			return "SKIING";
		}
		else if (sport_id == 5)
		{
			return "MOUNTAIN BIKE";
		}
		else if (sport_id == 6)
		{
			return "ROAD BIKE";
		}
		else if (sport_id == 7) 
		{
			return "SKATE";
		}
		else if (sport_id == 8) 
		{
			return "RUN";
		}
		else if (sport_id == 9) 
		{
			return "WAKEBOARDING";
		}
		else if (sport_id == 10) 
		{
			return "WAKE SURFING";
		}
		else 
		{
			return "Activity";
		}
	}
	
	private void activityRunningDialog()
	{
		final Dialog mDialog = new Dialog(Choose_activity.this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setCancelable(false);
		mDialog.setContentView(R.layout.infl_activity_running);
		mDialog.show();
		
		TextView activity_running_txt_single 			= (TextView)mDialog.findViewById(R.id.activity_running_txt_single);
		ButtonClass activity_running_button_ok_single 	= (ButtonClass)mDialog.findViewById(R.id.activity_running_button_ok_single);
		
		try
		{
			activity_running_txt_single.setText(getSportNameById(Integer.parseInt(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID)))+" activity is running, please stop that before starting new activity");
		}
		catch(Exception ex)
		{
			activity_running_txt_single.setText("Activity is running, please stop that before starting new activity");
		}
		activity_running_button_ok_single.setOnClickListener(new OnClickListener()
		{
			@SuppressLint("SimpleDateFormat") @Override
			public void onClick(View v) 
			{
				mDialog.cancel();
			}
		});
	}
}