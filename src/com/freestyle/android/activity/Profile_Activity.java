package com.freestyle.android.activity;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.R.bool;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.freestyle.android.R;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.EditTextClass;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.config.TextviewClass;
import com.freestyle.android.entity.clsUser;
import com.freestyle.android.util.clsGeneral;
import com.freestyle.test.clsPref;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

public class Profile_Activity extends BaseActivity implements OnClickListener, CoreFrame, OnEditorActionListener {
	Context mContext;
	Intent mIntent;
	ImageView mImageviewBack;
	ImageView mImageViewUserImage;
	Shared_preference mPreferenceProfileData;

	TextviewClass mTextviewSurfDataUpdate;
	TextviewClass mTextviewTideAlert;
	TextviewClass mTextviewTemperature;
	TextviewClass mTextviewActivityProfile;
	TextviewClass mTextviewBasicProfile;

	TextviewClass mTextviewUpdatePhoto;

	LinearLayout mLinearLayoutSurfDataUpdate;
	RelativeLayout mLinearLayoutTideAlert;
	LinearLayout mLinearLayoutTemperature;
	LinearLayout mLinearLayoutActivityProfile;
	LinearLayout mLinearLayoutBasicProfile;

	RelativeLayout mLinearLayoutUpdatePhoto;

	ImageView mImageViewSurfAuto;
	ImageView mImageViewSurfManual;
	TextviewClass mTextviewClassSurfManual;
	ImageView mImageViewTideAlert;
	ImageView mImageViewTemperatureImperial;
	ImageView mImageViewTemperatureMetric;

	RelativeLayout mRelativeLayoutWeight;
	RelativeLayout mRelativeLayoutHeight;
	RelativeLayout mRelativeLayoutAge;
	RelativeLayout mRelativeLayoutGender;
	RelativeLayout mRelativeLayoutSurfAuto;
	RelativeLayout mRelativeLayoutSurfManual;
	RelativeLayout mRelativeLayoutTemperatureImperial;
	RelativeLayout mRelativeLayoutTemperatureMetric;

	TextviewClass mTextviewWeight;
	TextviewClass mTextviewHeight;
	TextviewClass mTextviewAge;
	TextviewClass mTextviewGender;

	// basic Profile
	EditTextClass EDT_FNAME, EDT_LNAME, EDT_EMAIL, EDT_PASSWORD, EDT_PHONE;
	TextviewClass TV_BDATE;
	private Calendar cal;
	ProgressDialog PROGRESSBAR;

	ProgressBar mProgressBar;
	int weight_pos = 0;
	int height_pos = 0;

	private static int PICK_IMAGE = 1;

	String imagePath = "";

	ImageLoader mImageLoader;
	AlertDialog alert = null;
	DisplayImageOptions options;

	// ArrayList<String> mArrayListGender = new ArrayList<String>();

	String[] mGender;

	ArrayList<String> mArrayListAge = new ArrayList<String>();
	ArrayList<String> mArrayListHeight = new ArrayList<String>();
	ArrayList<String> mArrayListWeight = new ArrayList<String>();
	ArrayList<String> mArrayListManual = new ArrayList<String>();

	Shared_preference mSharedPreferencePosition;

	String mStringGender = "MALE";
	String mStringHeight = "6'0''";
	String mStringWeight = "195";
	String mStringAge = "18";
	String mStringManual = "3 hr";

	String mStringSurfAutoManual = "true";
	String mStringTideAlert = "true";
	String mStringUnit = "false";

	Boolean mBooleanUnitFlag = true;

	ImageView mImageviewProfilePicture;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_activity);

		mContext = Profile_Activity.this;

		mPreferenceProfileData = new Shared_preference(context);
		mSharedPreferencePosition = new Shared_preference(context);

		mGender = new String[] { "MALE", "FEMALE  " };

		mArrayListAge = new ArrayList<String>();
		for (int i = 10; i <= 120; i++) {
			mArrayListAge.add("" + i);
		}
		cal = Calendar.getInstance();
		PROGRESSBAR = new ProgressDialog(Profile_Activity.this);

		FillHeightList();
		FillWeightList();

		// mStringOldWeightKG = shared.getPrefrencewithDefault(mContext,
		// "old_weight_kg", "88");
		// mStringOldWeightLBS = shared.getPrefrencewithDefault(mContext,
		// "old_weight_lbs", "195");
		//
		// mStringOldHeightCM = shared.getPrefrencewithDefault(mContext,
		// "old_weight_cm", "182");
		// mStringOldHeightFT = shared.getPrefrencewithDefault(mContext,
		// "old_weight_ft", "6'0''");

		mArrayListManual = new ArrayList<String>();
		for (int i = 1; i <= 3; i++) {
			mArrayListManual.add("" + i + " hr");
		}

		initDrawer(Profile_Activity.this);

		mImageLoader = ImageLoader.getInstance();
		mImageLoader.init(ImageLoaderConfiguration.createDefault(this));

		mImageviewBack = (ImageView) findViewById(R.id.profile_back_img);
		mImageViewUserImage = (ImageView) findViewById(R.id.profile_user_image);

		mProgressBar = (ProgressBar) findViewById(R.id.progress);

		mImageviewProfilePicture = (ImageView) findViewById(R.id.main_imageview_profile_pic);

		mTextviewSurfDataUpdate = (TextviewClass) findViewById(R.id.general_textview_surf_update);
		mTextviewTideAlert = (TextviewClass) findViewById(R.id.general_textview_tide_alert);
		mTextviewTemperature = (TextviewClass) findViewById(R.id.general_textview_temperature);
		mTextviewActivityProfile = (TextviewClass) findViewById(R.id.general_textview_activity_profile);
		mTextviewBasicProfile = (TextviewClass) findViewById(R.id.general_textview_basic_profile);

		mTextviewUpdatePhoto = (TextviewClass) findViewById(R.id.general_textview_update_photo);

		mLinearLayoutSurfDataUpdate = (LinearLayout) findViewById(R.id.linear_surf_update);
		mLinearLayoutTideAlert = (RelativeLayout) findViewById(R.id.general_relative_tide_alert);
		mLinearLayoutTemperature = (LinearLayout) findViewById(R.id.linear_temperature);
		mLinearLayoutActivityProfile = (LinearLayout) findViewById(R.id.linear_activity_profile);
		mLinearLayoutBasicProfile = (LinearLayout) findViewById(R.id.linear_basic_profile);

		mLinearLayoutUpdatePhoto = (RelativeLayout) findViewById(R.id.general_relative_update_photo);

		mImageViewSurfAuto = (ImageView) findViewById(R.id.general_imageview_auto);
		mImageViewSurfManual = (ImageView) findViewById(R.id.general_imageview_manual);
		mTextviewClassSurfManual = (TextviewClass) findViewById(R.id.general_textview_manual);
		mImageViewTideAlert = (ImageView) findViewById(R.id.general_imageview_tide_alert);
		mImageViewTemperatureImperial = (ImageView) findViewById(R.id.general_imageview_temperature_imperial);
		mImageViewTemperatureMetric = (ImageView) findViewById(R.id.general_imageview_temperature_metric);

		mRelativeLayoutWeight = (RelativeLayout) findViewById(R.id.general_relative_weight);
		mRelativeLayoutHeight = (RelativeLayout) findViewById(R.id.general_relative_height);
		mRelativeLayoutAge = (RelativeLayout) findViewById(R.id.general_relative_age);
		mRelativeLayoutGender = (RelativeLayout) findViewById(R.id.general_relative_gender);

		mRelativeLayoutSurfAuto = (RelativeLayout) findViewById(R.id.general_relative_surf_auto);
		mRelativeLayoutSurfManual = (RelativeLayout) findViewById(R.id.general_relative_surf_manual);
		mRelativeLayoutTemperatureImperial = (RelativeLayout) findViewById(R.id.general_relative_temperature_imperial);
		mRelativeLayoutTemperatureMetric = (RelativeLayout) findViewById(R.id.general_relative_temperature_metric);

		mTextviewGender = (TextviewClass) findViewById(R.id.general_textview_gender);
		mTextviewAge = (TextviewClass) findViewById(R.id.general_textview_age);
		mTextviewHeight = (TextviewClass) findViewById(R.id.general_textview_height);
		mTextviewWeight = (TextviewClass) findViewById(R.id.general_textview_weight);

		mImageviewBack.setOnClickListener(this);
		mImageViewUserImage.setOnClickListener(this);

		mLinearLayoutTideAlert.setOnClickListener(this);

		mTextviewSurfDataUpdate.setOnClickListener(this);
		mTextviewTideAlert.setOnClickListener(this);
		mTextviewTemperature.setOnClickListener(this);
		mTextviewActivityProfile.setOnClickListener(this);
		mTextviewBasicProfile.setOnClickListener(this);

		mTextviewUpdatePhoto.setOnClickListener(this);

		mImageViewSurfAuto.setOnClickListener(this);
		mImageViewSurfManual.setOnClickListener(this);
		mTextviewClassSurfManual.setOnClickListener(this);
		mImageViewTideAlert.setOnClickListener(this);
		mImageViewTemperatureImperial.setOnClickListener(this);
		mImageViewTemperatureMetric.setOnClickListener(this);

		mRelativeLayoutWeight.setOnClickListener(this);
		mRelativeLayoutHeight.setOnClickListener(this);
		mRelativeLayoutAge.setOnClickListener(this);
		mRelativeLayoutGender.setOnClickListener(this);

		mRelativeLayoutSurfAuto.setOnClickListener(this);
		mRelativeLayoutSurfManual.setOnClickListener(this);
		mRelativeLayoutTemperatureImperial.setOnClickListener(this);
		mRelativeLayoutTemperatureMetric.setOnClickListener(this);
		mLinearLayoutUpdatePhoto.setOnClickListener(this);
		// bsic profile
		EDT_FNAME = (EditTextClass) findViewById(R.id.general_textview_fname);
		EDT_FNAME.setOnEditorActionListener(this);
		EDT_LNAME = (EditTextClass) findViewById(R.id.general_textview_lname);
		EDT_LNAME.setOnEditorActionListener(this);

		EDT_EMAIL = (EditTextClass) findViewById(R.id.general_textview_email);
		EDT_EMAIL.setOnEditorActionListener(this);

		EDT_PASSWORD = (EditTextClass) findViewById(R.id.general_textview_password);
		EDT_PASSWORD.setOnEditorActionListener(this);

		EDT_PHONE = (EditTextClass) findViewById(R.id.general_textview_phone);
		EDT_PHONE.setOnEditorActionListener(this);

		TV_BDATE = (TextviewClass) findViewById(R.id.general_textview_bdate);
		TV_BDATE.setOnClickListener(this);

		imagePath = shared.getPrefrence(mContext, Constants.PROFILE.USER_IMAGE).toString();

		if (!imagePath.equals("")) {
			if (imagePath != null && !imagePath.equalsIgnoreCase("")) {
				String mTempFilePath = "";
				if (!imagePath.startsWith("file://")) {
					mTempFilePath = "file://" + imagePath;
				} else {
					mTempFilePath = imagePath;
				}
				if (getmImageLoader() != null) {
					getmImageLoader().displayImage(mTempFilePath, mImageViewUserImage, getOptions(), new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {
							toggleLoadingVisiblity(true);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							toggleLoadingVisiblity(false);

						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							toggleLoadingVisiblity(false);
						}

						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							toggleLoadingVisiblity(false);
						}
					});
				}
			}

			mImageviewProfilePicture.setVisibility(View.INVISIBLE);
		} else {
			mImageviewProfilePicture.setVisibility(View.VISIBLE);
		}

		if (!shared.getPrefrence(mContext, Constants.PROFILE.USER_GENDER).equalsIgnoreCase("")) {
			mStringGender = shared.getPrefrence(mContext, Constants.PROFILE.USER_GENDER);
		}

		if (!shared.getPrefrence(mContext, Constants.PROFILE.USER_AGE).equalsIgnoreCase("")) {
			mStringAge = shared.getPrefrence(mContext, Constants.PROFILE.USER_AGE);
		}

		if (!shared.getPrefrence(mContext, Constants.PROFILE.USER_HEIGHT).equalsIgnoreCase("")) {
			mStringHeight = shared.getPrefrence(mContext, Constants.PROFILE.USER_HEIGHT);
		}

		if (!shared.getPrefrence(mContext, Constants.PROFILE.USER_WEIGHT).equalsIgnoreCase("")) {
			mStringWeight = shared.getPrefrence(mContext, Constants.PROFILE.USER_WEIGHT);
		}

		if (!shared.getPrefrence(mContext, Constants.PROFILE.USER_MANUAL).equalsIgnoreCase("")) {
			mStringManual = shared.getPrefrence(mContext, Constants.PROFILE.USER_MANUAL);
		}

		// surf auto manual
		if (!shared.getPrefrence(mContext, Constants.PROFILE.SURF_AUTO_MANUAL).equalsIgnoreCase("")) {
			mStringSurfAutoManual = shared.getPrefrencewithDefault(mContext, Constants.PROFILE.SURF_AUTO_MANUAL, "true");
		}

		if (mStringSurfAutoManual.equalsIgnoreCase("true")) {
			mImageViewSurfAuto.setVisibility(View.VISIBLE);
			mImageViewSurfManual.setVisibility(View.INVISIBLE);
			mTextviewClassSurfManual.setVisibility(View.INVISIBLE);
		} else {
			mImageViewSurfAuto.setVisibility(View.INVISIBLE);
			mImageViewSurfManual.setVisibility(View.VISIBLE);
			mTextviewClassSurfManual.setVisibility(View.VISIBLE);
			mTextviewClassSurfManual.setText(mStringManual);
		}

		// tide alert
		if (!shared.getPrefrence(mContext, Constants.PROFILE.TIDE_ALERT).equalsIgnoreCase("")) {
			mStringTideAlert = shared.getPrefrence(mContext, Constants.PROFILE.TIDE_ALERT);
		}

		if (mStringTideAlert.equalsIgnoreCase("true")) {
			mImageViewTideAlert.setVisibility(View.VISIBLE);
		} else {
			mImageViewTideAlert.setVisibility(View.INVISIBLE);
		}

		// units
		if (!shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase("")) {
			mStringUnit = shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC);
		}

		if (mStringUnit.equalsIgnoreCase("true")) {
			mImageViewTemperatureImperial.setVisibility(View.INVISIBLE);
			mImageViewTemperatureMetric.setVisibility(View.VISIBLE);
			mBooleanUnitFlag = false;
		} else {
			mImageViewTemperatureImperial.setVisibility(View.VISIBLE);
			mImageViewTemperatureMetric.setVisibility(View.INVISIBLE);
			mBooleanUnitFlag = true;
		}

		mTextviewGender.setText("" + mStringGender);
		mTextviewAge.setText("" + mStringAge);
		if (!mStringHeight.equalsIgnoreCase("")) {
			if (shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase("true")) {
				mTextviewHeight.setText("" + mStringHeight + " cm");
			} else {
				mTextviewHeight.setText("" + mStringHeight + " ft");
			}
		}

		if (!mStringWeight.equalsIgnoreCase("")) {
			if (shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase("true")) {
				mTextviewWeight.setText("" + mStringWeight + " kg");
			} else {
				mTextviewWeight.setText("" + mStringWeight + " lbs");
			}
		}
		mTextviewClassSurfManual.setText("" + mStringManual);
		setBasicProfileValue();
	}

	private void setBasicProfileValue() {
		clsPref _pref = new clsPref(Profile_Activity.this);
		EDT_EMAIL.setText(_pref.getUserEmail());
		EDT_FNAME.setText(_pref.getFirstName());
		EDT_LNAME.setText(_pref.getLastName());
		EDT_PASSWORD.setText(_pref.getUserPass());
		EDT_PHONE.setText(_pref.getPhoneNumber());
		
		TV_BDATE.setText(_pref.getBirthDate());
		mTextviewGender.setText(_pref.getGender().toUpperCase());

	}

	public void FillHeightList() {
		if (shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase("true")) {
			mBooleanUnitFlag = false;
			mArrayListHeight = new ArrayList<String>();
			for (int i = 20; i <= 250; i++) {
				mArrayListHeight.add("" + i);
			}
		} else {
			mBooleanUnitFlag = true;
			mArrayListHeight = new ArrayList<String>();
			String s[] = { "1'0''", "1'1''", "1'2''", "1'3''", "1'4''", "1'5''", "1'6''", "1'7''", "1'8''", "1'9''", "1'10''", "1'11''", "2'0''", "2'1''", "2'2''", "2'3''", "2'4''", "2'5''", "2'6''", "2'7''", "2'8''", "2'9''", "2'10''", "2'11''" + "", "3'0''", "3'1''", "3'2''", "3'3''", "3'4''", "3'5''", "3'6''", "3'7''", "3'8''", "3'9''", "3'10''", "3'11''", "4'0''", "4'1''", "4'2''", "4'3''", "4'4''", "4'5''", "4'6''", "4'7''", "4'8''", "4'9''", "4'10''", "4'11''" + "", "5'0''", "5'1''", "5'2''", "5'3''", "5'4''", "5'5''", "5'6''", "5'7''", "5'8''", "5'9''", "5'10''", "5'11''", "6'0''", "6'1''", "6'2''", "6'3''", "6'4''", "6'5''", "6'6''", "6'7''", "6'8''", "6'9''", "6'10''", "6'11''" + "", "7'0''", "7'1''", "7'2''", "7'3''", "7'4''", "7'5''", "7'6''", "7'7''", "7'8''", "7'9''", "7'10''", "7'11''" };
			for (int i = 0; i < s.length; i++) {
				mArrayListHeight.add(s[i]);
			}
		}
	}

	public void FillWeightList() {
		if (shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase("true")) {
			mBooleanUnitFlag = false;
			mArrayListWeight = new ArrayList<String>();
			for (int i = 10; i <= 150; i++) {
				mArrayListWeight.add("" + i);
			}
		} else {
			mBooleanUnitFlag = true;
			mArrayListWeight = new ArrayList<String>();
			for (int i = 20; i <= 300; i++) {
				mArrayListWeight.add("" + i);
			}
		}
	}

	public String KGToPound(long kgValue) {
		double POUNDS_PER_KILOGRAM = 2.20462262;
		String mSUffix = "";
		String mString = Math.round((kgValue * POUNDS_PER_KILOGRAM)) + mSUffix;

		if (mString.contains(".")) {
			mString = mString.substring(0, mString.indexOf("."));
		}

		return mString;
	}

	public String PoundToKg(long poundValue) {
		String mSUffix = "";
		double POUNDS_PER_KILOGRAM = 2.20462262;

		String mString = Math.round((poundValue / POUNDS_PER_KILOGRAM)) + mSUffix;

		if (mString.contains(".")) {
			mString = mString.substring(0, mString.indexOf("."));
		}

		return mString;
	}

	public String CMtoFt(long cmValue) {
		// float heightinft = heightInCm (say 178) / 30.48;
		// int ft = int value of (heightinft);
		// float inches = (decimal value of heightinft) * 12;
		// [4:22:46 PM] SP: 178/ 30.48 = 5.839895
		// 5 ft
		// 0.839895 X 12 = 10 inch

		double feetvalue = (cmValue / 30.48);
		String mStringscale = String.valueOf(feetvalue);
		mStringscale = mStringscale.substring(0, mStringscale.indexOf("."));
		int scale = Integer.parseInt(mStringscale);

		double precision = (feetvalue - ((double) scale));
		precision = precision * ((double) 12.0);
		DecimalFormat df = new DecimalFormat("###.#");
		double d = Double.valueOf(df.format(precision));
		double d1 = Math.round(d);

		String mStringprecision = String.valueOf(d1);
		mStringprecision = mStringprecision.substring(0, mStringprecision.indexOf("."));
		int mintprecision = Integer.parseInt(mStringprecision);

		if (mintprecision == 12) {
			scale = scale + 1;
			mintprecision = 0;
		}

		String returnVALUE = scale + "." + mintprecision;

		return returnVALUE;
	}

	public String FtToCm(double ftvalue) {
		// 6.7
		// (6' �� 12") + 7" = 79"
		// 79" �� 2.54 = 200.7 cm

		String mString = String.valueOf(ftvalue);
		String scale = mString.substring(0, mString.indexOf("."));
		String prec = mString.substring(mString.indexOf(".") + 1, mString.length());

		double inch = (Integer.parseInt(scale) * 12) + Integer.parseInt(prec);
		double cmvalue = inch * 2.54;

		String finalValue = String.valueOf(cmvalue);

		if (finalValue.contains(".")) {
			finalValue = finalValue.substring(0, finalValue.indexOf("."));
		}

		return finalValue;
	}

	@Override
	public void onBackPressed() {
		goBack("back");
	}

	@Override
	public void goBack(String target) {
		mIntent = new Intent(mContext, Settings_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target) {
		if (target.equals("setting")) {
			mIntent = new Intent(mContext, Settings_Activity.class);
			startActivity(mIntent);
			finish();
		}
	}

	@Override
	public void setTypefaceControls() {
	}

	@Override
	public void onClick(View v) {
		if (v == mImageviewBack) {
			goBack("back");
		} else if (v == mLinearLayoutUpdatePhoto) {
			pickImage();
		} else if (v == mImageViewUserImage) {
			pickImage();
		} else if (v == mTextviewSurfDataUpdate) {
			if (mLinearLayoutSurfDataUpdate.getVisibility() == View.VISIBLE) {
				mLinearLayoutSurfDataUpdate.setVisibility(View.GONE);
			} else {
				mLinearLayoutSurfDataUpdate.setVisibility(View.VISIBLE);
				mLinearLayoutTideAlert.setVisibility(View.GONE);
				mLinearLayoutTemperature.setVisibility(View.GONE);
				mLinearLayoutActivityProfile.setVisibility(View.GONE);
				mLinearLayoutBasicProfile.setVisibility(View.GONE);

				mLinearLayoutUpdatePhoto.setVisibility(View.VISIBLE);
			}
		} else if (v == mTextviewTideAlert) {
			if (mLinearLayoutTideAlert.getVisibility() == View.VISIBLE) {
				mLinearLayoutTideAlert.setVisibility(View.GONE);
			} else {
				mLinearLayoutSurfDataUpdate.setVisibility(View.GONE);
				mLinearLayoutTideAlert.setVisibility(View.VISIBLE);
				mLinearLayoutTemperature.setVisibility(View.GONE);
				mLinearLayoutActivityProfile.setVisibility(View.GONE);
				mLinearLayoutBasicProfile.setVisibility(View.GONE);

				mLinearLayoutUpdatePhoto.setVisibility(View.VISIBLE);
			}
		} else if (v == mTextviewTemperature) {
			if (mLinearLayoutTemperature.getVisibility() == View.VISIBLE) {
				mLinearLayoutTemperature.setVisibility(View.GONE);
			} else {
				mLinearLayoutSurfDataUpdate.setVisibility(View.GONE);
				mLinearLayoutTideAlert.setVisibility(View.GONE);
				mLinearLayoutTemperature.setVisibility(View.VISIBLE);
				mLinearLayoutActivityProfile.setVisibility(View.GONE);
				mLinearLayoutBasicProfile.setVisibility(View.GONE);

				mLinearLayoutUpdatePhoto.setVisibility(View.VISIBLE);
			}
		} else if (v == mTextviewActivityProfile) {
			if (mLinearLayoutActivityProfile.getVisibility() == View.VISIBLE) {
				mLinearLayoutActivityProfile.setVisibility(View.GONE);
			} else {
				mLinearLayoutSurfDataUpdate.setVisibility(View.GONE);
				mLinearLayoutTideAlert.setVisibility(View.GONE);
				mLinearLayoutTemperature.setVisibility(View.GONE);
				mLinearLayoutActivityProfile.setVisibility(View.VISIBLE);
				mLinearLayoutBasicProfile.setVisibility(View.GONE);

				mLinearLayoutUpdatePhoto.setVisibility(View.VISIBLE);
			}
		} else if (v == mTextviewBasicProfile) {
			if (mLinearLayoutBasicProfile.getVisibility() == View.VISIBLE) {
				mLinearLayoutBasicProfile.setVisibility(View.GONE);
			} else {
				mLinearLayoutSurfDataUpdate.setVisibility(View.GONE);
				mLinearLayoutTideAlert.setVisibility(View.GONE);
				mLinearLayoutTemperature.setVisibility(View.GONE);
				mLinearLayoutActivityProfile.setVisibility(View.GONE);
				mLinearLayoutBasicProfile.setVisibility(View.VISIBLE);

				mLinearLayoutUpdatePhoto.setVisibility(View.VISIBLE);
			}
		} else if (v == mTextviewUpdatePhoto) {
			if (mLinearLayoutUpdatePhoto.getVisibility() == View.VISIBLE) {
				mLinearLayoutUpdatePhoto.setVisibility(View.GONE);
			} else {
				mLinearLayoutSurfDataUpdate.setVisibility(View.GONE);
				mLinearLayoutTideAlert.setVisibility(View.GONE);
				mLinearLayoutTemperature.setVisibility(View.GONE);
				mLinearLayoutActivityProfile.setVisibility(View.GONE);
				mLinearLayoutBasicProfile.setVisibility(View.GONE);

				mLinearLayoutUpdatePhoto.setVisibility(View.VISIBLE);
			}
		} else if (v == mRelativeLayoutSurfAuto) {
			if (!shared.getPrefrence(mContext, "surf_auto_manual").equalsIgnoreCase(""))
				mStringSurfAutoManual = shared.getPrefrence(mContext, "surf_auto_manual");

			mImageViewSurfAuto.setVisibility(View.VISIBLE);
			mImageViewSurfManual.setVisibility(View.INVISIBLE);
			mTextviewClassSurfManual.setVisibility(View.INVISIBLE);
			shared.setPrefrence(mContext, "surf_auto_manual", "true");

		} else if (v == mRelativeLayoutSurfManual) {
			if (!shared.getPrefrence(mContext, "surf_auto_manual").equalsIgnoreCase(""))
				mStringSurfAutoManual = shared.getPrefrence(mContext, "surf_auto_manual");

			mImageViewSurfAuto.setVisibility(View.INVISIBLE);
			mImageViewSurfManual.setVisibility(View.VISIBLE);
			mTextviewClassSurfManual.setVisibility(View.VISIBLE);
			shared.setPrefrence(mContext, "surf_auto_manual", "false");

		} else if (v == mTextviewClassSurfManual) {
			showManual();
		} else if (v == mLinearLayoutTideAlert) {
			if (!shared.getPrefrence(mContext, "tide_alert").equalsIgnoreCase(""))
				mStringTideAlert = shared.getPrefrence(mContext, "tide_alert");

			if (mStringTideAlert.equalsIgnoreCase("true")) {
				mImageViewTideAlert.setVisibility(View.INVISIBLE);
				shared.setPrefrence(mContext, "tide_alert", "false");
			} else {
				mImageViewTideAlert.setVisibility(View.VISIBLE);
				shared.setPrefrence(mContext, "tide_alert", "true");
			}
		} else if (v == mRelativeLayoutTemperatureImperial) {
			if (!shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase(""))
				mStringUnit = shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC);

			mImageViewTemperatureImperial.setVisibility(View.VISIBLE);
			mImageViewTemperatureMetric.setVisibility(View.INVISIBLE);
			shared.setPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC, "false");
			mBooleanUnitFlag = true;

			FillHeightList();
			FillWeightList();

			if (!mStringWeight.equalsIgnoreCase("")) {
				mStringWeight = KGToPound(Long.parseLong(mStringWeight));
				mTextviewWeight.setText(mStringWeight + " lbs");
				shared.setPrefrence(mContext, "user_weight", mStringWeight);
			}
			if (!mStringHeight.equalsIgnoreCase("")) {
				mStringHeight = CMtoFt(Long.parseLong(mStringHeight));

				String mString = mStringHeight.replace(".", "'");
				mString = mString + "''";
				mStringHeight = mString;
				mTextviewHeight.setText(mStringHeight + " ft");
				shared.setPrefrence(mContext, "user_height", mStringHeight);
			}
		} else if (v == mRelativeLayoutTemperatureMetric) {
			if (!shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase(""))
				mStringUnit = shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC);

			mImageViewTemperatureImperial.setVisibility(View.INVISIBLE);
			mImageViewTemperatureMetric.setVisibility(View.VISIBLE);
			shared.setPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC, "true");

			mBooleanUnitFlag = false;

			FillHeightList();
			FillWeightList();

			if (!mStringWeight.equalsIgnoreCase("")) {
				mStringWeight = PoundToKg(Long.parseLong(mStringWeight));
				mTextviewWeight.setText(mStringWeight + " kg");
				shared.setPrefrence(mContext, "user_weight", mStringWeight);
			}
			if (!mStringHeight.equalsIgnoreCase("")) {
				mStringHeight = mStringHeight.replace("'", ".");
				mStringHeight = mStringHeight.substring(0, mStringHeight.length() - 2);
				mStringHeight = FtToCm(Double.parseDouble(mStringHeight));
				mTextviewHeight.setText(mStringHeight + " cm");
				shared.setPrefrence(mContext, "user_height", mStringHeight);
			}
		} else if (v == mRelativeLayoutWeight) {
			showWeight();
		} else if (v == mRelativeLayoutHeight) {
			showHeight();
		} else if (v == mRelativeLayoutAge) {
			showAge();
		} else if (v == mRelativeLayoutGender) {
			showGender();
		} else if (v == TV_BDATE) {
			// date picker
			createDialogWithoutDateField().show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == PICK_IMAGE && resultCode == FragmentActivity.RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String mStringimagePath = cursor.getString(columnIndex);
			cursor.close();

			imagePath = mStringimagePath;
			File file = new File(imagePath);

			BitmapFactory.Options option = new BitmapFactory.Options();
			option.inPreferredConfig = Bitmap.Config.ARGB_8888;

			option.inSampleSize = 2;

			mImageViewUserImage.setImageBitmap(BitmapFactory.decodeFile(imagePath, option));

			mImageviewProfilePicture.setVisibility(View.INVISIBLE);
			DemoApplication.ProfilePictureUri = Uri.fromFile(file);

			if (mStringimagePath != null && !mStringimagePath.equalsIgnoreCase("")) {
				String mTempFilePath = "";
				if (!mStringimagePath.startsWith("file://")) {
					mTempFilePath = "file://" + mStringimagePath;
				} else {
					mTempFilePath = mStringimagePath;
				}
				if (getmImageLoader() != null) {
					getmImageLoader().displayImage(mTempFilePath, mImageViewUserImage, getOptions(), new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {
							toggleLoadingVisiblity(true);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							toggleLoadingVisiblity(false);

						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							toggleLoadingVisiblity(false);
						}

						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							toggleLoadingVisiblity(false);
						}
					});
				}
			}

			mPreferenceProfileData.setPrefrence(mContext, "user_image", imagePath);
		}
	}

	public void buildOptions(int resource) {
		options = new DisplayImageOptions.Builder().showImageForEmptyUri(resource).showImageOnFail(resource).cacheInMemory(true).cacheOnDisc(true).bitmapConfig(Bitmap.Config.ARGB_8888).build();
	}

	public ImageLoader getmImageLoader() {
		return mImageLoader;
	}

	public DisplayImageOptions getOptions() {
		return options;
	}

	private void pickImage() {
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(i, PICK_IMAGE);
	}

	public void toggleLoadingVisiblity(boolean isVisible) {
		mProgressBar.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
		mImageViewUserImage.setVisibility(!isVisible ? View.VISIBLE : View.INVISIBLE);
	}

	public void gotoProfile() {
	};

	private void showAge() {
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.ctrolactivitydialog);
		dlg.show();

		mStringAge = shared.getPrefrence(mContext, "user_age");

		final String[] items = mArrayListAge.toArray(new String[mArrayListAge.size()]);

		Button cancelButton = (Button) dlg.findViewById(R.id.CancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dlg.cancel();
			}
		});

		Button saveButton = (Button) dlg.findViewById(R.id.SaveButton);
		saveButton.setText("Ok");
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shared.setPrefrence(mContext, "user_age", mStringAge);
				if (mStringAge.equalsIgnoreCase("")) {
					mStringAge = items[0];
					shared.setPrefrence(mContext, "user_age", mStringAge);
				}
				mTextviewAge.setText("" + mStringAge);
				dlg.dismiss();
			}
		});

		WheelView mWheelViewAge = (WheelView) dlg.findViewById(R.id.country);
		try {
			Integer a = (int) getResources().getDimension(R.dimen.testsp);
			mWheelViewAge.setTEXT_SIZE(a);
		} catch (Exception e) {
			mWheelViewAge.setTEXT_SIZE(60);
		}

		mWheelViewAge.setVisibleItems(5);

		mWheelViewAge.setAdapter(new ArrayWheelAdapter<String>(items));
		mWheelViewAge.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				mStringAge = items[wheel.getCurrentItem()];
			}
		});

		mWheelViewAge.setCurrentItem(setAge(mStringAge));
	}

	private void showGender() {
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.ctrolactivitydialog);
		dlg.show();

		mStringGender = shared.getPrefrence(mContext, "user_gender");

		// final String[] items = mArrayListGender.toArray(new
		// String[mArrayListGender.size()]);

		Button cancelButton = (Button) dlg.findViewById(R.id.CancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dlg.cancel();
			}
		});

		Button saveButton = (Button) dlg.findViewById(R.id.SaveButton);
		saveButton.setText("Ok");
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shared.setPrefrence(mContext, "user_gender", mStringGender);
				if (mStringGender.equalsIgnoreCase("")) {
					mStringGender = mGender[0];
					shared.setPrefrence(mContext, "user_gender", mStringGender);
				}
				mTextviewGender.setText("" + mStringGender);
				dlg.dismiss();
				userRegistration();
			}
		});

		WheelView mWheelView = (WheelView) dlg.findViewById(R.id.country);
		try {
			Integer a = (int) getResources().getDimension(R.dimen.testsp);
			mWheelView.setTEXT_SIZE(a);
		} catch (Exception e) {
			mWheelView.setTEXT_SIZE(60);
		}

		mWheelView.setVisibleItems(5);

		mWheelView.setAdapter(new ArrayWheelAdapter<String>(mGender));
		mWheelView.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				mStringGender = mGender[wheel.getCurrentItem()];
			}
		});

		mWheelView.setCurrentItem(setGender(mStringGender));
	}

	private void showHeight() {
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.ctrolactivitydialog);
		dlg.show();

		mStringHeight = shared.getPrefrence(mContext, "user_height");

		final String[] items = mArrayListHeight.toArray(new String[mArrayListHeight.size()]);

		Button cancelButton = (Button) dlg.findViewById(R.id.CancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dlg.cancel();
			}
		});

		Button saveButton = (Button) dlg.findViewById(R.id.SaveButton);
		saveButton.setText("Ok");
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shared.setPrefrence(mContext, "user_height", mStringHeight);
				if (mStringHeight.equalsIgnoreCase("")) {
					mStringHeight = items[0];
					shared.setPrefrence(mContext, "user_height", mStringHeight);
				}
				if (shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase("true")) {
					mTextviewHeight.setText("" + mStringHeight + " cm");
				} else {
					mTextviewHeight.setText("" + mStringHeight + " ft");
				}

				dlg.dismiss();
			}
		});

		WheelView mWheelView = (WheelView) dlg.findViewById(R.id.country);
		try {
			Integer a = (int) getResources().getDimension(R.dimen.testsp);
			mWheelView.setTEXT_SIZE(a);
		} catch (Exception e) {
			mWheelView.setTEXT_SIZE(60);
		}

		mWheelView.setVisibleItems(5);

		mWheelView.setAdapter(new ArrayWheelAdapter<String>(items));
		mWheelView.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				mStringHeight = items[wheel.getCurrentItem()];
			}
		});

		mWheelView.setCurrentItem(setHeight(mStringHeight));
	}

	private void showWeight() {
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.ctrolactivitydialog);
		dlg.show();

		mStringWeight = shared.getPrefrence(mContext, "user_weight");

		final String[] items = mArrayListWeight.toArray(new String[mArrayListWeight.size()]);

		Button cancelButton = (Button) dlg.findViewById(R.id.CancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dlg.cancel();
			}
		});

		Button saveButton = (Button) dlg.findViewById(R.id.SaveButton);
		saveButton.setText("Ok");
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shared.setPrefrence(mContext, "user_weight", mStringWeight);
				if (mStringWeight.equalsIgnoreCase("")) {
					mStringWeight = items[0];
					shared.setPrefrence(mContext, "user_weight", mStringWeight);
				}

				if (shared.getPrefrence(mContext, Constants.PROFILE.UNIT_IMPERIAL_METRIC).equalsIgnoreCase("true")) {
					mTextviewWeight.setText("" + mStringWeight + " kg");
				} else {
					mTextviewWeight.setText("" + mStringWeight + " lbs");
				}
				dlg.dismiss();
			}
		});

		WheelView mWheelView = (WheelView) dlg.findViewById(R.id.country);
		try {
			Integer a = (int) getResources().getDimension(R.dimen.testsp);
			mWheelView.setTEXT_SIZE(a);
		} catch (Exception e) {
			mWheelView.setTEXT_SIZE(60);
		}

		mWheelView.setVisibleItems(5);

		mWheelView.setAdapter(new ArrayWheelAdapter<String>(items));
		mWheelView.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				mStringWeight = items[wheel.getCurrentItem()];
			}
		});

		mWheelView.setCurrentItem(setWeight(mStringWeight));
	}

	private void showManual() {
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.ctrolactivitydialog);
		dlg.show();

		mStringManual = shared.getPrefrence(mContext, Constants.PROFILE.USER_MANUAL);

		final String[] items = mArrayListManual.toArray(new String[mArrayListManual.size()]);

		Button cancelButton = (Button) dlg.findViewById(R.id.CancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dlg.cancel();
			}
		});

		Button saveButton = (Button) dlg.findViewById(R.id.SaveButton);
		saveButton.setText("Ok");
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shared.setPrefrence(mContext, Constants.PROFILE.USER_MANUAL, mStringManual);
				if (mStringManual.equalsIgnoreCase("")) {
					mStringManual = items[0];
					shared.setPrefrence(mContext, Constants.PROFILE.USER_MANUAL, mStringManual);
				}
				mTextviewClassSurfManual.setText("" + mStringManual);
				dlg.dismiss();
			}
		});

		WheelView mWheelView = (WheelView) dlg.findViewById(R.id.country);
		try {
			Integer a = (int) getResources().getDimension(R.dimen.testsp);
			mWheelView.setTEXT_SIZE(a);
		} catch (Exception e) {
			mWheelView.setTEXT_SIZE(60);
		}

		mWheelView.setVisibleItems(5);

		mWheelView.setAdapter(new ArrayWheelAdapter<String>(items));
		mWheelView.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				mStringManual = items[wheel.getCurrentItem()];
			}
		});

		mWheelView.setCurrentItem(setManual(mStringManual));
	}

	private int setAge(String title) {
		for (int i = 0; i < mArrayListAge.size(); i++) {
			if (title.equals(mArrayListAge.get(i))) {
				return i;
			}
		}
		return 0;
	}

	private int setGender(String title) {
		for (int i = 0; i < mGender.length; i++) {
			if (title.equals(mGender[i])) {
				return i;
			}
		}
		return 0;
	}

	private int setHeight(String title) {
		for (int i = 0; i < mArrayListHeight.size(); i++) {
			if (title.equals(mArrayListHeight.get(i))) {
				return i;
			}
		}
		return 0;
	}

	private int setWeight(String title) {
		for (int i = 0; i < mArrayListWeight.size(); i++) {
			if (title.equals(mArrayListWeight.get(i))) {
				return i;
			}
		}
		return 0;
	}

	private int setManual(String title) {
		for (int i = 0; i < mArrayListManual.size(); i++) {
			if (title.equals(mArrayListManual.get(i))) {
				return i;
			}
		}
		return 0;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
			String _selectectedDate = selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String currentDateandTime = sdf.format(new Date());
			System.out.println("_selectectedDate  " + _selectectedDate);
			System.out.println("currentDateandTime  " + currentDateandTime);

			if (!isCurrentDate(currentDateandTime, _selectectedDate)) {
				TV_BDATE.setText(selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay);
				userRegistration();
			} else {
				clsGeneral.ShowToast(Profile_Activity.this, "Please select currect birthdate");
			}
		}
	};

	private DatePickerDialog createDialogWithoutDateField() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateandTime = sdf.format(new Date());
		String _temp[] = currentDateandTime.split("-");
		DatePickerDialog dpd = new DatePickerDialog(this, datePickerListener, Integer.parseInt(_temp[0]), (Integer.parseInt(_temp[1])-1), Integer.parseInt(_temp[2]));
		try {
			java.lang.reflect.Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
			for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
				if (datePickerDialogField.getName().equals("mDatePicker")) {
					datePickerDialogField.setAccessible(true);
					DatePicker datePicker = (DatePicker) datePickerDialogField.get(dpd);
					java.lang.reflect.Field[] datePickerFields = datePickerDialogField.getType().getDeclaredFields();
					// for (java.lang.reflect.Field datePickerField :
					// datePickerFields) {
					// Log.i("test", datePickerField.getName());
					// if ("mDaySpinner".equals(datePickerField.getName())) {
					// datePickerField.setAccessible(true);
					// Object dayPicker = new Object();
					// dayPicker = datePickerField.get(datePicker);
					// ((View) dayPicker).setVisibility(View.GONE);
					// }
					// }
				}

			}
		} catch (Exception ex) {
		}
		return dpd;

	}

	public boolean isCurrentDate(String date, String selDate) {
		boolean _isCurent = false;
		try {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			Date date1 = formatter.parse(date);

			Date date2 = formatter.parse(selDate);

			if (date1.compareTo(date2) <= 0) {
				_isCurent=true;
				System.out.println("date2 is Greater than my date1");
			}

		} catch (ParseException e1) {
			e1.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return _isCurent;
	}

	private void userRegistration() {
		// TODO Auto-generated method stub

		String _firstName, _lastName, _email, _pass;
		_firstName = EDT_FNAME.getText().toString().trim();
		_lastName = EDT_LNAME.getText().toString().trim();
		_email = EDT_EMAIL.getText().toString().trim();
		_pass = EDT_PASSWORD.getText().toString().trim();

		if (_email.equals("")) {

			new clsGeneral().ShowToast(Profile_Activity.this, "Please enter email!");
			return;
		}
		String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
		if (!(_email.matches(emailPattern))) {
			clsGeneral.ShowToast(Profile_Activity.this, "Invalid email id!");
			return;
		}

		// if (_pass.equals("")) {
		// clsGeneral.ShowToast(Profile_Activity.this,
		// "Please enter password!");
		// return;
		// }
		// if (_firstName.equals("")) {
		// clsGeneral.ShowToast(Profile_Activity.this,
		// "Please enter first name!");
		// return;
		// }
		// if (_lastName.equals("")) {
		// clsGeneral.ShowToast(Profile_Activity.this,
		// "Please enter last name!");
		// return;
		// }
		new asyncToUpdateAccount(_email, _pass, _firstName, _lastName, new clsPref(Profile_Activity.this).getFBID(), mTextviewGender.getText().toString(), TV_BDATE.getText().toString(), EDT_PHONE.getText().toString()).execute();

	}

	private class asyncToUpdateAccount extends AsyncTask<Void, Void, Void> {

		private Exception EX;
		String EMAIL, PASSWORD, FIRST_NAME, LAST_NAME, FACEBOOK_ID, GENDER,
				BIRTH_DATE, PHONE_NO;
		String MSG;

		public asyncToUpdateAccount(String email, String password, String firstName, String LastName, String facebookId, String gender, String birthDate, String phone) {

			this.EMAIL = email;
			this.PASSWORD = password;
			this.BIRTH_DATE = birthDate;
			this.FACEBOOK_ID = facebookId;
			this.GENDER = gender;
			this.PHONE_NO = phone;
			this.FIRST_NAME = firstName;
			this.LAST_NAME = LastName;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			PROGRESSBAR = ProgressDialog.show(Profile_Activity.this, "Loading", "Please wait");

		}

		@Override
		protected Void doInBackground(Void... voids) {
			try {
				MSG = new clsUser(Profile_Activity.this).signUpdateAccount(EMAIL, PASSWORD, FIRST_NAME, LAST_NAME, FACEBOOK_ID, GENDER, BIRTH_DATE, PHONE_NO, new clsPref(Profile_Activity.this).getLatitude(), new clsPref(Profile_Activity.this).getLongitude());
			} catch (Exception ex) {
				EX = ex;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (PROGRESSBAR.isShowing()) {
				PROGRESSBAR.dismiss();
			}
			if (EX != null) {
				clsGeneral.ShowToast(Profile_Activity.this, EX.getMessage().toString());
				return;
			}
			clsGeneral.ShowToast(Profile_Activity.this, MSG);

//			Intent _intent = new Intent(Profile_Activity.this, Home_Activity.class);
//			_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//			_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			// _intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			startActivity(_intent);
//			finish();
		}
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		userRegistration();
		return false;
	}
}
