package com.freestyle.android.activity;

import com.freestyle.android.R;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WatchCommand_Activity extends BaseActivity
{
	String[] countries;
	int[] pic;
	
	ImageView watch_back_img;
	
	LinearLayout fs_command_1_lay,fs_command_2_lay,fs_command_3_lay;
	ImageView fs_command_1_img,fs_command_2_img,fs_command_3_img;
	TextView fs_command_1_txt,fs_command_2_txt,fs_command_3_txt;
	TextView fs_command1_txt1,fs_command1_txt2,fs_command2_txt1,fs_command2_txt2,fs_command3_txt1,fs_command3_txt2;
	
	private String c1;
	private String c2;
	private String c3;
	
	Intent mIntent;
	Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.watchcommand_activity);
		
		initDrawer(WatchCommand_Activity.this);
		mContext = WatchCommand_Activity.this;
		countries = new String[]{ "Camera", "Play/Pause Music", "Find Phone", "Next Song", "Off"};

		pic=new int[]
				{ R.drawable.n_camera, R.drawable.n_play, R.drawable.n_find_phone, R.drawable.n_next_play, R.drawable.n_off_icon_b};

		watch_back_img = (ImageView)findViewById(R.id.watch_back_img);
		watch_back_img.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				onBackPressed();
			}
		});
		
		fs_command_1_lay=(LinearLayout)findViewById(R.id.fs_command_1_lay);
		fs_command_2_lay=(LinearLayout)findViewById(R.id.fs_command_2_lay);
		fs_command_3_lay=(LinearLayout)findViewById(R.id.fs_command_3_lay);
		
		fs_command_1_img = (ImageView) findViewById(R.id.fs_command_1_img);
		fs_command_2_img = (ImageView) findViewById(R.id.fs_command_2_img);
		fs_command_3_img = (ImageView) findViewById(R.id.fs_command_3_img);
		fs_command_1_txt = (TextView) findViewById(R.id.fs_command_1_txt);
		fs_command_2_txt = (TextView) findViewById(R.id.fs_command_2_txt);
		fs_command_3_txt = (TextView) findViewById(R.id.fs_command_3_txt);
		
		fs_command1_txt1 = (TextView)findViewById(R.id.fs_command1_txt1);
		fs_command1_txt2 = (TextView)findViewById(R.id.fs_command1_txt2);
		fs_command2_txt1 = (TextView)findViewById(R.id.fs_command2_txt1);
		fs_command2_txt2 = (TextView)findViewById(R.id.fs_command2_txt2);
		fs_command3_txt1 = (TextView)findViewById(R.id.fs_command3_txt1);
		fs_command3_txt2 = (TextView)findViewById(R.id.fs_command3_txt2);

		fs_command1_txt1.setText(Html.fromHtml("Press/hold <b>SET</b> button on watch"));
		fs_command1_txt2.setText(Html.fromHtml("to operate Command 1."));
		fs_command2_txt1.setText(Html.fromHtml("Press/hold <b>CONNECT</b> button on watch"));
		fs_command2_txt2.setText(Html.fromHtml("to operate Command 2."));
		fs_command3_txt1.setText(Html.fromHtml("Press/hold <b>COMMAND</b> button on"));
		fs_command3_txt2.setText(Html.fromHtml("watch to operate Command 3."));
		
		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0] );
		c2 = sharedata.getString("C2", countries[1] );
		c3 = sharedata.getString("C3", countries[2] );

		fs_command_1_txt.setText(c3+ "");
		fs_command_2_txt.setText(c1 + ")");
		fs_command_3_txt.setText(c2+ "");
		
		fs_command_1_img.setImageResource(pic[setPic(c1)]);
		fs_command_2_img.setImageResource(pic[setPic(c2)]);
		fs_command_3_img.setImageResource(pic[setPic(c3)]);

		showDefaultWin("C1");
		showDefaultWin("C2");
		showDefaultWin("C3");
		
		fs_command_1_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				showWin("C1");
			}
		});
		
		fs_command_2_lay.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				showWin("C2");
			}
		});
		
		fs_command_3_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				showWin("C3");
			}
		});
	}
	
	private void showWin(final String flag)
	{
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.ctrolactivitydialog);
		dlg.show();

		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0]);
		c2 = sharedata.getString("C2", countries[1]);
		c3 = sharedata.getString("C3", countries[2]);

		Button cancelButton = (Button) dlg.findViewById(R.id.CancelButton);
		cancelButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dlg.cancel();
			}
		});

		Button saveButton = (Button) dlg.findViewById(R.id.SaveButton);
		saveButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (flag.equals("C1"))
				{
					SharedPreferences.Editor sharedata = getSharedPreferences("control", 0).edit();
					sharedata.putString("C1", c1);
					sharedata.commit();

					fs_command_1_img.setImageResource(pic[setPic(c1)]);
					fs_command_1_txt.setText(c1 +"");
				}
				else if (flag.equals("C2"))
				{
					SharedPreferences.Editor sharedata = getSharedPreferences("control", 0).edit();
					sharedata.putString("C2", c2);
					sharedata.commit();

					fs_command_2_img.setImageResource(pic[setPic(c2)]);
					fs_command_2_txt.setText(c2 + "");
				}
				else
				{
					SharedPreferences.Editor sharedata = getSharedPreferences("control", 0).edit();
					sharedata.putString("C3", c3);
					sharedata.commit();

					fs_command_3_img.setImageResource(pic[setPic(c3)]);
					fs_command_3_txt.setText(c3 + "");
				}
				dlg.dismiss();
			}
		});

		WheelView country = (WheelView) dlg.findViewById(R.id.country);
		try
		{
			Integer a = (int) getResources().getDimension(R.dimen.testsp);
			country.setTEXT_SIZE(a);
		}
		catch (Exception e)
		{
			country.setTEXT_SIZE(60);
		}

		country.setVisibleItems(5);

		country.setAdapter(new ArrayWheelAdapter<String>(countries));
		country.addChangingListener(new OnWheelChangedListener()
		{
			public void onChanged(WheelView wheel, int oldValue, int newValue)
			{
				if (flag.equals("C1"))
					c1 = countries[wheel.getCurrentItem()];
				else if (flag.equals("C2"))
					c2 = countries[wheel.getCurrentItem()];
				else
					c3 = countries[wheel.getCurrentItem()];
			}
		});

		if (flag.equals("C1"))
			country.setCurrentItem(setPic(c1));
		else if (flag.equals("C2"))
			country.setCurrentItem(setPic(c2));
		else 
			country.setCurrentItem(setPic(c3));
	}
	
	private int setPic(String title)
	{
		for (int i = 0; i < countries.length; i++)
		{
			if (title.equals(countries[i]))
			{
				return i;
			}
		}
		return 0;
	}
	
	private void showDefaultWin(final String flag)
	{
		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0]);
		c2 = sharedata.getString("C2", countries[1]);
		c3 = sharedata.getString("C3", countries[2]);
		if (flag.equals("C1"))
		{
			SharedPreferences.Editor sharedata1 = getSharedPreferences("control", 0).edit();
			sharedata1.putString("C1", c1);
			sharedata1.commit();
			int temp = pic[setPic(c1)];
			fs_command_1_img.setImageResource(temp);
			fs_command_1_txt.setText(c1 +"");
		}
		else if (flag.equals("C2"))
		{
			SharedPreferences.Editor sharedata2 = getSharedPreferences("control", 0).edit();
			sharedata2.putString("C2", c2);
			sharedata2.commit();
			int temp = pic[setPic(c2)];
			fs_command_2_img.setImageResource(temp);
			fs_command_2_txt.setText(c2 + "");

		}
		else if (flag.equals("C3"))
		{
			SharedPreferences.Editor sharedata3 = getSharedPreferences("control", 0).edit();
			sharedata3.putString("C3", c3);
			sharedata3.commit();
			int temp = pic[setPic(c3)];
			fs_command_3_img.setImageResource(temp);
			fs_command_3_txt.setText(c3 + "");

		}
		else
		{
		}
		if (flag.equals("C1"))
		{
			c1 = countries[0];
		}
		else if (flag.equals("C2"))
		{
			c2 = countries[1];
		}
		else if (flag.equals("C3"))
		{
			c3 = countries[2];
		}
		else
		{
		}

		if (flag.equals("C1"))
		{
			setPic(c1);
		}
		else if (flag.equals("C2"))
		{
			setPic(c2);
		}
		else if (flag.equals("C3"))
		{
			setPic(c3);
		}
		else
		{
			
		}
	}
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		mIntent = new Intent(context, Settings_Activity.class);
		startActivity(mIntent);
		finish();
	}
	
}
