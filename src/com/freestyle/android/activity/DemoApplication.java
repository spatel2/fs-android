package com.freestyle.android.activity;

import java.util.ArrayList;
import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import com.freestyle.android.R;
import com.freestyle.android.bluebooth.BluetoothLeService;
import com.freestyle.android.config.GetAllData;

@ReportsCrashes(formKey = "", // will not be used
mailTo = "sahil@elegantmicroweb.com",
customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },                
mode = ReportingInteractionMode.TOAST,
resToastText = R.string.crash_toast_text)

public class DemoApplication extends Application 
{
	public static DbTool.TrackInfo Track = new DbTool.TrackInfo();
	
	private BluetoothLeService mBluetoothLeService;
	public static String mBluetoothDevice;

	public static ArrayList<BluetoothDevice> mLeDevices = new ArrayList<BluetoothDevice>();
	public static ArrayList<BluetoothDevice> connected = new ArrayList<BluetoothDevice>();

	public static final int STATE_DISCONNECTED	= 0;
	public static final int STATE_CONNECTING	= 1;
	public static final int STATE_CONNECTED		= 2;

	public static int mConnectionState = STATE_DISCONNECTED;

	public static DemoApplication context;
	public static ContactsManager Adressbook;
	public static Uri ProfilePictureUri=null;
	public static GetAllData mGetAllData;
	
	public static BluetoothLeService mBluetoothLeService_temp;
	
	//New Variable For Location
	public static float 	CurrentDistance		=0;
	public static double 	CurrentCalories		=0.0;
	public static float 	CurrentMaxSpeed		=0;
	public static float 	CurrentMinSpeed		=0;
	public static String 	CurrentCity			="";
	public static float 	CurrentSpeed		=0;
	public static String 	CurrentAltitude		="0.0";
	public static long 		CurrentTotalTime	=0; //seconds
	public static long 		CurrentStartTime	=0; //seconds
	public static long 		CurrentelapsedTime	=0; //seconds
	public static int 		CurrentHeartRate	=0;
	public static String 	startActivityDeviceTime		="";
	public static boolean	isFirstTimeStarted 	= true;
	public static boolean 	speedInKilometer;
	public static String 	CurrentDirection	="N/A";
	public static String 	CurrentLatLong		=	"";
	
	
	
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		Intent bleIntent = new Intent(this, BluetoothLeService.class);
		startService(bleIntent);
		
		mGetAllData = new GetAllData();
		bindService(bleIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
		Adressbook = new ContactsManager(this);
		
		context = this;
		ACRA.init(this);
	}

	@Override
	public void onTerminate()
	{
		super.onTerminate();
		unbindService(mServiceConnection);
	}

	public void ChangeConnectionState(int newState, String DeviceAddress)
	{
		mConnectionState = newState;
		mBluetoothDevice = DeviceAddress;
		Intent intent;

		switch (mConnectionState)
		{
		case STATE_CONNECTING:		intent=new Intent("BL_STATECHANGE_CONNECTING");		break;
		case STATE_CONNECTED:		intent=new Intent("BL_STATECHANGE_CONNECTED");		break;
		default:
		case STATE_DISCONNECTED:	intent=new Intent("BL_STATECHANGE_DISCONNECTED");	break;
		}

		sendBroadcast(intent);
	}

	private ServiceConnection mServiceConnection = new ServiceConnection() 
	{
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder service) 
		{
			mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
			if (mBluetoothLeService == null) 
			{
				Log.e("Application", "Binding of Bluetoothservice failed");
				return;
			}
			else
			{
				Intent intent =new Intent("BL_STATECHANGE_BL_SERVICE_UPDATED");
				sendBroadcast(intent);
			}

			if (!mBluetoothLeService.initialize(context)) 
			{
				Log.e("Application", "Initialisation of Bluetoothservice failed");
				return;
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) 
		{
			mBluetoothLeService = null;
		}
	};

	public BluetoothLeService getmBluetoothLeService()
	{
		return mBluetoothLeService;
	}

	public void setmBluetoothLeService(BluetoothLeService mBluetoothLeService)
	{
		this.mBluetoothLeService = mBluetoothLeService;
	}
}
