package com.freestyle.android.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraUtils;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.freestyle.android.R;

@SuppressWarnings("deprecation")
public class DemoCameraFragment extends CameraFragment 
{
	private static final String KEY_USE_FFC = "com.commonsware.cwac.camera.demo.USE_FFC";
	private MenuItem autoFocusItem=null;
	private MenuItem takePictureItem=null;
	private MenuItem flashItem=null;
	private MenuItem recordItem=null;
	private long lastFaceToast=0L;
	String flashMode=null;

	static DemoCameraFragment newInstance(boolean useFFC) 
	{
		DemoCameraFragment f=new DemoCameraFragment();
		Bundle args=new Bundle();

		args.putBoolean(KEY_USE_FFC, useFFC);
		f.setArguments(args);

		return(f);
	}

	@Override
	public void onCreate(Bundle state) 
	{
		super.onCreate(state);
		SimpleCameraHost.Builder builder = new SimpleCameraHost.Builder(new DemoCameraHost(getActivity()));
		setHost(builder.useFullBleedPreview(true).build());
	}

	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) 
	{
		View cameraView = super.onCreateView(inflater, container, savedInstanceState);
		View results=inflater.inflate(R.layout.fragment, container, false);

		((ViewGroup)results.findViewById(R.id.camera)).addView(cameraView);

		setRecordingItemVisibility();

		return(results);
	}

	@Override
	public void onPause() {
		super.onPause();

		getActivity().invalidateOptionsMenu();
	}

	void setRecordingItemVisibility() 
	{
		if (recordItem != null) 
		{
			if(getDisplayOrientation() != 0 && getDisplayOrientation() != 180) 
			{
				recordItem.setVisible(false);
			}
		}
	}

	void takeSimplePicture() {

		PictureTransaction xact=new PictureTransaction(getHost());

		if (flashItem!=null && flashItem.isChecked()) {
			xact.flashMode(flashMode);
		}

		takePicture(xact);
	}

	class DemoCameraHost extends SimpleCameraHost implements Camera.FaceDetectionListener {
		boolean supportsFaces=false;

		public DemoCameraHost(Context _ctxt) {
			super(_ctxt);
		}

		@Override
		public boolean useFrontFacingCamera() {
			if (getArguments() == null) {
				return(false);
			}

			return(getArguments().getBoolean(KEY_USE_FFC));
		}


		@Override
		public void saveImage(PictureTransaction xact, byte[] image) {

			ImageDisplayActivity.imageToShow=image;
			super.saveImage(xact, image);
			startActivity(new Intent(getActivity(), ImageDisplayActivity.class));
		}

		@Override
		public void autoFocusAvailable() {
			if (autoFocusItem != null) {
				autoFocusItem.setEnabled(true);

				if (supportsFaces)
					startFaceDetection();
			}
		}

		@Override
		public void autoFocusUnavailable() {
			if (autoFocusItem != null) {
				stopFaceDetection();

				if (supportsFaces)
					autoFocusItem.setEnabled(false);
			}
		}

		@Override
		public void onCameraFail(CameraHost.FailureReason reason) {
			super.onCameraFail(reason);

			//			Toast.makeText(getActivity(),
			//					"Sorry, but you cannot use the camera now!",
			//					Toast.LENGTH_LONG).show();
		}

		@Override
		public Parameters adjustPreviewParameters(Parameters parameters) 
		{
			flashMode = CameraUtils.findBestFlashModeMatch(parameters,Camera.Parameters.FLASH_MODE_RED_EYE,Camera.Parameters.FLASH_MODE_AUTO,Camera.Parameters.FLASH_MODE_ON);

			if (parameters.getMaxNumDetectedFaces() > 0) {
				supportsFaces=true;
			}
			else {
				//				Toast.makeText(getActivity(),"Face detection not available for this camera",Toast.LENGTH_LONG).show();
			}

			return(super.adjustPreviewParameters(parameters));
		}

		@Override
		public void onFaceDetection(Face[] faces, Camera camera) {
			if (faces.length > 0) 
			{
				long now=SystemClock.elapsedRealtime();

				if (now > lastFaceToast + 10000) {
					//					Toast.makeText(getActivity(), "I see your face!",
					//							Toast.LENGTH_LONG).show();
					lastFaceToast=now;
				}
			}
		}

		@Override
		@TargetApi(16)
		public void onAutoFocus(boolean success, Camera camera) {
			super.onAutoFocus(success, camera);

			takePictureItem.setEnabled(true);
		}

		@Override
		public boolean mirrorFFC() {
			return(true);
		}
	}
}