package com.freestyle.android.activity;

import java.util.ArrayList;
import com.freestyle.android.R;
import com.freestyle.android.config.CommonClass;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_log_map extends BaseActivity
{
	private GoogleMap googleMap;
	CommonClass mCommonClass;
	Context context;
	Intent mIntent = null;
	ImageView activity_log_back_img;
	TextView activity_log_title;
	String LatLongTrackRecord = "";

	ArrayList<Double> mLatArray = new ArrayList<Double>();
	ArrayList<Double> mLonArray = new ArrayList<Double>();

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_map);

		context = Activity_log_map.this;
		initDrawer(Activity_log_map.this);

		activity_log_back_img	=	(ImageView)findViewById(R.id.activity_log_back_img1);
		activity_log_title		=	(TextView)findViewById(R.id.activity_log_title);

		mIntent = getIntent();
		if(mIntent.getExtras()!=null)
		{
			try
			{
				activity_log_title.setText(mIntent.getExtras().getString("sportname"));
				LatLongTrackRecord = mIntent.getExtras().getString("LatLongArray");
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
		}

		if(check_Internet(Activity_log_map.this))
		{
			try
			{
				initilizeMap();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			Toast.makeText(Activity_log_map.this, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
		}
		
		activity_log_back_img.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				finish();	
			}
		});
	}

	@SuppressLint("NewApi")
	private void initilizeMap() 
	{
		if (googleMap == null) 
		{
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_log_location)).getMap();

			if (googleMap == null) 
			{
				Toast.makeText(getApplicationContext(),"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
			}

			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

			googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback()
			{
				@Override
				public void onMapLoaded() 
				{
					getLatLongArray();
					googleMap.clear();

					ArrayList<LatLng> arrayPoints = new ArrayList<LatLng>();
					PolylineOptions polylineOptions = new PolylineOptions(); 

					if(mLatArray.size()>0)
					{
						LatLng mapCenter = new LatLng(mLatArray.get(0), mLonArray.get(0));
						googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, 15));

						for(int i=0;i<mLatArray.size();i++)
						{
							// settin polyline in the map 
							polylineOptions.color(Color.BLUE); 
							polylineOptions.width(5);
							arrayPoints.add(new LatLng(mLatArray.get(i), mLonArray.get(i))); 
							if(i==mLatArray.size()-1)
							{
								googleMap.addMarker(new MarkerOptions()
								.position(new LatLng(mLatArray.get(i), mLonArray.get(i)))
								.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))); 
							}
						}
					}
					polylineOptions.addAll(arrayPoints); 
					googleMap.addPolyline(polylineOptions);
				}
			});
		}
	}

	private void getLatLongArray()
	{
		try
		{
			mLatArray.clear();
			mLonArray.clear();

			String[] parts = LatLongTrackRecord.split(",");
			for(int i=0;i<parts.length;i++)
			{
				if(!parts[i].equals(""))
				{
					String[] latlonArr = parts[i].split("#");
					
					mLatArray.add(Double.parseDouble(latlonArr[0]));
					mLonArray.add(Double.parseDouble(latlonArr[1]));
					
					System.out.println(latlonArr[0]+"");
					System.out.println(latlonArr[1]+"");
				}
			}
			
		}
		catch(Exception ex)
		{
			System.out.println("Error in Data conversion "+ ex);
		}
	}
}
