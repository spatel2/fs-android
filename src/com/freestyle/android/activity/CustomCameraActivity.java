package com.freestyle.android.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.freestyle.android.R;
import com.freestyle.android.config.Shared_preference;

public class CustomCameraActivity extends Activity  
{
	private DemoCameraFragment std=null;
	private DemoCameraFragment ffc=null;
	private DemoCameraFragment current=null;

	ImageButton mButton;
	ImageView mImageViewSwitchCamera;

	Shared_preference shared;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_camera);

		shared = new Shared_preference(this);
		shared.setPrefrence(this, "cam_status", "open");

		registerReceiver(mHandleMessageReceiver, new IntentFilter("com.android.captureImageForPhone"));

		mImageViewSwitchCamera = (ImageView)findViewById(R.id.swich_camera);
		mButton = (ImageButton)findViewById(R.id.take_picture);
		mButton.setEnabled(true);
		mButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				playAlertTone(getApplicationContext());
				mButton.setEnabled(false);
				current.takePicture();
			}
		});

		mImageViewSwitchCamera.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {

				if(!Tags.Camera_Flag)
				{
					if (std == null) {
						std=DemoCameraFragment.newInstance(false);
					}

					current=std;
					Tags.Camera_Flag = true;
				}
				else 
				{
					if (ffc == null) {
						ffc=DemoCameraFragment.newInstance(true);
					}

					current=ffc;
					Tags.Camera_Flag = false;
				}

				getFragmentManager().beginTransaction().replace(R.id.container, current).commit();
			}
		});

		onNavigationItemSelected(Tags.Camera_Flag);
	}

	public boolean onNavigationItemSelected(boolean position) 
	{
		if (position) 
		{
			if (std == null) 
			{
				std=DemoCameraFragment.newInstance(false);
			}

			current=std;
		}
		else {
			if (ffc == null) {
				ffc=DemoCameraFragment.newInstance(true);
			}

			current=ffc;
		}

		getFragmentManager().beginTransaction().replace(R.id.container, current).commit();

		return(true);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_CAMERA && current != null) 
		{
			current.takePicture();

			return(true);
		}

		return(super.onKeyDown(keyCode, event));
	}
	public void playAlertTone(final Context context)
	{
		Thread t = new Thread()
		{
			public void run()
			{
				MediaPlayer player = null;
				int countBeep = 0;
				while(countBeep<1)
				{
					player = MediaPlayer.create(context,R.raw.camera4);
					player.start();
					countBeep+=1;
					try 
					{
						Thread.sleep(player.getDuration());
						player.release();
					}
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
			}
		};

		t.start();   
	}

	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			playAlertTone(getApplicationContext());
			current.takePicture();
		}
	};

	@Override
	protected void onResume() 
	{
		super.onResume();
		if(mButton != null)
			mButton.setEnabled(true);

		shared.setPrefrence(this, "cam_status", "open");
		registerReceiver(mHandleMessageReceiver, new IntentFilter("com.android.captureImageForPhone"));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		shared.setPrefrence(this, "cam_status", "");
		unregisterReceiver(mHandleMessageReceiver);
	}

	@Override
	public void onBackPressed() 
	{
		shared.setPrefrence(this, "cam_status", "");
		Intent intent = new Intent(CustomCameraActivity.this, Home_Activity.class);
		startActivity(intent);
		finish();
	}
	@Override
	protected void onStop() 
	{
		shared.setPrefrence(this, "cam_status", "");
		super.onStop();
	}

	@Override
	protected void onDestroy() 
	{
		shared.setPrefrence(this, "cam_status", "");
		super.onDestroy();
	}
}
