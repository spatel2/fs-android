package com.freestyle.android.activity;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.freestyle.android.R;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.Shared_preference;
import com.opencsv.CSVWriter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_log_display extends BaseActivity implements OnClickListener
{
	TextView 	log_activity_name;
	ImageView 	log_back_img;
	Intent 		mIntent;
	Context 	context;
	Button 		log_export_btn;
	DbTool 		mDbTool;
	int 		SportID;
	String 		sportname;
	String		logForShare;
	int 		index_no=0;
	String 		LatLongTrackRecord="";

	private DbTool.TrackInfo TrackInfo; 

	TextView log_activity_txt;
	TextView log_location_txt;
	TextView log_date_txt;
	TextView log_time_txt;
	TextView log_duration_txt;
	TextView log_distance_txt;
	TextView log_avg_speed_txt;
	TextView log_max_speed_txt;
	TextView log_min_speed_txt;
	TextView log_heart_rate_txt;
	TextView log_calories_txt;
	TextView log_direction_txt;
	TextView log_altitude_txt;
	Button log_share_btn;
	Button log_share_delete;
	ImageView activity_log_map_img;

	//For preference of imperial and matric
	boolean isMetricTemp			=	false;
	Shared_preference shared;

	@SuppressLint("SimpleDateFormat") @Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_display);

		context = Activity_log_display.this;
		shared = new Shared_preference(context);
		
		initDrawer(Activity_log_display.this);
		if(shared.getPrefrencewithDefault(context, Constants.PROFILE.UNIT_IMPERIAL_METRIC,"false").toString().equals("true"))
		{
			isMetricTemp = true;
		}
		else
		{
			isMetricTemp = false;
		}

		//Summary TextViews
		log_activity_txt 	= (TextView)findViewById(R.id.log_activity_txt);
		log_location_txt 	= (TextView)findViewById(R.id.log_location_txt);
		log_date_txt 		= (TextView)findViewById(R.id.log_date_txt);
		log_time_txt 		= (TextView)findViewById(R.id.log_time_txt);
		log_duration_txt 	= (TextView)findViewById(R.id.log_duration_txt);
		log_distance_txt 	= (TextView)findViewById(R.id.log_distance_txt);
		log_avg_speed_txt 	= (TextView)findViewById(R.id.log_avg_speed_txt);
		log_max_speed_txt 	= (TextView)findViewById(R.id.log_max_speed_txt);
		log_min_speed_txt 	= (TextView)findViewById(R.id.log_min_speed_txt);
		log_heart_rate_txt 	= (TextView)findViewById(R.id.log_heart_rate_txt);
		log_calories_txt 	= (TextView)findViewById(R.id.log_calories_txt);
		log_direction_txt 	= (TextView)findViewById(R.id.log_direction_txt);
		log_altitude_txt 	= (TextView)findViewById(R.id.log_altitude_txt);
		log_activity_name 	= (TextView)findViewById(R.id.log_activity_name);
		log_back_img 		= (ImageView)findViewById(R.id.log_back_img);
		log_export_btn 		= (Button)findViewById(R.id.log_export_btn);
		log_share_btn		= (Button)findViewById(R.id.log_share_btn);
		log_share_delete	= (Button)findViewById(R.id.log_share_delete);
		activity_log_map_img= (ImageView)findViewById(R.id.activity_log_map_img);
		
		context = Activity_log_display.this;

		SportID = getIntent().getExtras().getInt("sportid");
		mDbTool = new DbTool(context);
		TrackInfo = mDbTool.GetTrack(String.valueOf(SportID));

		mIntent = getIntent();
		if(mIntent!=null)
		{
			sportname = mIntent.getExtras().getString("sportname");
		}

		log_activity_name.setText(sportname);

		Cursor mCursor = mDbTool.getSportByID(SportID+"");
		if(mCursor!=null)
		{
			if(mCursor.moveToNext())
			{
				
				index_no = mCursor.getInt(0);
				if(TrackInfo.track_unit.equals("m"))
				{
					if(isMetricTemp)
					{
						log_avg_speed_txt 	.setText("AVERAGE SPEED: "+mCursor.getString(2)	+" KPH");
						log_max_speed_txt 	.setText("MAX SPEED: "+mCursor.getString(3)		+" KPH");
						log_min_speed_txt 	.setText("MIN SPEED: "+mCursor.getString(4)		+" KPH");
						log_distance_txt 	.setText("DISTANCE: "+convertToOneDecimal(TrackInfo.Distance)		+" KM");
					}
					else
					{
						//Metric to imperial
						log_avg_speed_txt 	.setText("AVERAGE SPEED: "+ convertMetricToImperial(mCursor.getString(2))	+" MI/H");
						log_max_speed_txt 	.setText("MAX SPEED: "+ convertMetricToImperial(mCursor.getString(3))		+" MI/H");
						log_min_speed_txt 	.setText("MIN SPEED: "+ convertMetricToImperial(mCursor.getString(4))		+" MI/H");
						log_distance_txt 	.setText("DISTANCE: "+	convertMetricToImperial(TrackInfo.Distance			+"")+" MI");
					}
				}
				else
				{
					if(isMetricTemp)
					{
						//imperial to metric
						log_avg_speed_txt 	.setText("AVERAGE SPEED: "+ convertImperialToMetric(mCursor.getString(2))	+" KPH");
						log_max_speed_txt 	.setText("MAX SPEED: "+convertImperialToMetric(mCursor.getString(3))		+" KPH");
						log_min_speed_txt 	.setText("MIN SPEED: "+convertImperialToMetric(mCursor.getString(4))		+" KPH");
						log_distance_txt 	.setText("DISTANCE: "+convertImperialToMetric(TrackInfo.Distance			+"")+" KM");
					}
					else
					{
						log_avg_speed_txt 	.setText("AVERAGE SPEED: "+mCursor.getString(2)		+" MI/H");
						log_max_speed_txt 	.setText("MAX SPEED: "+mCursor.getString(3)			+" MI/H");
						log_min_speed_txt 	.setText("MIN SPEED: "+mCursor.getString(4)			+" MI/H");
						log_distance_txt 	.setText("DISTANCE: "+convertToOneDecimal(TrackInfo.Distance)		+" MI");
					}
				}
				log_heart_rate_txt 	.setText("HEART RATE: "+mCursor.getString(5)+" ");
				log_calories_txt 	.setText("CALORIES: "+mCursor.getString(6) +" KCAL");
				log_location_txt 	.setText("LOCATION: "+mCursor.getString(13));
			
				//To send data to next activity
				LatLongTrackRecord = TrackInfo.track_latlon;
			}
		}

		log_activity_txt 	.setText("ACTIVITY: "+sportname);	
		log_date_txt 		.setText("DATE: "+TrackInfo.track_date);
		log_time_txt 		.setText("TIME: "+TrackInfo.track_time);
		log_duration_txt 	.setText("DURATION: "+TrackInfo.Duration);
		

		log_direction_txt 	.setText("DIRECTION: "+TrackInfo.Direction);
		log_altitude_txt	.setText("ELEVATION: "+TrackInfo.Altitude);

		log_back_img	.setOnClickListener(this);
		log_export_btn	.setOnClickListener(this);
		log_export_btn	.setOnClickListener(this);
		log_share_btn	.setOnClickListener(this);
		log_share_delete.setOnClickListener(this);

		//Log for Share
		logForShare = 	log_activity_txt 	.getText().toString()+"\n"+
				log_date_txt 		.getText().toString()+"\n"+
				log_location_txt 	.getText().toString()+"\n"+
				log_time_txt 		.getText().toString()+"\n"+
				log_duration_txt 	.getText().toString()+"\n"+
				log_avg_speed_txt 	.getText().toString()+"\n"+
				log_min_speed_txt 	.getText().toString()+"\n"+
				log_max_speed_txt 	.getText().toString()+"\n"+
				log_distance_txt 	.getText().toString()+"\n"+
				log_heart_rate_txt 	.getText().toString()+"\n"+
				log_calories_txt 	.getText().toString()+"\n"+
				log_direction_txt 	.getText().toString()+"\n"+
				log_altitude_txt 	.getText().toString()+"\n";
		
		
		activity_log_map_img.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				mIntent = new Intent(Activity_log_display.this,Activity_log_map.class);
				mIntent.putExtra("sportname", sportname);
				mIntent.putExtra("LatLongArray", LatLongTrackRecord);
				startActivity(mIntent);
			}
		});
	}

	@Override
	public void onBackPressed() 
	{
		mIntent = new Intent(context, ActivityLog_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void onClick(View v) 
	{
		if(v==log_back_img)
		{
			mIntent = new Intent(context, ActivityLog_Activity.class);
			startActivity(mIntent);
			finish();
		}
		else if(v==log_export_btn)
		{
			createPDF();
		}
		else if(v==log_share_btn)
		{
			ShareLog();
		}
		else if(v==log_share_delete)
		{
			deleteRecordDialog();
		}
	}

	@SuppressLint("SimpleDateFormat") 
	public void createPDF()
	{
		String envirement_path = Environment.getExternalStorageDirectory().getAbsolutePath();

		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat ("dd MMM yyyy");
		String filePath = "/FS "+log_activity_name.getText().toString()+" "+ft.format(dNow)+ ".csv";
		CSVWriter writer = null;
		try 
		{			
			writer = new CSVWriter(new FileWriter(envirement_path+filePath), ',');

			Cursor mCursor = mDbTool.getSportByID(SportID+"");
			if(mCursor!=null)
			{
				if(mCursor.moveToNext())
				{	    
					String log1 ="ACTIVITY: 		#"+sportname;
					String log2 ="DATE: 			#"+TrackInfo.track_date;
					String log3 ="LOCATION: 		#"+mCursor.getString(13);
					String log4 ="TIME: 			#"+TrackInfo.track_time;
					String log5 ="DURATION: 		#"+TrackInfo.Duration;

					String log6="";
					String log7="";
					String log8="";

					if(TrackInfo.track_unit.equals("m"))
					{
						log6 ="AVERAGE SPEED: 	#"+mCursor.getString(2)+" KPH";
						log7 ="MIN SPEED: 		#"+mCursor.getString(4)+" KPH";
						log8 ="MAX SPEED: 		#"+mCursor.getString(3)+" KPH";
					}
					else
					{
						log6 ="AVERAGE SPEED: 	#"+mCursor.getString(2)+" MI/H";
						log7 ="MIN SPEED: 		#"+mCursor.getString(4)+" MI/H";
						log8 ="MAX SPEED: 		#"+mCursor.getString(3)+" MI/H";
					}

					String log9 ="DISTANCE: 		#"+convertToOneDecimal(TrackInfo.Distance);
					String log10 ="HEART RATE: 		#"+mCursor.getString(5)+" BPM";
					String log11 ="CALORIES: 		#"+mCursor.getString(6)+" KCAL";
					String log12 ="DIRECTION: 		#"+TrackInfo.Direction;
					String log13 ="ELEVATION: 		#"+TrackInfo.Altitude;

					String[] entries1 = log1.split("#");
					String[] entries2 = log2.split("#");
					String[] entries3 = log3.split("#");
					String[] entries4 = log4.split("#");
					String[] entries5 = log5.split("#");
					String[] entries6 = log6.split("#");
					String[] entries7 = log7.split("#");
					String[] entries8 = log8.split("#");
					String[] entries9 = log9.split("#");
					String[] entries10 = log10.split("#");
					String[] entries11 = log11.split("#");
					String[] entries12 = log12.split("#");
					String[] entries13 = log13.split("#");

					writer.writeNext(entries1);
					writer.writeNext(entries2);
					writer.writeNext(entries3);
					writer.writeNext(entries4);
					writer.writeNext(entries5);
					writer.writeNext(entries6);
					writer.writeNext(entries7);
					writer.writeNext(entries8);
					writer.writeNext(entries9);
					writer.writeNext(entries10);
					writer.writeNext(entries11);
					writer.writeNext(entries12);
					writer.writeNext(entries13);
					writer.close();

					exportLogDialog();
				}
			}
		} 
		catch (IOException e)
		{
			Toast.makeText(context, "Log generate error. Please try again ", Toast.LENGTH_SHORT).show();
		}
	}      

	private void ShareLog()
	{
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, logForShare);
		startActivity(sharingIntent);
	}

	private void exportLogDialog()
	{
		final Dialog mDialog = new Dialog(Activity_log_display.this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setCancelable(false);
		mDialog.setContentView(R.layout.infl_export_display);
		mDialog.show();

		ButtonClass export_button_ok_single = (ButtonClass)mDialog.findViewById(R.id.export_button_ok_single);

		export_button_ok_single.setOnClickListener(new OnClickListener()
		{
			@SuppressLint("SimpleDateFormat") @Override
			public void onClick(View v) 
			{
				mDialog.cancel();
			}
		});
	}
	
	
	private void deleteRecordDialog()
	{
		final Dialog mDialog = new Dialog(Activity_log_display.this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setCancelable(false);
		mDialog.setContentView(R.layout.infl_delect_display);
		mDialog.show();

		ButtonClass delete_button_yes_single = (ButtonClass)mDialog.findViewById(R.id.delete_button_yes_single);
		ButtonClass delete_button_no_single = (ButtonClass)mDialog.findViewById(R.id.delete_button_no_single);

		delete_button_yes_single.setOnClickListener(new OnClickListener()
		{
			@SuppressLint("SimpleDateFormat") 
			@Override
			public void onClick(View v) 
			{
				//Delete the record
				deleteRecord();
				mDialog.cancel();
				
				
				mIntent = new Intent(context, ActivityLog_Activity.class);
				startActivity(mIntent);
				finish();
			}
		});
		
		delete_button_no_single.setOnClickListener(new OnClickListener()
		{
			@SuppressLint("SimpleDateFormat") 
			@Override
			public void onClick(View v) 
			{
				
				mDialog.cancel();
			}
		});
	}


	private String convertImperialToMetric(String mString)
	{
		if(!mString.equals(""))
		{
			try
			{
				String val_str = ((Float.parseFloat(mString)*100)/60)+"";

				String[] SplitStringcal= val_str.split("\\.");
				//
				if (SplitStringcal[0].length() > 2)
				{
					SplitStringcal[0] = SplitStringcal[0].substring(0, 2);
				}

				if (SplitStringcal[1].length() > 1)
				{
					SplitStringcal[1] = SplitStringcal[1].substring(0, 1);
				}
				return SplitStringcal[0]+"."+SplitStringcal[1];
			}
			catch(Exception ex)
			{
				System.out.println(ex);
				return "0";
			}
		}
		else
		{
			return "0";
		}

	}

	private String convertMetricToImperial(String mString)
	{
		if(!mString.equals(""))
		{
			try
			{
				String val_str = ((Float.parseFloat(mString)*60)/100)+"";

				String[] SplitStringcal= val_str.split("\\.");
				//
				if (SplitStringcal[0].length() > 2)
				{
					SplitStringcal[0] = SplitStringcal[0].substring(0, 2);
				}

				if (SplitStringcal[1].length() > 1)
				{
					SplitStringcal[1] = SplitStringcal[1].substring(0, 1);
				}
				return SplitStringcal[0]+"."+SplitStringcal[1];
			}
			catch(Exception ex)
			{
				System.out.println(ex);
				return "0";
			}
		}
		else
		{
			return "0";
		}

	}

	private String convertToOneDecimal(float value)
	{
		String val_str = value+"";
		
		String[] SplitStringcal= val_str.split("\\.");
		//
		if (SplitStringcal[0].length() > 2)
		{
			SplitStringcal[0] = SplitStringcal[0].substring(0, 2);
		}
		
		if (SplitStringcal[1].length() > 1)
		{
			SplitStringcal[1] = SplitStringcal[1].substring(0, 1);
		}
		
		return SplitStringcal[0]+"."+SplitStringcal[1];
	}
	
	private void deleteRecord()
	{
		//Delete data from both table
		mDbTool.deleteSport(SportID+"");
		mDbTool.deleteTrack(SportID);
	}
}