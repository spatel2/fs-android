package com.freestyle.android.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.freestyle.android.R;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.TypefaceFont;

@SuppressLint("NewApi")
public class Search_Activity extends BaseActivity implements CoreFrame,OnClickListener
{
	ImageView search_back_img;
	LinearLayout search_connect_lay,main_option_lay;
	String TAG = "Search Activity";

	TypefaceFont mTypefaceFont;

	Context mContext;
	Intent mIntent;

	LinearLayout search_watch_lay,search_sync_lay;
	//Watch Code
	ListView mListView;

	SharedPreferences mSharedPreferenceSplash;
	Editor mEditorSplash;

	boolean mBooleanSplashFlag = true;

	ProgressDialog mProcessDialog;
	private Handler mHandlers;

	private static final long SCAN_PERIOD = 3000;
	private DeviceAdapter deviceAdapter;

	static List<String> deviceList = new ArrayList<String>();

	private String c1;
	private String c2;
	private String c3;

	private String countries[] = null;

	TextView search_date_txt;
	TextView search_time_txt;
	private Handler handler;

	@SuppressLint({ "InlinedApi", "SimpleDateFormat" }) @Override
	public void onCreate(Bundle savedInstanceState) 
	{

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_activity);

		mContext = Search_Activity.this;
		initDrawer(Search_Activity.this);

		mTypefaceFont = new TypefaceFont(mContext);
		search_back_img = (ImageView)findViewById(R.id.search_back_img);
		search_connect_lay = (LinearLayout)findViewById(R.id.search_connect_lay);
		main_option_lay = (LinearLayout)findViewById(R.id.main_option_lay);
		search_watch_lay = (LinearLayout)findViewById(R.id.search_watch_lay);
		search_sync_lay = (LinearLayout)findViewById(R.id.search_sync_lay);
		search_date_txt = (TextView)findViewById(R.id.search_date_txt);
		search_time_txt = (TextView)findViewById(R.id.search_time_txt);

		//Set Date
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");//HH:mm:ss");
		Date date = new Date();
		search_date_txt.setText(dateFormat.format(date));

		//For time
		handler = new Handler();
		handler.postDelayed(updateTimerThread, 1000);

		//Visibility Handling
		main_option_lay.setVisibility(View.VISIBLE);

		//Watch Code
		mListView = (ListView)findViewById(R.id.searched_listview);
		mHandlers = new Handler();
		countries = new String[] { "Camera", "Play/Pause Music", "Find Phone", "Next Song", "Off"};

		showDefaultWin("C1");
		showDefaultWin("C2");
		showDefaultWin("C3");

		//Search Watch Listener
		search_connect_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				scanLeDevice(true);	
				deviceAdapter = new DeviceAdapter();
				mListView.setAdapter(deviceAdapter);
				if(mListView.getAdapter().getCount() > 0)
				{
					mListView.setVisibility(View.VISIBLE);
					search_sync_lay.setVisibility(View.VISIBLE);
					search_watch_lay.setVisibility(View.VISIBLE);
					search_date_txt.setVisibility(View.VISIBLE);
					search_time_txt.setVisibility(View.VISIBLE);
				}
				else
				{
					mListView.setVisibility(View.GONE);
					search_sync_lay.setVisibility(View.GONE);
					search_watch_lay.setVisibility(View.GONE);
					search_date_txt.setVisibility(View.GONE);
					search_time_txt.setVisibility(View.GONE);
				}

				mListView.setOnItemClickListener(mDeviceClickListener);
				deviceAdapter.notifyDataSetChanged();
			}
		});

		deviceAdapter = new DeviceAdapter();
		mListView.setAdapter(deviceAdapter);
		if(mListView.getAdapter().getCount() > 0)
			mListView.setVisibility(View.VISIBLE);
		else
			mListView.setVisibility(View.GONE);

		mListView.setOnItemClickListener(mDeviceClickListener);

		if (deviceList.isEmpty())
		{
			if ((DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTED) || (DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTING))
			{
				deviceList.add(DemoApplication.mBluetoothDevice);
			}
			else
			{
				scanLeDevice(true);
			}
		}

		//Find Watch Layout
		search_watch_lay.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if (DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTED)
				{
					if(null != mBluetoothLeService)
					{
						mBluetoothLeService.WriteAppendixB(0X56);
					}
				}
			}
		});

		//Synchronize time layout
		search_sync_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showSynchronizeTime();
			}
		});

		search_back_img.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				onBackPressed();
			}
		});

		switch (DemoApplication.mConnectionState)
		{
		case DemoApplication.STATE_CONNECTED:
			search_sync_lay.setVisibility(View.VISIBLE);
			search_watch_lay.setVisibility(View.VISIBLE);
			search_date_txt.setVisibility(View.VISIBLE);
			search_time_txt.setVisibility(View.VISIBLE);
			break;
		case DemoApplication.STATE_CONNECTING:
		case DemoApplication.STATE_DISCONNECTED:
		default:
			search_sync_lay.setVisibility(View.GONE);
			search_watch_lay.setVisibility(View.GONE);
			search_date_txt.setVisibility(View.GONE);
			search_time_txt.setVisibility(View.GONE);
			break;
		}

	}

	@Override
	public void onBackPressed() 
	{
		goBack("");
	}

	@Override
	public void goBack(String target) 
	{
		mIntent = new Intent(mContext, Settings_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target)
	{

	}

	@Override
	public void setTypefaceControls() 
	{

	}

	@Override
	public void onClick(View v)
	{

	}

	class ViewHolder
	{
		TextView mTextViewName;
		TextView mTextViewConnected;
		ImageView mImageViewConnectedImage;
		TextView mButtonConnectDisconnect;
	}

	//Watch Code
	public class DeviceAdapter extends BaseAdapter
	{
		ViewHolder mViewHolder;

		public DeviceAdapter()
		{}

		@Override
		public int getCount() 
		{
			return deviceList.size();
		}

		public void clear() 
		{
			deviceList.clear();
		}

		@Override 
		public Object getItem(int position) 
		{
			return deviceList.get(position);
		}

		@Override
		public void notifyDataSetChanged() 
		{
			super.notifyDataSetChanged();
			ChangeAdapterSize();
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}

		public void ChangeAdapterSize()
		{
			float scale = getResources().getDisplayMetrics().density;
			ListView mListView = (ListView)findViewById(R.id.searched_listview);
			android.widget.RelativeLayout.LayoutParams list2 = (android.widget.RelativeLayout.LayoutParams) mListView.getLayoutParams();
			list2.height = (int) Math.ceil((float)deviceList.size() * 106.f * scale);
			mListView.setLayoutParams(list2);

			if(mListView.getAdapter().getCount() > 0)
				mListView.setVisibility(View.VISIBLE);
			else
				mListView.setVisibility(View.GONE);
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent)
		{
			if (getCount() == 0)
			{
				return null;
			}

			if(convertView == null)
			{
				mViewHolder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.device_element, null);

				mViewHolder.mTextViewName				= (TextView) convertView.findViewById(R.id.name);
				mViewHolder.mTextViewConnected			= (TextView) convertView.findViewById(R.id.pairedtxt);
				mViewHolder.mImageViewConnectedImage	= (ImageView) convertView.findViewById(R.id.paired);

				mViewHolder.mButtonConnectDisconnect	= (TextView)convertView.findViewById(R.id.btnConnectDisconnect);
				convertView.setTag(mViewHolder);
			} 
			else
			{
				mViewHolder = (ViewHolder) convertView.getTag();
			}

			ChangeAdapterSize();

			String DeviceAddress = deviceList.get(position);

			mViewHolder.mTextViewName.setText("Shark Tooth");

			switch (DemoApplication.mConnectionState)
			{
			case DemoApplication.STATE_CONNECTED:
				if (DeviceAddress.equals(DemoApplication.mBluetoothDevice))
				{
					mViewHolder.mButtonConnectDisconnect.setText("Disconnect");
					mViewHolder.mTextViewConnected.setVisibility(View.VISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.VISIBLE);
				}
				else
				{
					mViewHolder.mButtonConnectDisconnect.setText("Connect");
					mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				}
				break;
			case DemoApplication.STATE_CONNECTING:
				if (DeviceAddress.equals(DemoApplication.mBluetoothDevice))
				{
					mViewHolder.mButtonConnectDisconnect.setText("Connecting");
					mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				}
				else
				{
					mViewHolder.mButtonConnectDisconnect.setText("Connect");
					mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
					mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				}
				break;
			case DemoApplication.STATE_DISCONNECTED:
				mViewHolder.mButtonConnectDisconnect.setText("Connect");
				mViewHolder.mTextViewConnected.setVisibility(View.INVISIBLE);
				mViewHolder.mImageViewConnectedImage.setVisibility(View.INVISIBLE);
				break;
			}
			return convertView;
		}
	}

	private void showDefaultWin(final String flag)
	{

		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0]);
		c2 = sharedata.getString("C2", countries[1]);
		c3 = sharedata.getString("C3", countries[2]);
		if (flag.equals("C1"))
		{
			SharedPreferences.Editor sharedata1 = getSharedPreferences("control", 0).edit();
			sharedata1.putString("C1", c1);
			sharedata1.commit();
		}
		else if (flag.equals("C2"))
		{
			SharedPreferences.Editor sharedata2 = getSharedPreferences(
					"control", 0).edit();
			sharedata2.putString("C2", c2);
			sharedata2.commit();

		}
		else if (flag.equals("C3"))
		{
			SharedPreferences.Editor sharedata3 = getSharedPreferences(
					"control", 0).edit();
			sharedata3.putString("C3", c3);
			sharedata3.commit();
		}

		if (flag.equals("C1"))
		{
			c1 = countries[0];
		}
		else if (flag.equals("C2"))
		{
			c2 = countries[1];
		}
		else if (flag.equals("C3"))
		{
			c3 = countries[2];
		}

		if (flag.equals("C1"))
		{
			setPic(c1);
		}
		else if (flag.equals("C2"))
		{
			setPic(c2);
		}
		else if (flag.equals("C3"))
		{
			setPic(c3);
		}

	}	
	private int setPic(String title)
	{
		for (int i = 0; i < countries.length; i++)
		{
			if (title.equals(countries[i]))
			{
				return i;
			}
		}
		return 0;
	}

	@SuppressWarnings("deprecation")
	private synchronized void scanLeDevice(final boolean enable) 
	{
		if (enable) 
		{
			mProcessDialog = ProgressDialog.show(Search_Activity.this, "Please wait...", "Looking for watches...",true,true);
			// Stops scanning after a pre-defined scan period.
			mHandlers.postDelayed(new Runnable() 
			{
				@Override
				public void run() 
				{
					try 
					{
						if(mProcessDialog != null)
							mProcessDialog.dismiss();
					}
					catch (Exception e) 
					{
						e.printStackTrace();
					}

					mBluetoothAdapter.stopLeScan(mLeScanCallback);
				}
			}, SCAN_PERIOD);

			mBluetoothAdapter.startLeScan(mLeScanCallback);
		} 
		else 
		{
			if (mProcessDialog != null)// Note: this is needed, otherwise we will get a nullpointer OnDestroy, in cases where we entered the page, but never created the Progress Dialog
				mProcessDialog.dismiss();
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	} 

	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback()
	{
		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) 
		{
			runOnUiThread(new Runnable() 
			{
				@Override
				public void run() 
				{
					if(device != null)
					{
						String DeviceName = device.getName();

						try
						{
							if (DeviceName.equals("Geneva BLE Watch"))
							{
								String DeviceAddress = device.getAddress();
								boolean DevicePresent = false;

								for (int i=0; i<deviceList.size(); i++) 
								{
									if (deviceList.get(i).equals(DeviceAddress))
										DevicePresent = true;
								}

								if(!DevicePresent) 
								{
									deviceList.add(DeviceAddress);
								}

								deviceAdapter.notifyDataSetChanged();
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}	
				}
			});
		}
	};	

	private OnItemClickListener mDeviceClickListener = new OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,long id)
		{
			String device = deviceList.get(position);

			if (DemoApplication.mConnectionState == DemoApplication.STATE_CONNECTED)
			{
				String DeviceAddress = device;
				if (DeviceAddress.equals(DemoApplication.mBluetoothDevice))
				{
					mBluetoothLeService.disconnect();
					DemoApplication.mBluetoothDevice = null;

					deviceAdapter.clear();
					deviceAdapter.notifyDataSetChanged();
				}
			}
			else
			{
				//Note: This cancels the progress dialog, that's why this needs to be before showing the new one
				scanLeDevice(false);

				mProcessDialog = ProgressDialog.show(Search_Activity.this, "Please wait...", "Connecting...",true,true);

				DemoApplication.mBluetoothDevice = device;

				//Clear all other devices
				int NumDevices = deviceAdapter.getCount(); //Note, need to do this here, because in the loop this value changes due to deletion
				for (int i = NumDevices-1;i>=0;i--)
				{
					if (!deviceList.get(i).equals(DemoApplication.mBluetoothDevice))
						deviceList.remove(i);
				}

				deviceAdapter.notifyDataSetChanged();

				mBluetoothLeService.connect(DemoApplication.mBluetoothDevice,true);
			}
		}
	};

	protected void onResume()
	{
		super.onResume();
		mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
		mBooleanSplashFlag = mSharedPreferenceSplash.getBoolean(getResources().getString(R.string.splash_flag_value), true);

	};

	public void connected()
	{
		search_sync_lay.setVisibility(View.VISIBLE);
		search_watch_lay.setVisibility(View.VISIBLE);
		search_date_txt.setVisibility(View.VISIBLE);
		search_time_txt.setVisibility(View.VISIBLE);

		mHandlers.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				if(mProcessDialog != null && mProcessDialog.isShowing())
				{
					mProcessDialog.dismiss();
				}
				deviceAdapter.notifyDataSetChanged();
			}
		}, SCAN_PERIOD);
	}

	public void connecting()
	{
		search_sync_lay.setVisibility(View.GONE);
		search_watch_lay.setVisibility(View.GONE);
		search_date_txt.setVisibility(View.GONE);
		search_time_txt.setVisibility(View.GONE);
		deviceAdapter.notifyDataSetChanged();
	}

	public void disconnect()
	{
		if (mProcessDialog != null)
			mProcessDialog.dismiss();

		search_sync_lay.setVisibility(View.GONE);
		search_watch_lay.setVisibility(View.GONE);
		search_date_txt.setVisibility(View.GONE);
		search_time_txt.setVisibility(View.GONE);
		deviceAdapter.notifyDataSetChanged();
	}

	private void showSynchronizeTime() 
	{
		final Dialog dlg = new Dialog(this); 
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.synchronizationtime); 
		dlg.show(); 

		TextView cancelbutton = (TextView) dlg.findViewById(R.id.cancelbutton);
		cancelbutton.setOnClickListener(new OnClickListener() 
		{
			@Override 
			public void onClick(View v) 
			{
				dlg.cancel(); 
			} 
		}); 

		TextView timeText = (TextView) dlg.findViewById(R.id.timeText);
		timeText.setText(getTime("hh:mm a MM-dd-yyyy"));		
		Button surebutton = (Button) dlg.findViewById(R.id.surebutton);
		surebutton.setOnClickListener(new OnClickListener() 
		{
			@Override 
			public void onClick(View v) 
			{ 
				dlg.cancel();
				mProcessDialog = ProgressDialog.show(Search_Activity.this, "Please wait...", "synchronizing Time...",true,true);
				mProcessDialog.show();

				new Thread(new Runnable() 
				{
					public void run() 
					{
						try 
						{
							mBluetoothLeService.WriteAppendixB(0x50);
							android.os.SystemClock.sleep(4000);
							byte[] time = new byte[12]; 
							time[0] = 0x04; 
							time[1] = 12; 
							time[2] = 0; 
							Time t = new Time(); 
							final Calendar c = Calendar.getInstance();
							c.setTimeZone(TimeZone.getTimeZone("GMT+8:00")); 
							t.setToNow();
							String year = String.valueOf(t.year); 
							String one = year.substring(0, 2); 
							String two = year.substring(2, 4); 
							time[3] = 0; 
							time[4] = (byte) (Integer.parseInt(one)); 
							time[5] = (byte) (Integer.parseInt(two));
							time[6] = (byte)  (t.month + 1);
							time[7] = (byte)  t.monthDay;
							time[8] = (byte) (c.get(Calendar.DAY_OF_WEEK) - 1); 
							time[9] = (byte) t.hour;
							time[10] = (byte) t.minute; 
							time[11] = (byte) t.second; 
							mBluetoothLeService.Ble_Send_Data(time); 
							mProcessDialog.dismiss();
						} 
						catch (Exception e) 
						{
							e.printStackTrace();
						}
					}
				}).start();
			} 
		}); 
	}

	@SuppressLint("SimpleDateFormat") 
	public static String getTime(String fomat)
	{
		SimpleDateFormat sDateFormat = new SimpleDateFormat(fomat);
		String date = sDateFormat.format(new java.util.Date());
		return date;
	}

	private Runnable updateTimerThread = new Runnable()
	{
		@SuppressLint("SimpleDateFormat") @Override
		public void run() 
		{
			//Set Date
			DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss a");
			Date date = new Date();
			search_time_txt.setText(dateFormat.format(date));

			handler.postDelayed(this, 1000);
		}
	};

	public void onDestroy() 
	{
		handler.removeCallbacks(updateTimerThread);
		super.onDestroy();
	};
}
