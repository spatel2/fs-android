package com.freestyle.android.activity;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.FloatMath;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freestyle.android.R;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.DataHolder;
import com.freestyle.android.config.DatabaseConnectionAPI;
import com.freestyle.android.config.FavoriteData;
import com.freestyle.android.config.GetAllData;
import com.freestyle.android.config.GetAllData.GetGlobal;
import com.freestyle.android.config.GetAllData.GetGlobal.GetArea;
import com.freestyle.android.config.PostParseGet;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.config.TextviewClass;
import com.freestyle.android.config.TypefaceFont;

public class FindBeach_Activity extends BaseActivity  implements OnClickListener,CoreFrame
{
	BaseActivity mBaseActivity;
	public final static String TAG = "Home screen";
	Button oBtn1;
	ExpandableListView oELV;
	CommonClass mCommonClass = new CommonClass();
	Context context;
	TypefaceFont mTypefaceFont;
	ListView mListView,nearest_list,search_listview;
	ProgressDialog mProgressDialogFindBeach;
	PostParseGet mPostParseGet;
	GetAllData mGetAllData;
	ArrayList<GetArea> mArrayListArea;
	AreaListAdpter mAreaListAdpter;
	Shared_preference mSharedPreferenceFavorite,mSharedPreferenceLatLong;
	Double currentLat=0.0,currentLong=0.0;
	ArrayList<GetGlobal> mArrayListNearestData;

	ArrayList<String> subregion_id = new ArrayList<String>();
	ArrayList<String> region_id = new ArrayList<String>();
	ArrayList<String> country_id = new ArrayList<String>();
	ArrayList<String> beach_id = new ArrayList<String>();
	ArrayList<String> beach_name = new ArrayList<String>();
	ArrayList<String> beach_distance = new ArrayList<String>();

	ArrayList<String> country_id_arr 	= new ArrayList<String>();
	ArrayList<String> region_id_arr 	= new ArrayList<String>();
	ArrayList<String> subregion_id_arr 	= new ArrayList<String>();
	ArrayList<String> beach_id_arr 		= new ArrayList<String>();
	ArrayList<String> beach_distance_arr= new ArrayList<String>();

	ArrayList<String> final_country_id_arr	= new ArrayList<String>();
	ArrayList<String> final_region_id_arr	= new ArrayList<String>();
	ArrayList<String> final_subregion_id_arr= new ArrayList<String>();
	ArrayList<String> final_beach_id_arr	= new ArrayList<String>();

	float distance1 = 0;
	float distance2 = 0;
	float distance3 = 0;
	float distance4 = 0;

	private DatabaseConnectionAPI mDatabaseConnectionAPI;
	private Cursor mCursor;
	ArrayList<FavoriteData> mArrayListFavoriteData;
	private DataHolder mDataHolder;
	boolean flag = true;


	//Search Layout Controls
	RelativeLayout search_rel_lay;
	EditText search_beach_txt;
	ImageView clear_search_text;
	ImageView mImageViewClose;

	ArrayList<Integer> search_beach_id_arr			= new ArrayList<Integer>();
	ArrayList<Double> search_beach_distance_arr		= new ArrayList<Double>();
	ArrayList<Integer> search_subregion_id_arr		= new ArrayList<Integer>();
	ArrayList<Integer> search_region_id_arr			= new ArrayList<Integer>();
	ArrayList<Integer> search_country_id_arr		= new ArrayList<Integer>();


	TextviewClass mTextviewNoDataFound;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.findbeach_activity);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		context = this;
		mTypefaceFont = new TypefaceFont(context);
		mSharedPreferenceFavorite = new Shared_preference(FindBeach_Activity.this);
		mSharedPreferenceLatLong = new Shared_preference(FindBeach_Activity.this);
		initDrawer(FindBeach_Activity.this);

		search_rel_lay = (RelativeLayout)findViewById(R.id.search_rel_lay);
		search_beach_txt = (EditText)findViewById(R.id.search_beach_txt);
		clear_search_text = (ImageView)findViewById(R.id.clear_search_text);
		mImageViewClose = (ImageView)findViewById(R.id.find_beach_close);
		
		mImageViewClose.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				goBack("back");
			}
		});

		mTextviewNoDataFound = (TextviewClass)findViewById(R.id.textview_no_beach_found);

		clear_search_text.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				search_beach_txt.setText("");
				hideSoftKeyboard();
			}
		});
		mBaseActivity = new BaseActivity();

		mDatabaseConnectionAPI=new DatabaseConnectionAPI(FindBeach_Activity.this);

		try
		{
			mDatabaseConnectionAPI.createDataBase();
			mDatabaseConnectionAPI.getWritableDatabase();
			mDatabaseConnectionAPI.openDataBase();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		mArrayListNearestData = new ArrayList<GetGlobal>();

		mArrayListNearestData.clear();
		mListView = (ListView)findViewById(R.id.location_list);
		nearest_list = (ListView)findViewById(R.id.nearest_list);
		search_listview = (ListView)findViewById(R.id.search_listview);

		mPostParseGet=new PostParseGet(FindBeach_Activity.this);

		if(!(mSharedPreferenceLatLong.getPrefrence(FindBeach_Activity.this, Constants.PREFRENCE.ROAD_LATITUDE).toString().equals("") && mSharedPreferenceLatLong.getPrefrence(FindBeach_Activity.this, Constants.PREFRENCE.ROAD_LONGTITUDE).toString().equals("")))
		{
			currentLat = Double.parseDouble(mSharedPreferenceLatLong.getPrefrence(FindBeach_Activity.this, Constants.PREFRENCE.ROAD_LATITUDE));
			currentLong = Double.parseDouble(mSharedPreferenceLatLong.getPrefrence(FindBeach_Activity.this, Constants.PREFRENCE.ROAD_LONGTITUDE));
		}

		mGetAllData = new GetAllData();
		mGetAllData = DemoApplication.mGetAllData;

		if (mGetAllData.getData() == null) 
		{
			if(mCommonClass.CheckNetwork(context))
			{
				new GetAllDataProcess().execute();
			}
		} 
		else
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run() 
				{
					getAllData();
				}
			});
		}

		//Search Button click
		search_rel_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				if(!search_beach_txt.getText().toString().equals(""))
				{
					if(search_beach_txt.getText().toString().equals(""))
					{
						AllLayVisible();
					}
					else
					{
						searchBeach();
						hideSoftKeyboard();
					}
				}
			}
		});

		search_beach_txt.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				//				if(search_beach_txt.getText().toString().equals(""))
				//				{
				//					AllLayVisible();
				//				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) 
			{

			}

			@Override
			public void afterTextChanged(Editable s) 
			{
				if(!search_beach_txt.getText().toString().equals(""))
				{
					searchBeach();
				}
				else
				{
					AllLayVisible();
				}
			}
		});

	}

	@Override
	public void onBackPressed() 
	{
		goBack("back");
	}

	@Override
	public void onClick(View v) 
	{
	}

	public class AreaListAdpter extends BaseAdapter
	{
		ViewHolder mViewHolder;

		@Override
		public int getCount() 
		{
			return mArrayListArea.size();
		}

		@Override 
		public Object getItem(int position) 
		{
			return mArrayListArea.get(position);
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}

		@SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null)
			{
				mViewHolder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.infl_location_lay, null);

				mViewHolder.mTextViewTiltle = (TextviewClass) convertView.findViewById(R.id.country_txt_single);
				mViewHolder.mLinearLayout = (LinearLayout) convertView.findViewById(R.id.data_layout);

				convertView.setTag(mViewHolder);
			}
			else 
			{
				mViewHolder = (ViewHolder) convertView.getTag();
			}



			mViewHolder.mTextViewTiltle.setText(mArrayListArea.get(position).getName());

			mViewHolder.mLinearLayout.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{				
					DemoApplication.mGetAllData = mGetAllData;
					Intent intent = new Intent(FindBeach_Activity.this, Region_Activity.class);
					intent.putExtra("area_id", mArrayListArea.get(position).getId().toString());
					startActivity(intent);
					finish();
				}
			});

			return convertView;
		}
	}

	class ViewHolder
	{
		LinearLayout mLinearLayout;
		TextviewClass mTextViewTiltle;
	}

	public void sortArea(List<GetArea> itemList) {
		if(itemList!=null)
		{
			Collections.sort(itemList, new Comparator<GetArea>() {
				@Override
				public int compare(GetArea o1, GetArea o2) {
					return o1.name.compareTo(o2.name);
				}           
			});
		}
	}

	@Override
	public void goBack(String target)
	{
		mIntent = new Intent(context, Favourate_beach_Activity.class);
		startActivity(mIntent);
		finish();
	}

	@Override
	public void goForward(String target)
	{

	}

	@Override
	public void setTypefaceControls() 
	{

	}

	public class Nearest_beach_Adapter extends BaseAdapter
	{
		ArrayList<String> arr;
		Context mContext;

		Nearest_beach_Adapter(Context context, ArrayList<String> arraylist)
		{
			mContext = context;
			arr = arraylist;
		}
		@Override
		public int getCount() 
		{
			return arr.size();
		}
		@Override 
		public Object getItem(int position) 
		{
			return arr.get(position);
		}
		@Override
		public long getItemId(int position) 
		{
			return position;
		}
		@SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.infl_nearest_beach, null);
			} 

			LinearLayout infl_beach_lay_single = (LinearLayout)convertView.findViewById(R.id.infl_beach_lay_single);
			TextView beach_name_txt_single = (TextView)convertView.findViewById(R.id.beach_name_txt_single);
			TextView beach_distance_txt_single = (TextView)convertView.findViewById(R.id.beach_distance_txt_single);

			//			beach_name_txt_single.setText(arr.get(position));
			//			beach_distance_txt_single.setText(beach_distance.get(position)+" MI");
			DecimalFormat mDecimalFormatTwoDigit;
			mDecimalFormatTwoDigit = new DecimalFormat("0", new DecimalFormatSymbols(Locale.ENGLISH));

			if(position==0)
			{
				beach_distance_txt_single.setText(mDecimalFormatTwoDigit.format(distance1)+" MI");
			}
			else if(position==1)
			{
				beach_distance_txt_single.setText(mDecimalFormatTwoDigit.format(distance2)+" MI");
			}
			else if(position==2)
			{
				beach_distance_txt_single.setText(mDecimalFormatTwoDigit.format(distance3)+" MI");
			}
			else if(position==3)
			{
				beach_distance_txt_single.setText(mDecimalFormatTwoDigit.format(distance4)+" MI");
			}
			beach_name_txt_single.setText(mGetAllData.getData()
					.get(Integer.parseInt(final_country_id_arr.get(position))).getArea().getContent()
					.get(Integer.parseInt(final_region_id_arr.get(position))).getRegion().getContent()
					.get(Integer.parseInt(final_subregion_id_arr.get(position))).getSubregion().getSpots()
					.get(Integer.parseInt(final_beach_id_arr.get(position))).getTitle());




			infl_beach_lay_single.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					getData();
					if(mArrayListFavoriteData != null && mArrayListFavoriteData.size() > 0)
					{
						for (int i = 0; i < mArrayListFavoriteData.size(); i++) 
						{
							if(mArrayListFavoriteData.get(i).getSpot_Id().equalsIgnoreCase(mGetAllData.getData()
									.get(Integer.parseInt(final_country_id_arr.get(position))).getArea().getContent()
									.get(Integer.parseInt(final_region_id_arr.get(position))).getRegion().getContent()
									.get(Integer.parseInt(final_subregion_id_arr.get(position))).getSubregion().getSpots()
									.get(Integer.parseInt(final_beach_id_arr.get(position))).getId()))
							{
								flag = false;
							}
						}
					}
					if(!flag)
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(FindBeach_Activity.this);
						builder.setMessage("Already added as favorite");
						builder.setNegativeButton( "OK",new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								dialog.cancel();
								flag = true;
							}
						});
						builder.show();
					}
					else
					{
						ContentValues mContentValues=new ContentValues();
						mContentValues.put(Constants.FAVORITE_FIELDS.SPOT_ID,mGetAllData.getData()
								.get(Integer.parseInt(final_country_id_arr.get(position))).getArea().getContent()
								.get(Integer.parseInt(final_region_id_arr.get(position))).getRegion().getContent()
								.get(Integer.parseInt(final_subregion_id_arr.get(position))).getSubregion().getSpots()
								.get(Integer.parseInt(final_beach_id_arr.get(position))).getId());

						mContentValues.put(Constants.FAVORITE_FIELDS.SUB_REGION_ID,mGetAllData.getData()
								.get(Integer.parseInt(final_country_id_arr.get(position))).getArea().getContent()
								.get(Integer.parseInt(final_region_id_arr.get(position))).getRegion().getContent()
								.get(Integer.parseInt(final_subregion_id_arr.get(position))).getSubregion().getId());

						mContentValues.put(Constants.FAVORITE_FIELDS.REGION_ID,mGetAllData.getData()
								.get(Integer.parseInt(final_country_id_arr.get(position))).getArea().getContent()
								.get(Integer.parseInt(final_region_id_arr.get(position))).getRegion().getId());

						mContentValues.put(Constants.FAVORITE_FIELDS.COUNTRY_ID,mGetAllData.getData()
								.get(Integer.parseInt(final_country_id_arr.get(position))).getArea().getId());

						mDatabaseConnectionAPI.insertRecord(Constants.TableNames.FAVORITE, mContentValues);

						Intent intent = new Intent(context, Favourate_beach_Activity.class);
						startActivity(intent);
						finish();
					}
				}
			});
			return convertView;
		}
	}

	public void getData()
	{
		mCursor = mDatabaseConnectionAPI.onQueryGetCursor(Constants.TableNames.FAVORITE, null,null,null, null, null, null);
		if (mCursor != null) 
		{
			if (mCursor.getCount() > 0) 
			{
				mDataHolder = mDatabaseConnectionAPI.read(mCursor);
				mArrayListFavoriteData = new ArrayList<FavoriteData>();
				for (int i = 0; i < mDataHolder.getmRow().size(); i++) 
				{
					FavoriteData mData = new FavoriteData();
					mData.setSpot_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.SPOT_ID));
					mData.setSub_Region_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.SUB_REGION_ID));
					mData.setRegion_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.REGION_ID));
					mData.setCountry_Id(mDataHolder.getmRow().get(i).get(Constants.FAVORITE_FIELDS.COUNTRY_ID));
					mArrayListFavoriteData.add(mData);
				}
			}
		}
	}

	public class GetAllDataProcess extends AsyncTask<Void, Void, Void> 
	{
		@Override
		protected void onPreExecute()
		{
			mProgressDialogFindBeach = ProgressDialog.show(FindBeach_Activity.this,"", "Loading...", false);
			mProgressDialogFindBeach.setCancelable(false);
			super.onPreExecute();
		}
		@Override
		protected Void doInBackground(Void... params) 
		{
			mGetAllData = (GetAllData) mPostParseGet.GetAllData(mGetAllData);
			return null;
		}
		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Void result) 
		{   
			if (mProgressDialogFindBeach != null)
				mProgressDialogFindBeach.dismiss();
			if (mPostParseGet.isNetError)
				showAlert(FindBeach_Activity.this, getResources().getString(R.string.error_internet), getString(R.string.ok));
			else if (mPostParseGet.isOtherError)
				showAlert(FindBeach_Activity.this, getResources().getString(R.string.error_response), getString(R.string.ok));
			else 
			{
				if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
				{
					DemoApplication.mGetAllData=mGetAllData;
					mSharedPreferenceFavorite.setPrefrence(FindBeach_Activity.this, "favorite_data", "true");
					getAllData();
				}
			}
			super.onPostExecute(result);
		}
	}

	public void getAllData()
	{
		if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
		{
			mArrayListArea = new ArrayList<GetArea>();
			for (int i = 0; i < mGetAllData.getData().size(); i++)
			{
				if(!mGetAllData.getData().get(i).getArea().getName().equalsIgnoreCase("WCT Events"))
				{
					GetArea mAreaData= new GetArea();
					mAreaData.setId(mGetAllData.getData().get(i).getArea().getId());
					mAreaData.setName(mGetAllData.getData().get(i).getArea().getName());
					mArrayListArea.add(mAreaData);
				}
			}
			sortArea(mArrayListArea);
			mAreaListAdpter = new AreaListAdpter();
			mListView.setAdapter(mAreaListAdpter);
		}

		getBeachLocation_async mgetBeachLocation_async = new getBeachLocation_async();
		mgetBeachLocation_async.execute();

	}

	public void showAlert(Activity mActivity, String message, String buttonMessage)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(message);
		builder.setNegativeButton( buttonMessage,new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.cancel();
			}
		});
		builder.show();
	}

	public synchronized void fillPlaces()
	{  
		boolean nearSubregion = true;

		if(mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0)
		{
			for (int i = 0; i < mGetAllData.getData().size(); i++) 
			{
				for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size() ; j++) 
				{
					nearSubregion = true;
					for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) 
					{
						for (int m = 0; m < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().size(); m++) 
						{
							double distance = gps2m( mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLat(), mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLon());
							if(distance!=0)
							{
								if(distance<5000)
								{
									beach_distance_arr.add(distance+"");
									beach_id_arr.add(m+"");//mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId());
									subregion_id_arr.add(k+"");//mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId());
									region_id_arr.add(j+"");//mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId());
									country_id_arr.add(i+"");//mGetAllData.getData().get(i).getArea().getId());

								}
								else if(10000>distance)
								{
									nearSubregion = false;
									break;
								}
							}
							else 
							{
								break;
							}
						}
						if(nearSubregion== false)
						{
							break;
						}
					}
				}
			}
		}

		//Set Data in Shared Prefrence
		setValue();
	}

	private void setValue()
	{
		//Get Nearest Beach ID
		float min_distance=0;
		int min_id1 = 0;
		int min_id2 = 0;
		int min_id3 = 0;
		int min_id4 = 0;




		if(beach_distance_arr.size()>0)
		{
			min_distance=(Float.parseFloat(beach_distance_arr.get(0).toString()));
		}

		for(int i=0;i<beach_distance_arr.size();i++)
		{
			if((Float.parseFloat(beach_distance_arr.get(i).toString()))<=min_distance)
			{
				min_distance = (Float.parseFloat(beach_distance_arr.get(i).toString()));
				distance1=min_distance;
				min_id1 = i;
				System.out.println("--Distance : "+beach_distance_arr.get(i)+" "+beach_id_arr.get(i)+"");
			}
		}

		if(beach_distance_arr.size()>0)
		{
			min_distance=(Float.parseFloat(beach_distance_arr.get(0).toString()));
		}

		for(int i=0;i<beach_distance_arr.size();i++)
		{
			if((Float.parseFloat(beach_distance_arr.get(i).toString()))<=min_distance && i!=min_id1)
			{
				min_distance = (Float.parseFloat(beach_distance_arr.get(i).toString()));
				distance2=min_distance;
				min_id2 = i;
				System.out.println("--Distance : "+beach_distance_arr.get(i)+" "+beach_id_arr.get(i)+"");
			}
		}
		beach_distance_arr.remove(beach_distance_arr.get(min_id2));

		if(beach_distance_arr.size()>0)
		{
			min_distance=(Float.parseFloat(beach_distance_arr.get(0).toString()));
		}

		for(int i=0;i<beach_distance_arr.size();i++)
		{
			if((Float.parseFloat(beach_distance_arr.get(i).toString()))<=min_distance && i!=min_id1 && i!=min_id2)
			{
				min_distance = (Float.parseFloat(beach_distance_arr.get(i).toString()));
				distance3=min_distance;
				min_id3 = i;
				System.out.println("--Distance : "+beach_distance_arr.get(i)+" "+beach_id_arr.get(i)+"");
			}
		}

		beach_distance_arr.remove(beach_distance_arr.get(min_id3));

		if(beach_distance_arr.size()>0)
		{
			min_distance=(Float.parseFloat(beach_distance_arr.get(0).toString()));
		}

		for(int i=0;i<beach_distance_arr.size();i++)
		{
			if((Float.parseFloat(beach_distance_arr.get(i).toString()))<=min_distance && i!=min_id1 && i!=min_id2 && i!=min_id3)
			{
				min_distance = (Float.parseFloat(beach_distance_arr.get(i).toString()));
				distance4=min_distance;
				min_id4 = i;
				System.out.println("--Distance : "+beach_distance_arr.get(i)+" "+beach_id_arr.get(i)+"");
			}
		}

		final_country_id_arr.add(country_id_arr.get(min_id1));
		final_country_id_arr.add(country_id_arr.get(min_id2));
		final_country_id_arr.add(country_id_arr.get(min_id3));
		final_country_id_arr.add(country_id_arr.get(min_id4));

		final_beach_id_arr.add(beach_id_arr.get(min_id1));
		final_beach_id_arr.add(beach_id_arr.get(min_id2));
		final_beach_id_arr.add(beach_id_arr.get(min_id3));
		final_beach_id_arr.add(beach_id_arr.get(min_id4));

		final_region_id_arr.add(region_id_arr.get(min_id1));
		final_region_id_arr.add(region_id_arr.get(min_id2));
		final_region_id_arr.add(region_id_arr.get(min_id3));
		final_region_id_arr.add(region_id_arr.get(min_id4));

		final_subregion_id_arr.add(subregion_id_arr.get(min_id1));
		final_subregion_id_arr.add(subregion_id_arr.get(min_id2));
		final_subregion_id_arr.add(subregion_id_arr.get(min_id3));
		final_subregion_id_arr.add(subregion_id_arr.get(min_id4));
	}

	@SuppressLint("FloatMath") 
	private double gps2m(String lat_str, String lng_str)
	{
		float lng_a = 0,lat_a = 0;
		if(!(lat_str.equals("") && lng_str.equals("")))
		{
			lat_a =Float.parseFloat(lat_str);
			lng_a =Float.parseFloat(lng_str);

			float pk = (float) (180/3.14169);

			float a1 = lat_a / pk;
			float a2 = lng_a / pk;
			float b1 = (float) (currentLat / pk);
			float b2 = (float) (currentLong / pk);

			float t1 = FloatMath.cos(a1)*FloatMath.cos(a2)*FloatMath.cos(b1)*FloatMath.cos(b2);
			float t2 = FloatMath.cos(a1)*FloatMath.sin(a2)*FloatMath.cos(b1)*FloatMath.sin(b2);
			float t3 = FloatMath.sin(a1)*FloatMath.sin(b1);
			double tt = Math.acos(t1 + t2 + t3);

			return (6366000*tt)/1000;
		}
		else
		{
			return 0;
		}
	}

	public class getBeachLocation_async extends AsyncTask<Void, Void, Void> 
	{
		@Override
		protected synchronized Void doInBackground(Void... params)
		{
			fillPlaces();
			return null;
		}
		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Void result) 
		{
			nearest_list.setAdapter(new Nearest_beach_Adapter(context, final_beach_id_arr));
			super.onPostExecute(result);
		}
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		System.gc();
	}

	//Search Beach
	@SuppressLint("DefaultLocale") public synchronized void searchBeach()
	{  
		SearchBeachList mSearchBeachList = new SearchBeachList();
		AllLayGone();
		search_beach_distance_arr.clear();
		search_beach_id_arr.clear();
		search_subregion_id_arr.clear();
		search_region_id_arr.clear();
		search_country_id_arr.clear();

		mGetAllData =  DemoApplication.mGetAllData;

		for (int i = 0; i < mGetAllData.getData().size(); i++) 
		{
			for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) 
			{

				for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) 
				{

					for (int m = 0; m < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().size(); m++) 
					{
						//						double distance = gps2m (mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLat(), mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLon());
						//						if(distance!=0)
						//						{
						//							search_beach_distance_arr.add(distance);
						String beach_name1 = mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getTitle();
						String txt_beach_name = search_beach_txt.getText().toString();

						if(txt_beach_name.toLowerCase().charAt(0)==beach_name1.toLowerCase().charAt(0))
						{
							//						if(beach_name1.equalsIgnoreCase(search_beach_txt.getText().toString()))
							//						{
							boolean isUseful = true;

							for(int x=0;x<txt_beach_name.length();x++)
							{
								if(beach_name1.length()<txt_beach_name.length())
								{
									isUseful = false;
									break;
								}
								else
								{
									if(beach_name1.toLowerCase().charAt(x)!=txt_beach_name.toLowerCase().charAt(x) )
									{
										isUseful = false;
									}
								}

							}
							if(isUseful)
							{
								System.out.println(beach_name1+"");
								search_beach_id_arr.add(m);
								search_subregion_id_arr.add(k);
								search_region_id_arr.add(j);
								search_country_id_arr.add(i);
								search_listview.setAdapter(new SearchBeachList());
								mSearchBeachList.notifyDataSetChanged();
							}
						}
					}
				}
			}
		}

		if(search_beach_id_arr.size() > 0)
		{
			mTextviewNoDataFound.setVisibility(View.GONE);
		}
		else
		{
			mTextviewNoDataFound.setVisibility(View.VISIBLE);
		}
	}

	private void AllLayVisible()
	{
		findViewById(R.id.nearest_beach_title_lay).setVisibility(View.VISIBLE);
		nearest_list.setVisibility(View.VISIBLE);
		findViewById(R.id.nearestbeach_centerline).setVisibility(View.VISIBLE);
		mListView.setVisibility(View.VISIBLE);
		findViewById(R.id.by_loc_txt).setVisibility(View.VISIBLE);
		search_listview.setVisibility(View.GONE);
		mTextviewNoDataFound.setVisibility(View.GONE);

	}
	private void AllLayGone()
	{
		findViewById(R.id.nearest_beach_title_lay).setVisibility(View.GONE);
		nearest_list.setVisibility(View.GONE);
		findViewById(R.id.nearestbeach_centerline).setVisibility(View.GONE);
		mListView.setVisibility(View.GONE);
		findViewById(R.id.by_loc_txt).setVisibility(View.GONE);
		search_listview.setVisibility(View.VISIBLE);
	}


	private class SearchBeachList extends BaseAdapter
	{
		@Override
		public int getCount()
		{
			return search_beach_id_arr.size();
		}

		@Override 
		public Object getItem(int position)
		{
			return search_beach_id_arr.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return position;
		}

		@SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent)
		{
			if(convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.infl_location_lay, null);
			}
			TextView country_txt_single = (TextView)convertView.findViewById(R.id.country_txt_single);

			RelativeLayout mRelativeLayout = (RelativeLayout)convertView.findViewById(R.id.search_list_relative_main);

			country_txt_single.setText(mGetAllData.getData()
					.get(search_country_id_arr.get(position)).getArea().getContent()
					.get(search_region_id_arr.get(position)).getRegion().getContent()
					.get(search_subregion_id_arr.get(position)).getSubregion().getSpots()
					.get(search_beach_id_arr.get(position)).getTitle()+"");

			mRelativeLayout.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					getData();
					if(mArrayListFavoriteData != null && mArrayListFavoriteData.size() > 0)
					{
						for (int i = 0; i < mArrayListFavoriteData.size(); i++) 
						{
							if(mArrayListFavoriteData.get(i).getSpot_Id().equalsIgnoreCase(search_beach_id_arr.get(position)+""))
							{
								flag = false;
							}
						}
					}
					if(!flag)
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(FindBeach_Activity.this);
						builder.setMessage("Already added as favorite");
						builder.setNegativeButton( "OK",new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								dialog.cancel();
								flag = true;
							}
						});
						builder.show();
					}
					else
					{
						ContentValues mContentValues=new ContentValues();
						mContentValues.put(Constants.FAVORITE_FIELDS.SPOT_ID,mGetAllData.getData()
								.get(search_country_id_arr.get(position)).getArea().getContent()
								.get(search_region_id_arr.get(position)).getRegion().getContent()
								.get(search_subregion_id_arr.get(position)).getSubregion().getSpots()
								.get(search_beach_id_arr.get(position)).getId());

						mContentValues.put(Constants.FAVORITE_FIELDS.SUB_REGION_ID,mGetAllData.getData()
								.get(search_country_id_arr.get(position)).getArea().getContent()
								.get(search_region_id_arr.get(position)).getRegion().getContent()
								.get(search_subregion_id_arr.get(position)).getSubregion().getId());

						mContentValues.put(Constants.FAVORITE_FIELDS.REGION_ID,mGetAllData.getData()
								.get(search_country_id_arr.get(position)).getArea().getContent()
								.get(search_region_id_arr.get(position)).getRegion().getId());

						mContentValues.put(Constants.FAVORITE_FIELDS.COUNTRY_ID,mGetAllData.getData()
								.get(search_country_id_arr.get(position)).getArea().getId());

						mDatabaseConnectionAPI.insertRecord(Constants.TableNames.FAVORITE, mContentValues);

						Intent intent = new Intent(context, Favourate_beach_Activity.class);
						startActivity(intent);
						finish();
					}
				}
			});
			return convertView;
		}
	}

	public void hideSoftKeyboard() {
		if(getCurrentFocus()!=null) {
			InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		}
	}

	public void showSoftKeyboard(View view) {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		view.requestFocus();
		inputMethodManager.showSoftInput(view, 0);
	}
}