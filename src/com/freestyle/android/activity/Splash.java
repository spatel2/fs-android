package com.freestyle.android.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.freestyle.android.R;
import com.freestyle.android.bluebooth.GPSLocationServices;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.config.TextviewClass;
import com.freestyle.android.login.actSignUp;
import com.freestyle.test.clsPref;

public class Splash extends BaseActivity {
	ButtonClass splash_get_connected_btn;
	TextviewClass splash_skip_intro_txt;
	Intent mIntent;
	Context context;
	SharedPreferences mSharedPreferenceSplash;
	Editor mEditorSplash;
	boolean mBooleanSplashFlag = true;
	Shared_preference mSharedPreference;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);

		context = Splash.this;
		mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH, Context.MODE_PRIVATE);
		mSharedPreference = new Shared_preference(context);

		if (shared.getPrefrence(context, "user_gender").equalsIgnoreCase("")) {
			shared.setPrefrence(context, "user_gender", "MALE");
		}
		if (shared.getPrefrence(context, "user_age").equalsIgnoreCase("")) {
			shared.setPrefrence(context, "user_age", "18");
		}
		if (shared.getPrefrence(context, "user_height").equalsIgnoreCase("")) {
			shared.setPrefrence(context, "user_height", "6'0''");
		}
		if (shared.getPrefrence(context, "user_weight").equalsIgnoreCase("")) {
			shared.setPrefrence(context, "user_weight", "195");
		}
		if (shared.getPrefrence(context, Constants.PROFILE.SURF_AUTO_MANUAL).equalsIgnoreCase("")) {
			shared.setPrefrence(context, Constants.PROFILE.SURF_AUTO_MANUAL, "true");
		}

		// Stop current running activity
		mSharedPreference.setPrefrence(context, Constants.PREFRENCE.IS_ACTIVITY_RUNNING, "");
		mSharedPreference.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING, "");
		mSharedPreference.setPrefrence(context, Constants.PREFRENCE.SPORTS_ID, "");
		mSharedPreference.setPrefrence(context, Constants.PREFRENCE.MAP_START_LAT, "");
		mSharedPreference.setPrefrence(context, Constants.PREFRENCE.MAP_START_LON, "");
		mSharedPreference.setPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE, "");
		mSharedPreference.setPrefrence(context, Constants.PREFRENCE.ROAD_SPEED, "");

		DemoApplication.CurrentTotalTime = 0;

		// Stop the Service if running
		stopService(new Intent(context, GPSLocationServices.class));

		splash_get_connected_btn = (ButtonClass) findViewById(R.id.splash_get_connected_btn);
		splash_skip_intro_txt = (TextviewClass) findViewById(R.id.splash_skip_intro_txt);

		// To Get the SHA-1 key for facebook
		try {
			PackageInfo info = getPackageManager().getPackageInfo("com.freestyle.android", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				System.out.println("--keyhash = " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {
		}

		splash_get_connected_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mIntent = new Intent(context, Splash_Connect_your_watch_Activity.class);
				startActivity(mIntent);
				finish();
			}
		});

		splash_skip_intro_txt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Changing Prefrence
				final Dialog mDialog = new Dialog(Splash.this);
				mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				mDialog.setCancelable(false);
				mDialog.setContentView(R.layout.splash_dialog);
				mDialog.show();
				ButtonClass ok = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_okbutton);
				ButtonClass cancel = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_cancelbutton);

				ok.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH, Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value), true);
						mEditorSplash.commit();
						// Intent mIntent = new
						// Intent(Splash.this,Home_Activity.class);
						Intent mIntent;
						if (new clsPref(Splash.this).getUserId().equalsIgnoreCase("")) {
							mIntent = new Intent(Splash.this, actSignUp.class);
						} else {
							mIntent = new Intent(Splash.this, Home_Activity.class);
						}
						startActivity(mIntent);
						finish();
					}
				});

				cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH, Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value), false);
						mEditorSplash.commit();
						// Intent mIntent = new Intent(Splash.this,
						// Home_Activity.class);
						Intent mIntent;
						if (new clsPref(Splash.this).getUserId().equalsIgnoreCase("")) {
							mIntent = new Intent(Splash.this, actSignUp.class);
						} else {
							mIntent = new Intent(Splash.this, Home_Activity.class);
						}
						startActivity(mIntent);
						finish();
					}
				});
			}
		});
	}

	protected void onResume() {
		super.onResume();
		mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH, Context.MODE_PRIVATE);
		mBooleanSplashFlag = mSharedPreferenceSplash.getBoolean(getResources().getString(R.string.splash_flag_value), true);

		if (!mBooleanSplashFlag) {
			// Intent mIntent = new Intent(Splash.this, Home_Activity.class);
			Intent mIntent;
			if (new clsPref(Splash.this).getUserId().equalsIgnoreCase("")) {
				mIntent = new Intent(Splash.this, actSignUp.class);
			} else {
				mIntent = new Intent(Splash.this, Home_Activity.class);
			}
			startActivity(mIntent);
			finish();
		}
	};
}
