package com.freestyle.android.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.ImageView;
import com.freestyle.android.R;
import com.freestyle.android.config.CoreFrame;

public class Help_Activity extends BaseActivity implements OnClickListener,CoreFrame
{
	Context mContext;
	Intent mIntent;
	ImageView help_back_img;
	Animation mAnimation;
	WebView fs_help_webview;
	
	@SuppressLint("SetJavaScriptEnabled") @Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help_activity);
		
		mContext = Help_Activity.this;
		initDrawer(Help_Activity.this);

		help_back_img = (ImageView)findViewById(R.id.help_back_img);
		help_back_img.setOnClickListener(this);
		fs_help_webview = (WebView)findViewById(R.id.fs_help_webview);
		
		fs_help_webview.getSettings().setJavaScriptEnabled(true);
		fs_help_webview.setBackgroundColor(Color.TRANSPARENT);
		fs_help_webview.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
		
		fs_help_webview.loadUrl("http://kennethcoletime.com/app_api/fs/faq/feed.php");
	}


	@Override
	public void onClick(View v)
	{
		if(v==help_back_img)
		{
			goBack("back");
		}
	}

	@Override
	public void onBackPressed() 
	{
		goBack("back");
	}

	@Override
	public void goBack(String target)
	{
		finish();
	}

	@Override
	public void goForward(String target)
	{
	}

	@Override
	public void setTypefaceControls() 
	{
	}
}
