package com.freestyle.android.activity;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.freestyle.android.R;

public class Find_watch_dialog extends Activity
{
	Button you_find_btn_single;
	Thread t;
	int countBeep = 0;
	boolean ring_play=true;
	MediaPlayer player = new MediaPlayer();
	Vibrator mVibrator;
	AudioManager mAudioManager;
	int currentMusicVolume = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_watch_dialog);
		
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		currentMusicVolume= mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		this.setFinishOnTouchOutside(false);
		t = new Thread()
		{
			public void run()
			{
				// Get the current ringer volume as a percentage of the max ringer volume.
				int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
//				int maxRingerVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
//				double proportion = currentVolume/(double)maxRingerVolume;

				// Calculate a desired music volume as that same percentage of the max music volume.
//				maxMusicVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//				int desiredMusicVolume = (int)(proportion * maxMusicVolume);

				// Set the music stream volume.
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0 );
				while(ring_play)
				{
					player = MediaPlayer.create(Find_watch_dialog.this,R.raw.ringing);
					player.start();
					mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
					long[] pattern = { 1000, 1500, 0 };
					mVibrator.vibrate(pattern, 0);
					countBeep+=1;
					try 
					{
						Thread.sleep(player.getDuration());
						player.release();
					}
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
			}
		};
		t.start();   

		you_find_btn_single = (Button)findViewById(R.id.you_find_btn_single);
		you_find_btn_single.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				// Stop Beeping Code
				ring_play = false;
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentMusicVolume, 0 );
				player.stop();
				player.release();
				t.interrupt();
				t=null;
				mVibrator.cancel();
				mVibrator = null;
				finish();
			}
		});
	}

	@Override
	protected void onDestroy() 
	{
		ring_play = false;
		try
		{
			mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentMusicVolume, 0 );
			player.stop();
			player.release();
			t.interrupt();
			t=null;
			mVibrator.cancel();
			mVibrator = null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		super.onDestroy();
	}
}