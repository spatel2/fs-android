package com.freestyle.android.activity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.freestyle.android.bluebooth.BluetoothLeService;
import com.google.android.gms.internal.mc;

public class Surfline {
	private String TAG = "Surfline";
	private JSONObject JsonObject;
	private boolean isMetric;
	private int SpotID;
	private String FetchURL1 = "http://slapi01.surfline.com/v1/forecasts/";
	private String FetchURL2 = "?package=partner&units=m&user_key=abca50124d77d41504c4d53e29c8d359";
	private Boolean FinishedTask = false;
	// new variables
	double data[];
	public ArrayList<String> mArrayListTime = new ArrayList<String>();
	public ArrayList<String> mArrayListHeight = new ArrayList<String>();
	double mLastMinTide;
	int mCurrentIndex = -1;

	// end new variables

	// For sending data to the watch
	// Byte 0: Data Type, this value is fixed as 16. (Ref to ¡®Geneva Watch
	// Protocol Spec¡¯ )
	// Byte 1: Package Size ( 20 for this package )
	// Byte 2: Unit System ( 0: Metric, 1: Imperial )
	// Byte 3: Weather code (0 ~ 21 )
	// Byte 4 ~ 15: Beach Name (Max. 12 characters )
	// Byte 16, 17: Sunrise Hour, Minute
	// Byte 18, 19: Sunset Hour, Minute
	private static class Package0 {
		static byte[] Data = new byte[20];
	};

	// Byte 0: Data Type, this value is fixed as 17. (Ref to ¡®Geneva Watch
	// Protocol Spec¡¯ )
	// Byte 1: Package Size ( 18 for this package )
	// Byte 2: Min. Air Temperature ( -127 ~ 127 )
	// Byte 3: Max. Air Temperature ( -127 ~ 127 )
	// Byte 4: Min. Water Temperature ( -127 ~ 127 )
	// Byte 5: Max. Water Temperature ( -127 ~ 127 )
	// Byte 6, 7: Time of coming Hi Tide in Hour and Minute
	// Byte 8, 9: Size of coming Hi Tide ( -127.99 ~ 127.99 )
	// Byte 10, 11: Time of coming Lo Tide in Hour and Minute
	// Byte 12, 13: Size of coming Lo Tide ( -127.99 ~ 127.99 )
	// Byte 14, 15: Size of Min Swell ( 0 ~ 255.99 )
	// Byte 16, 17: Size of Max Swell ( 0 ~ 255.99 )
	private static class Package1 {
		static byte[] Data = new byte[18];
	};

	// Byte 0: Data Type, this value is fixed as 18. (Ref to ¡®Geneva Watch
	// Protocol Spec¡¯ )
	// Byte 1: Package Size ( 5 for this package )
	// Byte 2, 3: Wind Speed ( 0 ~ 255 )
	// Byte 4: Wind Direction ( 0 ~ 15 )
	private static class Package2 {
		static byte[] Data = new byte[12];
	};

	// For displaying data in the App
	private String Location = "";

	public String getLocation() {
		return Location;
	}

	private String WeatherType = "";

	public String getWeatherType() {
		return WeatherType;
	}

	private int AirTempMin;
	private int AirTempMax;
	private String AirTemperature = "";

	public String getAirTemperature() {
		return AirTemperature;
	}

	private int WaterTempMin;
	private int WaterTempMax;
	private String WaterTemperature = "";

	public String getWaterTemperature() {
		return WaterTemperature;
	}

	private String Sunrise;
	private String Sunset;

	public String getSunrise() {
		return Sunrise;
	}

	public String getSunset() {
		return Sunset;
	}

	private String TideLastTimeString = "Lo:10:30";
	private float TideLastSizeFloat = 0;
	private String TideLastSizeString = "";
	private boolean TideLastisHi = false;

	public String getTideLastTime() {
		return TideLastTimeString;
	}

	public String getTideLastSize() {
		return TideLastSizeString;
	}

	private String TideNextTimeString = " Hi:18:30";
	private float TideNextSizeFloat = 0;
	private String TideNextSizeString = "";
	private boolean TideNextisHi = true;

	public String getTideNextTime() {
		return TideNextTimeString;
	}

	public String getTideNextSize() {
		return TideNextSizeString;
	}

	private String TideNextAfterNextTimeString = "Lo:10:30";
	private float TideNextAfterNextSizeFloat = 0;
	private String TideNextAfterNextSizeString = "";
	private boolean TideNextAfterNextisHi = false;

	public String getTideNextAfterNextTime() {
		return TideNextAfterNextTimeString;
	}

	public String getTideNextAfterNextSize() {
		return TideNextAfterNextSizeString;
	}

	private float SurfTempMin = 0;
	private float SurfTempMax = 0;
	private String SurfSize = "";

	public String getSurfSize() {
		return SurfSize;
	}

	private int WindTempMin = 0;
	private int WindTempMax = 0;
	private String WindSpeed = "";

	public String getWindSpeed() {
		return WindSpeed;
	}

	private int WindTempDirection;
	private String WindDirection;

	public String getWindDirection() {
		return WindDirection;
	};

	private int SwellTempDirection;
	private String SwellDirection;

	public String getSwellDirection() {
		return SwellDirection;
	};

	private int SwellTempPeriod;
	private String SwellPeriod = "";

	public String getSwellPeriod() {
		return SwellPeriod;
	}

	private BluetoothLeService mBluetoothLeService;

	public Boolean getFinishedTask() {
		return FinishedTask;
	}

	Context Surfcontext;
	String RootTimeZone = "";

	public void UpdateData(boolean _isMetric, int _SpotID, boolean OfflineMode, final String TempString, Context context) {
		FinishedTask = false;
		isMetric = _isMetric;
		SpotID = _SpotID;
		Surfcontext = context;

		if (OfflineMode) {
			ServerIsOfflineUseStandardData();
			return;
		}
		new Thread(new Runnable() {
			@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
			public void run() {
				try {
					JsonObject = new JSONObject(TempString);
					Log.i(TAG, JsonObject.toString());

					// Location
					// ----------------------------------------------------------------------------------
					Location = JsonObject.getString("name");
					int LocationLength = Location.length();

					RootTimeZone = JsonObject.getString("timezone");

					byte[] TempByteArray = Location.toUpperCase().getBytes();// Note:
					// Send
					// only
					// Upper
					// case
					// characters
					for (int i = 0; i < 12; i++) {
						if (LocationLength > i)
							Package0.Data[4 + i] = TempByteArray[i];
						else
							Package0.Data[4 + i] = 0x20; // space
					}

					// Weather Type
					// ----------------------------------------------------------------------------------
					JSONArray TempWeatherArray = (JSONArray) JsonObject.getJSONObject("Weather").get("weather_type");
					WeatherType = TempWeatherArray.getString(0);

					// Sunrise Sunset
					// ----------------------------------------------------------------------------------
					String CurrentTimeZone = JsonObject.getJSONObject("Tide").getString("timezone");
					JSONArray TempSunPointArray = (JSONArray) JsonObject.getJSONObject("Tide").get("SunPoints");

					boolean gotSunrise = false;
					boolean gotSunset = false;
					long SunriseEpoch = 0;
					long SunsetEpoch = 0;
					for (int i = 0; i < TempSunPointArray.length(); i++) {
						JSONObject TempObject = (JSONObject) TempSunPointArray.get(i);
						if (TempObject.getString("type").equals("Sunrise")) {
							gotSunrise = true;
							SunriseEpoch = Long.parseLong(TempObject.getString("time"));
						} else {
							gotSunset = true;
							SunsetEpoch = Long.parseLong(TempObject.getString("time"));
						}

						if (gotSunrise && gotSunset)
							break;
					}

					if (gotSunrise) {
						Date mSunRiseDate = new Date(SunriseEpoch * 1000L);

						String mStringSunRise = UTCTime(mSunRiseDate);

						Calendar mCalendarSunRise = Calendar.getInstance();
						DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
						mCalendarSunRise.setTime(converter.parse(mStringSunRise));
						SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");

						if (!CurrentTimeZone.equalsIgnoreCase("")) {
							if (CurrentTimeZone.contains(".")) {
								mCalendarSunRise.add(Calendar.HOUR_OF_DAY, Integer.parseInt(CurrentTimeZone.substring(0, CurrentTimeZone.indexOf("."))));
								mCalendarSunRise.add(Calendar.MINUTE, 30);
							} else {
								mCalendarSunRise.add(Calendar.HOUR_OF_DAY, Integer.parseInt(CurrentTimeZone));
							}
						}
						Sunrise = String.format(dateFormat.format(mCalendarSunRise.getTime()));
					} else {
						Sunrise = "00:00";
					}

					if (gotSunset) {
						Date mSunSetDate = new Date(SunsetEpoch * 1000L);

						String mStringSunSet = UTCTime(mSunSetDate);

						Calendar mCalendarSunset = Calendar.getInstance();
						DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
						mCalendarSunset.setTime(converter.parse(mStringSunSet));
						SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
						if (CurrentTimeZone.contains(".")) {
							mCalendarSunset.add(Calendar.HOUR_OF_DAY, Integer.parseInt(CurrentTimeZone.substring(0, CurrentTimeZone.indexOf("."))));
							mCalendarSunset.add(Calendar.MINUTE, 30);
						} else {
							mCalendarSunset.add(Calendar.HOUR_OF_DAY, Integer.parseInt(CurrentTimeZone));
						}

						Sunset = String.format(dateFormat.format(mCalendarSunset.getTime()));
					} else {
						Sunset = "00:00";
					}

					// Air Temperature
					// ----------------------------------------------------------------------------------
					JSONArray TempAirMinArray = (JSONArray) JsonObject.getJSONObject("Weather").get("temp_min");
					AirTempMin = (int) Math.round(Double.parseDouble(TempAirMinArray.getString(0)));
					JSONArray TempAirMaxArray = (JSONArray) JsonObject.getJSONObject("Weather").get("temp_max");
					AirTempMax = (int) Math.round(Double.parseDouble(TempAirMaxArray.getString(0)));
					if (isMetric)
						AirTemperature = String.format("%d" + "-%d" + "¡ãC", AirTempMin, AirTempMax);
					else
						AirTemperature = String.format("%d" + "-%d" + "¡ãF", AirTempMin, AirTempMax);

					// Water Temperature
					// ----------------------------------------------------------------------------------
					// Note: Some stations send float some ints, which causes
					// parseInt to crash, workaround below works
					WaterTempMin = (int) Math.round(Double.parseDouble(JsonObject.getJSONObject("WaterTemp").getString("watertemp_min")));
					WaterTempMax = (int) Math.round(Double.parseDouble(JsonObject.getJSONObject("WaterTemp").getString("watertemp_max")));
					if (isMetric)
						WaterTemperature = String.format("%d" + "-%d" + "¡ãC", WaterTempMin, WaterTempMax);
					else
						WaterTemperature = String.format("%d" + "-%d" + "¡ãF", WaterTempMin, WaterTempMax);

					// Tide Info
					// ----------------------------------------------------------------------------------
					JSONArray TempTidePointArray = (JSONArray) JsonObject.getJSONObject("Tide").get("dataPoints");
					String TideTimeZone = "";
					TideTimeZone = JsonObject.getJSONObject("Tide").getString("timezone");

					ArrayList<String> tide_graph_array = new ArrayList<String>();
					ArrayList<Calendar> mArrayCalender = new ArrayList<Calendar>();
					mArrayCalender.clear();
					tide_graph_array.clear();
					Calendar mCalendarLast = Calendar.getInstance();
					Calendar mCalendarNext = Calendar.getInstance();

					for (int i = 0; i < TempTidePointArray.length(); i++) {
						JSONObject TempObject = (JSONObject) TempTidePointArray.get(i);

						Calendar c1 = Calendar.getInstance();

						DateFormat df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
						c1.setTime(df.parse(TempObject.getString("utctime") + ""));
						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								c1.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								c1.add(Calendar.MINUTE, 30);
							} else {
								c1.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
							}
						}
						Calendar c2 = Calendar.getInstance();

						boolean isSameDay = isSameDay(c1, c2);

						Calendar c3 = Calendar.getInstance();
						c3.add(Calendar.DAY_OF_YEAR, 1);

						String mString = TempObject.getString("type");

						if (isSameDay) {
							if (mString.equalsIgnoreCase("Sunrise") || mString.equalsIgnoreCase("Sunset")) {
							} else {
								double value1 = TempObject.getDouble("height");

								value1 = Double.parseDouble(new DecimalFormat("#.#").format(value1));

								tide_graph_array.add(String.valueOf(value1));

								Calendar dateObj = Calendar.getInstance();
								DateFormat sdfAM = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
								dateObj.setTime(sdfAM.parse(TempObject.getString("utctime") + ""));

								if (!TideTimeZone.equalsIgnoreCase("")) {
									if (TideTimeZone.contains(".")) {
										dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
										dateObj.add(Calendar.MINUTE, 30);
									} else {
										dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
									}
								}
								mArrayCalender.add(dateObj);
							}
						}
					}

					float Tidemax = 0, Tidemin = 1000;

					// java.util.Date mDate = new java.util.Date();
					// String utc_time_str = UTCTime(mDate);
					// Calendar mCalendar = Calendar.getInstance();
					//
					// DateFormat df4 = new
					// SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
					// mCalendar.setTime(df4.parse(utc_time_str));
					//
					// if(!TideTimeZone.equalsIgnoreCase(""))
					// {
					// if(TideTimeZone.contains("."))
					// {
					// mCalendar.add(Calendar.HOUR_OF_DAY,
					// Integer.parseInt(TideTimeZone.substring(0,
					// TideTimeZone.indexOf("."))));
					// mCalendar.add(Calendar.MINUTE, 30);
					// }
					// else
					// {
					// mCalendar.add(Calendar.HOUR_OF_DAY,
					// Integer.parseInt(TideTimeZone));
					// }
					// }

					ArrayList<String> TempArrayList1;

					TempArrayList1 = tide_graph_array;

					for (int j = 2; j < TempArrayList1.size() - 2; j++) {
						// if(mArrayCalender.get(j).after(mCalendar))
						// {
						float a = (float) Double.parseDouble(TempArrayList1.get(j));
						if (a >= Tidemax) {
							Tidemax = a;
							mCalendarNext = mArrayCalender.get(j);
						}
						// }
					}

					ArrayList<String> TempArrayList2;

					TempArrayList2 = tide_graph_array;

					for (int j = 2; j < TempArrayList2.size() - 2; j++) {
						// if(mArrayCalender.get(j).after(mCalendar))
						// {
						float b = (float) Double.parseDouble(TempArrayList2.get(j));
						if (b <= Tidemin) {
							Tidemin = b;
							mCalendarLast = mArrayCalender.get(j);
						}
						// }
					}

					Tidemin = Float.parseFloat(convertToSingalDecimal(String.valueOf(Tidemin)));
					Tidemax = Float.parseFloat(convertToSingalDecimal(String.valueOf(Tidemax)));

					float mtideMin = Tidemin;
					float mtideMax = Tidemax;

					Calendar mCalendarCurrentGMTTime = Calendar.getInstance();
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
					mCalendarCurrentGMTTime.setTime(df.parse(CurrentGMTTime()));
					if (!TideTimeZone.equalsIgnoreCase("")) {
						if (TideTimeZone.contains(".")) {
							mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
							mCalendarCurrentGMTTime.add(Calendar.MINUTE, 30);
						} else {
							mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
						}
					}
					for (int i = 0; i < mArrayCalender.size(); i++) {
						if (mCalendarCurrentGMTTime.before(mArrayCalender.get(i)) || mCalendarCurrentGMTTime.equals(mArrayCalender.get(i))) {
							TideNextisHi = true;
							TideLastisHi = false;
						} else {
							TideLastisHi = true;
							TideNextisHi = false;
						}
					}
					if (isMetric) {
						TideLastSizeString = mtideMin + "m";
						TideLastSizeFloat = Float.parseFloat(mtideMin + "f");
						TideNextSizeString = mtideMax + "m";
						TideNextSizeFloat = Float.parseFloat(mtideMax + "f");
						;
						SimpleDateFormat df1 = new SimpleDateFormat("hh:mm");
						String LastTime = df1.format(mCalendarLast.getTime());
						TideLastTimeString = "Lo:" + LastTime.trim().toString();
						SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
						String NextTime = df2.format(mCalendarNext.getTime());
						TideNextTimeString = "Hi:" + NextTime.trim().toString();
					} else {
						TideLastSizeString = mtideMin + "m";
						TideLastSizeFloat = Float.parseFloat(mtideMin + "f");
						;
						TideNextSizeString = mtideMax + "m";
						TideNextSizeFloat = Float.parseFloat(mtideMax + "f");
						;
						SimpleDateFormat df1 = new SimpleDateFormat("hh:mm");
						String LastTime = df1.format(mCalendarLast.getTime());
						TideLastTimeString = "Lo:" + LastTime.trim().toString();
						SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
						String NextTime = df2.format(mCalendarNext.getTime());
						TideNextTimeString = "Hi:" + NextTime.trim().toString();
					}

					TideNextAfterNextTimeString = "Lo:16:23";
					TideNextAfterNextSizeFloat = -2.7f;
					TideNextAfterNextSizeString = "-2.7m";
					TideNextAfterNextisHi = false;

					// new tide start

					// New parsing code
					ArrayList<Double> _heightArray = new ArrayList<Double>();
					ArrayList<String> _timeArray = new ArrayList<String>();
					ArrayList<Calendar> _calanderArray = new ArrayList<Calendar>();
					JSONObject _jobj = new JSONObject(new JSONObject(TempString).getString("Tide"));

					JSONArray _jArrray = _jobj.getJSONArray("dataPoints");
					String _timeZone = _jobj.getString("timezone");
					int _cnt = 0;

					boolean _currentAdded = false;
					for (int i = 0; i < _jArrray.length(); i++) {
						JSONObject TempObject = (JSONObject) _jArrray.get(i);

						SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
						dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));

						// Local time zone
						SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

						Calendar _cal = Calendar.getInstance();
						Calendar _calCurrent = Calendar.getInstance();

						_calCurrent.setTime(dateFormatLocal.parse(dateFormatGmt.format(new Date())));
						// if (!today) {
						// _calCurrent.add(Calendar.DAY_OF_YEAR, 1);
						// }
						// System.out.println("after " + _calCurrent.getTime());
						DateFormat _df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
						_cal.setTime(_df.parse(TempObject.getString("utctime") + ""));

						if (!_timeZone.equalsIgnoreCase("")) {
							if (_timeZone.contains(".")) {
								String[] _time = new String[2];
								_time[0] = _timeZone.substring(0, _timeZone.indexOf("."));
								_time[1] = _timeZone.substring(_timeZone.indexOf(".") + 1);
								_cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_time[0]));
								_calCurrent.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_time[0]));
								if (_time[1].length() == 1) {
									_time[1] = _time[1] + "0";
								}
								int _minuts = (Integer.parseInt(_time[1]) * 60) / 100;
								_cal.add(Calendar.MINUTE, _minuts);
								_calCurrent.add(Calendar.MINUTE, _minuts);
							} else {
								_cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_timeZone));
								_calCurrent.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_timeZone));

							}

							if ((TempObject.getString("type").equalsIgnoreCase("NORMAL") || TempObject.getString("type").equals("Low") || TempObject.getString("type").equals("High")) && (getDate(_cal).equalsIgnoreCase(getDate(_calCurrent)))) {
								double _value = TempObject.getDouble("height");
								// _value = Double.parseDouble(new
								// DecimalFormat("#.#").format(_value));
								_heightArray.add(_value);
								SimpleDateFormat _df1 = new SimpleDateFormat("hh:mm a");
								String _currentTime = _df1.format(_cal.getTime());
								_timeArray.add(_currentTime);
								_calanderArray.add(_cal);
								System.out.println("curr date " + _calCurrent.getTime() + "  Tide date " + _cal.getTime());
								if (_calCurrent.before(_cal) && !_currentAdded) {
									mCurrentIndex = _cnt;
									_currentAdded = true;

								}
								_cnt++;
							}
						}
					}

					if (_currentAdded == false && mCurrentIndex == -1) {
						mCurrentIndex = _heightArray.size() - 1;
					}
					mArrayListHeight = new ArrayList<String>();
					mArrayListTime = new ArrayList<String>();
					data = new double[_timeArray.size()];
					for (int _i = 0; _i < _timeArray.size(); _i++) {
						data[_i] = Double.parseDouble(_heightArray.get(_i).toString());
						mArrayListTime.add(_timeArray.get(_i));
						Double _value = Double.parseDouble(_heightArray.get(_i).toString());
						_value = round(_value, 1);
						// if (isMetric) {
						// mArrayListHeight.add(String.valueOf(_value) + " m");
						// } else {
						// mArrayListHeight.add(String.valueOf(_value) + " ft");
						// }
						mArrayListHeight.add(String.valueOf(_value));
					}
					System.out.println("   New Cur IndX  " + mCurrentIndex);
					System.out.println("points >>> " + _heightArray);
					System.out.println("Time >>> " + _timeArray);
					setData();
					// new tide end

					// Surf Info
					// ----------------------------------------------------------------------------------
					JSONArray TempSwellSizeArray;

					String mStringSurfLocalDate = JsonObject.getJSONObject("Wind").getString("startDate_pretty_LOCAL");
					Calendar CalendarSurfStartDateGMT = Calendar.getInstance();
					SimpleDateFormat sdf1 = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
					CalendarSurfStartDateGMT.setTime(sdf1.parse(mStringSurfLocalDate));

					SimpleDateFormat df1 = new SimpleDateFormat("HHmm");
					String surfGMTtime = df1.format(CalendarSurfStartDateGMT.getTime());

					int mIntsurfGMTTime = Integer.parseInt(surfGMTtime);

					int SurfArrayValue = 0;

					if (mIntsurfGMTTime < 0700) {
						SurfArrayValue = 0;
					} else if (mIntsurfGMTTime >= 0700 && mIntsurfGMTTime < 1400) {
						SurfArrayValue = 1;
					} else if (mIntsurfGMTTime >= 1400 && mIntsurfGMTTime < 1900) {
						SurfArrayValue = 2;
					} else if (mIntsurfGMTTime >= 1900) {
						SurfArrayValue = 3;
					}

					// swell height
					TempSwellSizeArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_height1");
					String mStringsurfHeight = "";
					if (TempSwellSizeArray.length() > 0) {
						JSONArray TempSwellSizeObject;
						TempSwellSizeObject = (JSONArray) TempSwellSizeArray.get(0);
						float surfHeight = (float) TempSwellSizeObject.getDouble(SurfArrayValue);
						surfHeight = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(surfHeight)));
						if (isMetric) {
							mStringsurfHeight = (surfHeight) + " m";
						} else {
							mStringsurfHeight = (surfHeight) + " ft";
						}
					}

					// Surf Min
					JSONArray TempSurfMin = (JSONArray) JsonObject.getJSONObject("Surf").get("surf_min");

					if (TempSurfMin.length() > 0) {

						JSONArray TempSurfMinObject = (JSONArray) TempSurfMin.get(0);
						SurfTempMin = (float) TempSurfMinObject.getDouble(SurfArrayValue);

					}

					// Surf Max
					JSONArray TempSurfMax = (JSONArray) JsonObject.getJSONObject("Surf").get("surf_max");

					if (TempSurfMax.length() > 0) {

						JSONArray TempSurfMaxObject;
						TempSurfMaxObject = (JSONArray) TempSurfMax.get(0);
						SurfTempMax = (float) TempSurfMaxObject.getDouble(SurfArrayValue);
					}

					SurfTempMin = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMin)));
					SurfTempMax = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMax)));

					if (isMetric)
						SurfSize = String.format("%.1f" + "-%.1f" + "m", SurfTempMin, SurfTempMax);
					else
						SurfSize = String.format("%.1f" + "-%.1f" + "ft", SurfTempMin, SurfTempMax);
					// Swell Direction
					// ----------------------------------------------------------------------------------
					JSONArray TempSwellDirectionArray;
					TempSwellDirectionArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_direction1");

					if (TempSwellDirectionArray.length() > 0) {
						JSONArray TempSwellDirectionObject;
						TempSwellDirectionObject = (JSONArray) TempSwellDirectionArray.get(0);
						SwellTempDirection = (int) TempSwellDirectionObject.getDouble(SurfArrayValue);

						if (SwellTempDirection <= 10) {
							SwellDirection = "N";
							SwellTempDirection = 0;
						} else if (SwellTempDirection > 10 && SwellTempDirection <= 30) {
							SwellDirection = "NNE";
							SwellTempDirection = 1;
						} else if (SwellTempDirection > 30 && SwellTempDirection <= 50) {
							SwellDirection = "NE";
							SwellTempDirection = 2;
						} else if (SwellTempDirection > 50 && SwellTempDirection < 80) {
							SwellDirection = "ENE";
							SwellTempDirection = 3;
						} else if (SwellTempDirection >= 80 && SwellTempDirection <= 100) {
							SwellDirection = "E";
							SwellTempDirection = 4;
						} else if (SwellTempDirection > 100 && SwellTempDirection <= 130) {
							SwellDirection = "ESE";
							SwellTempDirection = 5;
						} else if (SwellTempDirection > 130 && SwellTempDirection <= 150) {
							SwellDirection = "SE";
							SwellTempDirection = 6;
						} else if (SwellTempDirection > 150 && SwellTempDirection < 170) {
							SwellDirection = "SSE";
							SwellTempDirection = 7;
						} else if (SwellTempDirection >= 170 && SwellTempDirection <= 190) {
							SwellDirection = "S";
							SwellTempDirection = 8;
						} else if (SwellTempDirection > 190 && SwellTempDirection <= 210) {
							SwellDirection = "SSW";
							SwellTempDirection = 9;
						} else if (SwellTempDirection > 210 && SwellTempDirection <= 230) {
							SwellDirection = "SW";
							SwellTempDirection = 10;
						} else if (SwellTempDirection > 230 && SwellTempDirection < 260) {
							SwellDirection = "WSW";
							SwellTempDirection = 11;
						} else if (SwellTempDirection >= 260 && SwellTempDirection <= 280) {
							SwellDirection = "W";
							SwellTempDirection = 12;
						} else if (SwellTempDirection > 280 && SwellTempDirection <= 310) {
							SwellDirection = "WNW";
							SwellTempDirection = 13;
						} else if (SwellTempDirection > 310 && SwellTempDirection <= 330) {
							SwellDirection = "NW";
							SwellTempDirection = 14;
						} else if (SwellTempDirection > 330 && SwellTempDirection < 350) {
							SwellDirection = "NNW";
							SwellTempDirection = 15;
						} else if (SwellTempDirection >= 350) {
							SwellDirection = "N";
							SwellTempDirection = 0;
						}
					} else {
						SwellTempDirection = 0;
						SwellDirection = "N/A";
					}

					// Swell Period
					// ----------------------------------------------------------------------------------

					JSONArray TempSwellPeriodArray;
					TempSwellPeriodArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_period1");
					if (TempSwellPeriodArray.length() > 0) {
						JSONArray TempSwellPeriodObject;
						TempSwellPeriodObject = (JSONArray) TempSwellPeriodArray.get(0);
						SwellTempPeriod = (int) (TempSwellPeriodObject.getDouble(SurfArrayValue));
					} else {
						SwellTempPeriod = 0;
					}

					SwellPeriod = String.format("%d" + "s", SwellTempPeriod);

					// Wind Info
					// ----------------------------------------------------------------------------------
					WindTempMin = 0;
					WindTempMax = 0;
					String mStringLocalDate = JsonObject.getJSONObject("Wind").getString("startDate_pretty_LOCAL");

					Calendar CalendarWindStartDateGMT = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
					CalendarWindStartDateGMT.setTime(sdf.parse(mStringLocalDate));

					SimpleDateFormat sdformat = new SimpleDateFormat("HHmm");
					String WindGMTtime = sdformat.format(CalendarWindStartDateGMT.getTime());

					int mIntWindGMTTime = Integer.parseInt(WindGMTtime);

					int WindArrayValue = 0;

					if (mIntWindGMTTime < 0200) {
						WindArrayValue = 0;
					} else if (mIntWindGMTTime >= 0200 && mIntWindGMTTime < 0500) {
						WindArrayValue = 1;
					} else if (mIntWindGMTTime >= 0500 && mIntWindGMTTime < Integer.parseInt("0800")) {
						WindArrayValue = 2;
					} else if (mIntWindGMTTime >= Integer.parseInt("0800") && mIntWindGMTTime < 1200) {
						WindArrayValue = 3;
					} else if (mIntWindGMTTime >= 1200 && mIntWindGMTTime < 1500) {
						WindArrayValue = 4;
					} else if (mIntWindGMTTime >= 1500 && mIntWindGMTTime < 1800) {
						WindArrayValue = 5;
					} else if (mIntWindGMTTime >= 1800 && mIntWindGMTTime < 2100) {
						WindArrayValue = 6;
					} else if (mIntWindGMTTime >= 2100) {
						WindArrayValue = 7;
					}

					String wind_speed_str = JsonObject.getString("Wind");
					JSONObject wind_data_obj = new JSONObject(wind_speed_str);
					JSONArray wind_arr = wind_data_obj.getJSONArray("wind_speed");

					JSONArray wind_speed;
					wind_speed = new JSONArray(wind_arr.getString(0));

					WindTempMin = (int) Math.round(Double.parseDouble(wind_speed.getString(WindArrayValue)));
					WindTempMax = (int) Math.round(Double.parseDouble(wind_speed.getString(WindArrayValue)) * 1.3);

					if (isMetric) {
						WindSpeed = String.format(String.valueOf(WindTempMin) + "-" + String.valueOf(WindTempMin) + " km/h", WindTempMin, WindTempMax);
					} else
						WindSpeed = String.format(String.valueOf(WindTempMin) + "-" + String.valueOf(WindTempMin) + " kts", WindTempMin, WindTempMax);

					JSONArray TempWindDirectionArray = (JSONArray) JsonObject.getJSONObject("Wind").get("wind_direction");
					JSONArray TempwindDirectionObject = (JSONArray) TempWindDirectionArray.get(0);// Note:
					// also
					// an
					// array
					WindTempDirection = TempwindDirectionObject.getInt(WindArrayValue);

					if (WindTempDirection <= 10) {
						WindDirection = "N";
						WindTempDirection = 0;
					} else if (WindTempDirection > 10 && WindTempDirection <= 30) {
						WindDirection = "NNE";
						WindTempDirection = 1;
					} else if (WindTempDirection > 30 && WindTempDirection <= 50) {
						WindDirection = "NE";
						WindTempDirection = 2;
					} else if (WindTempDirection > 50 && WindTempDirection < 80) {
						WindDirection = "ENE";
						WindTempDirection = 3;
					} else if (WindTempDirection >= 80 && WindTempDirection <= 100) {
						WindDirection = "E";
						WindTempDirection = 4;
					} else if (WindTempDirection > 100 && WindTempDirection <= 130) {
						WindDirection = "ESE";
						WindTempDirection = 5;
					} else if (WindTempDirection > 130 && WindTempDirection <= 150) {
						WindDirection = "SE";
						WindTempDirection = 6;
					} else if (WindTempDirection > 150 && WindTempDirection < 170) {
						WindDirection = "SSE";
						WindTempDirection = 7;
					} else if (WindTempDirection >= 170 && WindTempDirection <= 190) {
						WindDirection = "S";
						WindTempDirection = 8;
					} else if (WindTempDirection > 190 && WindTempDirection <= 210) {
						WindDirection = "SSW";
						WindTempDirection = 9;
					} else if (WindTempDirection > 210 && WindTempDirection <= 230) {
						WindDirection = "SW";
						WindTempDirection = 10;
					} else if (WindTempDirection > 230 && WindTempDirection < 260) {
						WindDirection = "WSW";
						WindTempDirection = 11;
					} else if (WindTempDirection >= 260 && WindTempDirection <= 280) {
						WindDirection = "W";
						WindTempDirection = 12;
					} else if (WindTempDirection > 280 && WindTempDirection <= 310) {
						WindDirection = "WNW";
						WindTempDirection = 13;
					} else if (WindTempDirection > 310 && WindTempDirection <= 330) {
						WindDirection = "NW";
						WindTempDirection = 14;
					} else if (WindTempDirection > 330 && WindTempDirection < 350) {
						WindDirection = "NNW";
						WindTempDirection = 15;
					} else if (WindTempDirection >= 350) {
						WindDirection = "N";
						WindTempDirection = 0;
					}

					FinishedTask = true;
				} catch (Exception e) {
					e.printStackTrace();
					ServerIsOfflineUseStandardData();
				}
				SendDataToWatch();
			}
		}).start();
	}

	private void setData() {

		ArrayList<Double> _FirstHalf = new ArrayList<Double>();
		ArrayList<Double> _SecondHalf = new ArrayList<Double>();
		double _firstMinHeight = 50, _firstMaxHieght = 0, _secondMinHieght = 50, _secondMaxHeight = 0;
		int _firstMinIndex = 0, _firstMaxIndex = 0, _secondMaxIndex = 0, _secondMinIndex = 0;
		int _hafIndex = data.length / 2;
		ArrayList<String> xVals = new ArrayList<String>();
		double _data[] = new double[data.length];
		for (int i = 0; i < data.length; i++) {
			if (data[i] < mLastMinTide) {
				mLastMinTide = data[i];
			}
			xVals.add((1990 + i) + "");
			if (i >= 0 && i < data.length) {

				if (i < _hafIndex) {
					_FirstHalf.add(data[i]);
					if (i > 1) {
						if (i == 2) {
							_firstMinIndex = 2;
							_firstMinHeight = data[i];
							_firstMaxIndex = 2;
							_firstMaxHieght = data[i];
						}
						if (data[i] < _firstMinHeight) {
							_firstMinHeight = data[i];
							_firstMinIndex = i;
						}
						if (data[i] > _firstMaxHieght) {
							_firstMaxHieght = data[i];
							_firstMaxIndex = i;
						}
					}
				} else if (i >= _hafIndex) {
					_SecondHalf.add(data[i]);
					if (i < data.length - 2) {
						if (i == _hafIndex + 1) {
							_secondMinIndex = i;
							_secondMinHieght = data[i];
							_secondMaxIndex = i;
							_secondMaxHeight = data[i];
						}

						if (data[i] < _secondMinHieght) {
							_secondMinHieght = data[i];
							_secondMinIndex = i;
						}
						if (data[i] > _secondMaxHeight) {
							_secondMaxHeight = data[i];
							_secondMaxIndex = i;

						}
					}
				}
			}
		}

		// / add lastMinTide
		for (int _i = 0; _i < data.length; _i++) {
			double absVal = Math.abs(mLastMinTide);
			_data[_i] = data[_i] + absVal + 1.5;
		}

		// //////////fine tune index start

		if (data.length > 0) {
			// fine tune idexes
			if (_firstMinIndex < _firstMaxIndex && _secondMaxIndex < _secondMinIndex) {
				// calculate first max again, which should be less then firstmin
				// index
				_firstMaxIndex = 0;
				// float _firstMaxHeight = 0;
				for (int i = 1; i < _firstMinIndex; i++) {
					float _height = Float.parseFloat(_FirstHalf.get(i).toString());
					if (i == 1) {
						_firstMaxIndex = 1;
						_firstMaxHieght = _height;
					}
					if (_height > _firstMaxHieght) {
						_firstMaxHieght = _height;
						_firstMaxIndex = i;
					}
				}
			}

			if (_firstMaxIndex < _firstMinIndex && _secondMinIndex < _secondMaxIndex && _secondMinIndex - _firstMinIndex < 5) {
				// calculate second min again, which should be greater then
				// secondmax index
				_secondMinIndex = -1;
				// float _secondMinHeight = 0;
				for (int i = _secondMaxIndex - _FirstHalf.size() + 1; i < _SecondHalf.size() - 2; i++) {
					float _height = Float.parseFloat(_SecondHalf.get(i).toString());
					if (i == _secondMaxIndex - _FirstHalf.size() + 1) {
						_secondMinIndex = _secondMaxIndex - _FirstHalf.size() + 1;
						_secondMinHieght = _height;
					}

					if (_height < _secondMinHieght) {
						_secondMinHieght = _height;
						_secondMinIndex = i;
					}
				}
				if (_secondMinIndex != -1) {
					_secondMinIndex = _secondMinIndex + _FirstHalf.size();
				}
			}
		}

		// get values to display in watch
		// -------------------------------
		ArrayList<String> tempChartData = new ArrayList<String>();
		ArrayList<String> tempTimeData = new ArrayList<String>();

		for (int i = mCurrentIndex; i < mArrayListHeight.size(); i++) {
			tempChartData.add(mArrayListHeight.get(i));
			tempTimeData.add(mArrayListTime.get(i));
		}

		// get min index
		float _minHeight = 0;
		int _minIndex = 0;
		if (mCurrentIndex <= _firstMinIndex) {
			_minHeight = (float) _firstMinHeight;
			_minIndex = _firstMinIndex;
			if (_secondMinHieght < _firstMinHeight) {
				_minHeight = (float) _secondMinHieght;
				_minIndex = _secondMinIndex;
			}
			TideLastSizeFloat = (float) round((round(Double.parseDouble(mArrayListHeight.get(_minIndex)), 2) * 10) / 10,1);
			// TideLastSizeFloat = roundf([[NSString
			// stringWithFormat:@"%.2f",[[mutableChartData
			// objectAtIndex:_minIndex] floatValue]] floatValue] * 10.0)/10.0;
			TideLastTimeString = mArrayListTime.get(_minIndex);
		} else if (mCurrentIndex > _firstMinIndex && mCurrentIndex <= _secondMinIndex) {
			_minHeight = (float) _secondMinHieght;
			_minIndex = _secondMinIndex;
			TideLastSizeFloat = (float) round((round(Double.parseDouble(mArrayListHeight.get(_minIndex)), 2) * 10) / 10,1);

			// TideLastSizeFloat = roundf([[NSString
			// stringWithFormat:@"%.2f",[[mutableChartData
			// objectAtIndex:_minIndex] floatValue]] floatValue] * 10.0)/10.0;
			TideLastTimeString = mArrayListTime.get(_minIndex);

			// TideLastTimeString=[arrayTideTime objectAtIndex:_minIndex];
		} else {
			if (tempChartData.size() > 0) {
				for (int i = 0; i < tempChartData.size(); i++) {
					float _height = Float.parseFloat(tempChartData.get(i));// [[tempChartData
					// objectAtIndex:i]
					// floatValue];
					if (i == 0) {
						_minIndex = 0;
						_minHeight = _height;
					}

					if (_height < _minHeight) {
						_minHeight = _height;
						_minIndex = i;
					}
				}
				TideLastSizeFloat = (float) round((round(Double.parseDouble(tempChartData.get(_minIndex)), 2) * 10) / 10,1);

				// TideLastSizeFloat = roundf([[NSString
				// stringWithFormat:@"%.2f",[[tempChartData
				// objectAtIndex:_minIndex] floatValue]] floatValue] *
				// 10.0)/10.0;
				TideLastTimeString = tempTimeData.get(_minIndex);

				// TideLastTimeString=[tempTimeData objectAtIndex:_minIndex];
			}
		}
		// get max index
		float _maxHeight = 0;
		int _maxIndex = 0;
		if (mCurrentIndex <= _firstMaxIndex) {
			_maxHeight = (float) _firstMaxHieght;// irstMaxHeight;
			_maxIndex = _firstMaxIndex;
			if (_secondMaxHeight > _firstMaxHieght) {
				_maxHeight = (float) _secondMaxHeight;
				_maxIndex = _secondMaxIndex;
			}
			System.out.println("TideNextSizeFloat >>>> 1 >>"+mArrayListHeight.get(_maxIndex)); 
			TideNextSizeFloat = (float) round((round(Double.parseDouble(mArrayListHeight.get(_maxIndex)), 2) * 10) / 10,1);

			// TideNextSizeFloat=roundf([[NSString
			// stringWithFormat:@"%.2f",[[mutableChartData
			// objectAtIndex:_maxIndex] floatValue]] floatValue] * 10.0)/10.0;
			TideNextTimeString = mArrayListTime.get(_maxIndex);

			// TideNextTimeString=[arrayTideTime objectAtIndex:_maxIndex];
		} else if (mCurrentIndex > _firstMaxIndex && mCurrentIndex <= _secondMaxIndex) {
			_maxHeight = (float) _secondMaxHeight;
			_maxIndex = _secondMaxIndex;
			System.out.println("TideNextSizeFloat >>>> 2 >>"+mArrayListHeight.get(_maxIndex)); 

			TideNextSizeFloat = (float) round((round(Double.parseDouble(mArrayListHeight.get(_maxIndex)), 2) * 10) / 10,1);
			TideNextTimeString = mArrayListTime.get(_maxIndex);

			// TideNextSizeFloat=roundf([[NSString
			// stringWithFormat:@"%.2f",[[mutableChartData
			// objectAtIndex:_maxIndex] floatValue]] floatValue] * 10.0)/10.0;
			// TideNextTimeString=[arrayTideTime objectAtIndex:_maxIndex];
		} else {
			if (tempChartData.size() > 0) {
				for (int i = 0; i < tempChartData.size(); i++) {
					float _height = Float.parseFloat(tempChartData.get(i));// [[tempChartData
					// objectAtIndex:i]
					// floatValue];
					if (i == 0) {
						_maxIndex = 0;
						_maxHeight = _height;
					}

					if (_height > _maxHeight) {
						_maxHeight = _height;
						_maxIndex = i;
					}
				}
				System.out.println("TideNextSizeFloat >>>> 3 >>"+tempChartData.get(_maxIndex)); 

				TideNextSizeFloat = (float) round((round(Double.parseDouble(tempChartData.get(_maxIndex)), 2) * 10) / 10,1);
				TideNextTimeString = tempTimeData.get(_maxIndex);

				// TideNextSizeFloat=roundf([[NSString
				// stringWithFormat:@"%.2f",[[tempChartData
				// objectAtIndex:_maxIndex] floatValue]] floatValue] *
				// 10.0)/10.0;
				// TideNextTimeString=[tempTimeData objectAtIndex:_maxIndex];
			}
		}

		System.out.println("TideLastSizeFloat   " + TideLastSizeFloat);
		System.out.println("TideLastTimeString   " + TideLastTimeString);
		System.out.println("TideNextSizeFloat   " + TideNextSizeFloat);
		System.out.println("TideNextTimeString   " + TideNextTimeString);
		TideLastTimeString = "Lo:" + getFullTime(TideLastTimeString);
		TideNextTimeString = "Hi:" + getFullTime(TideNextTimeString);

		System.out.println("TideLastTimeString   " + TideLastTimeString);
		System.out.println("TideNextTimeString   " + TideNextTimeString);


	}

	// new function
	public double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	// new function
	public String getDate(Calendar cal) {
		String _date = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
		_date = dateFormat.format(cal.getTime());
		// System.out.println(_date);
		return _date;
	}

	// new function
	public String getFullTime(String time) {
		String newTime = "";
		try {

			Date _d = (new SimpleDateFormat("hh:mm a")).parse(time);
			newTime = (new SimpleDateFormat("HH:mm")).format(_d);
			System.out.println("New time" + newTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newTime;
	}

	@SuppressLint("SimpleDateFormat")
	public String CurrentGMTTime() {
		java.util.Date mDate = new java.util.Date();
		// creating DateFormat for converting time from local timezone to GMT
		DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return converter.format(mDate);
	}

	private String convertToSingalDecimalForSurf(String val_str) {
		String[] SplitStringcal = val_str.split("\\.");
		int i = 0;
		if (SplitStringcal[1].length() >= 2) {
			SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
			i = Integer.parseInt(SplitStringcal[1]);
			if (i % 10 >= 5) {
				i = i + (10 - (i % 10));
			} else {
				i = Integer.parseInt(SplitStringcal[1].substring(0, 1));
			}
		} else {
			i = Integer.parseInt(SplitStringcal[1]);
		}
		return SplitStringcal[0] + "." + i;
	}

	private String convertToSingalDecimal(String val_str) {
		String[] SplitStringcal = val_str.split("\\.");
		int i = 0;
		if (SplitStringcal[1].length() == 2) {
			SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
			i = Integer.parseInt(SplitStringcal[1]);
			if (i % 10 >= 5) {
				i = i + (10 - (i % 10));
			} else {
				i = Integer.parseInt(SplitStringcal[1].substring(0, 1));
			}
		} else {
			i = Integer.parseInt(SplitStringcal[1]);
		}
		return SplitStringcal[0] + "." + i;
	}

	public boolean isSameDay(Calendar cal1, Calendar cal2) {
		if (cal1 == null || cal2 == null)
			return false;
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
	}

	@SuppressLint("SimpleDateFormat")
	public String UTCTime(java.util.Date mdate) {
		// creating DateFormat for converting time from local timezone to GMT
		DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("UTC"));

		return converter.format(mdate);
	}

	public Surfline(BluetoothLeService _mBluetoothLeService) {

		mBluetoothLeService = _mBluetoothLeService;
	}

	public void SendDataToWatch() {
		boolean isNegative = false; // Note negative values are represented by a
		// 1 in the highest bit!
		byte TempByte;

		// -------------------------------------------------------------------------
		// ------------------------------- Package 0
		// -------------------------------
		// -------------------------------------------------------------------------
		Package0.Data[0] = 0x10; // Data Type
		Package0.Data[1] = 20; // Package Size
		if (isMetric)
			Package0.Data[2] = 0; // Byte 2: Unit System ( 0: Metric, 1:
		// Imperial )
		else
			Package0.Data[2] = 1;

		// Byte 3: Weather code (0 ~ 21 )
		if (WeatherType.equals("clear")) {
			Package0.Data[3] = 0;
		} else if (WeatherType.equals("mostly sunny")) {
			Package0.Data[3] = 1;
		} else if (WeatherType.equals("sunny")) {
			Package0.Data[3] = 2;
		} else if (WeatherType.equals("partly sunny")) {
			Package0.Data[3] = 3;
		} else if (WeatherType.equals("cloudy")) {
			Package0.Data[3] = 4;
		} else if (WeatherType.equals("mostly cloudy")) {
			Package0.Data[3] = 5;
		} else if (WeatherType.equals("overcast")) {
			Package0.Data[3] = 6;
		} else if (WeatherType.equals("partly cloudy")) {
			Package0.Data[3] = 7;
		} else if (WeatherType.equals("rain")) {
			Package0.Data[3] = 8;
		} else if (WeatherType.equals("heavy rain")) {
			Package0.Data[3] = 9;
		} else if (WeatherType.equals("scattered showers")) {
			Package0.Data[3] = 10;
		} else if (WeatherType.equals("sleet")) {
			Package0.Data[3] = 11;
		} else if (WeatherType.equals("snow")) {
			Package0.Data[3] = 12;
		} else if (WeatherType.equals("flurries")) {
			Package0.Data[3] = 13;
		} else if (WeatherType.equals("fog")) {
			Package0.Data[3] = 14;
		} else if (WeatherType.equals("hazy")) {
			Package0.Data[3] = 15;
		} else if (WeatherType.equals("scattered showers possible t-storms")) {
			Package0.Data[3] = 16;
		} else if (WeatherType.equals("heavy rain possible t-storms")) {
			Package0.Data[3] = 17;
		} else if (WeatherType.equals("scattered showers t-storms")) {
			Package0.Data[3] = 18;
		} else if (WeatherType.equals("possible t-storms")) {
			Package0.Data[3] = 19;
		} else if (WeatherType.equals("thunderstorms")) {
			Package0.Data[3] = 20;
		} else /* if (WeatherType.equals("unknown")) */{
			Package0.Data[3] = 21;
		}

		// Byte 4 ~ 15: Beach Name (Max. 12 characters )
		// Already written when parsing JSON Data

		// Byte 16, 17: Sunrise Hour, Minute
		String[] SplitStringSunriseTime = Sunrise.split("\\:");
		SplitStringSunriseTime[0] = SplitStringSunriseTime[0].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringSunriseTime[1] = SplitStringSunriseTime[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		Package0.Data[16] = (byte) Integer.parseInt(SplitStringSunriseTime[0]);
		Package0.Data[17] = (byte) Integer.parseInt(SplitStringSunriseTime[1]);

		// Byte 18, 19: Sunset Hour, Minute
		String[] SplitStringSunsetTime = Sunset.split("\\:");
		SplitStringSunsetTime[0] = SplitStringSunsetTime[0].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringSunsetTime[1] = SplitStringSunsetTime[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		int hours = Integer.parseInt(SplitStringSunsetTime[0]) + 12;
		Package0.Data[18] = (byte) hours;
		Package0.Data[19] = (byte) Integer.parseInt(SplitStringSunsetTime[1]);

		// -------------------------------------------------------------------------
		// ------------------------------- Package 1
		// -------------------------------
		// -------------------------------------------------------------------------
		Package1.Data[0] = 0x11; // Data Type
		Package1.Data[1] = 18; // Package Size

		// Note: All these values can be negative, but we don't need to treat
		// this specially
		Package1.Data[2] = (byte) AirTempMin; // Byte 2: Min. Air Temperature (
		// -127 ~ 127 )
		Package1.Data[3] = (byte) AirTempMax; // Byte 3: Max. Air Temperature (
		// -127 ~ 127 )
		Package1.Data[4] = (byte) WaterTempMin; // Byte 4: Min. Water
		// Temperature ( -127 ~ 127 )
		Package1.Data[5] = (byte) WaterTempMax; // Byte 5: Max. Water
		// Temperature ( -127 ~ 127 )

		// Byte 6, 7: Time of Last Tide in Hour and Minute
		if (TideLastTimeString.contains("--:--")) {
			// No last Tide information available force tide time to be 0xff
			// 0xff
			Package1.Data[6] = (byte) 0xFF;
			Package1.Data[7] = (byte) 0xFF;

			if (!TideLastisHi)
				Package1.Data[8] = (byte) (0x00 | 10000000);
		} else {

			try
			{
				String[] SplitStringLastTime = TideLastTimeString.split("\\:");
				
				SplitStringLastTime[1] = SplitStringLastTime[1].replace(" ", "");// Note:
				// Remove
				// whitespaces
				// so
				// parseInt
				// works
				
				try
				{
					SplitStringLastTime[2] = SplitStringLastTime[2].replace(" ", "");// Note:
				}
				catch(Exception e)
				{
					SplitStringLastTime[2] = "00";
				}
				// Remove
				// whitespaces
				// so
				// parseInt
				// works
				Package1.Data[6] = (byte) Integer.parseInt(SplitStringLastTime[1]);
				Package1.Data[7] = (byte) Integer.parseInt(SplitStringLastTime[2]);
			}
			catch(Exception e)
			{
				e.getMessage();
			}
			// Byte 8, 9: Size of Last Tide ( -127.99 ~ 127.99 )
			isNegative = false;
			if (TideLastSizeFloat < 0.f) {
				isNegative = true;
				TideLastSizeFloat = -TideLastSizeFloat;
			}
			String TempTideLast = Float.toString(TideLastSizeFloat);
			String[] SplitStringLastSize = TempTideLast.split("\\.");
			SplitStringLastSize[0] = SplitStringLastSize[0].replace(" ", "");// Note:
			// Remove
			// whitespaces
			// so
			// parseInt
			// works
			SplitStringLastSize[1] = SplitStringLastSize[1].replace(" ", "");// Note:
			// Remove
			// whitespaces
			// so
			// parseInt
			// works
			Package1.Data[8] = (byte) Integer.parseInt(SplitStringLastSize[0]);

			TempByte = (byte) Integer.parseInt(SplitStringLastSize[1]);
			if ((TempByte < 10) && (SplitStringLastSize[1].length() < 2)) // 0.01
				// ->
				// 1
				// 0.1
				// ->
				// 10
				TempByte = (byte) (TempByte * 10);

			Package1.Data[9] = TempByte; // Note: Always 2 digits after
			// comma:5.1 = 10 5.06 = 6 5.64 = 64
			if (!TideLastisHi)
				Package1.Data[8] = (byte) (Package1.Data[8] | 10000000);
			if (isNegative)
				Package1.Data[8] = (byte) (Package1.Data[8] | 01000000);
		}

		// Byte 10, 11: Time of Next Tide in Hour and Minute
		String[] SplitStringNextTime = TideNextTimeString.split("\\:");
		SplitStringNextTime[1] = SplitStringNextTime[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringNextTime[2] = SplitStringNextTime[2].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		Package1.Data[10] = (byte) Integer.parseInt(SplitStringNextTime[1]);
		Package1.Data[11] = (byte) Integer.parseInt(SplitStringNextTime[2]);

		// Byte 12, 13: Size of Next Tide ( -127.99 ~ 127.99 )
		isNegative = false;
		if (TideNextSizeFloat < 0.f) {
			isNegative = true;
			TideNextSizeFloat = -TideNextSizeFloat;
		}
		String TempTideNext = Float.toString(TideNextSizeFloat);
		String[] SplitStringNextSize = TempTideNext.split("\\.");
		SplitStringNextSize[0] = SplitStringNextSize[0].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringNextSize[1] = SplitStringNextSize[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		Package1.Data[12] = (byte) Integer.parseInt(SplitStringNextSize[0]);

		TempByte = (byte) Integer.parseInt(SplitStringNextSize[1]);
		if ((TempByte < 10) && (SplitStringNextSize[1].length() < 2)) // 0.01 ->
			// 1 0.1
			// -> 10
			TempByte = (byte) (TempByte * 10);

		Package1.Data[13] = TempByte; // Note: Always 2 digits after comma:5.1 =
		// 10 5.06 = 6 5.64 = 64
		if (!TideNextisHi)
			Package1.Data[12] = (byte) (Package1.Data[12] | 10000000);
		if (isNegative)
			Package1.Data[12] = (byte) (Package1.Data[12] | 01000000);

		// Byte 14, 15: Time of Next Tide After Next in Hour and Minute
		String[] SplitStringNextAfterNextTime = TideNextAfterNextTimeString.split("\\:");
		SplitStringNextAfterNextTime[1] = SplitStringNextAfterNextTime[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringNextAfterNextTime[2] = SplitStringNextAfterNextTime[2].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		Package1.Data[14] = (byte) Integer.parseInt(SplitStringNextAfterNextTime[1]);
		Package1.Data[15] = (byte) Integer.parseInt(SplitStringNextAfterNextTime[2]);

		// Byte 16, 17: Size of Next Tide After Next( -127.99 ~ 127.99 )
		isNegative = false;
		if (TideNextAfterNextSizeFloat < 0.f) {
			isNegative = true;
			TideNextAfterNextSizeFloat = -TideNextAfterNextSizeFloat;
		}
		String TempTideNextAfterNext = Float.toString(TideNextAfterNextSizeFloat);
		String[] SplitStringNextAfterNextSize = TempTideNextAfterNext.split("\\.");
		SplitStringNextAfterNextSize[0] = SplitStringNextAfterNextSize[0].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringNextAfterNextSize[1] = SplitStringNextAfterNextSize[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		Package1.Data[16] = (byte) Integer.parseInt(SplitStringNextAfterNextSize[0]);

		TempByte = (byte) Integer.parseInt(SplitStringNextAfterNextSize[1]);
		if ((TempByte < 10) && (SplitStringNextAfterNextSize[1].length() < 2)) // 0.01
			// ->
			// 1
			// 0.1
			// ->
			// 10
			TempByte = (byte) (TempByte * 10);

		Package1.Data[17] = TempByte; // Note: Always 2 digits after comma:5.1 =
		// 10 5.06 = 6 5.64 = 64
		if (!TideNextAfterNextisHi)
			Package1.Data[16] = (byte) (Package1.Data[16] | 10000000);
		if (isNegative)
			Package1.Data[16] = (byte) (Package1.Data[16] | 01000000);

		// -------------------------------------------------------------------------
		// ------------------------------- Package 2
		// -------------------------------
		// -------------------------------------------------------------------------
		// Byte 0: Data Type, this value is fixed as 18. (Ref to ¡®Geneva
		// Watch Protocol Spec¡¯ )
		Package2.Data[0] = 18;

		// Byte 1: Package Size ( 5 for this package )
		Package2.Data[1] = 12;

		// Byte 2, 3: Wind Speed ( 0 ~ 255 )
		Package2.Data[2] = (byte) WindTempMin;
		Package2.Data[3] = (byte) WindTempMax;

		// Byte 4: Wind Direction ( 0 ~ 15 )
		Package2.Data[4] = (byte) WindTempDirection;

		// Byte 5, 6: Size of Min Swell ( 0 ~ 255.99 )
		String TempSurfMin = Float.toString(SurfTempMin);
		String[] SplitStringSurfSizeMin = TempSurfMin.split("\\.");
		SplitStringSurfSizeMin[0] = SplitStringSurfSizeMin[0].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringSurfSizeMin[1] = SplitStringSurfSizeMin[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		if (SplitStringSurfSizeMin[1].length() > 2)
			SplitStringSurfSizeMin[1] = SplitStringSurfSizeMin[1].substring(0, 2);

		Package2.Data[5] = (byte) Integer.parseInt(SplitStringSurfSizeMin[0]);
		TempByte = (byte) Integer.parseInt(SplitStringSurfSizeMin[1]);
		if (TempByte < 10)
			TempByte = (byte) (TempByte * 10);
		Package2.Data[6] = TempByte; // Note: Always 2 digits after comma:5.1 =
		// 10 5.06 = 6 5.64 = 64

		// Byte 7, 8: Size of Max Swell ( 0 ~ 255.99 )
		String TempSurfMax = Float.toString(SurfTempMax);
		String[] SplitStringSurfSizeMax = TempSurfMax.split("\\.");
		SplitStringSurfSizeMax[0] = SplitStringSurfSizeMax[0].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		SplitStringSurfSizeMax[1] = SplitStringSurfSizeMax[1].replace(" ", "");// Note:
		// Remove
		// whitespaces
		// so
		// parseInt
		// works
		if (SplitStringSurfSizeMax[1].length() > 2)
			SplitStringSurfSizeMax[1] = SplitStringSurfSizeMax[1].substring(0, 2);

		Package2.Data[7] = (byte) Integer.parseInt(SplitStringSurfSizeMax[0]);
		TempByte = (byte) Integer.parseInt(SplitStringSurfSizeMax[1]);
		if (TempByte < 10 && TempByte > 0)
			TempByte = (byte) (TempByte * 10);
		Package2.Data[8] = TempByte; // Note: Always 2 digits after comma:5.1 =
		// 10 5.06 = 6 5.64 = 64

		// Byte 9: Direction of Swell ( 0 ~ 15 )
		Package2.Data[9] = (byte) SwellTempDirection;

		// Byte 10, 11: Swell Period ( 0 ~ 99.99 )
		// Update, no digits after comma, because we are dealing in seconds and
		// waves cannot be measured that correctly anyway
		Package2.Data[10] = (byte) SwellTempPeriod;
		Package2.Data[11] = 0;

		if (mBluetoothLeService != null) {
			mBluetoothLeService.WriteAppendixB(0x5B);
			android.os.SystemClock.sleep(2000);
			mBluetoothLeService.Ble_Send_Data(Package0.Data);
			android.os.SystemClock.sleep(2000);
			mBluetoothLeService.Ble_Send_Data(Package1.Data);
			android.os.SystemClock.sleep(2000);
			mBluetoothLeService.Ble_Send_Data(Package2.Data);

			Intent SurfDebugIntent = new Intent("SURFDEBUG");
			Bundle bundle = new Bundle();
			String s1 = String.format("Pack0:%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d\n", Package0.Data[0], Package0.Data[1], Package0.Data[2], Package0.Data[3], Package0.Data[4], Package0.Data[5], Package0.Data[6], Package0.Data[7], Package0.Data[8], Package0.Data[9], Package0.Data[10], Package0.Data[11], Package0.Data[12], Package0.Data[13], Package0.Data[14], Package0.Data[15], Package0.Data[16], Package0.Data[17], Package0.Data[18], Package0.Data[19]);
			String s2 = String.format("Pack1:%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d\n", Package1.Data[0], Package1.Data[1], Package1.Data[2], Package1.Data[3], Package1.Data[4], Package1.Data[5], Package1.Data[6], Package1.Data[7], Package1.Data[8], Package1.Data[9], Package1.Data[10], Package1.Data[11], Package1.Data[12], Package1.Data[13], Package1.Data[14], Package1.Data[15], Package1.Data[16], Package1.Data[17]);
			String s3 = String.format("Pack2:%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d", Package2.Data[0], Package2.Data[1], Package2.Data[2], Package2.Data[3], Package2.Data[4], Package2.Data[5], Package2.Data[6], Package2.Data[7], Package2.Data[8], Package2.Data[9], Package2.Data[10], Package2.Data[11]);
			bundle.putString("Package0", s1 + s2 + s3);
			SurfDebugIntent.putExtras(bundle);
			Surfcontext.sendBroadcast(SurfDebugIntent);
		}
	}

	private void ServerIsOfflineUseStandardData() {
		// Location
		// ----------------------------------------------------------------------------------
		Location = "Beach Offline";
		int LocationLength = Location.length();
		byte[] TempByteArray = Location.toUpperCase().getBytes();// Note: Send
		// only
		// Upper
		// case
		// characters
		for (int i = 0; i < 12; i++) {
			if (LocationLength > i)
				Package0.Data[4 + i] = TempByteArray[i];
			else
				Package0.Data[4 + i] = 0x20; // space
		}

		// Weather Type
		// ----------------------------------------------------------------------------------
		WeatherType = "sunny";

		// Sunrise Sunset
		// ----------------------------------------------------------------------------------
		Sunrise = "05:23";
		Sunset = "17:56";

		// Air Temperature
		// ----------------------------------------------------------------------------------
		AirTempMin = -11;
		AirTempMax = 16;
		AirTemperature = "-11-16¡ãC";

		// Water Temperature
		// ----------------------------------------------------------------------------------
		WaterTempMin = -15;
		WaterTempMax = 18;
		WaterTemperature = "-15-18¡ãC";

		// Tide Info
		// ----------------------------------------------------------------------------------
		TideLastTimeString = "Lo:10:30";
		TideLastSizeFloat = -3.5f;
		TideLastSizeString = "-3.5m";
		TideLastisHi = false;

		TideNextTimeString = "Hi:15:18";
		TideNextSizeFloat = 4.7f;
		TideNextSizeString = "+4.7m";
		TideNextisHi = true;

		TideNextAfterNextTimeString = "Lo:16:23";
		TideNextAfterNextSizeFloat = -2.7f;
		TideNextAfterNextSizeString = "-2.7m";
		TideNextAfterNextisHi = false;

		// Surf Info
		// ----------------------------------------------------------------------------------
		SurfTempMin = 6.5f;
		SurfTempMax = 7.8f;
		SurfSize = "6.5-7.8m";

		SwellTempPeriod = 15;
		SwellPeriod = "15s";

		// Wind Info
		// ----------------------------------------------------------------------------------
		WindTempMin = 15;
		WindTempMax = 18;
		WindSpeed = "15.3-18.6 km/h";

		WindTempDirection = 5;
		WindDirection = "ESE";

		SwellTempDirection = 123;
		SwellTempDirection = SwellTempDirection / 27;
		SwellDirection = "WSW";

		FinishedTask = true;

		SendDataToWatch();
	}
}
