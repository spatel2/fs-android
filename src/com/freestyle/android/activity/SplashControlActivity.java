package com.freestyle.android.activity;

import com.freestyle.android.R;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.TextviewClass;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SplashControlActivity extends BaseActivity
{

	ButtonClass mButtonExit;

	SharedPreferences mSharedPreferenceSplash;
	Editor mEditorSplash;

	private ImageView c1tv;
	private ImageView c2tv;
	private ImageView c3tv;
	private TextviewClass cTextOne;
	private TextviewClass cTextTwo;
	private TextviewClass cTextThree;
	private String c1;
	private String c2;
	private String c3;
	private String countries[] = null;
	private int pic[] = null;

	boolean mBooleanSplashFlag = true;
	TextviewClass main_control_control_txt, main_control_text1;
	TextviewClass splash_command1_txt1,splash_command1_txt2,splash_command2_txt1,splash_command2_txt2,splash_command3_txt1,splash_command3_txt2;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_control_watch);

		main_control_control_txt = (TextviewClass)findViewById(R.id.main_control_control_txt);
		main_control_control_txt.setText(Html.fromHtml("To send commands to your phone, in <b>TIME MODE/</b> "));
		main_control_text1 = (TextviewClass)findViewById(R.id.main_control_control_txt1);
		main_control_text1.setText(Html.fromHtml("press <b>INFO (A)</b> to open Control Screen."));

		countries = new String[]
				{ "Camera", "Play/Pause Music", "Find Phone", "Next Song", "Off"};

		pic=new int[]
				{ R.drawable.n_camera_blue, R.drawable.n_play_blue, R.drawable.n_find_phone_blue, R.drawable.n_next_play_blu, R.drawable.n_off_icon_b};

		LinearLayout command1=(LinearLayout)findViewById(R.id.main_command1layout_linear);
		LinearLayout command2=(LinearLayout)findViewById(R.id.main_command2layout_linear);
		LinearLayout command3=(LinearLayout)findViewById(R.id.main_command3layout_linear);
		c1tv = (ImageView) findViewById(R.id.main_c1itemicon);
		c2tv = (ImageView) findViewById(R.id.main_c2itemicon);
		c3tv = (ImageView) findViewById(R.id.main_c3itemicon);
		cTextOne = (TextviewClass) findViewById(R.id.main_cTextOne);
		cTextTwo = (TextviewClass) findViewById(R.id.main_cTextTwo);
		cTextThree = (TextviewClass) findViewById(R.id.main_cTextThree);

		splash_command1_txt1 = (TextviewClass)findViewById(R.id.splash_command1_txt1);
		splash_command1_txt2 = (TextviewClass)findViewById(R.id.splash_command1_txt2);
		splash_command2_txt1 = (TextviewClass)findViewById(R.id.splash_command2_txt1);
		splash_command2_txt2 = (TextviewClass)findViewById(R.id.splash_command2_txt2);
		splash_command3_txt1 = (TextviewClass)findViewById(R.id.splash_command3_txt1);
		splash_command3_txt2 = (TextviewClass)findViewById(R.id.splash_command3_txt2);

		splash_command1_txt1.setText(Html.fromHtml("Press/hold <b>SET</b> button on watch"));
		splash_command1_txt2.setText(Html.fromHtml("to operate Command 1."));
		splash_command2_txt1.setText(Html.fromHtml("Press/hold <b>CONNECT</b> button on watch"));
		splash_command2_txt2.setText(Html.fromHtml("to operate Command 2."));
		splash_command3_txt1.setText(Html.fromHtml("Press/hold <b>COMMAND</b> button on"));
		splash_command3_txt2.setText(Html.fromHtml("watch to operate Command 3."));


		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0] );
		c2 = sharedata.getString("C2", countries[1] );
		c3 = sharedata.getString("C3", countries[2] );


		cTextThree.setText(c3+ "");
		cTextOne.setText(c1 + ")");
		cTextTwo.setText(c2+ "");

		c1tv.setImageResource(pic[setPic(c1)]);
		c2tv.setImageResource(pic[setPic(c2)]);
		c3tv.setImageResource(pic[setPic(c3)]);

		showDefaultWin("C1");
		showDefaultWin("C2");
		showDefaultWin("C3");

		command1.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showWin("C1");
			}
		});

		command2.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showWin("C2");
			}
		});


		command3.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showWin("C3");
			}
		});

		mButtonExit = (ButtonClass)findViewById(R.id.button_finish_setup);

		mButtonExit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {

				final Dialog mDialog = new Dialog(SplashControlActivity.this);
				mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				mDialog.setCancelable(false);
				mDialog.setContentView(R.layout.splash_dialog);
				mDialog.show();
				ButtonClass ok = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_okbutton);
				ButtonClass cancel = (ButtonClass) mDialog.findViewById(R.id.splash_dialog_cancelbutton);
				ok.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value),true);
						mEditorSplash.commit();
						Intent mIntent = new Intent(SplashControlActivity.this,Home_Activity.class);
						startActivity(mIntent);
						finish();
					}
				});

				cancel.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						mDialog.cancel();
						mSharedPreferenceSplash = getSharedPreferences(Tags.SHARED_PREFERENCE_SPLASH,Context.MODE_PRIVATE);
						mEditorSplash = mSharedPreferenceSplash.edit();
						mEditorSplash.putBoolean(getResources().getString(R.string.splash_flag_value),false);
						mEditorSplash.commit();
						Intent mIntent = new Intent(SplashControlActivity.this,Home_Activity.class);
						startActivity(mIntent);
						finish();
					}
				});
			}
		});
	}

	private void showWin(final String flag)
	{
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.ctrolactivitydialog);
		dlg.show();

		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0]);
		c2 = sharedata.getString("C2", countries[1]);
		c3 = sharedata.getString("C3", countries[2]);

		ButtonClass cancelButton = (ButtonClass) dlg.findViewById(R.id.CancelButton);
		cancelButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dlg.cancel();
			}
		});

		ButtonClass saveButton = (ButtonClass) dlg.findViewById(R.id.SaveButton);
		saveButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (flag.equals("C1"))
				{
					SharedPreferences.Editor sharedata = getSharedPreferences("control", 0).edit();
					sharedata.putString("C1", c1);
					sharedata.commit();

					c1tv.setImageResource(pic[setPic(c1)]);
					cTextOne.setText(c1 +"");
				}
				else if (flag.equals("C2"))
				{
					SharedPreferences.Editor sharedata = getSharedPreferences("control", 0).edit();
					sharedata.putString("C2", c2);
					sharedata.commit();

					c2tv.setImageResource(pic[setPic(c2)]);
					cTextTwo.setText(c2 + "");
				}
				else 
				{
					SharedPreferences.Editor sharedata = getSharedPreferences("control", 0).edit();
					sharedata.putString("C3", c3);
					sharedata.commit();

					c3tv.setImageResource(pic[setPic(c3)]);
					cTextThree.setText(c3 + "");
				}
				dlg.dismiss();
			}
		});

		WheelView country = (WheelView) dlg.findViewById(R.id.country);
		try
		{
			Integer a = (int) getResources().getDimension(R.dimen.testsp);
			country.setTEXT_SIZE(a);
		}
		catch (Exception e)
		{
			country.setTEXT_SIZE(60);
		}

		country.setVisibleItems(5);

		country.setAdapter(new ArrayWheelAdapter<String>(countries));
		country.addChangingListener(new OnWheelChangedListener()
		{
			public void onChanged(WheelView wheel, int oldValue, int newValue)
			{
				if (flag.equals("C1"))
					c1 = countries[wheel.getCurrentItem()];
				else if (flag.equals("C2"))
					c2 = countries[wheel.getCurrentItem()];
				else 
					c3 = countries[wheel.getCurrentItem()];
			}
		});

		if (flag.equals("C1"))
			country.setCurrentItem(setPic(c1));
		else if (flag.equals("C2"))
			country.setCurrentItem(setPic(c2));
		else 
			country.setCurrentItem(setPic(c3));
	}

	private void showDefaultWin(final String flag)
	{
		SharedPreferences sharedata = getSharedPreferences("control", 0);
		c1 = sharedata.getString("C1", countries[0]);
		c2 = sharedata.getString("C2", countries[1]);
		c3 = sharedata.getString("C3", countries[2]);
		if (flag.equals("C1"))
		{
			SharedPreferences.Editor sharedata1 = getSharedPreferences("control", 0).edit();
			sharedata1.putString("C1", c1);
			sharedata1.commit();
			int temp = pic[setPic(c1)];
			c1tv.setImageResource(temp);
			cTextOne.setText(c1 +"");
		}
		else if (flag.equals("C2"))
		{
			SharedPreferences.Editor sharedata2 = getSharedPreferences(
					"control", 0).edit();
			sharedata2.putString("C2", c2);
			sharedata2.commit();
			int temp = pic[setPic(c2)];
			c2tv.setImageResource(temp);
			cTextTwo.setText(c2 + "");

		}
		else if (flag.equals("C3"))
		{
			SharedPreferences.Editor sharedata3 = getSharedPreferences(
					"control", 0).edit();
			sharedata3.putString("C3", c3);
			sharedata3.commit();
			int temp = pic[setPic(c3)];
			c3tv.setImageResource(temp);
			cTextThree.setText(c3 + "");

		}
		else
		{
		}
		if (flag.equals("C1"))
		{
			c1 = countries[0];
		}
		else if (flag.equals("C2"))
		{
			c2 = countries[1];
		}
		else if (flag.equals("C3"))
		{
			c3 = countries[2];
		}
		else
		{
		}

		if (flag.equals("C1"))
		{
			setPic(c1);
		}
		else if (flag.equals("C2"))
		{
			setPic(c2);
		}
		else if (flag.equals("C3"))
		{
			setPic(c3);
		}
		else
		{
		}
	}

	private int setPic(String title)
	{
		for (int i = 0; i < countries.length; i++)
		{
			if (title.equals(countries[i]))
			{
				return i;
			}
		}
		return 0;
	}
}
