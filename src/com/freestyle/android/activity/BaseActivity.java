package com.freestyle.android.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.freestyle.android.R;
import com.freestyle.android.bluebooth.BluetoothLeService;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.GetAllData;
import com.freestyle.android.config.PostParseGet;
import com.freestyle.android.config.Shared_preference;
import com.navdrawer.SimpleSideDrawer;

public class BaseActivity extends Activity
{
	public static BluetoothLeService mBluetoothLeService;
	public Context context = null;
	public static final String ACTION_NOTIFICATION = "action_notification";
	public String address;
	public DemoApplication app;
	protected BluetoothAdapter mBluetoothAdapter;
	Bitmap yourSelectedImage;

	SimpleSideDrawer slide_me;
	Intent mIntent;
	ImageView drawer_icon;
	//	ImageView drwr_user_icon_img;
	//	TextView drwr_user_name_txt;
	ImageView d_profile_pic_lay;
	RelativeLayout mRelativeLayoutpic;
	LinearLayout d_settings_lay,d_home_lay,d_start_activity_lay,d_surf_lay,d_activity_log_lay,d_camera_lay,d_photo_gallary_lay;
	ArrayList<String> drawer_array = new ArrayList<String>();
	private LocationManager mLocationManager=null;

	ProgressDialog mProgressDialogFindBeach;
	PostParseGet mPostParseGet;
	GetAllData mGetAllData;
	CommonClass mCommonClass = new CommonClass();
	Shared_preference mSharedPreferenceFavorite;

	Date mDate;
	SimpleDateFormat mDay;
	SimpleDateFormat mHour;
	Shared_preference shared;
	ImageView mImageviewProfilePic;

	GetAllDataProcess mGetAllDataProcess = new GetAllDataProcess();

	@SuppressLint("SimpleDateFormat") 
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);

		if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) 
		{
			Toast.makeText(this,"Bluetooth is not supported on this device", Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		mSharedPreferenceFavorite = new Shared_preference(BaseActivity.this);

		shared = new Shared_preference(BaseActivity.this);
		mPostParseGet=new PostParseGet(BaseActivity.this);
		mGetAllData = new GetAllData();
		mGetAllData = DemoApplication.mGetAllData;

		mDate = new Date();
		mDay = new SimpleDateFormat("dd");
		mHour = new SimpleDateFormat("HH");

		if(check_Internet(BaseActivity.this))
		{
			if (mGetAllData.getData() == null) 
			{
				new GetAllDataProcess().execute();
			} 
		}

		//For auto update of api, will update on day change
		if(check_Internet(BaseActivity.this))
		{
			if(!(mGetAllDataProcess.getStatus()==AsyncTask.Status.PENDING))
			{
				if(shared.getPrefrence(context, Constants.PROFILE.SURF_AUTO_MANUAL).equals("true"))
				{	
					if(!shared.getPrefrence(context, Constants.PREFRENCE.API_day).equals(mDay.format(mDate)+""))
					{
						mGetAllDataProcess.execute();
					}
				}
				else
				{
					// Manual
					if(!shared.getPrefrence(context, Constants.PREFRENCE.API_day).equals(mDay.format(mDate)+""))
					{
						mGetAllDataProcess.execute();
					}
					else 
					{
						if(shared.getPrefrence(context, Constants.PROFILE.USER_MANUAL).equals("1 hr"))
						{
							int temp_hour=0;
							int api_hour=1;

							temp_hour = Integer.parseInt(mHour.format(mDate)+"");
							if(temp_hour - api_hour > 1)
							{
								mGetAllDataProcess.execute();
							}
						}
						else if(shared.getPrefrence(context, Constants.PROFILE.USER_MANUAL).equals("2 hr"))
						{
							int temp_hour=0;
							int api_hour=2;

							temp_hour = Integer.parseInt(mHour.format(mDate)+"");
							if(temp_hour - api_hour > 2)
							{
								mGetAllDataProcess.execute();
							}
						}
						else if(shared.getPrefrence(context, Constants.PROFILE.USER_MANUAL).equals("3 hr"))
						{
							int temp_hour=0;
							int api_hour=3;

							temp_hour = Integer.parseInt(mHour.format(mDate)+"");
							if(temp_hour - api_hour > 3)
							{
								mGetAllDataProcess.execute();
							}
						}
					}
				}
			}
		}


		if(check_Internet(BaseActivity.this))
		{
			if(mLocationManager==null)
			{
				mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
				if(!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
				{
					startGPS();
				}
				else
				{
					startService(new Intent(BaseActivity.this, Location_Update_Service.class));
				}
			}
		}

		// Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
		// BluetoothAdapter through BluetoothManager.
		final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();

		// Checks if Bluetooth is supported on the device.
		if (mBluetoothAdapter == null) 
		{
			Toast.makeText(this, "Bluetooth is  not supported on this device", Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		if (!mBluetoothAdapter.isEnabled())
		{
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivity(enableIntent);
		}

		app = (DemoApplication) getApplication();
		mBluetoothLeService = app.getmBluetoothLeService();

		registerReceiver(ConnectionChangedReceiver, makeGattUpdateIntentFilter());
		context = this;
	}

	@SuppressWarnings("deprecation")
	protected void initDrawer(Activity mActivity)
	{
		slide_me= new SimpleSideDrawer(mActivity);
		slide_me.setLeftBehindContentView(R.layout.drawer_lay);

		drawer_icon = (ImageView)findViewById(R.id.drawer_icon);
		drawer_icon.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				slide_me.toggleDrawer();
				setNewProfilePic();
			}
		});

		mImageviewProfilePic = (ImageView)findViewById(R.id.imageview_profile_pic);
		d_profile_pic_lay = (ImageView)findViewById(R.id.d_profile_pic_lay);
		mRelativeLayoutpic = (RelativeLayout)findViewById(R.id.relative_pic);
		d_settings_lay = (LinearLayout)findViewById(R.id.d_settings_lay);
		d_home_lay = (LinearLayout)findViewById(R.id.d_home_lay);
		d_start_activity_lay = (LinearLayout)findViewById(R.id.d_start_activity_lay);
		d_surf_lay = (LinearLayout)findViewById(R.id.d_surf_lay);
		d_activity_log_lay = (LinearLayout)findViewById(R.id.d_activity_log_lay);
		d_camera_lay = (LinearLayout)findViewById(R.id.d_camera_lay);
		d_photo_gallary_lay = (LinearLayout)findViewById(R.id.d_photo_gallary_lay);

		//For Profile Picture
		if(DemoApplication.ProfilePictureUri==null)
		{
			mImageviewProfilePic.setVisibility(View.VISIBLE);
		}
		else
		{
			setNewProfilePic();
		}


		mRelativeLayoutpic.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("profile");
			}
		});
		d_settings_lay.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("setting");
			}
		});
		d_start_activity_lay.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("start_activity");
			}
		});
		d_home_lay.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("home");
			}
		});
		d_surf_lay.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("surf");
			}
		});
		d_activity_log_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("activity_log");	
			}
		});
		d_camera_lay.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("camera_lay");
			}
		});
		d_photo_gallary_lay.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				DrawerClick("gallery");
			}
		});

		drawer_array.add("FIRST LAST NAME");
		drawer_array.add("HOME");
		drawer_array.add("SURF");
		drawer_array.add("START ACTIVITY");
		drawer_array.add("ACTIVITY LOG");
		drawer_array.add("SETTINGS");
		drawer_array.add("CAMERA");
		drawer_array.add("PHOTO GALLERY");
	}

	@Override
	public void onDestroy() 
	{
		unregisterReceiver(ConnectionChangedReceiver);
		super.onDestroy();
	}

	//Call when bluetooth device disconnected
	public void disconnect()
	{
	}

	//Call when bluetooth device is connecting
	public void connecting()
	{
	}

	//Call when bluetooth device connected
	public void connected()
	{
	}

	private final BroadcastReceiver ConnectionChangedReceiver = new BroadcastReceiver()  
	{ 
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			final String action = intent.getAction();

			if (action.equals("BL_STATECHANGE_CONNECTING")) 
			{
				connecting();
			} 
			else if (action.equals("BL_STATECHANGE_CONNECTED")) 
			{
				connected();
			} 
			else if (action.equals("BL_STATECHANGE_DISCONNECTED")) 
			{
				disconnect();
			} 
			else if (action.equals("BL_STATECHANGE_BL_SERVICE_UPDATED"))
			{
				mBluetoothLeService = app.getmBluetoothLeService();
			}
		}
	};

	private IntentFilter makeGattUpdateIntentFilter()
	{
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("BL_STATECHANGE_CONNECTING");
		intentFilter.addAction("BL_STATECHANGE_CONNECTED");
		intentFilter.addAction("BL_STATECHANGE_DISCONNECTED");
		intentFilter.addAction("BL_STATECHANGE_BL_SERVICE_UPDATED");
		return intentFilter;
	}

	@SuppressWarnings("deprecation")
	public void DrawerClick(String target)
	{
		slide_me.toggleDrawer();

		if(target.equals("setting"))
		{
			gotoSetting();
		}
		else if(target.equals("start_activity"))
		{
			gotoStartActivity();
		}
		else if(target.equals("profile"))
		{
			gotoProfile();
		}
		else if(target.equals("surf"))
		{
			gotoSurf();
		}
		else if(target.equals("gallery"))
		{
			mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://media/internal/images/media"));
			startActivity(mIntent);
		}
		else if(target.equals("music"))
		{
			mIntent = new Intent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);
			startActivity(mIntent);
		}
		else if(target.equalsIgnoreCase("camera_lay"))
		{
			mIntent = new Intent(context,CustomCameraActivity.class);
			startActivity(mIntent);
		}
		else if(target.equalsIgnoreCase("activity_log"))
		{
			gotoActivityLog();
		}
		else if(target.equalsIgnoreCase("home"))
		{
			gotoHome();
		}
	}

	@SuppressWarnings("deprecation")
	public void setNewProfilePic()
	{
		try
		{
			String imagePath = ""; 
			imagePath = shared.getPrefrence(BaseActivity.this, "user_image").toString();

			if(!imagePath.equals(""))
			{
				mImageviewProfilePic.setVisibility(View.GONE);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				int imageHeight = options.outHeight;
				int imageWidth = options.outWidth;

				options.inJustDecodeBounds = false;
				options.inSampleSize = calculateInSampleSize(options, imageWidth, imageHeight);

				d_profile_pic_lay.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeFile(imagePath,options)));
			}
			else
			{
				mImageviewProfilePic.setVisibility(View.VISIBLE);
			}

		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
	}

	public int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) 
	{
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 2;

		if (height > reqHeight || width > reqWidth) 
		{
			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) 
			{
				inSampleSize *= 2;
			}
		}
		return inSampleSize;
	}

	public void gotoHome()
	{
		mIntent = new Intent(context, Home_Activity.class);
		startActivity(mIntent);
		finish();
	}

	public void gotoSurf()
	{
		mIntent = new Intent(context, Favourate_beach_Activity.class);
		startActivity(mIntent);
		finish();
	}

	public void gotoStartActivity()
	{
		mIntent = new Intent(context, Choose_activity.class);
		startActivity(mIntent);
		finish();
	}

	public void gotoActivityLog()
	{
		mIntent = new Intent(context, ActivityLog_Activity.class);
		startActivity(mIntent);
		finish();
	}

	public void gotoSetting()
	{
		mIntent = new Intent(context, Settings_Activity.class);
		startActivity(mIntent);
		finish();
	}

	public void gotoProfile()
	{
		mIntent = new Intent(context, Profile_Activity.class);
		startActivity(mIntent);
	}

	private void startGPS()
	{
		final AlertDialog.Builder builder =
				new AlertDialog.Builder(BaseActivity.this);
		final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
		final String message = "Enable either GPS or any other location"
				+ " service to find current location.  Click OK to go to"
				+ " location services settings to let you do so.";

		builder.setMessage(message).setPositiveButton("OK",	new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface d, int id) 
			{
				BaseActivity.this.startActivity(new Intent(action));
				startService(new Intent(BaseActivity.this, Location_Update_Service.class));
				d.dismiss();
			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface d, int id) 
			{
				d.cancel();
			}
		});
		builder.create().show();
	}

	public class GetAllDataProcess extends AsyncTask<Void, Void, Void> 
	{
		@Override
		protected void onPreExecute()
		{
			mProgressDialogFindBeach = ProgressDialog.show(BaseActivity.this,"", "Loading...", false);
			mProgressDialogFindBeach.setCancelable(false);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			mGetAllData = (GetAllData) mPostParseGet.GetAllData(mGetAllData);
			return null;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Void result) 
		{   
			if (mProgressDialogFindBeach != null)
				mProgressDialogFindBeach.dismiss();
			if (mPostParseGet.isNetError)
				showAlert(BaseActivity.this, getResources().getString(R.string.error_internet), getString(R.string.ok));
			else if (mPostParseGet.isOtherError)
				showAlert(BaseActivity.this, getResources().getString(R.string.error_response), getString(R.string.ok));
			else 
			{
				if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
				{
					DemoApplication.mGetAllData=mGetAllData;
					mSharedPreferenceFavorite.setPrefrence(BaseActivity.this, "favorite_data", "true");
				}
			}
			shared.setPrefrence(context, Constants.PREFRENCE.API_day, mDay.format(mDate)+"");
			shared.setPrefrence(context, Constants.PREFRENCE.API_hour, mHour.format(mDate)+"");

			super.onPostExecute(result);
		}
	}

	public void showAlert(Activity mActivity, String message, String buttonMessage)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(message);
		builder.setNegativeButton( buttonMessage,new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.cancel();
			}
		});
		builder.show();
	}

	public boolean check_Internet(Context mContext)
	{
		ConnectivityManager mConnectivityManager;
		NetworkInfo mNetworkInfo;
		mConnectivityManager= (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

		if(mNetworkInfo != null && mNetworkInfo.isConnectedOrConnecting())
			return true;

		return false;
	}
}