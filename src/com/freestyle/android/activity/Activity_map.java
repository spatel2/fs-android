package com.freestyle.android.activity;

import java.util.ArrayList;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.freestyle.android.R;
import com.freestyle.android.config.Shared_preference;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class Activity_map extends BaseActivity implements com.google.android.gms.location.LocationListener, OnClickListener
{
	private GoogleMap mMap;
	LocationClient mLocationClient;
	Shared_preference shared;
	Context context;
	ImageView activity_map_back_img;
	LatLng mLatLng;
	TextView activity_map_title;
	ArrayList<Double> mLatArray = new ArrayList<Double>();
	ArrayList<Double> mLonArray = new ArrayList<Double>();
	boolean location_set = true;
	float zoomLevel = 15;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		context = Activity_map.this;
		initDrawer(Activity_map.this);
		shared = new Shared_preference(context);

		activity_map_back_img	= (ImageView)findViewById(R.id.activity_map_back_img);
		activity_map_title		= (TextView)findViewById(R.id.activity_map_title);

		Intent mIntent = getIntent();
		if(mIntent!=null)
		{
			activity_map_title.setText(mIntent.getExtras().getString("activity_name"));
		}

		if(check_Internet(Activity_map.this))
		{
			if (mMap == null) 
			{
				mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_location)).getMap();
			}

			mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			loadLocationUpdater();
		}
		else
		{
			Toast.makeText(Activity_map.this, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
		}
		
		mMap.setOnCameraChangeListener(new OnCameraChangeListener()
		{
			@Override
			public void onCameraChange(CameraPosition position) 
			{
				zoomLevel = position.zoom;
				if(zoomLevel<15)
				{
					zoomLevel = 15;
				}
				
			}
		});
		
		activity_map_back_img.setOnClickListener(this);
	}

	@Override
	public void onLocationChanged(Location mLocation) 
	{
		mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
		mMap.clear();
		
		if(location_set)
		{
			location_set = false;
		}
		
		getLatLongArray();
		mMap.clear();

		ArrayList<LatLng> arrayPoints = new ArrayList<LatLng>();
		PolylineOptions polylineOptions = new PolylineOptions(); 

		if(mLatArray.size()>1)
		{
			for(int i=0;i<mLatArray.size();i++)
			{
				// setting polyline in the map 
				polylineOptions.color(Color.BLUE); 
				polylineOptions.width(5);
				arrayPoints.add(new LatLng(mLatArray.get(i), mLonArray.get(i))); 
				
				if(i==mLatArray.size()-1)
				{
					mMap.addMarker(new MarkerOptions()
					.position(new LatLng(mLatArray.get(i), mLonArray.get(i))));
					
					mLatLng = new LatLng(mLatArray.get(i), mLonArray.get(i));
					if(location_set)
					{
						mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 15));
						location_set = false;
					}
					else
					{
						mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, zoomLevel));
					}
				}
			}
		}
		else
		{
			final LatLng currentPosition = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
			mMap.addMarker(new MarkerOptions()
			.position(currentPosition)
			.title(activity_map_title.getText().toString()));
		}
		polylineOptions.addAll(arrayPoints); 
		mMap.addPolyline(polylineOptions);
	}


	
	void loadLocationUpdater() 
	{
		mLocationClient = new LocationClient(this, new ConnectionCallbacks() 
		{
			@Override
			public void onDisconnected() 
			{
			}
			@Override
			public void onConnected(Bundle connectionHint) 
			{
				Location lastLocation = mLocationClient.getLastLocation();

				if (lastLocation != null) 
				{
				}
				LocationRequest mLocationRequest = LocationRequest.create();
				mLocationRequest.setInterval(10000);
				mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
				mLocationClient.requestLocationUpdates(mLocationRequest, Activity_map.this);

			}
		}, new OnConnectionFailedListener() 
		{
			@Override
			public void onConnectionFailed(ConnectionResult result) 
			{
				if (result.hasResolution()) 
				{
					try 
					{
						// Start an Activity that tries to resolve the error
						result.startResolutionForResult(Activity_map.this, 403);
					}
					catch (IntentSender.SendIntentException e) 
					{
						// Log the error
						e.printStackTrace();
					}
				}
				else 
				{
					Log.d("locationClient.onConnectionFailed",result.toString());
					GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), Activity_map.this, 403).show();
				}
			}
		});
	}

	@Override
	protected synchronized void onStart() 
	{
		mLocationClient.connect();
		super.onStart();
	}

	@Override
	protected void onStop()
	{
		if (mLocationClient.isConnected()) 
		{
			mLocationClient.removeLocationUpdates(this);
		}
		mLocationClient.disconnect();
		super.onStop();
	}

	@Override
	public void onClick(View v) 
	{
		finish();
	}

	private void getLatLongArray()
	{
		try
		{
			mLatArray.clear();
			mLonArray.clear();

			String[] parts = DemoApplication.CurrentLatLong.split(",");
			for(int i=0;i<parts.length;i++)
			{
				if(!parts[i].equals(""))
				{
					String[] latlonArr = parts[i].split("#");
					
					mLatArray.add(Double.parseDouble(latlonArr[0]));
					mLonArray.add(Double.parseDouble(latlonArr[1]));
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error in Data conversion "+ ex);
		}
		
		System.out.println("Display Array");
		for(int i=0;i<mLatArray.size();i++)
		{
			System.out.println("****"+mLatArray.get(i)+" "+mLonArray.get(i));
		}
	}
}
