package com.freestyle.android.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.freestyle.android.R;
import com.freestyle.android.bluebooth.GPSLocationServices;
import com.freestyle.android.config.ButtonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.pojo.Sport;

public class RoadBike_Activity extends BaseActivity implements  OnClickListener
{
	Context context;
	Bundle mBundle;
	Shared_preference shared;
	String region_id;
	ProgressDialog mProgressDialog;
	Intent mIntent;
	ImageView drawer_icon;
	RelativeLayout road_playpause_rel;
	RelativeLayout road_stop_rel;
	RelativeLayout road_pause_rel;

	TextView roadbike_activity_name;
	TextView activity_km_max_txt;
	TextView activity_km_min_txt;
	ImageView roadbike_back_img;
	TextView road_pause_txt;
	int user_age=0,user_height=0,user_weight=0;

	//For Saving Log
	DbTool db;
	private int SportsType = 0;

	private TextView activity_altitude_txt;
	private TextView activity_direction_txt;
	private TextView activity_calories_txt;
	private TextView activity_bpm_txt;
	private TextView activity_distance_txt;
	private TextView activity_km_txt;
	private TextView activity_current_time;
	private Handler handler;
	private long CurrentActivityDuration;

	private static final String TAG = "SportDetailLog";
	AlertDialog alertDialog = null;
	LinearLayout activity_location;

	int watch_seconds = 0;
	ImageView play_pause_imgview;

	private float	min_speed	= 0;
	private float	max_speed	= 0;

	//For preference of imperial and metric
	boolean isMetricTemp			=	false;

	TextView activity_km_label_txt;
	TextView activity_max_km_label_txt;
	TextView activity_min_km_label_txt;
	TextView activity_ditsance_label_txt;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.roadbike_activity);

		context = RoadBike_Activity.this;
		initDrawer(RoadBike_Activity.this);
		shared = new Shared_preference(context);
		handler = new Handler();
		db = new DbTool(this);

		DemoApplication.mBluetoothLeService_temp = BaseActivity.mBluetoothLeService;

		road_playpause_rel 	= (RelativeLayout)findViewById(R.id.road_playpause_rel);
		road_stop_rel 		= (RelativeLayout)findViewById(R.id.road_stop_rel);
		road_pause_rel		= (RelativeLayout)findViewById(R.id.road_pause_rel);

		roadbike_activity_name 	= (TextView)findViewById(R.id.roadbike_activity_name);
		activity_current_time 	= (TextView)findViewById(R.id.activity_current_time);
		activity_km_txt 		= (TextView)findViewById(R.id.activity_km_txt);
		activity_distance_txt	= (TextView)findViewById(R.id.activity_distance_txt);
		activity_bpm_txt 		= (TextView)findViewById(R.id.activity_bpm_txt);
		activity_calories_txt 	= (TextView)findViewById(R.id.activity_calories_txt);
		activity_direction_txt 	= (TextView)findViewById(R.id.activity_direction_txt);
		activity_altitude_txt 	= (TextView)findViewById(R.id.activity_altitude_txt);
		activity_km_max_txt		= (TextView)findViewById(R.id.activity_km_max_txt);
		activity_km_min_txt		= (TextView)findViewById(R.id.activity_km_min_txt);

		activity_location		= (LinearLayout)findViewById(R.id.activity_location);
		play_pause_imgview		= (ImageView)findViewById(R.id.play_pause_imgview);
		roadbike_back_img		= (ImageView)findViewById(R.id.roadbike_back_img);

		activity_km_label_txt		= (TextView)findViewById(R.id.activity_km_label_txt);
		activity_max_km_label_txt	= (TextView)findViewById(R.id.activity_max_km_label_txt);
		activity_min_km_label_txt	= (TextView)findViewById(R.id.activity_min_km_label_txt);
		activity_ditsance_label_txt	= (TextView)findViewById(R.id.activity_ditsance_label_txt);
		road_pause_txt				= (TextView)findViewById(R.id.road_pause_txt);

		//Data unit Display
		if(shared.getPrefrencewithDefault(context, Constants.PROFILE.UNIT_IMPERIAL_METRIC,"false").toString().equals("true"))
		{
			isMetricTemp = true;
			activity_km_label_txt.setText("KM/H");
			activity_max_km_label_txt.setText("KM/H");
			activity_min_km_label_txt.setText("KM/H");
			activity_ditsance_label_txt.setText("KM");
		}
		else
		{
			isMetricTemp = false;
			activity_km_label_txt.setText("M/H");
			activity_max_km_label_txt.setText("M/H");
			activity_min_km_label_txt.setText("M/H");
			activity_ditsance_label_txt.setText("M");
		}

		mIntent = getIntent();
		if(mIntent!=null)
		{
			SportsType = Integer.parseInt(mIntent.getExtras().getString("sports_id").toString());
			roadbike_activity_name.setText(getSportNameById(SportsType)+"");

			if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
			{
				if(shared.getPrefrence(context, Constants.PREFRENCE.SPORTS_ID).equals(mIntent.getExtras().getString("sports_id").toString()))
				{
					//When activity start
					shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING, "true");
					resumeActivity();
				}
				else
				{
					registerReceiver(UpdateReceiver,new IntentFilter("LocationDataUpdate"));
					shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYPAUSED, "");
					stopService(new Intent(context, GPSLocationServices.class));

					//Set total time null
					shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING, "");
					SportRecordStop();

					try
					{
						handler.removeCallbacks(updateTimerThread);
						DemoApplication.CurrentTotalTime=0;
					}
					catch(Exception ex)
					{
						System.out.println(ex);
					}
				}
			}
			// if previous activity running and start new one
			shared.setPrefrence(context, Constants.PREFRENCE.SPORTS_ID, SportsType+"");
		}

		//Start Chronometer
		road_playpause_rel.setOnClickListener(this);
		road_stop_rel.setOnClickListener(this);
		activity_location.setOnClickListener(this);
		roadbike_back_img.setOnClickListener(this);
		road_pause_rel.setOnClickListener(this);




		//Status of button colors
		if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
		{
			road_pause_rel.setBackgroundColor(Color.parseColor("#F4D50F"));


			if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYPAUSED).equals("true"))
			{
				road_pause_rel.setBackgroundColor(Color.parseColor("#404040"));
			}
			else
			{
				road_pause_rel.setBackgroundColor(Color.parseColor("#F4D50F"));
			}
		}


		if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
		{
			if(!shared.getPrefrence(context,  Constants.PREFRENCE.ISACTIVITYPAUSED).equals("true"))
			{
				handler.postDelayed(updateTimerThread, 1000);
			}
		}

	}

	@Override
	public void onBackPressed() 
	{
		mIntent = new Intent(context, Choose_activity.class);
		startActivity(mIntent);
		finish();
	}

	@SuppressLint("SimpleDateFormat") @Override
	public void onClick(View v) 
	{
		if(v==road_playpause_rel)
		{
			if(!shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
			{

				handler.postDelayed(updateTimerThread, 1000);

				registerReceiver(UpdateReceiver,new IntentFilter("LocationDataUpdate"));

				//When activity start
				shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING, "true");
				shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYPAUSED, "");

				road_pause_rel.setBackgroundColor(Color.parseColor("#F4D50F"));
				startService(new Intent(context, GPSLocationServices.class));

				//Saving Coordinate for Map for Current Activity
				shared.setPrefrence(context, Constants.PREFRENCE.MAP_START_LAT, shared.getPrefrence(context, Constants.PREFRENCE.ROAD_LATITUDE));
				shared.setPrefrence(context, Constants.PREFRENCE.MAP_START_LON, shared.getPrefrence(context, Constants.PREFRENCE.ROAD_LONGTITUDE));

				//Saving current device time for activity log
				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();

				if(DemoApplication.isFirstTimeStarted)
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
					Date date = new Date();
					DemoApplication.startActivityDeviceTime = dateFormat.format(date);

					DemoApplication.isFirstTimeStarted = false;
				}
			}
		}
		else if(v==road_stop_rel)
		{
			if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
			{
				stopActivityDialog();
			}
		}
		else if(v==activity_location)
		{
			if(check_Internet(RoadBike_Activity.this))
			{
				if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
				{
					mIntent = new Intent(context, Activity_map.class);
					mIntent.putExtra("activity_name", roadbike_activity_name.getText().toString());
					startActivity(mIntent);
				}
			}
			else
			{
				Toast.makeText(context, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
			}
		}
		else if(v==roadbike_back_img)
		{
			mIntent = new Intent(context, Choose_activity.class);
			startActivity(mIntent);
			finish();
		}
		else if(v==road_pause_rel)
		{
			if(shared.getPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING).equals("true"))
			{
				if(shared.getPrefrence(context,  Constants.PREFRENCE.ISACTIVITYPAUSED).equals("true"))
				{
					registerReceiver(UpdateReceiver,new IntentFilter("LocationDataUpdate"));

					//When activity start
					shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING, "true");
					shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYPAUSED, "");
					road_pause_rel.setBackgroundColor(Color.parseColor("#F4D50F"));
					startService(new Intent(context, GPSLocationServices.class));

					//Saving Coordinate for Map for Current Activity
					shared.setPrefrence(context, Constants.PREFRENCE.MAP_START_LAT, shared.getPrefrence(context, Constants.PREFRENCE.ROAD_LATITUDE));
					shared.setPrefrence(context, Constants.PREFRENCE.MAP_START_LON, shared.getPrefrence(context, Constants.PREFRENCE.ROAD_LONGTITUDE));

					road_pause_txt.setText("PAUSE");

					DemoApplication.CurrentelapsedTime = DemoApplication.CurrentTotalTime;
					DemoApplication.CurrentStartTime = System.currentTimeMillis();

					handler.postDelayed(updateTimerThread, 1000);
				}
				else
				{
					shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYPAUSED, "true");
					road_pause_rel.setBackgroundColor(Color.parseColor("#404040"));
					stopService(new Intent(context, GPSLocationServices.class));
					road_pause_txt.setText("RESUME");

					handler.removeCallbacks(updateTimerThread);
				}
			}
		}
	}
	@Override
	public void onDestroy()
	{
		handler.removeCallbacks(updateTimerThread);
		super.onDestroy();
	}
	public void SportRecordStop()
	{
		unregisterReceiver(UpdateReceiver);
		InsertActivityData();
	}

	void UpdateUI()
	{
		float speed =0;
		float distance = 0;

		if(!shared.getPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE).equals(""))
		{
			distance = Float.parseFloat(shared.getPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE));
		}

		if(!shared.getPrefrence(context, Constants.PREFRENCE.ROAD_SPEED).equals(""))
		{
			speed = Float.parseFloat(shared.getPrefrence(context, Constants.PREFRENCE.ROAD_SPEED));
		}

		if(speed!=0)
		{
			if(speed>0)
			{
				if(min_speed==0)
				{
					min_speed = speed;
				}
				if(speed<min_speed)
				{
					min_speed = speed;
				}
				if(speed>max_speed)
				{
					max_speed = speed;
				}
			}
		}
		String temp_dist = distance+"";
		String[] distance_arr	= 	temp_dist.split("\\.");
		if(distance_arr[1].length()>2)
		{
			distance_arr[1] = distance_arr[1].substring(0,2);
		}

		if(isMetricTemp)
		{
			activity_km_max_txt		.setText(convertToMatric(max_speed));
			activity_km_min_txt		.setText(convertToMatric(min_speed));
			activity_distance_txt	.setText(distance_arr[0]+"."+distance_arr[1]+"");
			activity_km_txt			.setText(convertToMatric(speed));
		}
		else
		{
			activity_km_max_txt		.setText(convertToMatric(max_speed));
			activity_km_min_txt		.setText(convertToMatric(min_speed));
			activity_distance_txt	.setText(distance_arr[0]+"."+distance_arr[1]+""+"");
			activity_km_txt			.setText(convertToMatric(speed));
		}

		String temp_cal 		= 	DemoApplication.CurrentCalories+"";
		String[] SplitStringcal	= 	temp_cal.split("\\.");

		activity_calories_txt	.setText(SplitStringcal[0]+"");
		activity_bpm_txt		.setText("--");
		activity_altitude_txt	.setText(DemoApplication.CurrentAltitude);
		activity_direction_txt	.setText(DemoApplication.CurrentDirection);
	}

	private String convertToMatric(float value)
	{
		String val_str = value+"";
		String[] SplitStringcal= val_str.split("\\.");
		//
		if (SplitStringcal[0].length() > 2)
		{
			SplitStringcal[0] = SplitStringcal[0].substring(0, 2);
		}

		if (SplitStringcal[1].length() > 1)
		{
			SplitStringcal[1] = SplitStringcal[1].substring(0, 1);
		}

		return SplitStringcal[0]+"."+SplitStringcal[1];
	}

	private BroadcastReceiver UpdateReceiver = new BroadcastReceiver() 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			final String action = intent.getAction();
			Log.i(TAG, "action:"+action);

			if (action.equals("LocationDataUpdate")) 
			{
				UpdateUI();
			}
		}
	};

	private Runnable updateTimerThread = new Runnable()
	{
		@Override
		public void run() 
		{
			CurrentActivityDuration = DemoApplication.CurrentTotalTime;
			byte hours	= (byte) (CurrentActivityDuration / 3600);
			byte minutes = (byte) ((CurrentActivityDuration % 3600) / 60);
			byte seconds = (byte) (CurrentActivityDuration % 60);
			activity_current_time.setText(String.format("%02d:%02d:%02d", hours, minutes, seconds));
			handler.postDelayed(this, 1000);
			watch_seconds++;
			Log.e("Time theread in roadbike", "Time format");
		}
	};

	private String getSportNameById(int sport_id)
	{
		if(sport_id==1)
		{
			return "SURF";
		}
		else if(sport_id==2)
		{
			return "SUP";
		}
		else if(sport_id==3)
		{
			return "SNOWBOARD";
		}
		else if(sport_id==4)
		{
			return "SKIING";
		}
		else if(sport_id==5)
		{
			return "MOUNTAIN BIKE";
		}
		else if(sport_id==6)
		{
			return "ROAD BIKE";
		}
		else if(sport_id==7)
		{
			return "SKATE";
		}
		else if(sport_id==8)
		{
			return "RUN";
		}
		else if(sport_id==9)
		{
			return "WAKEBOARDING";
		}
		else if(sport_id==10)
		{
			return "WAKE SURFING";
		}
		else
		{
			return "Activity";
		}
	}

	@SuppressLint("SimpleDateFormat") 
	private void InsertActivityData()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");// HH:mm:ss");
		Date date = new Date();
		String today_date = dateFormat.format(date);
		try
		{
			Sport mSport = new Sport();
			mSport.setAltitide(activity_altitude_txt.getText().toString());
			mSport.setId(SportsType+"");
			mSport.setType(SportsType+"");
			mSport.setAveSpeed(activity_km_txt.getText().toString());
			mSport.setMaxSpeed(activity_km_max_txt.getText().toString());
			mSport.setMinSpeed(activity_km_min_txt.getText().toString());
			mSport.setDistance(shared.getPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE));
			mSport.setHeartSate(activity_bpm_txt.getText().toString());
			mSport.setCalories(activity_calories_txt.getText().toString());
			mSport.setDirection(DemoApplication.CurrentDirection);
			mSport.setPosition("Position");
			mSport.setDateId(today_date);
			mSport.setCity(DemoApplication.CurrentCity);
			mSport.setWeather("");
			mSport.setSpeed(activity_km_txt.getText().toString());
			mSport.setTime(activity_current_time.getText().toString());
			mSport.setMaxHeartSate(activity_bpm_txt.getText().toString());

			System.out.println(mSport+"");
			db.insertSport(mSport);

			//Set all the variable to 0 when activity is finished
			clearGlobalVariable();

		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}

		mIntent = new Intent(RoadBike_Activity.this, Choose_activity.class);
		startActivity(mIntent);
		finish();
	}

	private void clearGlobalVariable()
	{
		//Clear Global Data for new
		DemoApplication.CurrentDistance = 0;
		DemoApplication.CurrentCalories = 0;
		DemoApplication.CurrentMaxSpeed = 0;
		DemoApplication.CurrentMinSpeed = 0;
		DemoApplication.CurrentCity 	= "";
		DemoApplication.CurrentTotalTime 	= 0;
		DemoApplication.isFirstTimeStarted 	= true;
		DemoApplication.startActivityDeviceTime 	= "";
		DemoApplication.CurrentDirection 	= "--";
		DemoApplication.CurrentLatLong		=	"";
		DemoApplication.CurrentStartTime	=	0;
		DemoApplication.CurrentelapsedTime	=	0;

	}

	private void stopActivityDialog()
	{
		final Dialog mDialog = new Dialog(RoadBike_Activity.this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setCancelable(false);
		mDialog.setContentView(R.layout.roadbike_dialog);
		mDialog.show();

		ButtonClass roadbike_pop_yes_btn = (ButtonClass)mDialog.findViewById(R.id.roadbike_pop_yes_btn);
		ButtonClass roadbike_pop_no_btn = (ButtonClass)mDialog.findViewById(R.id.roadbike_pop_no_btn);

		roadbike_pop_yes_btn.setOnClickListener(new OnClickListener()
		{
			@SuppressLint("SimpleDateFormat") 
			@Override
			public void onClick(View v) 
			{
				shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYPAUSED, "");
				stopService(new Intent(context, GPSLocationServices.class));
				shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING, "");

				//To ask about saving log
				DbTool TrackDataBase = new DbTool(context);
				ContentValues cv = new ContentValues();

				SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");// HH:mm:ss");
				Date date = new Date();
				String today_date = dateFormat.format(date);

				cv.put(DbTool.TRACK_TIMESTAMP,		DemoApplication.CurrentTotalTime);
				cv.put(DbTool.TRACK_SPORT_TYPE,		SportsType);
				cv.put(DbTool.TRACK_DISTANCE,		shared.getPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE));
				cv.put(DbTool.TRACK_DURATION,		activity_current_time.getText().toString());
				cv.put(DbTool.TRACK_CALORIES,		activity_calories_txt.getText().toString());
				cv.put(DbTool.TRACK_NOTES,			"");
				cv.put(DbTool.TRACK_DIRECTION,		DemoApplication.CurrentDirection);
				cv.put(DbTool.TRACK_ALTITUDE,		DemoApplication.CurrentAltitude);
				cv.put(DbTool.TRACK_DATE,			today_date);
				cv.put(DbTool.TRACK_TIME,			DemoApplication.startActivityDeviceTime);
				cv.put(DbTool.TRACK_LATLON,			DemoApplication.CurrentLatLong);

				if(isMetricTemp)
				{
					cv.put(DbTool.TRACK_UNIT,		"m");
				}
				else
				{
					cv.put(DbTool.TRACK_UNIT,		"i");
				}
				Log.i("Data push to dartabase", "***"+cv);

				TrackDataBase.AddTrack(cv);
				TrackDataBase.close();

				SportRecordStop();
				mDialog.cancel();
			}
		});

		roadbike_pop_no_btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				mDialog.cancel();

				shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYPAUSED, "");
				stopService(new Intent(context, GPSLocationServices.class));
				shared.setPrefrence(context, Constants.PREFRENCE.ISACTIVITYRUNNING, "");

				unregisterReceiver(UpdateReceiver);

				clearGlobalVariable();

				mIntent = new Intent(context, Choose_activity.class);
				startActivity(mIntent);
				finish();
			}
		});
	}

	void resumeActivity()
	{
		registerReceiver(UpdateReceiver,new IntentFilter("LocationDataUpdate"));
		//		int heart_rate = Integer.parseInt(shared.getPrefrence(context, Constants.PREFRENCE.HEART_RATE).toString());
		float speed =0;
		if(!shared.getPrefrence(context, Constants.PREFRENCE.ROAD_SPEED).equals(""))
		{
			speed = Float.parseFloat(shared.getPrefrence(context, Constants.PREFRENCE.ROAD_SPEED));
		}
		activity_distance_txt.setText(shared.getPrefrence(context, Constants.PREFRENCE.ROAD_DISTANCE).toString());
		activity_km_txt.setText	(shared.getPrefrence(context, Constants.PREFRENCE.ROAD_SPEED).toString());
		//		activity_bpm_txt.setText(heart_rate+"");
		activity_bpm_txt.setText("--");
		activity_altitude_txt.setText(DemoApplication.CurrentAltitude);
		activity_direction_txt.setText(DemoApplication.CurrentDirection);

		if(speed!=0)
		{
			if(speed>0)
			{
				if(min_speed==0)
				{
					min_speed = speed;
				}
				if(speed<min_speed)
				{
					min_speed = speed;
				}
				if(speed>max_speed)
				{
					max_speed = speed;
				}
			}
		}
		activity_km_max_txt.setText(max_speed+"");
		activity_km_min_txt.setText(min_speed+"");
		activity_calories_txt.setText(convertToTwoDecimal(DemoApplication.CurrentCalories)); //DemoApplication.CurrentCalories+"");
	}

	private String convertToTwoDecimal(double mDoutle_val)
	{
		String val_str = mDoutle_val+"";
		String[] SplitStringcal= val_str.split("\\.");

		if (SplitStringcal[1].length() > 2)
		{
			SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
		}
		return SplitStringcal[0]+"."+SplitStringcal[1];
	}
}