package com.freestyle.android.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.freestyle.android.R;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.GetAllData;
import com.freestyle.android.config.GetAllData.GetGlobal.GetSubRegion;
import com.freestyle.android.config.TextviewClass;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class Region2_Activity extends BaseActivity implements OnClickListener,CoreFrame
{
	Context context;
	Bundle mBundle;
	String region_id;
	CommonClass mCommonClass = new CommonClass();
	PullToRefreshListView mListViewRegion;
	ImageView region2_back_img;

	Intent mIntent;
	ImageView drawer_icon;

	GetAllData mGetAllData;

	ArrayList<GetSubRegion> mArrayListSubRegion;
	SubRegionListAdpter mRegionListAdpter;

	String country_id;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.region2_activity);

		context = getApplicationContext();

		region2_back_img = (ImageView)findViewById(R.id.region2_back_img);

		region2_back_img.setOnClickListener(this);

		initDrawer(Region2_Activity.this);

		region_id = getIntent().getStringExtra("region_id");
		country_id = getIntent().getStringExtra("country_id");

		mGetAllData = DemoApplication.mGetAllData;

		mListViewRegion = (PullToRefreshListView)findViewById(R.id.region2_listview);

		getAllData();

	}

	@Override
	public void onBackPressed() 
	{
		goBack("back");
	}

	@Override
	public void goBack(String target) 
	{
		mIntent = new Intent(context, Region_Activity.class);
		mIntent.putExtra("area_id", country_id);
		startActivity(mIntent);
		finish();
	}


	@Override
	public void goForward(String target) 
	{
	}


	@Override
	public void setTypefaceControls()
	{
	}

	@Override
	public void onClick(View v)
	{
		if(v==region2_back_img)
		{
			goBack("");
		}

	}

	public class SubRegionListAdpter extends BaseAdapter{

		ViewHolder mViewHolder;

		@Override
		public int getCount() {
			return mArrayListSubRegion.size();
		}

		@Override 
		public Object getItem(int position) {
			return mArrayListSubRegion.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			if(convertView == null){

				mViewHolder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.infl_location_lay, null);

				mViewHolder.mTextViewTiltle = (TextviewClass) convertView.findViewById(R.id.country_txt_single);
				mViewHolder.mLinearLayout = (LinearLayout) convertView.findViewById(R.id.data_layout);

				convertView.setTag(mViewHolder);
			} else {
				mViewHolder = (ViewHolder) convertView.getTag();
			}

			mViewHolder.mTextViewTiltle.setText(mArrayListSubRegion.get(position).getName());

			mViewHolder.mLinearLayout.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{				
					Intent intent = new Intent(context, Region3_Activity.class);
					intent.putExtra("country_id", country_id);
					intent.putExtra("region_id", region_id);	
					intent.putExtra("subregion_id", mArrayListSubRegion.get(position).getId());
					startActivity(intent);
					finish();
				}
			});

			return convertView;
		}
	}

	class ViewHolder
	{
		LinearLayout mLinearLayout;
		TextviewClass mTextViewTiltle;
	}

	public void getAllData()
	{
		if (mGetAllData != null && mGetAllData.getData()!= null && mGetAllData.getData().size() > 0 )
		{
			mArrayListSubRegion = new ArrayList<GetSubRegion>();
			for (int i = 0; i < mGetAllData.getData().size(); i++) 
			{
				if(mGetAllData.getData().get(i).getArea().getId().equalsIgnoreCase(country_id))
				{
					for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) {

						if(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId().equalsIgnoreCase(region_id))
						{
							for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) {
								GetSubRegion mAreaData= new GetSubRegion();
								mAreaData.setId(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId());
								mAreaData.setName(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getName());
								mArrayListSubRegion.add(mAreaData);
							}
						}
					}
				}
			}
			sortSubRegion(mArrayListSubRegion);
			mRegionListAdpter = new SubRegionListAdpter();
			mListViewRegion.setAdapter(mRegionListAdpter);
		}
	}
	
	public void sortSubRegion(List<GetSubRegion> itemList) {
		if(itemList!=null)
		{
			Collections.sort(itemList, new Comparator<GetSubRegion>() {
				@Override
				public int compare(GetSubRegion o1, GetSubRegion o2) {
					return o1.name.compareTo(o2.name);
				}           
			});
		}
	}
}
