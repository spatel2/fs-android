
package com.freestyle.android.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;


class ContactsManager
{
	List<AddressbookEntry> AdressList = new ArrayList<AddressbookEntry>();
	class AddressbookEntry
	{
		String Name;	//18 is the largest callerID sendable to the watch
		String Number;		
	}


	public ContactsManager (final Context context)
	{
		new Thread(new Runnable() 
		{
			public void run() 
			{
				ContentResolver cr = context.getContentResolver();   

				//Note: Cursors begin at row index -1 (before the first row). If the Cursor references multiple rows, 
				//looping through them with the while loop as you have suggested is the preferred method. 
				//It will call moveToNext(), which moves you to index 0 (the first row), and go from there.
				Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,null, null, null);  

				if (cursor.getCount() == 0)
					return;

				cursor.moveToFirst();

				//Get the columns in the database
				int IDColumn = cursor.getColumnIndex(ContactsContract.Contacts._ID);
				int HasPhoneNumberColumn = cursor.getColumnIndex( ContactsContract.Contacts.HAS_PHONE_NUMBER );

				//Go through all the rows in the database
				while (!cursor.isAfterLast())     
				{     
					//Check if contact has a phone number
					if (cursor.getInt(HasPhoneNumberColumn) > 0) 
					{
						// _ID = Row ID 
						String contactId = cursor.getString(IDColumn);  

						// Phonenumbers of contact
						Cursor phoneNumbers = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, 
								ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = "    
										+ contactId, null, null);           

						//Obtain telephone numbers ( there may be more than one number )     
						while (phoneNumbers.moveToNext())     
						{  
							String strPhoneNumber = phoneNumbers.getString(phoneNumbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)); 

							if (name.length()>18) 
								name = name.substring(0,18);
							AddressbookEntry TempEntry = new AddressbookEntry();
							TempEntry.Name = name;
							TempEntry.Number = strPhoneNumber;
							AdressList.add(TempEntry);
						}	
						phoneNumbers.close(); 
					}     
					cursor.moveToNext();
				}  
				cursor.close();   
			}
		}).start();
	}


	public String getContactName (String PhoneNumber)
	{ 
		Iterator<AddressbookEntry> it = AdressList.iterator();
		try
		{
			while(it.hasNext())
			{
				AddressbookEntry TempEntry = it.next();
				
				if(android.telephony.PhoneNumberUtils.compare(TempEntry.Number, PhoneNumber))
				{
					return TempEntry.Name;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return PhoneNumber;	
	}			
}
