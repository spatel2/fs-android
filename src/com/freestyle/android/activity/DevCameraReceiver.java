package com.freestyle.android.activity;

import com.freestyle.android.config.Shared_preference;

import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;

@SuppressWarnings("deprecation")
public class DevCameraReceiver extends BroadcastReceiver 
{
	MediaPlayer player;
	Context context;
	Shared_preference shared;
	AlertDialog mAlertDialog;

	int NoOfCamera;
	public SharedPreferences music; 
	private String countries[] = new String[] { "Camera", "Play/Pause Music", "Find Phone", "Next Song", "Off"};
	String command1,command2,command3;

	boolean isLock = false;
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		this.context = context;
		
		KeyguardManager myKeyManager = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);

		if( myKeyManager.inKeyguardRestrictedInputMode()) {

			System.out.println("Lock Mode");
		} else {
			isLock = true;
		}

		NoOfCamera=Camera.getNumberOfCameras();

		shared = new Shared_preference(context);

		if(NoOfCamera==1)
		{
			countries = new String[] { "Camera", "Play/Pause Music", "Find Phone", "Next Song", "Off"};
		}
		else if(NoOfCamera==2)
		{
			countries = new String[] { "Camera", "Play/Pause Music", "Find Phone", "Next Song", "Off"};

		}
		int commandvalue = intent.getIntExtra("CommandValue", 0);
		if (commandvalue == 176) //command1
		{
			SharedPreferences sharedata = context.getSharedPreferences("control", 0);

			command1=sharedata.getString("C1","default");
			if(command1.equals(countries[0]))  //camera
			{
				if(isLock)
				{
					if(intent.getAction().equals("com.freestyle.android.bluebooth.command"))
					{

						if(shared.getPrefrence(context, "cam_status").equals(""))
						{
							Intent oIntentCamera = new Intent(context,CustomCameraActivity.class);
							oIntentCamera.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							context.startActivity(oIntentCamera); 

						}
						else
						{
							Intent intent_capture = new Intent();//(context,CustomCameraActivity.MyCaptureReceiver.class);
							intent_capture.setAction("com.android.captureImageForPhone");
							context.sendBroadcast(intent_capture);
						}
					}
				}
			}

			else if(command1.equals(countries[2]))  // Find phone
			{	
				Intent mIntent = new Intent(context,Find_watch_dialog.class);
				mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(mIntent); 

			}

			else if (command1.equals(countries[1]))  //  play/pause music 
			{

				music= context.getSharedPreferences("music", 0);
				AudioManager mAudioManager = (AudioManager) context.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
				Editor editor = music.edit();
				if(!mAudioManager.isMusicActive())
				{
					Intent i1 = new Intent("com.android.music.musicservicecommand");
					i1.putExtra("command", "play");
					editor.putBoolean("play", false);
					editor.commit();
					context.sendBroadcast(i1);

				}
				else
				{
					if (mAudioManager.isMusicActive())
					{
						Intent i = new Intent(
								"com.android.music.musicservicecommand");
						i.putExtra("command", "pause");
						context.sendBroadcast(i);

					}
				}
			}

			if(command1.equals(countries[3]))  //  next song
			{
				AudioManager mAudioManager = (AudioManager) context.getApplicationContext()
						.getSystemService(Context.AUDIO_SERVICE);
				if (mAudioManager.isMusicActive())
				{
					Intent i = new Intent(
							"com.android.music.musicservicecommand");
					i.putExtra("command", "next");
					context.sendBroadcast(i);
				}
				else
				{
					Intent i3 = new Intent(
							"com.android.music.musicservicecommand");
					i3.putExtra("command", "play");
					context.sendBroadcast(i3);

					Intent i = new Intent(
							"com.android.music.musicservicecommand");
					i.putExtra("command", "next");
					context.sendBroadcast(i);
				}
			}

			if(NoOfCamera==2)
			{
				if (command1.equals(countries[4])) {
				}
			}
		}

		else if(commandvalue == 177) //Command2
		{

			SharedPreferences sharedata = context.getSharedPreferences("control", 0);

			command1=sharedata.getString("C2","default");

			if(command1.equals(countries[0]))  //camera
			{
				if(isLock)
				{
					if(intent.getAction().equals("com.freestyle.android.bluebooth.command"))
					{

						if(shared.getPrefrence(context, "cam_status").equals(""))
						{
							Intent oIntentCamera = new Intent(context,CustomCameraActivity.class);
							oIntentCamera.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							context.startActivity(oIntentCamera); 

						}
						else
						{
							Intent intent_capture = new Intent();//(context,CustomCameraActivity.MyCaptureReceiver.class);
							intent_capture.setAction("com.android.captureImageForPhone");
							context.sendBroadcast(intent_capture);
						}
					}
				}
			}

			else if(command1.equals(countries[2]))  // Find phone
			{
				Intent mIntent = new Intent(context,Find_watch_dialog.class);
				mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(mIntent); 
			}

			else if (command1.equals(countries[1]))  //  play/pause music 
			{

				music= context.getSharedPreferences("music", 0);
				AudioManager mAudioManager = (AudioManager) context.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
				Editor editor = music.edit();
				if(!mAudioManager.isMusicActive())
				{
					Intent i1 = new Intent("com.android.music.musicservicecommand");
					i1.putExtra("command", "play");
					editor.putBoolean("play", false);
					editor.commit();
					context.sendBroadcast(i1);

				}
				else
				{
					if (mAudioManager.isMusicActive())
					{
						Intent i = new Intent(
								"com.android.music.musicservicecommand");
						i.putExtra("command", "pause");
						context.sendBroadcast(i);

					}
				}
			}

			if(command1.equals(countries[3]))  //  next song
			{
				AudioManager mAudioManager = (AudioManager) context.getApplicationContext()
						.getSystemService(Context.AUDIO_SERVICE);
				if (mAudioManager.isMusicActive())
				{
					Intent i = new Intent("com.android.music.musicservicecommand");
					i.putExtra("command", "next");
					context.sendBroadcast(i);
				}
				else
				{
					Intent i3 = new Intent("com.android.music.musicservicecommand");
					i3.putExtra("command", "play");
					context.sendBroadcast(i3);

					Intent i = new Intent("com.android.music.musicservicecommand");
					i.putExtra("command", "next");
					context.sendBroadcast(i);
				}
			}

			if(NoOfCamera==2)
			{
				if (command1.equals(countries[4])) {
				}
			}
		}

		else if(commandvalue == 178) //command3
		{
			SharedPreferences sharedata = context.getSharedPreferences("control", 0);

			command1=sharedata.getString("C3","default");

			if(command1.equals(countries[0]))  //camera
			{
				if(isLock)
				{
					if(intent.getAction().equals("com.freestyle.android.bluebooth.command"))
					{
						if(shared.getPrefrence(context, "cam_status").equals(""))
						{
							Intent oIntentCamera = new Intent(context,CustomCameraActivity.class);
							oIntentCamera.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							context.startActivity(oIntentCamera); 

						}
						else
						{
							Intent intent_capture = new Intent();//(context,CustomCameraActivity.MyCaptureReceiver.class);
							intent_capture.setAction("com.android.captureImageForPhone");
							context.sendBroadcast(intent_capture);
						}
					}
				}
			}

			else if(command1.equals(countries[2]))  // Find phone
			{
				Intent mIntent = new Intent(context,Find_watch_dialog.class);
				mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(mIntent); 
				
				
			}

			else if (command1.equals(countries[1]))  //  play/pause music 
			{
				music= context.getSharedPreferences("music", 0);
				AudioManager mAudioManager = (AudioManager) context.getApplicationContext()
						.getSystemService(Context.AUDIO_SERVICE);

				Editor editor = music.edit();
				if(!mAudioManager.isMusicActive())
				{

					Intent i1 = new Intent(
							"com.android.music.musicservicecommand");
					i1.putExtra("command", "play");
					editor.putBoolean("play", false);
					editor.commit();
					context.sendBroadcast(i1);
				}
				else
				{
					if (mAudioManager.isMusicActive())
					{
						Intent i = new Intent("com.android.music.musicservicecommand");
						i.putExtra("command", "pause");
						context.sendBroadcast(i);
					}
				}
			}

			if(command1.equals(countries[3]))  //  next song
			{
				AudioManager mAudioManager = (AudioManager) context.getApplicationContext()
						.getSystemService(Context.AUDIO_SERVICE);
				if (mAudioManager.isMusicActive())
				{
					Intent i = new Intent(
							"com.android.music.musicservicecommand");
					i.putExtra("command", "next");
					context.sendBroadcast(i);
				}
				else
				{
					Intent i3 = new Intent(
							"com.android.music.musicservicecommand");
					i3.putExtra("command", "play");
					context.sendBroadcast(i3);

					Intent i = new Intent(
							"com.android.music.musicservicecommand");
					i.putExtra("command", "next");
					context.sendBroadcast(i);
				}
			}

			if(NoOfCamera==2)
			{
				if (command1.equals(countries[4])) {
				}
			}
		}
	}


}