package com.freestyle.android.activity;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.freestyle.android.R;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.CoreFrame;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.config.TextviewClass;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

public class Home_Activity extends BaseActivity implements CoreFrame, OnClickListener {
	Context mContext;
	Intent mIntent;
	RelativeLayout home_social_rel, home_start_rel, home_connect_rel,
	home_music_rel;
	RelativeLayout mRelativeLayoutStartHome;
	LinearLayout home_beach_linear_lay;

	// Beach Data
	TextviewClass home_beach_title_txt;
	TextviewClass home_beach_subtitle_txt;
	TextviewClass home_surf_height_txt;
	TextviewClass home_low_temp_txt;
	TextviewClass home_wind_txt;
	TextviewClass home_low_tide_txt;
	TextviewClass home_temp_weather_txt;
	TextviewClass home_weather_type;
	TextviewClass home_surf_text_sec;
	TextviewClass home_surf_direction_txt;
	TextviewClass home_wind_direction_txt;

	Shared_preference shared;
	String mStringProfileImage = "";

	// for getting nearest beach
	ProgressDialog mProgressDialog;
	int watertemp_min = 0;
	int watertemp_max = 0;
	int weather_min = 0;
	int weather_max = 0;
	float surf_min = 0;
	float surf_max = 0;
	int wind_min = 0;
	int wind_max = 0;
	String tide_high = "";
	String tide_low = "";
	String sunrise = "", sunset = "";
	String sunrise_time = "", sunset_time = "";
	String beach_id = "", region2_id = "";
	double beach_lat = 0, beach_long = 0;
	String TAG = "Nearest Beach";
	String result_surf = "", subtitle_txt = "";
	String WeatherType = "";
	String country_id = "", region_id = "", subregion_id = "";

	ImageView mImageViewWeatherIcon, home_tide_imageview;

	// For Get Nearest Beach
	ArrayList<String> country_id_arr = new ArrayList<String>();
	ArrayList<String> region_id_arr = new ArrayList<String>();
	ArrayList<String> subregion_id_arr = new ArrayList<String>();
	ArrayList<String> beach_id_arr = new ArrayList<String>();
	ArrayList<String> beach_name_arr = new ArrayList<String>();
	ArrayList<String> beach_distance_arr = new ArrayList<String>();
	Double currentLat = 0.0, currentLong = 0.0;

	private GoogleMap googleMap;
	boolean islatlongAvailable = true;

	private int SwellTempDirection;
	private String SwellDirection;

	public String getSwellDirection() {
		return SwellDirection;
	};

	private int SwellTempPeriod;
	private String SwellPeriod = "";

	public String getSwellPeriod() {
		return SwellPeriod;
	}

	ArrayList<String> tide_graph_array = new ArrayList<String>();
	boolean isMetricTemp = false;
	String RootTimeZone = "";
	ArrayList<Calendar> mArrayListCalendarArray = new ArrayList<Calendar>();
	public ArrayList<String> mArrayListTideType = new ArrayList<String>();
	Surfline mSurfline;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_activity);

		mContext = Home_Activity.this;
		shared = new Shared_preference(context);

		home_social_rel = (RelativeLayout) findViewById(R.id.home_social_rel);
		home_start_rel = (RelativeLayout) findViewById(R.id.home_start_rel);
		home_connect_rel = (RelativeLayout) findViewById(R.id.home_connect_rel);
		home_music_rel = (RelativeLayout) findViewById(R.id.home_music_rel);

		home_beach_title_txt = (TextviewClass) findViewById(R.id.home_beach_title_txt);
		home_beach_subtitle_txt = (TextviewClass) findViewById(R.id.home_beach_subtitle_txt);
		home_surf_height_txt = (TextviewClass) findViewById(R.id.home_surf_height_txt);
		home_low_temp_txt = (TextviewClass) findViewById(R.id.home_low_temp_txt);
		home_wind_txt = (TextviewClass) findViewById(R.id.home_wind_txt);
		home_low_tide_txt = (TextviewClass) findViewById(R.id.home_low_tide_txt);
		home_temp_weather_txt = (TextviewClass) findViewById(R.id.home_temp_weather_txt);
		home_weather_type = (TextviewClass) findViewById(R.id.clear_sunny_txt);
		home_surf_text_sec = (TextviewClass) findViewById(R.id.home_surf_text);
		home_surf_direction_txt = (TextviewClass) findViewById(R.id.home_surf_direction_txt);
		home_wind_direction_txt = (TextviewClass) findViewById(R.id.home_wind_direction_txt);
		home_tide_imageview = (ImageView) findViewById(R.id.home_tide_imageview);

		mImageViewWeatherIcon = (ImageView) findViewById(R.id.home_weather_icon);

		mRelativeLayoutStartHome = (RelativeLayout) findViewById(R.id.home_relative_start_activity);
		home_beach_linear_lay = (LinearLayout) findViewById(R.id.home_beach_linear_lay);

		mStringProfileImage = shared.getPrefrence(mContext, "user_image");

		if (!(shared.getPrefrence(mContext, Constants.PREFRENCE.ROAD_LATITUDE).toString().equals("") && shared.getPrefrence(mContext, Constants.PREFRENCE.ROAD_LONGTITUDE).toString().equals(""))) {
			currentLat = Double.parseDouble(shared.getPrefrence(mContext, Constants.PREFRENCE.ROAD_LATITUDE));
			currentLong = Double.parseDouble(shared.getPrefrence(mContext, Constants.PREFRENCE.ROAD_LONGTITUDE));
		}

		if (!mStringProfileImage.equals("")) {
			File file = new File(mStringProfileImage);
			Uri uri = Uri.fromFile(file);

			DemoApplication.ProfilePictureUri = uri;
			setNewProfilePic();
		}

		if (check_Internet(mContext)) {
			mProgressDialog = ProgressDialog.show(context, "Loading", "Please wait");
		}

		if (shared.getPrefrencewithDefault(context, "unit_imperial_metric", "false").toString().equals("true")) {
			isMetricTemp = true;
		} else {
			isMetricTemp = false;
		}

		home_social_rel.setOnClickListener(this);
		home_start_rel.setOnClickListener(this);
		home_connect_rel.setOnClickListener(this);
		home_music_rel.setOnClickListener(this);

		mRelativeLayoutStartHome.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = "http://www.freestyleusa.com/";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		initDrawer(Home_Activity.this);
		setBeachData();

		home_beach_linear_lay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mIntent = new Intent(context, Nearest_beaches_fragment.class);
				mIntent.putExtra("subregion_id", shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID));
				mIntent.putExtra("region_id", shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID));
				mIntent.putExtra("country_id", shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID));
				mIntent.putExtra("beach_id", shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_ID));
				startActivity(mIntent);
			}
		});
	}

	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_home)).getMap();

			if (googleMap == null) {
				Toast.makeText(getApplicationContext(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
			}

			googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			googleMap.getUiSettings().setZoomControlsEnabled(false);
			googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {
				@Override
				public void onMapLoaded() {
					LatLng mapCenter = new LatLng(beach_lat, beach_long);
					googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, 16));
					googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
				}
			});
		}
	}

	@Override
	public void onBackPressed() 
	{
		goBack("Finish the app");
	}
	
	@Override
	public void goBack(String target) 
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(Home_Activity.this);
		builder.setMessage("Are you sure you want to exit?");
		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				finish();
				dialog.cancel();
			}
		});

		builder.setNegativeButton("NO", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.cancel();
			}
		});
		builder.show();
	}


	@SuppressWarnings("deprecation")
	@Override
	public void goForward(String target) {
		if (target.equals("start_activity")) {
			mIntent = new Intent(mContext, Choose_activity.class);
			startActivity(mIntent);
			finish();
		} else if (target.equals("social")) {
			mIntent = new Intent(mContext, SocialActivity.class);
			startActivity(mIntent);
		} else if (target.equals("music")) {
			mIntent = new Intent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);
			startActivity(mIntent);
		} else if (target.equals("connect")) {
			mIntent = new Intent(mContext, Search_Activity.class);
			startActivity(mIntent);
		}
	}

	@Override
	public void setTypefaceControls() {
	}

	@Override
	public void onClick(View v) {
		if (v == home_start_rel) {
			goForward("start_activity");
		} else if (v == home_social_rel) {
			goForward("social");
		} else if (v == home_music_rel) {
			goForward("music");
		} else if (v == home_connect_rel) {
			goForward("connect");
		}
	}

	@Override
	public void gotoHome() {
	}

	private void setBeachData() {
		beach_id = shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_ID);
		if (!beach_id.equals("")) {
			if (mCommonClass.CheckNetwork(mContext)) {
				// Get beach Data
				Home_get_beach_data mHome_get_beach_data = new Home_get_beach_data();
				mHome_get_beach_data.execute();
			}
		} else {
			if (check_Internet(mContext)) {
				if (shared.getPrefrence(mContext, Constants.PREFRENCE.TEMP_LAT).equals("")) {
					getBeachLocation_async mgetBeachLocation_async = new getBeachLocation_async();
					mgetBeachLocation_async.execute();
				} else {
					GetRegionBeachName(false);

					Home_get_beach_data mHome_get_beach_data = new Home_get_beach_data();
					mHome_get_beach_data.execute();
				}
			}
		}
	}

	// Getting data by beach id
	@SuppressLint("DefaultLocale")
	public class Home_get_beach_data extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

		@Override
		protected synchronized String doInBackground(Void... params) {
			return GetRegionBeachName(true);
		}

		@SuppressLint("SimpleDateFormat")
		@SuppressWarnings("unused")
		protected void onPostExecute(String result) {
			Log.i("Result", result);
			result_surf = result;
			try {
				try {
					JSONObject JsonObject = new JSONObject(result);
					Log.i(TAG, JsonObject.toString());

					// Location
					// ----------------------------------------------------------------------------------
					String Location = JsonObject.getString("name");
					int LocationLength = Location.length();

					RootTimeZone = JsonObject.getString("timezone");

					// Water Temperature
					// ----------------------------------------------------------------------------------
					watertemp_min = (int) Math.round(Double.parseDouble(JsonObject.getJSONObject("WaterTemp").getString("watertemp_min")));
					watertemp_max = (int) Math.round(Double.parseDouble(JsonObject.getJSONObject("WaterTemp").getString("watertemp_max")));

					DecimalFormat mDecimalFormatTwoDigit;
					mDecimalFormatTwoDigit = new DecimalFormat("0", new DecimalFormatSymbols(Locale.ENGLISH));

					if (isMetricTemp) {
						home_low_temp_txt.setText(mDecimalFormatTwoDigit.format(watertemp_min) + (char) 0x00B0 + "c");
					} else {
						home_low_temp_txt.setText(mDecimalFormatTwoDigit.format(watertemp_min) + (char) 0x00B0 + "f");
					}

					// Air Temperature
					// ----------------------------------------------------------------------------------
					JSONArray TempAirMinArray = (JSONArray) JsonObject.getJSONObject("Weather").get("temp_min");
					weather_min = (int) Math.round(Double.parseDouble(TempAirMinArray.getString(0)));
					JSONArray TempAirMaxArray = (JSONArray) JsonObject.getJSONObject("Weather").get("temp_max");
					weather_max = (int) Math.round(Double.parseDouble(TempAirMaxArray.getString(0)));

					if (isMetricTemp) {
						home_temp_weather_txt.setText(mDecimalFormatTwoDigit.format(weather_min) + " - " + mDecimalFormatTwoDigit.format(weather_max) + (char) 0x00B0 + "c");
					} else {
						home_temp_weather_txt.setText(mDecimalFormatTwoDigit.format(weather_min) + " - " + mDecimalFormatTwoDigit.format(weather_max) + (char) 0x00B0 + "f");
					}

					JSONArray TempWeatherArray = (JSONArray) JsonObject.getJSONObject("Weather").get("weather_type");
					WeatherType = TempWeatherArray.getString(0);

					if (WeatherType.equals("clear")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					} else if (WeatherType.equals("mostly sunny")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					} else if (WeatherType.equals("sunny")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					} else if (WeatherType.equals("partly sunny")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.partly_cloudy_day);
					} else if (WeatherType.equals("cloudy")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("mostly cloudy")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.overcast);
					} else if (WeatherType.equals("overcast")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.overcast);
					} else if (WeatherType.equals("partly cloudy")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.partly_cloudy_day);
					} else if (WeatherType.equals("rain")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("heavy rain")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("scattered showers")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.rain);
					} else if (WeatherType.equals("sleet")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.snow);
					} else if (WeatherType.equals("snow")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.snow);
					} else if (WeatherType.equals("flurries")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.snow);
					} else if (WeatherType.equals("fog")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.fog);
					} else if (WeatherType.equals("hazy")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.fog);
					} else if (WeatherType.equals("scattered showers possible t-storms")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("heavy rain possible t-storms")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("scattered showers t-storms")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("possible t-storms")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else if (WeatherType.equals("thunderstorms")) {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.thunderstorms);
					} else {
						home_weather_type.setText(WeatherType);
						mImageViewWeatherIcon.setImageResource(R.drawable.clear_sky_day);
					}

					// Tide Info
					// ----------------------------------------------------------------------------------
					JSONArray TempTidePointArray = (JSONArray) JsonObject.getJSONObject("Tide").get("dataPoints");
					String TideTimeZone = "";
					TideTimeZone = JsonObject.getJSONObject("Tide").getString("timezone");

					tide_graph_array = new ArrayList<String>();
					tide_graph_array.clear();
					mArrayListTideType = new ArrayList<String>();
					mArrayListTideType.clear();
					mArrayListCalendarArray = new ArrayList<Calendar>();
					mArrayListCalendarArray.clear();

					for (int i = 0; i < TempTidePointArray.length(); i++) {
						JSONObject TempObject = (JSONObject) TempTidePointArray.get(i);

						Calendar c1 = Calendar.getInstance();

						DateFormat df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
						c1.setTime(df.parse(TempObject.getString("utctime") + ""));
						if (!TideTimeZone.equalsIgnoreCase("")) {
							if (TideTimeZone.contains(".")) {
								c1.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
								c1.add(Calendar.MINUTE, 30);
							} else {
								c1.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
							}
						}
						Calendar c2 = Calendar.getInstance();

						boolean isSameDay = isSameDay(c1, c2);

						Calendar c3 = Calendar.getInstance();
						c3.add(Calendar.DAY_OF_YEAR, 1);

						String mString = TempObject.getString("type");

						if (isSameDay) {
							if (mString.equalsIgnoreCase("Sunrise") || mString.equalsIgnoreCase("Sunset")) {
							} else {
								double value1 = TempObject.getDouble("height");

								value1 = Double.parseDouble(new DecimalFormat("#.##").format(value1));

								tide_graph_array.add(String.valueOf(value1));

								String type = TempObject.getString("type");
								mArrayListTideType.add(type);

								Calendar dateObj = Calendar.getInstance();
								DateFormat sdfAM = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
								dateObj.setTime(sdfAM.parse(TempObject.getString("utctime") + ""));

								if (!TideTimeZone.equalsIgnoreCase("")) {
									if (TideTimeZone.contains(".")) {
										dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
										dateObj.add(Calendar.MINUTE, 30);
									} else {
										dateObj.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
									}
								}
								mArrayListCalendarArray.add(dateObj);
							}
						}
					}

					float Tidemax = 0, Tidemin = 0;
					Calendar mCalendarCurrentGMTTime = Calendar.getInstance();
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
					mCalendarCurrentGMTTime.setTime(df.parse(CurrentGMTTime()));
					if (!TideTimeZone.equalsIgnoreCase("")) {
						if (TideTimeZone.contains(".")) {
							mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone.substring(0, TideTimeZone.indexOf("."))));
							mCalendarCurrentGMTTime.add(Calendar.MINUTE, 30);
						} else {
							mCalendarCurrentGMTTime.add(Calendar.HOUR_OF_DAY, Integer.parseInt(TideTimeZone));
						}
					}
					for (int i = 0; i < mArrayListCalendarArray.size(); i++) {
						if (mCalendarCurrentGMTTime.before(mArrayListCalendarArray.get(i)) || mCalendarCurrentGMTTime.equals(mArrayListCalendarArray.get(i))) {
							Tidemin = (float) Double.parseDouble(tide_graph_array.get(i));
							if (mArrayListTideType.get(i).equalsIgnoreCase("high")) {
								// up arrow
								home_tide_imageview.setVisibility(View.VISIBLE);
								home_tide_imageview.setImageResource(R.drawable.arrow_up);
							} else if (mArrayListTideType.get(i).equalsIgnoreCase("low")) {
								// down arrow
								home_tide_imageview.setVisibility(View.VISIBLE);
								home_tide_imageview.setImageResource(R.drawable.arrow_down);
							} else {
								// nothing draw
								home_tide_imageview.setVisibility(View.GONE);
							}
							break;
						} else {
							Tidemin = 0;
						}
					}
					Tidemin = Float.parseFloat(convertToSingalDecimal(String.valueOf(Tidemin)));

					// if (isMetricTemp) {
					// home_low_tide_txt.setText(Tidemin + " m");
					// } else {
					// home_low_tide_txt.setText(Tidemin + " ft");
					// }

					// Surf Info
					float SurfTempMin = 0, SurfTempMax = 0;
					String SurfSize;
					JSONArray TempSwellSizeArray;
					JSONArray SurfPeriodSchedule;

					String mStringSurfLocalDate = JsonObject.getJSONObject("Wind").getString("startDate_pretty_LOCAL");
					Calendar CalendarSurfStartDateGMT = Calendar.getInstance();
					SimpleDateFormat sdf1 = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
					CalendarSurfStartDateGMT.setTime(sdf1.parse(mStringSurfLocalDate));

					SimpleDateFormat df1 = new SimpleDateFormat("HHmm");
					String surfGMTtime = df1.format(CalendarSurfStartDateGMT.getTime());

					int mIntsurfGMTTime = Integer.parseInt(surfGMTtime);

					int SurfArrayValue = 0;

					if (mIntsurfGMTTime < 0700) {
						SurfArrayValue = 0;

					} else if (mIntsurfGMTTime >= 0700 && mIntsurfGMTTime < 1400) {
						SurfArrayValue = 1;
					} else if (mIntsurfGMTTime >= 1400 && mIntsurfGMTTime < 1900) {
						SurfArrayValue = 2;
					} else if (mIntsurfGMTTime >= 1900) {
						SurfArrayValue = 3;
					}

					// Swell Height
					TempSwellSizeArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_height1");
					String mStringsurfHeight = "";
					if (TempSwellSizeArray.length() > 0) {
						JSONArray TempSwellSizeObject;
						TempSwellSizeObject = (JSONArray) TempSwellSizeArray.get(0);

						float surfHeight = (float) TempSwellSizeObject.getDouble(SurfArrayValue);
						surfHeight = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(surfHeight)));
						if (isMetricTemp) {
							mStringsurfHeight = (surfHeight) + " m";
						} else {
							mStringsurfHeight = (surfHeight) + " ft";
						}
					}

					// Surf Min
					JSONArray TempSurfMin = (JSONArray) JsonObject.getJSONObject("Surf").get("surf_min");
					if (TempSurfMin.length() > 0) {

						JSONArray TempSurfMinObject;
						TempSurfMinObject = (JSONArray) TempSurfMin.get(0);

						SurfTempMin = (float) TempSurfMinObject.getDouble(SurfArrayValue);

					}

					// Surf Max
					JSONArray TempSurfMax = (JSONArray) JsonObject.getJSONObject("Surf").get("surf_max");

					if (TempSurfMax.length() > 0) {

						JSONArray TempSurfMaxObject;
						TempSurfMaxObject = (JSONArray) TempSurfMax.get(0);
						SurfTempMax = (float) TempSurfMaxObject.getDouble(SurfArrayValue);
					}

					if (isMetricTemp) {
						SurfTempMin = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMin)));
						SurfTempMax = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMax)));
						SurfSize = (SurfTempMin) + "-" + (SurfTempMax) + " m";
					} else {
						SurfTempMin = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMin)));
						SurfTempMax = Float.parseFloat(convertToSingalDecimalForSurf(String.valueOf(SurfTempMax)));
						SurfSize = (SurfTempMin) + "-" + (SurfTempMax) + " ft";
					}

					home_surf_height_txt.setText(SurfSize);

					// Swell Direction
					// ----------------------------------------------------------------------------------
					JSONArray TempSwellDirectionArray;
					TempSwellDirectionArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_direction1");

					if (TempSwellDirectionArray.length() > 0) {
						JSONArray TempSwellDirectionObject;
						TempSwellDirectionObject = (JSONArray) TempSwellDirectionArray.get(0);
						SwellTempDirection = (int) TempSwellDirectionObject.getDouble(SurfArrayValue);
						if (SwellTempDirection <= 10)
							SwellDirection = "N";
						else if (SwellTempDirection > 10 && SwellTempDirection <= 30)
							SwellDirection = "NNE";
						else if (SwellTempDirection > 30 && SwellTempDirection <= 50)
							SwellDirection = "NE";
						else if (SwellTempDirection > 50 && SwellTempDirection <= 80)
							SwellDirection = "ENE";
						else if (SwellTempDirection > 80 && SwellTempDirection <= 100)
							SwellDirection = "E";
						else if (SwellTempDirection > 100 && SwellTempDirection <= 130)
							SwellDirection = "ESE";
						else if (SwellTempDirection > 130 && SwellTempDirection <= 150)
							SwellDirection = "SE";
						else if (SwellTempDirection > 150 && SwellTempDirection <= 170)
							SwellDirection = "SSE";
						else if (SwellTempDirection > 170 && SwellTempDirection <= 190)
							SwellDirection = "S";
						else if (SwellTempDirection > 190 && SwellTempDirection <= 210)
							SwellDirection = "SSW";
						else if (SwellTempDirection > 210 && SwellTempDirection <= 230)
							SwellDirection = "SW";
						else if (SwellTempDirection > 230 && SwellTempDirection <= 260)
							SwellDirection = "WSW";
						else if (SwellTempDirection > 260 && SwellTempDirection <= 280)
							SwellDirection = "W";
						else if (SwellTempDirection > 280 && SwellTempDirection <= 310)
							SwellDirection = "WNW";
						else if (SwellTempDirection > 310 && SwellTempDirection <= 330)
							SwellDirection = "NW";
						else if (SwellTempDirection > 330 && SwellTempDirection <= 350)
							SwellDirection = "NNW";
						else if (SwellTempDirection > 350)
							SwellDirection = "N";
					} else {
						SwellTempDirection = 0;
						SwellDirection = "N/A";
					}

					// Swell Period
					// ----------------------------------------------------------------------------------

					JSONArray TempSwellPeriodArray;
					TempSwellPeriodArray = (JSONArray) JsonObject.getJSONObject("Surf").get("swell_period1");
					if (TempSwellPeriodArray.length() > 0) {
						JSONArray TempSwellPeriodObject;
						TempSwellPeriodObject = (JSONArray) TempSwellPeriodArray.get(0);
						SwellTempPeriod = (int) (TempSwellPeriodObject.getDouble(SurfArrayValue));
					} else {
						SwellTempPeriod = 0;
					}

					SwellPeriod = String.format("%d" + "s", SwellTempPeriod);

					home_surf_text_sec.setText(mStringsurfHeight + " @ " + SwellPeriod + " " + SwellDirection);
					home_surf_direction_txt.setText(SwellDirection);

					// Wind Info
					// ----------------------------------------------------------------------------------
					JSONArray TempWindSpeedArray = (JSONArray) JsonObject.getJSONObject("Wind").get("wind_speed");
					Float WindTempMin = 1000.f;
					Float WindTempMax = -1000.f;
					JSONArray TempwindSpeedObject = (JSONArray) TempWindSpeedArray.get(0);// Note:
					// also
					// an
					// array
					for (int i = 0; i < TempwindSpeedObject.length(); i++) {
						Log.i("Checkpoint", "5");
						float TempWindSpeed = (float) TempwindSpeedObject.getDouble(i);
						if (WindTempMin > TempWindSpeed)
							wind_min = (int) TempWindSpeed;
						if (WindTempMax < TempWindSpeed)
							wind_max = (int) TempWindSpeed;
					}

					SendDatatoWatch mSendDatatoWatch = new SendDatatoWatch();
					mSendDatatoWatch.execute();

				} catch (Exception e) {
					e.printStackTrace();
				}

				// Wind Speed
				JSONObject mJsonObject = new JSONObject(result);
				int max = 0, min = 0;

				String mStringLocalDate = mJsonObject.getJSONObject("Wind").getString("startDate_pretty_LOCAL");

				Calendar CalendarWindStartDateGMT = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
				CalendarWindStartDateGMT.setTime(sdf.parse(mStringLocalDate));

				SimpleDateFormat df1 = new SimpleDateFormat("HHmm");
				String WindGMTtime = df1.format(CalendarWindStartDateGMT.getTime());

				int mIntWindGMTTime = Integer.parseInt(WindGMTtime);

				int WindArrayValue = 0;

				if (mIntWindGMTTime < 0200) {
					WindArrayValue = 0;
				} else if (mIntWindGMTTime >= 0200 && mIntWindGMTTime < 0500) {
					WindArrayValue = 1;
				} else if (mIntWindGMTTime >= 0500 && mIntWindGMTTime < Integer.parseInt("0800")) {
					WindArrayValue = 2;
				} else if (mIntWindGMTTime >= Integer.parseInt("0800") && mIntWindGMTTime < 1200) {
					WindArrayValue = 3;
				} else if (mIntWindGMTTime >= 1200 && mIntWindGMTTime < 1500) {
					WindArrayValue = 4;
				} else if (mIntWindGMTTime >= 1500 && mIntWindGMTTime < 1800) {
					WindArrayValue = 5;
				} else if (mIntWindGMTTime >= 1800 && mIntWindGMTTime < 2100) {
					WindArrayValue = 6;
				} else if (mIntWindGMTTime >= 2100) {
					WindArrayValue = 7;
				}

				String wind_speed_str = mJsonObject.getString("Wind");
				JSONObject wind_data_obj = new JSONObject(wind_speed_str);
				JSONArray wind_arr = wind_data_obj.getJSONArray("wind_speed");

				JSONArray wind_speed;
				wind_speed = new JSONArray(wind_arr.getString(0));

				if (isMetricTemp) {
					home_wind_txt.setText(Math.round(wind_min) + "-" + Math.round(wind_max) + " km/h");
				} else {
					home_wind_txt.setText(Math.round(wind_min) + "-" + Math.round(wind_max) + " kts");
				}

				min = (int) Math.round(Double.parseDouble(wind_speed.getString(WindArrayValue)));
				max = (int) Math.round(Double.parseDouble(wind_speed.getString(WindArrayValue)) * 1.3);

				wind_min = min;
				wind_max = max;

				if (isMetricTemp) {
					home_wind_txt.setText(wind_min + "-" + wind_max + " km/h");
				} else {
					home_wind_txt.setText(wind_min + "-" + wind_max + " kts");
				}

				// Wind Direction
				// ----------------------------------------------------------------------------------

				JSONArray TempWindDirectionArray = (JSONArray) mJsonObject.getJSONObject("Wind").get("wind_direction");
				String WindDirection = "";
				int WindTempDirection = 0;
				if (TempWindDirectionArray.length() > 0) {
					JSONArray TempWindDirectionObject;
					TempWindDirectionObject = (JSONArray) TempWindDirectionArray.get(0);// Note:
					// Also
					// an
					// Array
					WindTempDirection = (int) TempWindDirectionObject.getDouble(WindArrayValue);
					if (WindTempDirection <= 10)
						WindDirection = "N";
					else if (WindTempDirection > 10 && WindTempDirection <= 30)
						WindDirection = "NNE";
					else if (WindTempDirection > 30 && WindTempDirection <= 50)
						WindDirection = "NE";
					else if (WindTempDirection > 50 && WindTempDirection <= 80)
						WindDirection = "ENE";
					else if (WindTempDirection > 80 && WindTempDirection <= 100)
						WindDirection = "E";
					else if (WindTempDirection > 100 && WindTempDirection <= 130)
						WindDirection = "ESE";
					else if (WindTempDirection > 130 && WindTempDirection <= 150)
						WindDirection = "SE";
					else if (WindTempDirection > 150 && WindTempDirection <= 170)
						WindDirection = "SSE";
					else if (WindTempDirection > 170 && WindTempDirection <= 190)
						WindDirection = "S";
					else if (WindTempDirection > 190 && WindTempDirection <= 210)
						WindDirection = "SSW";
					else if (WindTempDirection > 210 && WindTempDirection <= 230)
						WindDirection = "SW";
					else if (WindTempDirection > 230 && WindTempDirection <= 260)
						WindDirection = "WSW";
					else if (WindTempDirection > 260 && WindTempDirection <= 280)
						WindDirection = "W";
					else if (WindTempDirection > 280 && WindTempDirection <= 310)
						WindDirection = "WNW";
					else if (WindTempDirection > 310 && WindTempDirection <= 330)
						WindDirection = "NW";
					else if (WindTempDirection > 330 && WindTempDirection <= 350)
						WindDirection = "NNW";
					else if (WindTempDirection > 350)
						WindDirection = "N";
				} else {
					WindTempDirection = 0;
					WindDirection = "N/A";
				}

				home_wind_direction_txt.setText(WindDirection);
				mProgressDialog.dismiss();

				try {
					String mStringLat = "";
					mStringLat = shared.getPrefrence(Home_Activity.this, Constants.PREFRENCE.HOME_BEACH_LAT);
					String mStringLong = "";
					mStringLong = shared.getPrefrence(Home_Activity.this, Constants.PREFRENCE.HOME_BEACH_LONG);
					if (!mStringLat.equalsIgnoreCase("") && !mStringLong.equalsIgnoreCase("")) {
						beach_lat = Double.parseDouble(mStringLat);
						beach_long = Double.parseDouble(mStringLong);

						if (check_Internet(context)) {
							initilizeMap();
						}

					} else {
						// Toast.makeText(context, "No coordinates available",
						// Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				// New parsing code
				ArrayList<Double> _heightArray = new ArrayList<Double>();
				ArrayList<String> _timeArray = new ArrayList<String>();
				ArrayList<Calendar> _calanderArray = new ArrayList<Calendar>();
				JSONObject _jobj = new JSONObject(new JSONObject(result).getString("Tide"));

				JSONArray _jArrray = _jobj.getJSONArray("dataPoints");
				String _timeZone = _jobj.getString("timezone");
				int _cnt = 0;
				int _currentIndex = -1;
				int _nextDayFirstIndex = 0;
				boolean _currentAdded = false;
				for (int i = 0; i < _jArrray.length(); i++) {
					JSONObject TempObject = (JSONObject) _jArrray.get(i);

					SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
					dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));

					// Local time zone
					SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

					Calendar _cal = Calendar.getInstance();
					Calendar _calCurrent = Calendar.getInstance();

					_calCurrent.setTime(dateFormatLocal.parse(dateFormatGmt.format(new Date())));
					// if (!today) {
					// _calCurrent.add(Calendar.DAY_OF_YEAR, 1);
					// }
					// System.out.println("after " + _calCurrent.getTime());
					DateFormat _df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
					_cal.setTime(_df.parse(TempObject.getString("utctime") + ""));

					if (!_timeZone.equalsIgnoreCase("")) {
						if (_timeZone.contains(".")) {
							String[] _time = new String[2];
							_time[0] = _timeZone.substring(0, _timeZone.indexOf("."));
							_time[1] = _timeZone.substring(_timeZone.indexOf(".") + 1);
							_cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_time[0]));
							_calCurrent.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_time[0]));
							if (_time[1].length() == 1) {
								_time[1] = _time[1] + "0";
							}
							int _minuts = (Integer.parseInt(_time[1]) * 60) / 100;
							_cal.add(Calendar.MINUTE, _minuts);
							_calCurrent.add(Calendar.MINUTE, _minuts);
						} else {
							_cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_timeZone));
							_calCurrent.add(Calendar.HOUR_OF_DAY, Integer.parseInt(_timeZone));

						}

						if ((TempObject.getString("type").equalsIgnoreCase("NORMAL") || TempObject.getString("type").equals("Low") || TempObject.getString("type").equals("High")) && (getDate(_cal).equalsIgnoreCase(getDate(_calCurrent)))) {
							double _value = TempObject.getDouble("height");
							// _value = Double.parseDouble(new
							// DecimalFormat("#.#").format(_value));
							_nextDayFirstIndex = i;
							_heightArray.add(_value);
							SimpleDateFormat _df1 = new SimpleDateFormat("hh:mm a");
							String _currentTime = _df1.format(_cal.getTime());
							_timeArray.add(_currentTime);
							_calanderArray.add(_cal);
							System.out.println("curr date " + _calCurrent.getTime() + "  Tide date " + _cal.getTime());
							if (_calCurrent.before(_cal) && !_currentAdded) {
								_currentIndex = _cnt;
								_currentAdded = true;
								if (isMetricTemp) {
									home_low_tide_txt.setText(round(_value, 1) + " m");
								} else {
									home_low_tide_txt.setText(round(_value, 1) + " ft");
								}
								if (TempObject.getString("type").equalsIgnoreCase("high")) {
									// up arrowmcal
									home_tide_imageview.setVisibility(View.VISIBLE);
									home_tide_imageview.setImageResource(R.drawable.arrow_up);
								} else if (TempObject.getString("type").equalsIgnoreCase("low")) {
									// down arrow
									home_tide_imageview.setVisibility(View.VISIBLE);
									home_tide_imageview.setImageResource(R.drawable.arrow_down);
								} else {
									// nothing draw
									home_tide_imageview.setVisibility(View.GONE);
								}
							}
							_cnt++;
						}
					}
				}
				if (!_currentAdded) {
					JSONObject TempObject = (JSONObject) _jArrray.get(_nextDayFirstIndex + 1);
					double _value = TempObject.getDouble("height");
					if (isMetricTemp) {
						home_low_tide_txt.setText(round(_value, 1) + " m");
					} else {
						home_low_tide_txt.setText(round(_value, 1) + " ft");
					}
					if (TempObject.getString("type").equalsIgnoreCase("high")) {
						// up arrowmcal
						home_tide_imageview.setVisibility(View.VISIBLE);
						home_tide_imageview.setImageResource(R.drawable.arrow_up);
					} else if (TempObject.getString("type").equalsIgnoreCase("low")) {
						// down arrow
						home_tide_imageview.setVisibility(View.VISIBLE);
						home_tide_imageview.setImageResource(R.drawable.arrow_down);
					} else {
						// nothing draw
						home_tide_imageview.setVisibility(View.GONE);
					}
				}

			} catch (Exception e) {
				System.out.println(e + "");
				mProgressDialog.dismiss();
			}
		}
	}

	// new function

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	// new function
	public String getDate(Calendar cal) {
		String _date = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
		_date = dateFormat.format(cal.getTime());
		// System.out.println(_date);
		return _date;
	}

	public boolean isSameDay(Calendar cal1, Calendar cal2) {
		if (cal1 == null || cal2 == null)
			return false;
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
	}

	private String convertToSingalDecimal(String val_str) {
		String[] SplitStringcal = val_str.split("\\.");
		int i = 0;
		if (SplitStringcal[1].length() == 2) {
			SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
			i = Integer.parseInt(SplitStringcal[1]);
			if (i % 10 >= 5) {
				i = i + (10 - (i % 10));
			} else {
				i = Integer.parseInt(SplitStringcal[1].substring(0, 1));
			}
		} else {
			i = Integer.parseInt(SplitStringcal[1]);
		}
		return SplitStringcal[0] + "." + i;
	}

	private synchronized String GetRegionBeachName(boolean flag) {
		country_id = shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID);
		region_id = shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID);
		subregion_id = shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID);
		beach_id = shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_ID);

		if (mGetAllData != null && mGetAllData.getData() != null && mGetAllData.getData().size() > 0) {
			for (int i = 0; i < mGetAllData.getData().size(); i++) {
				if (mGetAllData.getData().get(i).getArea().getId().equalsIgnoreCase(country_id)) {
					for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) {
						if (mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId().equalsIgnoreCase(region_id)) {
							for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) {

								if (mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId().equalsIgnoreCase(subregion_id)) {
									for (int m = 0; m < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().size(); m++) {
										if (mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId().equalsIgnoreCase(beach_id)) {
											try {
												home_beach_title_txt.setText(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getTitle());
												home_beach_subtitle_txt.setText(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getName() + ", " + mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getName());
												shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_LAT, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLat());
												shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_LONG, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLon());
											} catch (Exception e) {
												e.getMessage();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (flag) {
			String strURL = "";
			if (isMetricTemp) {
				strURL = com.freestyle.android.config.Constants.URL.FORECAST_URL + beach_id + "?package=partner&units=m&user_key=abca50124d77d41504c4d53e29c8d359";
			} else {
				strURL = com.freestyle.android.config.Constants.URL.FORECAST_URL + beach_id + "?package=partner&user_key=abca50124d77d41504c4d53e29c8d359";
			}
			return mCommonClass.GetConnection(strURL);
		} else {
			return "";
		}
	}

	// Get Nearest Beach
	public class getBeachLocation_async extends AsyncTask<Void, Void, Void> {
		@Override
		protected synchronized Void doInBackground(Void... params) {
			fillPlaces();
			return null;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			GetRegionBeachName(false);

			Home_get_beach_data mHome_get_beach_data = new Home_get_beach_data();
			mHome_get_beach_data.execute();
		}
	}

	public synchronized void fillPlaces() {
		boolean nearSubregion = true;
		boolean isFirst = true;
		double temp_distance = 0;

		if (mGetAllData != null && mGetAllData.getData() != null && mGetAllData.getData().size() > 0) {
			for (int i = 0; i < mGetAllData.getData().size(); i++) {
				for (int j = 0; j < mGetAllData.getData().get(i).getArea().getContent().size(); j++) {
					nearSubregion = true;
					for (int k = 0; k < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().size(); k++) {
						for (int m = 0; m < mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().size(); m++) {
							double distance = gps2m(mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLat(), mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLon());
							if (distance != 0) {
								if (isFirst) {
									temp_distance = distance;
									if (distance < 5000) {
										shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_ID, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId());
										shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID, mGetAllData.getData().get(i).getArea().getId());
										shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId());
										shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId());
										shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_LAT, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLat());
										shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_LONG, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLon());
										System.out.println("Distance: " + distance);
										isFirst = false;
									} else if (10000 > distance) {
										nearSubregion = false;
										break;
									}
								} else {
									if (distance < 5000) {
										if (distance <= temp_distance) {
											temp_distance = distance;

											shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_ID, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getId());
											shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_AREA_ID, mGetAllData.getData().get(i).getArea().getId());
											shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_REGION_ID, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getId());
											shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_SUBREGION_ID, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getId());
											shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_LAT, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLat());
											shared.setPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_LONG, mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getLon());
											System.out.println("Distance: " + distance + " " + mGetAllData.getData().get(i).getArea().getContent().get(j).getRegion().getContent().get(k).getSubregion().getSpots().get(m).getTitle());
										}
									} else if (10000 > distance) {
										nearSubregion = false;
										break;
									}
								}
							} else {
								break;
							}
						}
						if (nearSubregion == false) {
							break;
						}
					}
				}
			}
		}

		// Set Data in Shared Prefrence
		// setValue();

		if (shared.getPrefrence(mContext, Constants.PREFRENCE.HOME_BEACH_ID).equals("")) {
			if (mCommonClass.CheckNetwork(mContext)) {
				Home_get_beach_data mHome_get_beach_data = new Home_get_beach_data();
				mHome_get_beach_data.execute();
			}
		} else {
			if (shared.getPrefrence(mContext, Constants.PREFRENCE.TEMP_LAT).equals("") || shared.getPrefrence(mContext, Constants.PREFRENCE.TEMP_LOT).equals("") || shared.getPrefrence(mContext, Constants.PREFRENCE.TEMP_LAT).equals("0.0") || shared.getPrefrence(mContext, Constants.PREFRENCE.TEMP_LAT).equals("0.0")) {
				// Toast.makeText(mContext, "GPS service is not Enabled",
				// Toast.LENGTH_SHORT).show();
				Log.i("Location Service:", "cant find coordinates yet");
			} else {
				shared.setPrefrence(mContext, Constants.PREFRENCE.TEMP_LAT, shared.getPrefrence(mContext, Constants.PREFRENCE.ROAD_LATITUDE));
				shared.setPrefrence(mContext, Constants.PREFRENCE.TEMP_LOT, shared.getPrefrence(mContext, Constants.PREFRENCE.ROAD_LONGTITUDE));
			}
		}
	}

	@SuppressLint("FloatMath")
	private double gps2m(String lat_str, String lng_str) {
		float lng_a = 0, lat_a = 0;
		if (!(lat_str.equals("") && lng_str.equals(""))) {
			lat_a = Float.parseFloat(lat_str);
			lng_a = Float.parseFloat(lng_str);

			float pk = (float) (180 / 3.14169);

			float a1 = lat_a / pk;
			float a2 = lng_a / pk;
			float b1 = (float) (currentLat / pk);
			float b2 = (float) (currentLong / pk);

			float t1 = FloatMath.cos(a1) * FloatMath.cos(a2) * FloatMath.cos(b1) * FloatMath.cos(b2);
			float t2 = FloatMath.cos(a1) * FloatMath.sin(a2) * FloatMath.cos(b1) * FloatMath.sin(b2);
			float t3 = FloatMath.sin(a1) * FloatMath.sin(b1);
			double tt = Math.acos(t1 + t2 + t3);

			return (6366000 * tt) / 1000;
		} else {
			return 0;
		}
	}

	@SuppressLint("SimpleDateFormat")
	public String CurrentGMTTime() {
		Date mDate = new Date();
		// creating DateFormat for converting time from local timezone to GMT
		DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("GMT"));
		return converter.format(mDate);
	}

	@SuppressLint("SimpleDateFormat")
	public String UTCTime(Date mdate) {
		// creating DateFormat for converting time from local timezone to GMT
		DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return converter.format(mdate);
	}

	private String convertToSingalDecimalForSurf(String val_str) {
		String[] SplitStringcal = val_str.split("\\.");
		int i = 0;
		if (SplitStringcal[1].length() >= 2) {
			SplitStringcal[1] = SplitStringcal[1].substring(0, 2);
			i = Integer.parseInt(SplitStringcal[1]);
			if (i % 10 >= 5) {
				i = i + (10 - (i % 10));
			} else {
				i = Integer.parseInt(SplitStringcal[1].substring(0, 1));
			}
		} else {
			i = Integer.parseInt(SplitStringcal[1]);
		}
		return SplitStringcal[0] + "." + i;
	}

	public class SendDatatoWatch extends AsyncTask<Void, Void, String> {
		boolean isOfline = false;

		@Override
		protected void onPreExecute() {
			if (mCommonClass.CheckNetwork(context)) {
				isOfline = false;
			} else {
				isOfline = true;
			}
			mSurfline = new Surfline(mBluetoothLeService);
			super.onPreExecute();
		}

		@Override
		protected synchronized String doInBackground(Void... params) {
			mSurfline.UpdateData(isMetricTemp, Integer.parseInt(beach_id), false, result_surf, context);
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
		}
	}
}
