package com.freestyle.android.activity;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.provider.MediaStore;

public class Utility {



	public static String getPath(Uri uri, Activity activity) 
	{
		String[] projection = { MediaStore.MediaColumns.DATA };
		Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public static boolean SaveBitmapToFileCache(Bitmap bitmap, String strFilePath, String filename)
	{

		File file = new File(strFilePath + File.separatorChar + filename);
		OutputStream out = null;

		try 
		{
			file.createNewFile();

			out = new FileOutputStream(file);
			bitmap.compress(CompressFormat.JPEG, 100, out);
			out.close();

			return true;
		}
		catch (Exception e) 
		{
			return false;
		}
	}
}