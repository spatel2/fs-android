package com.freestyle.test;

import android.content.Context;
import android.content.SharedPreferences;

public class clsPref {

	SharedPreferences PREFS;
	SharedPreferences.Editor PREFS_EDITOR;
	Context CONTEXT;

	public clsPref(Context context) {
		try {
			CONTEXT = context;
			String _prefName = "FreeStyle";
			PREFS = context.getSharedPreferences(_prefName, 0);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void putString(String key, String value) {
		PREFS_EDITOR = PREFS.edit();
		PREFS_EDITOR.putString(key, value);
		PREFS_EDITOR.commit();
	}

	private String getString(String key) {
		return PREFS.getString(key, "");
	}

	private void putInteger(String key, Integer value) {
		PREFS_EDITOR = PREFS.edit();
		PREFS_EDITOR.putInt(key, value);
		PREFS_EDITOR.commit();
	}

	private Integer getInteger(String key) {
		return PREFS.getInt(key, 0);
	}

	private void putBoolean(String key, boolean value) {
		PREFS_EDITOR = PREFS.edit();
		PREFS_EDITOR.putBoolean(key, value);
		PREFS_EDITOR.commit();
	}

	private boolean getBoolean(String key) {
		return PREFS.getBoolean(key, false);
	}

	public void putIsAnyActivityRunning(Boolean isRunnunig) {
		putBoolean("isRunning", isRunnunig);
	}

	public boolean getIsAnActivityRunning() {
		return getBoolean("isRunning");
	}

	public void putCurrentTimerTime(String CurrentTimerTime) {
		putString("CurrentTimerTime", CurrentTimerTime);
	}

	String getCurrentTimerTime() {
		// TODO Auto-generated method stub
		return getString("CurrentTimerTime");
	}

	public void putCurrentSystemTime(String CurrentSystemTime) {
		putString("CurrentSystemTime", CurrentSystemTime);
	}

	String getCurrentSystemTime() {
		// TODO Auto-generated method stub
		return getString("CurrentSystemTime");
	}

	public void putRunningActivityStatus(String RunnunigActivityStatus) {
		putString("RunningActivityStatus", RunnunigActivityStatus);
	}

	public String getRunningActivityStatus() {
		return getString("RunningActivityStatus");
	}

	public void putRunningActivityName(String runningActivityName) {
		putString("RunningActivityName", runningActivityName);
	}

	public String getRunningActivityName() {
		return getString("RunningActivityName");
	}

	public void putUserId(String userId) {
		putString("UserId", userId);
	}

	public String getUserId() {
		return getString("UserId");
	}

	public void putUserEmail(String email) {
		putString("Email", email);
	}

	public String getUserEmail() {
		return getString("Email");
	}

	public void putUserPass(String pass) {
		putString("Password", pass);
	}

	public String getUserPass() {
		return getString("Password");
	}

	public void putFirstName(String fName) {
		putString("FirstName", fName);
	}

	public String getFirstName() {
		return getString("FirstName");
	}

	public void putLastName(String lName) {
		putString("LastName", lName);
	}

	public String getLastName() {
		return getString("LastName");
	}

	public void putFBID(String FBID) {
		putString("FBID", FBID);
	}

	public String getFBID() {
		return getString("FBID");
	}

	public void putGender(String gender) {
		putString("Gender", gender);
	}

	public String getGender() {
		return getString("Gender");
	}

	public void putBirthDate(String bDate) {
		putString("BirthDate", bDate);
	}

	public String getBirthDate() {
		return getString("BirthDate");
	}

	public void putPhoneNumber(String phoneNumber) {
		putString("PhoneNumber", phoneNumber);
	}

	public String getPhoneNumber() {
		return getString("PhoneNumber");
	}

	public void putLatitude(String latitude) {
		putString("Latitude", latitude);
	}

	public String getLatitude() {
		return getString("Latitude");
	}

	public void putLongitude(String Longitude) {
		putString("Longitude", Longitude);
	}

	public String getLongitude() {
		return getString("Longitude");
	}

	public void clearAll() {
		putIsAnyActivityRunning(false);
		putRunningActivityName("");
		putUserId("");
		putUserPass("");
		putUserEmail("");
		putFirstName("");
		putLastName("");
		putBirthDate("");
		putGender("");
		putPhoneNumber("");
		putLatitude("");
		putLongitude("");

	}
}
