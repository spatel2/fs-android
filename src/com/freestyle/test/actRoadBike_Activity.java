package com.freestyle.test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.freestyle.android.R;
import com.freestyle.android.activity.ActivityLog_Activity;
import com.freestyle.android.activity.Activity_map;
import com.freestyle.android.activity.BaseActivity;
import com.freestyle.android.activity.DbTool;
import com.freestyle.android.activity.DemoApplication;
import com.freestyle.android.config.CommonClass;
import com.freestyle.android.config.Constants;
import com.freestyle.android.config.DatabaseConnectionAPI;
import com.freestyle.android.config.Shared_preference;
import com.freestyle.android.pojo.Sport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class actRoadBike_Activity extends BaseActivity implements
		OnClickListener, ConnectionCallbacks, OnConnectionFailedListener,
		LocationListener {

	// *s* Start
	double distance;
	String DIRECTION = "--";
	private long startTime = 0L;
	private boolean isUpdateOn = false;
	Handler customHandler = new Handler();
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	boolean IS_PAUSE_CLICKED = false;
	double PREVIOUS_LATITUDE = 0.00, PREVIOUS_LONGITUDE = 0.00;
	int AGE = 27;
	String Gender = "M";
	double caloriesBurned = 0.0;
	int HEARTRATE = 110;
	int WEIGHT = 72;

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private Location mLastLocation;

	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	// boolean flag to toggle periodic location updates
	private boolean mRequestingLocationUpdates = false;

	private LocationRequest mLocationRequest;

	// Location updates intervals in sec
	private static int UPDATE_INTERVAL = 1000; // 1 sec
	private static int FATEST_INTERVAL = 500; // 0.5 sec
	private static int DISPLACEMENT = 0; // 0 meters

	// UI elements
	private TextView lblLocation;
	private Button btnShowLocation, btnStartLocationUpdates;
	// *S* End

	Context context;
	Bundle mBundle;
	Shared_preference shared;
	String region_id;
	ProgressDialog mProgressDialog;
	CommonClass mCommonClass = new CommonClass();
	Intent mIntent;
	ImageView drawer_icon;
	RelativeLayout road_playpause_rel, road_stop_rel, road_lock_rel;
	TextView road_play_txt;
	TextView roadbike_activity_name;
	TextView activity_km_max_txt;
	TextView activity_km_min_txt;
	int user_age = 0, user_height = 0, user_weight = 0;

	// For Saving Log
	private DatabaseConnectionAPI mDatabaseConnectionAPI;

	DbTool db;
	private final int SPORTSTATE_INIT = 0;
	private final int SPORTSTATE_RUNNING = 1;
	private final int SPORTSTATE_FINISHED = 3;
	private int CurrentState = SPORTSTATE_INIT;
	private int SportsType = 0;
	private TextView activity_altitude_txt;
	private TextView activityDIRECTION_txt;
	private TextView activity_calories_txt;
	private TextView activity_bpm_txt;
	private TextView activity_distance_txt;
	private TextView activity_km_txt;
	private TextView activity_current_time;
	private Handler handler;
	private long CurrentActivityDuration;
	private static final String TAG = "SportDetailLog";
	// private boolean isUpdateOn = false;
	AlertDialog alertDialog = null;
	LinearLayout activity_location;

	int watch_seconds = 0;
	ImageView play_pause_imgview;

	private float MIN_SPEED = 0;
	private float MAX_SPEED = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_roadbike_activity);
		// First we need to check availability of play services

/*#*/

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		if (checkPlayServices()) {

			// Building the GoogleApi client
			buildGoogleApiClient();

			createLocationRequest();
		}
		context = actRoadBike_Activity.this;
		initDrawer(actRoadBike_Activity.this);
		shared = new Shared_preference(context);
		handler = new Handler();
		// DisplayLog = true;
		db = new DbTool(actRoadBike_Activity.this);

		DemoApplication.mBluetoothLeService_temp = BaseActivity.mBluetoothLeService;

		road_playpause_rel = (RelativeLayout) findViewById(R.id.road_playpause_rel);
		road_stop_rel = (RelativeLayout) findViewById(R.id.road_stop_rel);
		road_play_txt = (TextView) findViewById(R.id.road_play_txt);
		road_lock_rel = (RelativeLayout) findViewById(R.id.road_lock_rel);

		roadbike_activity_name = (TextView) findViewById(R.id.roadbike_activity_name);
		activity_current_time = (TextView) findViewById(R.id.activity_current_time);
		activity_km_txt = (TextView) findViewById(R.id.activity_km_txt);
		activity_distance_txt = (TextView) findViewById(R.id.activity_distance_txt);
		activity_bpm_txt = (TextView) findViewById(R.id.activity_bpm_txt);
		activity_calories_txt = (TextView) findViewById(R.id.activity_calories_txt);
		activityDIRECTION_txt = (TextView) findViewById(R.id.activity_direction_txt);
		activity_altitude_txt = (TextView) findViewById(R.id.activity_altitude_txt);
		activity_km_max_txt = (TextView) findViewById(R.id.activity_km_max_txt);
		activity_km_min_txt = (TextView) findViewById(R.id.activity_km_min_txt);

		activity_location = (LinearLayout) findViewById(R.id.activity_location);
		play_pause_imgview = (ImageView) findViewById(R.id.play_pause_imgview);

		if (!shared.getPrefrence(context, "user_age").toString().equals("")) {
			user_age = Integer.parseInt(shared
					.getPrefrence(context, "user_age").toString());
		}
		if (!shared.getPrefrence(context, "user_height").toString().equals("")) {
			user_height = Integer.parseInt(shared.getPrefrence(context,
					"user_height").toString());
		}
		if (!shared.getPrefrence(context, "user_weight").toString().equals("")) {
			user_weight = Integer.parseInt(shared.getPrefrence(context,
					"user_weight").toString());
		}

		// Database
		mDatabaseConnectionAPI = new DatabaseConnectionAPI(context);
		try {
			mDatabaseConnectionAPI.createDataBase();
			mDatabaseConnectionAPI.openDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}

		mIntent = getIntent();
		if (mIntent != null) {
		SportsType = Integer.parseInt(mIntent.getExtras()
				.getString("sports_id").toString());
		roadbike_activity_name.setText(getSportNameById(SportsType) + "");
		clsPref _pref = new clsPref(actRoadBike_Activity.this);
		if(_pref.getIsAnActivityRunning()==false || _pref.getRunningActivityName().equals(""))
		{
			_pref.putIsAnyActivityRunning(true);
			_pref.putRunningActivityName(String.valueOf(getSportNameById(SportsType)));
			road_play_txt.setText("START"); 
			road_playpause_rel.setBackgroundColor(Color.parseColor("#33aa33"));
			
		}
		else
		{
			
			 road_play_txt.setText("PAUSE");
			 road_playpause_rel.setBackgroundColor(Color.parseColor("#FFC40D"));
			 CurrentState = SPORTSTATE_RUNNING;
			 checkPlayServices();

//				 Resuming the periodic location updates
				 if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
				 startLocationUpdates();
				 }
			 
		}
		// if previous activity running and start new one
					shared.setPrefrence(context, Constants.PREFRENCE.SPORTS_ID, SportsType+"");
		}
		// Register receiver
		// registerReceiver(AlertReceiver, AlertIntentFilter());

		// Start Chronometer
		road_playpause_rel.setOnClickListener(this);
		road_stop_rel.setOnClickListener(this);
		road_lock_rel.setOnClickListener(this);
		activity_location.setOnClickListener(this);

	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void onClick(View v) {
		if (v == road_playpause_rel) {
			// registerReceiver(UpdateReceiver,new
			// IntentFilter("LocationDataUpdate"));

			if (road_play_txt.getText().toString().equals("PAUSE")) {
				// PAUSE
				IS_PAUSE_CLICKED = true;
				shared.setPrefrence(context,
						Constants.PREFRENCE.ISACTIVITYPAUSED, "true");
				isUpdateOn = false;
				road_play_txt.setText("START");
				CurrentState = SPORTSTATE_RUNNING;
				road_playpause_rel.setBackgroundColor(Color
						.parseColor("#33aa33"));
				timeSwapBuff += timeInMilliseconds;
				customHandler.removeCallbacks(updateTimerThread);

				startLocationUpdates();
				// stopService(new Intent(context, GPSLocationServices.class));
			} else {
				// start
				IS_PAUSE_CLICKED = false;
				shared.setPrefrence(context,
						Constants.PREFRENCE.ISACTIVITYRUNNING, "true");
				shared.setPrefrence(context,
						Constants.PREFRENCE.ISACTIVITYPAUSED, "");
				road_play_txt.setText("PAUSE");
				CurrentState = SPORTSTATE_RUNNING;
				road_playpause_rel.setBackgroundColor(Color
						.parseColor("#FFC40D"));
				startLocationUpdates();
				startTime = SystemClock.uptimeMillis();
				customHandler.postDelayed(updateTimerThread, 0);

			}
		} else if (v == road_stop_rel) {
			IS_PAUSE_CLICKED = false;
			if (CurrentState == SPORTSTATE_RUNNING) {
				startTime = 0L;
				clsPref _pref = new clsPref(actRoadBike_Activity.this);
				_pref.clearAll();
				shared.setPrefrence(context, Constants.PREFRENCE.SPORTS_ID,"");
				timeInMilliseconds = 0L;
				timeSwapBuff = 0L;
				updatedTime = 0L;
				road_play_txt.setText("START");
				road_playpause_rel.setBackgroundColor(Color
						.parseColor("#33aa33"));

				customHandler.removeCallbacks(updateTimerThread);
				activity_current_time.setText("00:00:00");
				stopLocationUpdates();
				InsertActivityData();

			}
		} else if (v == activity_location) {
			mIntent = new Intent(context, Activity_map.class);
			startActivity(mIntent);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// unregisterReceiver(AlertReceiver);
		// stopService(new Intent(context, GPSLocationServices.class));
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		checkPlayServices();

		// Resuming the periodic location updates
		// if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
		// startLocationUpdates();
		// }
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		
		
		//stopLocationUpdates();
	}

	/**
	 * Method to display the location on UI
	 * */
	private void updateUi() {

		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);

		if (mLastLocation != null) {

			activity_distance_txt.setText("" + mLastLocation.getSpeed());

			activity_bpm_txt.setText(String.valueOf(HEARTRATE));
			activity_altitude_txt.setText(String.format("%.1f",
					mLastLocation.getAltitude()));
			activityDIRECTION_txt.setText("" + mLastLocation.getBearing());

			float speed = (float) (mLastLocation.getSpeed() * 3.6);

			if (speed != 0) {
				if (speed > 0) {
					if (MIN_SPEED == 0) {
						MIN_SPEED = speed;
					}
					if (speed < MIN_SPEED) {
						MIN_SPEED = speed;
					}
					if (speed > MAX_SPEED) {
						MAX_SPEED = speed;
					}
				}
			}

			activity_km_min_txt.setText(String.format("%.1f", MIN_SPEED));
			activity_km_max_txt.setText(String.format("%.1f", MAX_SPEED));
			activity_distance_txt.setText(String
					.format("%.3f", distance / 1000));
			activityDIRECTION_txt.setText(DIRECTION);
			activity_km_txt.setText(String.format("%.1f", speed));

		} else {

//			Toast.makeText(
//					getApplicationContext(),
//					"Couldn't get the location. Make sure location is enabled on the device",
//					3000).show();
		}
	}

	/**
	 * Creating google api client object
	 * */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	/**
	 * Creating location request object
	 * */
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
//				Toast.makeText(getApplicationContext(),
//						"This device is not supported.", Toast.LENGTH_LONG)
//						.show();
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Starting the location updates
	 * */
	protected void startLocationUpdates() {

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, this);

	}

	/**
	 * Stopping location updates
	 */
	protected void stopLocationUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates(
				mGoogleApiClient, this);
	}

	/**
	 * Google api callback methods
	 */
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
				+ result.getErrorCode());
	}

	@Override
	public void onConnected(Bundle arg0) {

		// Once connected with google api, get the location
		// updateUi();
		//
		// if (mRequestingLocationUpdates) {
		// startLocationUpdates();
		// }
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onLocationChanged(Location location) {
		// Assign the new location
		mLastLocation = location;

		if (PREVIOUS_LATITUDE != location.getLatitude()
				|| PREVIOUS_LONGITUDE != location.getLongitude()) {
			// new\
			if ((int) PREVIOUS_LATITUDE != 0 || (int) PREVIOUS_LONGITUDE != 0) {
				float a[] = new float[1];
				location.distanceBetween(PREVIOUS_LATITUDE, PREVIOUS_LONGITUDE,
						location.getLatitude(), location.getLongitude(), a);
				distance = distance + a[0];
			}

		}

		PREVIOUS_LATITUDE = location.getLatitude();
		PREVIOUS_LONGITUDE = location.getLongitude();
		getDirection(location.getBearing());

		// Displaying the new location on UI
		if (IS_PAUSE_CLICKED==false) {
			updateUi();
		}

	}

	public static double distFrom(double lat1, double lng1, double lat2,
			double lng2) {
		double earthRadius = 3958.75;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
				* Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;
		return dist * 3.6;
	}

	public void getDirection(float bearing) {

		int _bearing = (int) bearing / 27;
		switch (_bearing) {
		case 0:
			DIRECTION = "N";
			break;
		case 1:
			DIRECTION = "NNE";
			break;
		case 2:
			DIRECTION = "NE";
			break;
		case 3:
			DIRECTION = "ENE";
			break;
		case 4:
			DIRECTION = "E";
			break;
		case 5:
			DIRECTION = "ESE";
			break;
		case 6:
			DIRECTION = "SE";
			break;
		case 7:
			DIRECTION = "SSE";
			break;
		case 8:
			DIRECTION = "S";
			break;
		case 9:
			DIRECTION = "SSW";
			break;
		case 10:
			DIRECTION = "SW";
			break;
		case 11:
			DIRECTION = "WSW";
			break;
		case 12:
			DIRECTION = "W";
			break;
		case 13:
			DIRECTION = "WNW";
			break;
		case 14:
			DIRECTION = "NW";
			break;
		case 15:
			DIRECTION = "NNW";
			break;
		default:
			DIRECTION = "N/A";
			break;
		}
	}

	// private Runnable updateTimerThread = new Runnable() {
	// @Override
	// public void run() {
	// byte hours = (byte) (CurrentActivityDuration / 3600);
	// byte minutes = (byte) ((CurrentActivityDuration % 3600) / 60);
	// byte seconds = (byte) (CurrentActivityDuration % 60);
	// activity_current_time.setText(String.format("%02d:%02d:%02d",
	// hours, minutes, seconds));
	// handler.postDelayed(this, 1000);
	// Log.i("------------->","TIMER"+hours+","+ minutes+","+seconds);
	// watch_seconds++;
	// }
	// };
	private Runnable updateTimerThread = new Runnable() {
		public void run() {
			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
			updatedTime = timeSwapBuff + timeInMilliseconds;

			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;
			int hours = (int) (mins / 60);
			secs = secs % 60;
			int milliseconds = (int) (updatedTime % 1000);
			activity_current_time.setText(String.format("%02d", hours) + ":"
					+ String.format("%02d", mins) + ":"
					+ String.format("%02d", secs));
			Log.i("------------->", "TIMER" + hours + "," + mins + "," + secs);
			caloriesBurned = (((AGE * 0.2017) - (WEIGHT * 0.09036)
					+ (HEARTRATE * 0.6309) - 55.0969) * (secs / 3600)) / 4.184;
			activity_calories_txt
					.setText(String.format("%.0f", caloriesBurned));
			customHandler.postDelayed(this, 0);
		}

	};

	private String getSportNameById(int sport_id) {
		if (sport_id == 1) {
			return "SURF";
		} else if (sport_id == 2) {
			return "SUP";
		} else if (sport_id == 3) {
			return "SNOWBOARD";
		} else if (sport_id == 4) {
			return "SKI";
		} else if (sport_id == 5) {
			return "MOUNTAIN BIKE";
		} else if (sport_id == 6) {
			return "ROAD BIKE";
		} else if (sport_id == 7) {
			return "SKATE";
		} else if (sport_id == 8) {
			return "RUN";
		} else if (sport_id == 9) {
			return "WAKEBOARDING";
		} else if (sport_id == 10) {
			return "WAKE SURFING";
		} else {
			return "Activity";
		}
	}

	@SuppressLint("SimpleDateFormat")
	private void InsertActivityData() {
		
//		Toast.makeText(getApplicationContext(), "Your activity is saved. You can view it from activity log section.", Toast.LENGTH_LONG).show();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");// HH:mm:ss");
		Date date = new Date();
		String today_date = dateFormat.format(date);

		try {
			Sport mSport = new Sport();
			mSport.setAltitide(activity_altitude_txt.getText().toString());
			mSport.setId(SportsType + "");
			mSport.setType(SportsType + "");
			mSport.setAveSpeed(activity_km_txt.getText().toString());
			mSport.setMaxSpeed(activity_km_max_txt.getText().toString());
			mSport.setMinSpeed(activity_km_min_txt.getText().toString());
			mSport.setDistance(shared.getPrefrence(context,
					Constants.PREFRENCE.ROAD_DISTANCE));
			mSport.setHeartSate(activity_bpm_txt.getText().toString());
			mSport.setCalories(activity_calories_txt.getText().toString());
			mSport.setDirection(activityDIRECTION_txt.getText().toString());
			mSport.setPosition("Position");
			mSport.setDateId(today_date);
			mSport.setCity("City");
			mSport.setWeather("Weather");
			mSport.setSpeed(activity_km_txt.getText().toString());
			mSport.setTime(activity_current_time.getText().toString());
			mSport.setMaxHeartSate(activity_bpm_txt.getText().toString());
			long a = db.insertSport(mSport);
			
			Log.i("------>",""+a);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex);
		}
		mIntent = new Intent(actRoadBike_Activity.this,
				ActivityLog_Activity.class);
		startActivity(mIntent);
		finish();
	}

}