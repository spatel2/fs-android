package com.github.mikephil.charting.interfaces;

import java.util.ArrayList;

import android.content.Context;

import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.utils.FillFormatter;

public interface LineDataProvider extends BarLineScatterCandleDataProvider {

	public LineData getLineData();

	/**
	 * Sets a custom FillFormatter to the chart that handles the position of the
	 * filled-line for each DataSet. Set this to null to use the default logic.
	 * 
	 * @param formatter
	 */
	public void setFillFormatter(FillFormatter formatter);

	/**
	 * Returns the FillFormatter that handles the position of the filled-line.
	 * 
	 * @return
	 */
	public FillFormatter getFillFormatter();

	public int getFirstMinIndex();

	public int getFirstMaxIndex();

	public int getSecondMinIndex();

	public int getSecondMaxIndex();

	public int getCurrentTimeIndex();

	public int getSize();

	public double getLastMinTime();

	public ArrayList<String> getTimeVal();

	public ArrayList<String> getHeightVal();

	public Context getContex();
}
